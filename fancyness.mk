ifneq ($(V),)
  SILENCE           =
else
  SILENCE           = @
endif

SHOW_COMMAND        := @printf "%-15s%s\n"
SHOW_CXX            := $(SHOW_COMMAND) "[ $(shell echo $(CXX) | cut -d\  -f1) ] "
SHOW_CC             := $(SHOW_COMMAND) "[ $(shell echo $(CC) | cut -d\  -f1) ] "
SHOW_CLEAN          := $(SHOW_COMMAND) "[ CLEAN ] "
SHOW_GEN            := $(SHOW_COMMAND) "[ GEN ] "
SHOW_MAKE           := $(SHOW_COMMAND) "[ $(MAKE) ] "
SHOW_INSTALL        := $(SHOW_COMMAND) "[ install ] "
SHOW_AR             := $(SHOW_COMMAND) "[ $(AR) ] "