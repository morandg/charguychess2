The software for the DIY chess hardware we want to build with my father.

# Kernel driver
The application needs the [charguychess driver](kernel/driver) to run. Make sure
that it is loaded when starting the application.

## Communication protocol
The communication with the hardware is done with an ASCII protocol that is
[documented here](doc/hardware-protocol.md).

# UCI engines
If you want to play against the computer, an UCI engine must be available. Here
is a list of tested open source engines that worked with CharGuyChess:

* [Stockfish](https://stockfishchess.org/)
* [Fruit 2.1](http://arctrix.com/nas/chess/fruit/)
* [Octochess](http://octochess.org/)

Feedbacks are welcome if other engines give issues or have been proven to be
working.

# Compilation
Compilation can be done using the toplevel Makefile. To compile everything, run
```make```. To compile the application run ```make app``` and run
```make driver``` to compile the driver.

## make variables
Some variables can be given on the command line to change the build:

* **DEBUG**: Set to *1* to compile the debug mode
* **V**: Set to *1* to compile in verbose mode

## Simulator
An hardware simulator can be used when no hardware is available. It will be
automatically compiled with the application if you have *SDL2* and *OpenGL*
devlopment libraries available on your system.

## Unit tests
Unit tests will be automatically run and compiled if you have
[rglib mocking library](https://gitlab.com/morandg/lib-r4nd0m6uy) available on
your system.

# Running the application
First load the charguychess kernel module and make sure that it is R/W
accesible:
```sh
$ sudo insmod kernel/driver/charguychess.ko
$ sudo chmod ugo+rw /dev/charguychess
```

Then the application can be started:
```sh
$ ./app/build/release/charguychess
```

It is possible to interact with the application using a telnet connection:
```sh
$ telnet localhost 2323
Welcome to CharGuyChess telnet server!
Type "help" for more information

CharGuyChess> 
```

Or using the simulator:
```sh
$ ./app/build/release/charguychess_simulator
```

## Configuration files
The application can be configured using a
[libconfig](https://hyperrealm.github.io/libconfig/) file. An example is
available [here](app/config/example.conf)

If availbe on your system, logging can be achieved with
[log4cplus](https://sourceforge.net/projects/log4cplus/). An example is also
available [here](app/config/log4cplus.ini)

# Charguychess on embedded devices
The project can be compiled with yocto, recipes are available in my
[personnal meta-layer](https://gitlab.com/morandg/meta-r4nd0m6uy).
