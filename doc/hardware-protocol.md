Description of the communication protocol with the CharGuyChess hardware

# Main concept
The protocol is ASCII based and consist of 8 bits registers encoded in
hexadecimal. Each register can be read or written indepedently.

When a rigster has changed, its value is automatically sent by the
device.

# Framing
Each message message is separated by a return line (```\n```) character.

An *register event* from the device consist of two fields of one byte
each or two chars. The first field contains the address of the register
that changed, the second its value:
```txt
<address><value>\n
```

When reading a register, only the address must be sent. The answer
contains the read address with its corresponding value:
```txt
<address>\n
<address><value>\n
```

When writing a register, its address and its new value must be given.
The write is acknowldeged with the address of the written register and
its new content:
```txt
<address><value>\n
<address><value>\n
```

When writing a read only register, the write will be acknowledged
normally with the actual content of the register.

When reading or writing an invalid register or sending malformed messages, the
will be ignored by the device without notice.

# Examples
The register ```0xAB``` has changed and contains the value ```0x12```:
```txt
AB12\n
```

Request the value of the register ```0x0A``` that contains the value
```0x01```:
```txt
0A\n
0A01\n
```

Write the register ```0x09``` with the value ```0x12```:
```txt
0912\n
0912\n
```

# Registers description

| Address    | R/W | Description        |
|:----------:|:---:|:-------------------|
| ```0x00``` | RO  | Pieces on Rank 1   |
| ```0x01``` | RO  | Pieces on Rank 2   |
| ```0x02``` | RO  | Pieces on Rank 3   |
| ```0x03``` | RO  | Pieces on Rank 4   |
| ```0x04``` | RO  | Pieces on Rank 5   |
| ```0x05``` | RO  | Pieces on Rank 6   |
| ```0x06``` | RO  | Pieces on Rank 7   |
| ```0x07``` | RO  | Pieces on Rank 8   |
| ```0x08``` | RW  | Ranks LED          |
| ```0x09``` | RW  | File LEDs          |
| ```0x0A``` | RW  | Pieces LEDs        |
| ```0x0B``` | RO  | Buttons status     |
| ```0xFF``` | RO  | Request all status |

## 0x00 : pieces on Rank 1
Pieces on the first rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A1 | B1 | C1 | D1 | E1 | F1 | G1 | H1 |

## 0x01 : pieces on Rank 2
Pieces on the second rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A2 | B2 | C2 | D2 | E2 | F2 | G2 | H2 |

## 0x02 : pieces on Rank 3
Pieces on the third rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A3 | B3 | C3 | D3 | E3 | F3 | G3 | H3 |

## 0x03 : pieces on Rank 4
Pieces on the fourth rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A4 | B4 | C4 | D4 | E4 | F4 | G4 | H4 |

## 0x04 : pieces on Rank 5
Pieces on the fifth rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A5 | B5 | C5 | D5 | E5 | F5 | G5 | H5 |

## 0x05 : pieces on Rank 6
Pieces on the sixth rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A6 | B6 | C6 | D6 | E6 | F6 | G6 | H6 |

## 0x06 : pieces on Rank 7
Pieces on the seventh rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A7 | B7 | C7 | D7 | E7 | F7 | G7 | H7 |

## 0x07 : pieces on Rank 8
Pieces on the eigth rank:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A8 | B8 | C8 | D8 | E8 | F8 | G8 | H8 |

## 0x08 : Rank LEDs
On/off status of rank LEDs:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| 8  | 7  | 6  | 5  | 4  | 3  | 2  | 1  |

## 0x09 : File LEDs
On/off status of file LEDs:

| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| A  | B  | C  | D  | E  | F  | G  | H  |

## 0x0A : Pieces LEDs
On/off status of pieces LEDs:

| 7     | 6     | 5          | 4          | 3    | 2      | 1      | 0     |
|:-----:|:-----:|:----------:|:----------:|:----:|:------:|:------:|:-----:|
| White | Black | *reserved* | *reserved* | Rook | Knight | Bishop | Queen |

## 0x0B : Buttons status
On/off buttons status:

| 7          | 6          | 5          | 4          | 3    | 2      | 1      | 0     |
|:----------:|:----------:|:----------:|:----------:|:----:|:------:|:------:|:-----:|
| *reserved* | *reserved* | *reserved* | *reserved* | Rook | Knight | Bishop | Queen |

## 0xFF : Request all status
After reading this registers, status of all registers are sent by the
device. Writing to this register has no effect.

| 7          | 6          | 5          | 4          | 3          | 2          | 1          | 0          |
|:----------:|:----------:|:----------:|:----------:|:----------:|:----------:|:----------:|:----------:|
| *reserved* | *reserved* | *reserved* | *reserved* | *reserved* | *reserved* | *reserved* | *reserved* |

