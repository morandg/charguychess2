# Hardware
The driver communicates with MCP23017 i2c IO expanders. There are 6 of them:
* One to read ranks 1 and 2
* One to read ranks 3 and 4
* One to read ranks 5 and 6
* One to read ranks 7 and 8
* One to blink ranks and file LEDs
* One for the buttons, player LEDs and pieces LEDs

# Communication protocol
The driver creates a ```/dev/charguychess``` character device where the
communication is done using the [ASCII cgc protocol](hardware-protocol.md).

The driver translates the ASCII commands into i2c communication. It also reads
the i2c bus to inform changes to the user, this way, the device can be read
using a ```select(...)``` family call.

# Device tree
The driver is configured using device tree. First it needs to be instantated
this way:
```txt
/ {
	charguychess {
		compatible = "charguychess";
	};
};
```

Each i2c device is instantiated on a bus as follow:
```txt
&i2c1 
	cgc_rank_12@20 {
		compatible = "charguychess,rank_12";
		reg = <0x20>;
	};
	cgc_rank_34@21 {
		compatible = "charguychess,rank_34";
		reg = <0x21>;
	};
	cgc_rank_56@22 {
		compatible = "charguychess,rank_56";
		reg = <0x22>;
	};
	cgc_rank_78@23 {
		compatible = "charguychess,rank_78";
		reg = <0x23>;
	};
	cgc_rf_leds@24 {
		compatible = "charguychess,rf_leds";
		reg = <0x24>;
	};
};
```
