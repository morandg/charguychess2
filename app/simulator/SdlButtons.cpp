/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <SDL_opengl.h>

#include <rglib/logging/LogMacros.hpp>

#include <cgc-protocol/registers_defs.h>

#include "SdlButtons.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
SdlButtons::SdlButtons(
    MainLoopMonitor& mainLoopMonitor, RegistersMonitor& registers):
  m_mainLoopMonitor(mainLoopMonitor),
  m_registers(registers)
{
}

//------------------------------------------------------------------------------
SdlButtons::~SdlButtons()
{
}

//------------------------------------------------------------------------------
int SdlButtons::draw(unsigned int buttonNum, bool isPressed)
{
  if(isPressed)
    glColor3f(0, 0.5, 0);
  else
    glColor3f(0.5, 0, 0);

  glBegin(GL_QUADS);
  glVertex2f(-.8, -.8);
  glVertex2f(-.8, .8);
  glVertex2f(.8, .8);
  glVertex2f(.8, -.8);
  glEnd();

  glColor3f(0, 0, 0);
  switch(buttonNum)
  {
  case 0:
    drawQueen();
    break;
  case 1:
    drawBishop();
    break;
  case 2:
    drawKnight();
    break;
  case 3:
    drawRook();
    break;
  default:
    break;
  }

  return 0;
}

//------------------------------------------------------------------------------
void SdlButtons::onLeftClick(unsigned int buttonNum)
{
  uint8_t buttonsStatus;
  uint8_t buttonMask;

  m_registers.read(CGC_REGISTER_BUTTONS_STATUS, buttonsStatus);

  switch(buttonNum)
  {
  case 0:
    buttonMask = CGC_REGISTER_MASK_BUTTON_QUEEN;
    break;
  case 1:
    buttonMask = CGC_REGISTER_MASK_BUTTON_BISHOP;
    break;
  case 2:
    buttonMask = CGC_REGISTER_MASK_BUTTON_KNIGHT;
    break;
  case 3:
    buttonMask = CGC_REGISTER_MASK_BUTTON_ROOK;
    break;
  default:
    LOGER() << "Unknowns buttons number " << buttonNum;
    return;
  }

  if(buttonsStatus & buttonMask)
    buttonsStatus &= ~buttonMask;
  else
    buttonsStatus |= buttonMask;

  m_mainLoopMonitor.sendRegisterEvent(
      CGC_REGISTER_BUTTONS_STATUS, buttonsStatus);
}

//------------------------------------------------------------------------------
void SdlButtons::drawRook()
{
  glBegin(GL_QUADS);
  glVertex2f(-.5, -.8);
  glVertex2f(-.5, 0);
  glVertex2f(.5, 0);
  glVertex2f(.5, -.8);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.5, 0);
  glVertex2f(-.5, .2);
  glVertex2f(-.3, .2);
  glVertex2f(-.3, 0);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.1, 0);
  glVertex2f(-.1, .2);
  glVertex2f(.1, .2);
  glVertex2f(.1, 0);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(.3, 0);
  glVertex2f(.3, .2);
  glVertex2f(.5, .2);
  glVertex2f(.5, 0);
  glEnd();
}

//------------------------------------------------------------------------------
void SdlButtons::drawKnight()
{
  glBegin(GL_QUADS);
  glVertex2f(-.5, -.8);
  glVertex2f(-.5, -.6);
  glVertex2f(.5, -.6);
  glVertex2f(.5, -.8);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.1, -.6);
  glVertex2f(-.1, 0);
  glVertex2f(.1, 0);
  glVertex2f(.1, -.6);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.1, 0);
  glVertex2f(-.1, .2);
  glVertex2f(.5, .2);
  glVertex2f(.5, 0);
  glEnd();
}

//------------------------------------------------------------------------------
void SdlButtons::drawBishop()
{
  glBegin(GL_QUADS);
  glVertex2f(-.5, -.8);
  glVertex2f(-.5, -.6);
  glVertex2f(.5, -.6);
  glVertex2f(.5, -.8);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.1, -.6);
  glVertex2f(-.1, 0);
  glVertex2f(.1, 0);
  glVertex2f(.1, -.6);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.4, 0);
  glVertex2f(-.1, .8);
  glVertex2f(.1, .8);
  glVertex2f(.4, 0);
  glEnd();
}

//------------------------------------------------------------------------------
void SdlButtons::drawQueen()
{
  glBegin(GL_QUADS);
  glVertex2f(-.5, -.8);
  glVertex2f(-.5, -.6);
  glVertex2f(.5, -.6);
  glVertex2f(.5, -.8);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.1, -.6);
  glVertex2f(-.1, 0);
  glVertex2f(.1, 0);
  glVertex2f(.1, -.6);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(-.4, 0);
  glVertex2f(-.4, .8);
  glVertex2f(0, .1);
  glVertex2f(0, 0);
  glEnd();

  glBegin(GL_QUADS);
  glVertex2f(0, 0);
  glVertex2f(0, .1);
  glVertex2f(.4, .8);
  glVertex2f(.4, 0);
  glEnd();
}

}       // namespace
