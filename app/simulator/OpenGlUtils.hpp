/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGCSIM_OPEN_GL_UTILS_HPP_
#define _CGCSIM_OPEN_GL_UTILS_HPP_

#include <rglib/geometry/Rectangle.hpp>

namespace cgc_simulator {

class OpenGlUtils
{
public:
  OpenGlUtils() = delete;
  ~OpenGlUtils() = delete;

  static void openGlToSdlCoordinate(
      rglib::Vector2D& screenSize, rglib::Rectangle& openglRectangle);
  static void sdlCoordinateToStandard(rglib::Rectangle& sdlRectangle);
};

}       // namespace
#endif  // _CGCSIM_OPEN_GL_UTILS_HPP_
