/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>
#include <cstring>

#include <rglib/logging/LogMacros.hpp>

#include "LoopEventQuit.hpp"
#include "LoopEventRegisterChanged.hpp"
#include "MainLoopMonitor.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
MainLoopMonitor::MainLoopMonitor(
    rglib::IEventLoop& mainLoop, CharGuyChessDriver& driver):
  m_mainLoop(mainLoop),
  m_driver(driver)
{
}

//------------------------------------------------------------------------------
MainLoopMonitor::~MainLoopMonitor()
{
}

//------------------------------------------------------------------------------
int MainLoopMonitor::init()
{
  if(m_pendingEvents.init())
    return -1;

  rglib::IPipe::create(m_syncPipe);
  if(!m_syncPipe)
  {
    LOGER() << "Cannot create synchronization pipe";
    return -1;
  }

  if(m_syncPipe->open())
  {
    LOGER() << "Cannot open syncrhonziation pipe";
    return -1;
  }

  if(m_mainLoop.addHandledIo(
      *this, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST))
  {
    LOGER() << "Cannot register main loop synchronzier to main loop";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int MainLoopMonitor::sendQuit()
{
  std::unique_ptr<ILoopEvent> event;

  LOGDB() << "Sending quit event to main loop";

  event.reset(new LoopEventQuit(m_mainLoop));
  if(m_pendingEvents.add(event))
    return -1;

  return wakeupMainThread();
}

//------------------------------------------------------------------------------
int MainLoopMonitor::sendRegisterEvent(uint8_t reg, uint8_t value)
{
  std::unique_ptr<ILoopEvent> event;

  LOGDB() << "Sending register event to main loop";

  event.reset(new LoopEventRegisterChanged(m_driver, reg, value));
  if(m_pendingEvents.add(event))
    return -1;

  return wakeupMainThread();
}

//------------------------------------------------------------------------------
rglib::FileDescriptor MainLoopMonitor::getFileDescriptor()
{
  return m_syncPipe->getReadEnd().getFileDescriptor();
}

//------------------------------------------------------------------------------
void MainLoopMonitor::onReadReady()
{
  std::unique_ptr<ILoopEvent> event;

  // We are in main loop thread!
  LOGDB() << "Got an event from the SDL thread";

  ackMainThreadEvent();

  m_pendingEvents.next(event);
  if(!event)
  {
    LOGER() << "BUG? No event peding from SDL";
    assert(0);  // Should not happen
  }
  else
    event->execute();
}

//------------------------------------------------------------------------------
int MainLoopMonitor::wakeupMainThread()
{
  int ret;
  char buffer = 'a';

  assert(m_syncPipe);

  ret = m_syncPipe->getWriteEnd().write(&buffer, 1);
  if(ret < 0)
  {
    LOGER() << "Cannot wake up main thread:" << strerror(-ret);
    assert(0);  // should not happen
    return -1;
  }
  else if(ret == 0)
  {
    LOGER() << "Nothing written to synchronization pipe";
    assert(0);  // should not happen
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int MainLoopMonitor::ackMainThreadEvent()
{
  int ret;
  char buffer;

  assert(m_syncPipe);

  ret = m_syncPipe->getReadEnd().read(&buffer, 1);
  if(ret < 0)
  {
    LOGER() << "Cannot ack main thread event:" << strerror(-ret);
    assert(0);  // should not happen
    return -1;
  }
  else if(ret == 0)
  {
    LOGER() << "Nothing read from synchronization pipe";
    assert(0);  // should not happen
    return -1;
  }

  return 0;
}

}       // namespace
