/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <SDL_opengl.h>

#include <rglib/logging/LogMacros.hpp>

#include <cgc-protocol/registers_defs.h>

#include "OpenGlUtils.hpp"
#include "SdlWindow.hpp"

namespace cgc_simulator {

static const int X_RES                  = 550;
static const int Y_RES                  = 500;
static const int GRID_WIDTH             = 11;
static const int GRID_HEIGHT            = 9;

//------------------------------------------------------------------------------
SdlWindow::SdlWindow(
    MainLoopMonitor& mainLoopMonitor, RegistersMonitor& registers):
  m_registers(registers),
  m_sdlWindow(nullptr),
  m_sdlGlContext(nullptr),
  m_chessBoard(mainLoopMonitor, registers),
  m_buttons(mainLoopMonitor, registers)
{
}

//------------------------------------------------------------------------------
SdlWindow::~SdlWindow()
{
  if(m_sdlGlContext)
    SDL_GL_DeleteContext(m_sdlGlContext);
  if(m_sdlWindow)
    SDL_DestroyWindow(m_sdlWindow);
  SDL_Quit();
}

//------------------------------------------------------------------------------
int SdlWindow::init()
{
  if(SDL_Init(SDL_INIT_VIDEO))
  {
    LOGER() << "SDL initialization failed: " << SDL_GetError();
    return -1;
  }

  m_sdlWindow = SDL_CreateWindow(
      "charguychess hardware simulator",
      SDL_WINDOWPOS_CENTERED,
      SDL_WINDOWPOS_CENTERED,
      X_RES,
      Y_RES,
      SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  if(!m_sdlWindow)
  {
    LOGER() << "Error creating SDL window: " << SDL_GetError();
    return -1;
  }

  m_sdlGlContext = SDL_GL_CreateContext(m_sdlWindow);
  if(!m_sdlGlContext)
  {
    LOGER() << "Error creating OpenGl context: " << SDL_GetError();
    return -1;
  }

  if(SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1))
  {
    LOGER() << "Cannot set OpenGl double buffering: " << SDL_GetError();
    return -1;
  }

  if(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24))
  {
    LOGER() << "Cannot set OpenGl color deppth: " << SDL_GetError();
    return -1;
  }

  if(SDL_GL_SetSwapInterval(1))
  {
    LOGER() << "Cannot set OpenGl swap interval: " << SDL_GetError();
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int SdlWindow::draw()
{
  rglib::Vector2D windowSize;

  glClearColor(0.5, 0.5, 0.5, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  getWindowSize(windowSize);

  drawChessBoard(windowSize);
  drawRanksLeds(windowSize);
  drawFilesLeds(windowSize);
  drawPlayersLeds(windowSize);
  drawButtons(windowSize);

  SDL_GL_SwapWindow(m_sdlWindow);

  return 0;
}

//------------------------------------------------------------------------------
void SdlWindow::onLeftMouseButton(int x, int y)
{
  rglib::Vector2D windowSize;
  rglib::Rectangle chessBoard;
  rglib::Vector2D clickedLocation(x, y);

  getWindowSize(windowSize);
  openGlBoardLocation(windowSize, chessBoard);
  OpenGlUtils::openGlToSdlCoordinate(windowSize, chessBoard);

  if(isOnChessBoard(chessBoard, clickedLocation))
    m_chessBoard.onLeftClick(clickedLocation, chessBoard);
  else
    checkButtonPressed(windowSize, clickedLocation);
}

//------------------------------------------------------------------------------
void SdlWindow::drawChessBoard(const rglib::Vector2D& windowSize)
{
  buildChessboardViewport(windowSize);
  m_chessBoard.draw();
}

//------------------------------------------------------------------------------
void SdlWindow::drawRanksLeds(const rglib::Vector2D& windowSize)
{
  rglib::Rectangle ledSquare;
  int SquareWidth = getSquareWidth(windowSize);
  uint8_t ledsStatus;

  m_registers.read(CGC_REGISTER_RANK_LEDS, ledsStatus);

  openGlRankOneLedLocation(windowSize, ledSquare);
  for(int i = 8 ; i > 0 ; --i)
  {
    drawLed(ledSquare, ledsStatus & (1 << (i - 1)));

    ledSquare = rglib::Rectangle(
        ledSquare.getX() + SquareWidth,
        ledSquare.getY(),
        ledSquare.getWidth(), ledSquare.getHeight());
  }
}

//------------------------------------------------------------------------------
void SdlWindow::drawFilesLeds(const rglib::Vector2D& windowSize)
{
  rglib::Rectangle ledSquare;
  int SquareWidth = getSquareWidth(windowSize);
  uint8_t ledsStatus;

  m_registers.read(CGC_REGISTER_FILE_LEDS, ledsStatus);

  openGlFileALedLocation(windowSize, ledSquare);
  for(int i = 8 ; i > 0 ; --i)
  {
    drawLed(ledSquare, ledsStatus & (1 << (i - 1)));

    ledSquare = rglib::Rectangle(
        ledSquare.getX(),
        ledSquare.getY() + SquareWidth,
        ledSquare.getWidth(),
        ledSquare.getHeight());
  }
}

//------------------------------------------------------------------------------
void SdlWindow::drawPlayersLeds(const rglib::Vector2D& windowSize)
{
  rglib::Rectangle ledSquare;
  uint8_t ledsStatus;

  m_registers.read(CGC_REGISTER_PIECES_LEDS, ledsStatus);

  openGlWhiteLedLocation(windowSize, ledSquare);
  drawLed(ledSquare, ledsStatus & CGC_REGISTER_MASK_LED_WHITE);
  ledSquare = rglib::Rectangle(
      ledSquare.getX(),
      ledSquare.getY() + (7 * ledSquare.getHeight()),
      ledSquare.getWidth(),
      ledSquare.getHeight());
  drawLed(ledSquare, ledsStatus & CGC_REGISTER_MASK_LED_BLACK);
}

//------------------------------------------------------------------------------
void SdlWindow::drawButtons(const rglib::Vector2D& windowSize)
{
  int squareWidth = getSquareWidth(windowSize);
  uint8_t pieceLeds;
  uint8_t buttonsStatus;

  m_registers.read(CGC_REGISTER_PIECES_LEDS, pieceLeds);
  m_registers.read(CGC_REGISTER_BUTTONS_STATUS, buttonsStatus);

  for(unsigned int i = 0 ; i < 4 ; ++i)
  {
    bool isLedOn;
    bool isButtonPressed;

    glViewport(
        squareWidth * 10,
        windowSize.getY() - ((i + 2) * squareWidth),
        (GLsizei)squareWidth,
        (GLsizei)squareWidth);

    switch(i)
    {
    case 0:
      isLedOn = pieceLeds & CGC_REGISTER_MASK_LED_QUEEN;
      isButtonPressed = buttonsStatus & CGC_REGISTER_MASK_BUTTON_QUEEN;
      break;
    case 1:
      isLedOn = pieceLeds & CGC_REGISTER_MASK_LED_BISHOP;
      isButtonPressed = buttonsStatus & CGC_REGISTER_MASK_BUTTON_BISHOP;
      break;
    case 2:
      isLedOn = pieceLeds & CGC_REGISTER_MASK_LED_KNIGHT;
      isButtonPressed = buttonsStatus & CGC_REGISTER_MASK_BUTTON_KNIGHT;
      break;
    case 3:
      isLedOn = pieceLeds & CGC_REGISTER_MASK_LED_ROOK;
      isButtonPressed = buttonsStatus & CGC_REGISTER_MASK_BUTTON_ROOK;
      break;
    }

    // Background color is led status
    if(isLedOn)
      glColor3f(0, 1, 0);
    else
      glColor3f(0, 0, 0);

    glBegin(GL_QUADS);
    glVertex2f(-1, -1);
    glVertex2f(-1, 1);
    glVertex2f(1, 1);
    glVertex2f(1, -1);
    glEnd();


    m_buttons.draw(i, isButtonPressed);
  }
}

//------------------------------------------------------------------------------
void SdlWindow::drawLed(const rglib::Rectangle& ledSquare, bool isOn)
{
  glViewport(
      ledSquare.getX(),
      ledSquare.getY(),
      (GLsizei)ledSquare.getHeight(),
      (GLsizei)ledSquare.getWidth());

  if(isOn)
    glColor3f(0, 1, 0);
  else
    glColor3f(0, 0, 0);

  glBegin(GL_QUADS);
  glVertex2f(-0.2, -0.2);
  glVertex2f(-0.2, 0.2);
  glVertex2f(0.2, 0.2);
  glVertex2f(0.2, -0.2);
  glEnd();
}

//------------------------------------------------------------------------------
void SdlWindow::buildChessboardViewport(const rglib::Vector2D& windowSize)
{
  rglib::Rectangle board;

  openGlBoardLocation(windowSize, board);
  glViewport(
      board.getX(),
      board.getY(),
      (GLsizei)board.getHeight(),
      (GLsizei)board.getWidth());
}

//------------------------------------------------------------------------------
int SdlWindow::getSquareWidth(const rglib::Vector2D& windowSize)
{
  int squareWidth;

  squareWidth = windowSize.getX() / GRID_WIDTH;
  if(squareWidth * GRID_HEIGHT > windowSize.getY())
    squareWidth = windowSize.getY() / GRID_HEIGHT;

  return squareWidth;
}

//------------------------------------------------------------------------------
void SdlWindow::openGlBoardLocation(
    const rglib::Vector2D& windowSize, rglib::Rectangle& boardSize)
{
  int squareWidth = getSquareWidth(windowSize);

  boardSize = rglib::Rectangle(
      squareWidth,
      windowSize.getY() - (8 * squareWidth),
      squareWidth * 8,
      squareWidth * 8);
}

//------------------------------------------------------------------------------
void SdlWindow::openGlRankOneLedLocation(
    const rglib::Vector2D& windowSize, rglib::Rectangle& rankOne)
{
  int squareWidth = getSquareWidth(windowSize);

  rankOne = rglib::Rectangle(
      squareWidth,
      windowSize.getY() - (9 * squareWidth),
      squareWidth,
      squareWidth);
}

//------------------------------------------------------------------------------
void SdlWindow::openGlFileALedLocation(
    const rglib::Vector2D& windowSize, rglib::Rectangle& fileALed)
{
  int squareWidth = getSquareWidth(windowSize);

  fileALed = rglib::Rectangle(
      0,
      windowSize.getY() - (8 * squareWidth),
      squareWidth,
      squareWidth);
}

//------------------------------------------------------------------------------
void SdlWindow::openGlWhiteLedLocation(
    const rglib::Vector2D& windowSize, rglib::Rectangle& whiteLed)
{
  int squareWidth = getSquareWidth(windowSize);

  whiteLed = rglib::Rectangle(
      squareWidth * 9,
      windowSize.getY() - (8 * squareWidth),
      squareWidth,
      squareWidth);
}

//------------------------------------------------------------------------------
void SdlWindow::getWindowSize(rglib::Vector2D& windowSize)
{
  int windowWidth;
  int windowHeight;

  SDL_GetWindowSize(m_sdlWindow, &windowWidth, &windowHeight);
  windowSize = rglib::Vector2D(windowWidth, windowHeight);
}

//------------------------------------------------------------------------------
bool SdlWindow::isOnChessBoard(
    const rglib::Rectangle& boardSdlLocation,
    const rglib::Vector2D& clickLocation)
{
  rglib::Rectangle boardLocationStandard = boardSdlLocation;

  OpenGlUtils::sdlCoordinateToStandard(boardLocationStandard);

  return boardLocationStandard.isIn(clickLocation);
}

//------------------------------------------------------------------------------
void SdlWindow::checkButtonPressed(
    const rglib::Vector2D& windowSize,
    const rglib::Vector2D& clickLocation)
{
  int squareWidth = getSquareWidth(windowSize);
  rglib::Rectangle buttonLocation;

  for(unsigned int i = 0 ; i < 4 ; ++i)
  {
    buttonLocation = rglib::Rectangle(
        squareWidth * 10,
        (i + 1) * squareWidth,
        squareWidth,
        squareWidth);

    OpenGlUtils::sdlCoordinateToStandard(buttonLocation);
    if(buttonLocation.isIn(clickLocation))
    {
      m_buttons.onLeftClick(i);
      break;
    }
  }
}

}       // namespace
