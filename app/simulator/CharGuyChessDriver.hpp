/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGCSIM_CHAR_GUY_CHESS_DRIVER_HPP_
#define _CGCSIM_CHAR_GUY_CHESS_DRIVER_HPP_

#include <rglib/event-loop/IEventLoop.hpp>
#include <rglib/io/IFile.hpp>

#include <cgc-protocol/parser.h>

#include "../src/hardware/driver/CgcProtocolParser.hpp"

#include "RegistersMonitor.hpp"

namespace cgc_simulator {

class CharGuyChessDriver:
    public rglib::IHandledIo,
    public charguychess::ICgcProtocolParserEvent
{
public:
  CharGuyChessDriver(
      rglib::IEventLoop& eventLoop, RegistersMonitor& registers);
  virtual ~CharGuyChessDriver();

  int init();
  int writeRegister(uint8_t reg, uint8_t value);

  // rglib::IHandledIo
  virtual rglib::FileDescriptor getFileDescriptor() override;
  virtual void onReadReady() override;

  // charguychess::ICgcProtocolParserEvent
  virtual void onRegisterChanged(uint8_t reg, uint8_t value) override;

private:
  rglib::IEventLoop& m_eventLoop;
  RegistersMonitor& m_registers;
  charguychess::CgcProtocolParser m_parser;
  std::unique_ptr<rglib::IFile> m_serialPort;
};

}       // namespace
#endif  // _CGCSIM_CHAR_GUY_CHESS_DRIVER_HPP_
