/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_SDL_WINDOW_HPP_
#define _CGC_SDL_WINDOW_HPP_

#include <SDL.h>

#include <rglib/geometry/Rectangle.hpp>

#include "MainLoopMonitor.hpp"
#include "RegistersMonitor.hpp"
#include "ChessBoard.hpp"
#include "SdlButtons.hpp"

namespace cgc_simulator {

class SdlWindow
{
public:
  SdlWindow(MainLoopMonitor& mainLoopMonitor, RegistersMonitor& registers);
  virtual ~SdlWindow();

  int init();
  int draw();
  void onLeftMouseButton(int x, int y);

private:
  RegistersMonitor& m_registers;
  SDL_Window* m_sdlWindow;
  SDL_GLContext m_sdlGlContext;
  ChessBoard m_chessBoard;
  SdlButtons m_buttons;

  void getWindowSize(rglib::Vector2D& windowSize);
  void drawChessBoard(const rglib::Vector2D& windowSize);
  void drawRanksLeds(const rglib::Vector2D& windowSize);
  void drawFilesLeds(const rglib::Vector2D& windowSize);
  void drawPlayersLeds(const rglib::Vector2D& windowSize);
  void drawButtons(const rglib::Vector2D& windowSize);
  void drawLed(const rglib::Rectangle& ledSquare, bool isOn);
  void buildChessboardViewport(const rglib::Vector2D& windowSize);
  int getSquareWidth(const rglib::Vector2D& windowSize);
  void openGlBoardLocation(
      const rglib::Vector2D& windowSize, rglib::Rectangle& boardSize);
  void openGlRankOneLedLocation(
      const rglib::Vector2D& windowSize, rglib::Rectangle& rankOne);
  void openGlFileALedLocation(
      const rglib::Vector2D& windowSize, rglib::Rectangle& rankALed);
  void openGlWhiteLedLocation(
      const rglib::Vector2D& windowSize, rglib::Rectangle& whiteLed);
  bool isOnChessBoard(
      const rglib::Rectangle& boardSdlLocation,
      const rglib::Vector2D& clickLocation);
  void checkButtonPressed(
      const rglib::Vector2D& windowSize,
      const rglib::Vector2D& clickLocation);
};

}       // namespace
#endif  // _CGC_SDL_WINDOW_HPP_
