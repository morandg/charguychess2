/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "PendingEvents.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
PendingEvents::PendingEvents()
{
}

//------------------------------------------------------------------------------
PendingEvents::~PendingEvents()
{
}

//------------------------------------------------------------------------------
int PendingEvents::init()
{
  rglib::IMutex::create(m_eventsMutex);

  if(!m_eventsMutex)
  {
    LOGER() << "Cannot create pending events mutex";
    return -1;
  }
  return m_eventsMutex->init();
}

//------------------------------------------------------------------------------
int PendingEvents::add(std::unique_ptr<ILoopEvent>& nextEvent)
{
  if(getMutex())
    return -1;

  m_pendingEvents.push_back(std::move(nextEvent));

  return releaseMutex();
}

//------------------------------------------------------------------------------
int PendingEvents::next(std::unique_ptr<ILoopEvent>& nextEvent)
{
  if(getMutex())
    return -1;

  if(m_pendingEvents.empty())
  {
    LOGER() << "BUG? Getting event when no event is pending!";
    return releaseMutex();
  }

  nextEvent = std::move(m_pendingEvents.front());
  m_pendingEvents.pop_front();

  return releaseMutex();
}

//------------------------------------------------------------------------------
int PendingEvents::getMutex()
{
  if(m_eventsMutex->get())
  {
    LOGER() << "Cannot get events mutex";
    assert(0);  // Should not happen
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int PendingEvents::releaseMutex()
{
  if(m_eventsMutex->release())
  {
    LOGER() << "Cannot release events mutex";
    assert(0);  // Should not happen
    return -1;
  }

  return 0;
}

}       // namespace
