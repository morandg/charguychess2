/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <SDL_opengl.h>

#include <cgc-protocol/registers_defs.h>

#include "ChessBoard.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
ChessBoard::ChessBoard(
    MainLoopMonitor& mainLoopMonitor, RegistersMonitor& registers):
  m_mainLoopMonitor(mainLoopMonitor),
  m_registers(registers)
{
}

//------------------------------------------------------------------------------
ChessBoard::~ChessBoard()
{
}

//------------------------------------------------------------------------------
int ChessBoard::draw()
{
  float x = -1;
  float y = -1;
  float width = 2. / 8;
  bool isBlackSquare = true;
  uint8_t regValue;

  for(uint8_t rank = CGC_REGISTER_RANK1; rank <= CGC_REGISTER_RANK8 ; ++rank)
  {
    m_registers.read(rank, regValue);

    for(int file = 8 ; file > 0 ; --file)
    {
      if(isBlackSquare)
        glColor3f(0, 0, 0);
      else
        glColor3f(1, 1, 1);

      glBegin(GL_QUADS);
      glVertex2f(x, y);
      glVertex2f(x, y + width);
      glVertex2f(x + width, y + width);
      glVertex2f(x + width, y);
      glEnd();

      if(regValue & (1 << (file - 1)))
      {
        float margin  = .1;
        glColor3f(.5, .5, .5);

        glBegin(GL_QUADS);
        glVertex2f(x + margin, y + margin);
        glVertex2f(x + margin, y + width - margin);
        glVertex2f(x + width - margin, y + width - margin);
        glVertex2f(x + width - margin, y + margin);
        glEnd();
      }

      x += width;
      isBlackSquare = !isBlackSquare;
    }

    isBlackSquare = !isBlackSquare;
    x = -1;
    y += width;
  }

  return 0;
}

//------------------------------------------------------------------------------
void ChessBoard::onLeftClick(
    const rglib::Vector2D& point, const rglib::Rectangle boardLocation)
{
  int squareWidth;
  uint8_t clickedFile = 0;
  uint8_t clickedRank = 0;
  uint8_t rankRegister;
  uint8_t rankValue;
  int squareBorder;

  squareWidth = boardLocation.getWidth() / 8;

  // Check which file
  squareBorder = boardLocation.getX() + squareWidth;
  while(point.getX() > squareBorder)
  {
    clickedFile++;
    squareBorder += squareWidth;
  }
  clickedFile = 0x80 >> clickedFile;

  // Check which rank
  squareBorder = boardLocation.getY() + boardLocation.getHeight() - squareWidth;
  while(point.getY() < squareBorder)
  {
    clickedRank++;
    squareBorder -= squareWidth;
  }
  rankRegister = CGC_REGISTER_RANK1 + clickedRank;

  m_registers.read(rankRegister, rankValue);
  if(rankValue & clickedFile)
    rankValue &= ~clickedFile;
  else
    rankValue |= clickedFile;

  m_mainLoopMonitor.sendRegisterEvent(rankRegister, rankValue);
}

}       // namespace
