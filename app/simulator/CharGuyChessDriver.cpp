/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include <cgc-protocol/registers_defs.h>
#include <cgc-protocol/serializer.h>

#include "CharGuyChessDriver.hpp"

namespace cgc_simulator {

static const std::string CHARGUYCHESS_DEVICE = "/dev/charguychess";

//------------------------------------------------------------------------------
CharGuyChessDriver::CharGuyChessDriver(
    rglib::IEventLoop& eventLoop, RegistersMonitor& registers):
  m_eventLoop(eventLoop),
  m_registers(registers)
{
}

//------------------------------------------------------------------------------
CharGuyChessDriver::~CharGuyChessDriver()
{
}

//------------------------------------------------------------------------------
int CharGuyChessDriver::init()
{
  unsigned int bufferSize = 8;
  char buffer[bufferSize];
  unsigned int serializedSize;
  int ret;

  rglib::IFile::create(m_serialPort);

  ret = m_serialPort->open(CHARGUYCHESS_DEVICE, rglib::OpenFlag::READ_WRITE);
  if(ret < 0)
  {
    LOGER() << "Cannot open charguychess device " << CHARGUYCHESS_DEVICE <<
        ": " << strerror(-ret);
    return -1;
  }

  if(m_eventLoop.addHandledIo(
      *this, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST))
  {
    LOGER() << "Cannot register charguychess serial port to event loop";
    return -1;
  }

  if(m_parser.init(*this))
  {
    LOGER() << "Cannot initialize cgc protocol parser";
    return -1;
  }

  if(cgc_serializer_read_request(
      buffer, bufferSize, CGC_REGISTER_ALL_STATUS, &serializedSize))
  {
    LOGER() << "Cannot serialize request all registers request";
    return -1;
  }

  ret = m_serialPort->write(buffer, serializedSize);
  if(ret < 0)
  {
    LOGER() << "Cannot send data to charguychess device: " << strerror(-ret);
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int CharGuyChessDriver::writeRegister(uint8_t reg, uint8_t value)
{
  unsigned int bufferSize = 8;
  char buffer[bufferSize];
  unsigned int serializedSize;
  int ret;

  assert(m_serialPort);

  if(cgc_serializer_write_request(
      buffer, bufferSize, reg, value, &serializedSize))
  {
    LOGER() << "Cannot serialize write register request";
    return -1;
  }

  ret = m_serialPort->write(buffer, serializedSize);
  if(ret < 0)
  {
    LOGER() << "Cannot write to charguychess device: " << strerror(-ret);
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
rglib::FileDescriptor CharGuyChessDriver::getFileDescriptor()
{
  assert(m_serialPort);

  return m_serialPort->getFileDescriptor();
}

//------------------------------------------------------------------------------
void CharGuyChessDriver::onReadReady()
{
  unsigned int bufferSize = 128;
  char buffer[bufferSize];
  int ret;

  assert(m_serialPort);

  ret = m_serialPort->read(buffer, bufferSize);
  if(ret < 0)
  {
    LOGER() << "Cannot read from charguychess device: " << strerror(-ret);
    return;
  }

  m_parser.parseBuffer(buffer, ret);
}

//------------------------------------------------------------------------------
void CharGuyChessDriver::onRegisterChanged(uint8_t reg, uint8_t value)
{
  LOGDB() << "Register changed 0x" << std::hex << (int)reg <<
      ":0x" << (int)value;

  m_registers.write(reg, value);
}

}       // namespace
