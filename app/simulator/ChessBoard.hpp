/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGCSIM_CHESS_BOARD_HPP_
#define _CGCSIM_CHESS_BOARD_HPP_

#include <rglib/geometry/Rectangle.hpp>

#include "MainLoopMonitor.hpp"
#include "RegistersMonitor.hpp"

namespace cgc_simulator {

class ChessBoard
{
public:
  ChessBoard(MainLoopMonitor& mainLoopMonitor, RegistersMonitor& registers);
  virtual ~ChessBoard();

  int draw();
  int init();
  void onLeftClick(
      const rglib::Vector2D& point, const rglib::Rectangle boardLocation);

private:
  MainLoopMonitor& m_mainLoopMonitor;
  RegistersMonitor& m_registers;
};

}       // namespace
#endif  // _CGCSIM_CHESS_BOARD_HPP_
