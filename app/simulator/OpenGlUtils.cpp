/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "OpenGlUtils.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
void OpenGlUtils::openGlToSdlCoordinate(
      rglib::Vector2D& screenSize, rglib::Rectangle& openglRectangle)
{
  /**
   * As this was not brainfucking enough, openGL has origin on bottom left while
   * SDL uses top left origin1
   */
  openglRectangle = rglib::Rectangle(
      openglRectangle.getX(),
      screenSize.getY() - openglRectangle.getY() - openglRectangle.getHeight(),
      openglRectangle.getHeight(),
      openglRectangle.getWidth());
}

//------------------------------------------------------------------------------
void OpenGlUtils::sdlCoordinateToStandard(rglib::Rectangle& sdlRectangle)
{
  // Yet another transformation as rglib works with standard coordinate!
  sdlRectangle =
      rglib::Rectangle(sdlRectangle.getX(),
          sdlRectangle.getY() + sdlRectangle.getHeight(),
          sdlRectangle.getWidth(),
          sdlRectangle.getHeight());
}

}       // namespace
