/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <SDL.h>

#include <rglib/logging/LogMacros.hpp>

#include "SdlLoop.hpp"

namespace cgc_simulator {

static const Uint32 MAX_FPS         = 20;
static const Uint32 FRAME_DURATION  = 1000 / MAX_FPS;

//------------------------------------------------------------------------------
SdlLoop::SdlLoop(
    rglib::IEventLoop& mainLoop,
    RegistersMonitor& registers,
    CharGuyChessDriver& driver):
  m_isRunning(false),
  m_mainLoopMonitor(mainLoop, driver),
  m_window(m_mainLoopMonitor, registers)
{
}

//------------------------------------------------------------------------------
SdlLoop::~SdlLoop()
{
  stop();
}

//------------------------------------------------------------------------------
int SdlLoop::init()
{
  if(m_mainLoopMonitor.init())
    return -1;

  return startSdlThread();
}

//------------------------------------------------------------------------------
void SdlLoop::stop()
{
  LOGDB() << "Stopping SDL loop ...";

  m_isRunning = false;
  if(m_sdlThread)
    m_sdlThread->wait();
}

//------------------------------------------------------------------------------
void SdlLoop::run()
{
  Uint32 lastTick;

  LOGDB() << "SDL loop enter";

  // It seems that this must be done is the same thread as drawing
  if(m_window.init())
  {
    requestQuit();
    return;
  }

  while(m_isRunning)
  {
    SDL_Event event;
    Uint32 loopDuration;

    lastTick = SDL_GetTicks();

    // Get SDL event
    while(SDL_PollEvent(&event) != 0 )
    {
      switch(event.type)
      {
      case SDL_QUIT:
        requestQuit();
        break;
      case SDL_MOUSEBUTTONDOWN:
        if(event.button.button == SDL_BUTTON_LEFT)
          leftMouseButtonEvent();
        break;
      default:
        break;
      }
    }

    m_window.draw();

    // Limit FPS
    loopDuration = SDL_GetTicks() - lastTick;
    if(loopDuration < FRAME_DURATION)
      SDL_Delay(FRAME_DURATION - loopDuration);
  }

  LOGDB() << "SDL thread exited";
}

//------------------------------------------------------------------------------
int SdlLoop::startSdlThread()
{
  rglib::IThread::create(m_sdlThread);
  if(!m_sdlThread)
  {
    LOGER() << "Cannot create SDL thread!";
    return -1;
  }

  m_isRunning = true;
  if(m_sdlThread->start(*this))
  {
    LOGER() << "Cannot start SDL thread";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
void SdlLoop::requestQuit()
{
  LOGDB() << "Got SDL quit event";

  if(m_mainLoopMonitor.sendQuit())
    LOGER() << "Cannot send SDL quit event to main loop";
}

//------------------------------------------------------------------------------
void SdlLoop::leftMouseButtonEvent()
{
  int x;
  int y;

  SDL_GetMouseState(&x, &y);
  m_window.onLeftMouseButton(x, y);
}

}       // namespace
