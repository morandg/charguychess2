/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <getopt.h>
#include <signal.h>

#include <iostream>

#include <rglib/logging/LogMacros.hpp>
#include <rglib/event-loop/IEventLoop.hpp>

#include "../src/QuitHandler.hpp"
#include "../src/Version.hpp"

#include "Simulator.hpp"
#include "CharGuyChessDriver.hpp"

/* Constants */
static const rglib::ILogSink::LogLevel DEFAULT_LOG_LEVEL =
    rglib::ILogSink::INFO;

/* locals */
rglib::ILogSink::LogLevel maxLogLevel = DEFAULT_LOG_LEVEL;
std::unique_ptr<charguychess::QuitHandler> quitHandler;

//------------------------------------------------------------------------------
void showHelp(const char* progName)
{
  std::cout << progName << std::endl;
  std::cout << std::endl;
  std::cout << "-h|--help           Show this help" << std::endl;
  std::cout << "-v|--version        Show version" << std::endl;
  std::cout << "-l|--loglevel       Max log level " <<
      "(" <<(int)DEFAULT_LOG_LEVEL << ") " << std::endl <<
      "                      debug:" << (int)rglib::ILogSink::DEBUG <<
      std::endl <<
      "                      error:" << (int)rglib::ILogSink::ERROR <<
      std::endl;
}

//------------------------------------------------------------------------------
void showVersion()
{
  std::cout << charguychess::VERSION_MAJOR << "." <<
      charguychess::VERSION_MINOR << "." <<
      charguychess::VERSION_BUGFIX <<
      " (" << charguychess::VERSION_GIT << ")" << std::endl;
}

//------------------------------------------------------------------------------
int parseArgs(int argc, char* argv[])
{
  int opt;
  static struct option long_options[] = {
      {"help",          no_argument,        0, 'h'},
      {"version",       no_argument,        0, 'v'},
      {"loglevel",      required_argument,  0, 'l'},
      {0,               0,                  0, 0}
  };

  while ((opt = getopt_long(argc, argv, "hvl:", long_options, 0)) != -1)
  {
    switch (opt)
    {
    case 'h':
      showHelp(argv[0]);
      return 1;
    case 'v':
      showVersion();
      return 1;
    case 'l':
      maxLogLevel = (rglib::ILogSink::LogLevel)atoi(optarg);
      break;
    default:
      showHelp(argv[0]);
      return -1;
    }
  }

  return 0;
}

//------------------------------------------------------------------------------
int initEventLoop(std::unique_ptr<rglib::IEventLoop>& el)
{
  rglib::IEventLoop::create(el);
  if(!el)
  {
    LOGER() << "Could not create event loop!";
    return -1;
  }

  if(el->init())
  {
    LOGER() << "Could not initialize event loop";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int initLogging()
{
  rglib::LoggerInstance::getInstance().consoleLogger();
  rglib::LoggerInstance::getInstance().setMaxLevel(maxLogLevel);

  return 0;
}

//------------------------------------------------------------------------------
int initQuitHandler(rglib::IEventLoop& el)
{
  quitHandler.reset(new charguychess::QuitHandler(el));

  el.addHandledSignal(*quitHandler, SIGINT);
  el.addHandledSignal(*quitHandler, SIGTERM);

  return 0;
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  int ret;
  std::unique_ptr<rglib::IEventLoop> el;
  std::unique_ptr<cgc_simulator::Simulator> simulator;

  ret = parseArgs(argc, argv);
  if(ret < 0)
    return ret;
  else if(ret > 0)
    return 0;

  if(initLogging())
    return -1;
  if(initEventLoop(el))
    return -1;
  if(initQuitHandler(*el))
    return -1;
  simulator.reset(new cgc_simulator::Simulator(*el));
  if(simulator->init())
    return -1;

  ret = el->run();

  LOGIN() << "Exiting simulator with code " << ret;

  return ret;
}
