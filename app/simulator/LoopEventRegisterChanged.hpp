/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGCSIM_LOOP_EVENT_REGISTER_CHANGED_HPP_
#define _CGCSIM_LOOP_EVENT_REGISTER_CHANGED_HPP_

#include "ILoopEvent.hpp"
#include "CharGuyChessDriver.hpp"

namespace cgc_simulator {

class LoopEventRegisterChanged:
    public ILoopEvent
{
public:
  LoopEventRegisterChanged(
      CharGuyChessDriver& driver, uint8_t reg, uint8_t value);
  virtual ~LoopEventRegisterChanged();

  virtual void execute() override;

private:
  CharGuyChessDriver&  m_driver;
  uint8_t m_reg;
  uint8_t m_value;
};

}       // namespace
#endif  // _CGCSIM_LOOP_EVENT_REGISTER_CHANGED_HPP_
