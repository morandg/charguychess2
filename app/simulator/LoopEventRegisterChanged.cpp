/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LoopEventRegisterChanged.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
LoopEventRegisterChanged::LoopEventRegisterChanged(
      CharGuyChessDriver& driver, uint8_t reg, uint8_t value):
  m_driver(driver),
  m_reg(reg),
  m_value(value)
{
}

//------------------------------------------------------------------------------
LoopEventRegisterChanged::~LoopEventRegisterChanged()
{
}

//------------------------------------------------------------------------------
void LoopEventRegisterChanged::execute()
{
  m_driver.writeRegister(m_reg, m_value);
}

}       // namespace
