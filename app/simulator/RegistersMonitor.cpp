/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include <cgc-protocol/registers.h>

#include "RegistersMonitor.hpp"

namespace cgc_simulator {

//------------------------------------------------------------------------------
RegistersMonitor::RegistersMonitor()
{
}

//------------------------------------------------------------------------------
RegistersMonitor::~RegistersMonitor()
{
}

//------------------------------------------------------------------------------
int RegistersMonitor::init()
{
  rglib::IMutex::create(m_mutex);

  if(m_mutex->init())
  {
    LOGER() << "Canont initialize registers mutex";
    return -1;
  }

  return cgc_registers_init();
}

//------------------------------------------------------------------------------
int RegistersMonitor::read(uint8_t reg, uint8_t& value)
{
  int ret;

  assert(m_mutex);

  if(m_mutex->get())
  {
    assert(0);
    return -1;
  }

  ret = cgc_registers_read(reg, &value);

  if(m_mutex->release())
  {
    assert(0);
    return -1;
  }

  return ret;
}

//------------------------------------------------------------------------------
int RegistersMonitor::write(uint8_t reg, uint8_t value)
{
  int ret;

  assert(m_mutex);

  if(m_mutex->get())
  {
    assert(0);
    return -1;
  }

  ret = cgc_registers_write(reg, value);

  if(m_mutex->release())
  {
    assert(0);
    return -1;
  }

  return ret;
}

}       // namespace
