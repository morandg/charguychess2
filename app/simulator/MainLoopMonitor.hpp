/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGCSIM_MAIN_LOOP_MONITOR_HPP_
#define _CGCSIM_MAIN_LOOP_MONITOR_HPP_

#include <rglib/event-loop/IEventLoop.hpp>
#include <rglib/ipc/IPipe.hpp>

#include "PendingEvents.hpp"
#include "CharGuyChessDriver.hpp"

namespace cgc_simulator {

class MainLoopMonitor:
    rglib::IHandledIo
{
public:
  MainLoopMonitor(rglib::IEventLoop& mainLoop, CharGuyChessDriver& driver);
  virtual ~MainLoopMonitor();

  int init();
  int sendQuit();
  int sendRegisterEvent(uint8_t reg, uint8_t value);

  // IHandledIo
  virtual rglib::FileDescriptor getFileDescriptor() override;
  virtual void onReadReady() override;

private:
  rglib::IEventLoop& m_mainLoop;
  CharGuyChessDriver& m_driver;
  PendingEvents m_pendingEvents;
  std::unique_ptr<rglib::IPipe> m_syncPipe;

  int wakeupMainThread();
  int ackMainThreadEvent();
};

}       // namespace
#endif  // _CGCSIM_MAIN_LOOP_MONITOR_HPP_
