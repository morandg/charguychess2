/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGCSIM_PENDING_EVENTS_HPP_
#define _CGCSIM_PENDING_EVENTS_HPP_

#include <memory>
#include <list>

#include <rglib/threads/IMutex.hpp>

#include "ILoopEvent.hpp"

namespace cgc_simulator {

class PendingEvents
{
public:
  PendingEvents();
  virtual ~PendingEvents();

  int init();
  int add(std::unique_ptr<ILoopEvent>& nextEvent);
  int next(std::unique_ptr<ILoopEvent>& nextEvent);

private:
  std::list<std::unique_ptr<ILoopEvent>> m_pendingEvents;
  std::unique_ptr<rglib::IMutex> m_eventsMutex;

  int getMutex();
  int releaseMutex();
};

}       // namespace
#endif  // _CGCSIM_PENDING_EVENTS_HPP_
