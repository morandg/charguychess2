/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/PiecesBitboards.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PiecesBitboards)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPiecesValue)
{
  PiecesBitboards pBb(Color::BLACK);

  CHECK_EQUAL((BitBoardValue)0xFFFF000000000000, pBb.pieces().getValue());
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackKingOnE8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::E, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::KING, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackQueenOnD8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::D, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::QUEEN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackBishopOnF8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::F, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::BISHOP, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackBishopOnC8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::C, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::BISHOP, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackKnightOnG8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::G, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::KNIGHT, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackKnightOnB8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::B, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::KNIGHT, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackRookOnH8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::H, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::ROOK, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, BlackRookOnA8)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::A, Rank::EIGHT), p));
  CHECK_EQUAL((int)Piece::ROOK, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnH7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::H, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnG7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::G, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnF7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::F, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnE7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::E, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnD7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::D, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnC7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::C, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnB7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::B, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, blackPawnOnA7)
{
  Piece p;
  PiecesBitboards pBb(Color::BLACK);

  CHECK(pBb.pieceAt(Square(File::A, Rank::SEVEN), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePiecesValue)
{
  PiecesBitboards pBb(Color::WHITE);

  CHECK_EQUAL((BitBoardValue)0x000000000000FFFF, pBb.pieces().getValue());
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, pieceNotFoundOnEmptySquare)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK_FALSE(pBb.pieceAt(Square(File::A, Rank::EIGHT), p));
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteKingOnE1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::E, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::KING, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteQueenOnD1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::D, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::QUEEN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteBishopOnF1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::F, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::BISHOP, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteBishopOnC1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::C, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::BISHOP, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteKnightOnG1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::G, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::KNIGHT, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteKnightOnB1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::B, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::KNIGHT, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteRookOnH1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::H, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::ROOK, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whiteRookOnA1)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::A, Rank::ONE), p));
  CHECK_EQUAL((int)Piece::ROOK, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnH2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::H, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnG2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::G, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnF2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::F, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnE2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::E, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnD2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::D, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnC2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::C, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnB2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::B, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, whitePawnOnA2)
{
  Piece p;
  PiecesBitboards pBb(Color::WHITE);

  CHECK(pBb.pieceAt(Square(File::A, Rank::TWO), p));
  CHECK_EQUAL((int)Piece::PAWN, (int)p);
}

//------------------------------------------------------------------------------
TEST(PiecesBitboards, constructor)
{
  PiecesBitboards pBb(Color::WHITE);
}
