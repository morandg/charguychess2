/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_TESTS_CHESS_ENGINE_MOCK_HPP_
#define _CGC_TESTS_CHESS_ENGINE_MOCK_HPP_

#include "../../src/chess/engine/IChessEngine.hpp"

namespace charguychess {
namespace tests {

class ChessEngineMock:
    public IChessEngine
{
public:
  ChessEngineMock();
  virtual ~ChessEngineMock();

  // IChessEngine
  virtual int init() override;
  virtual int start() override;
  virtual void addObserver(IChessEngineObserver& observer) override;
  virtual void removeObserver(IChessEngineObserver& observer) override;
  virtual bool isRunning() const override;
  virtual bool isReady() const override;
  virtual const std::string& getName() const override;
  virtual const std::string& getAuthor() const override;
  virtual int searchDepth(unsigned int depth, const IChessGame& game) override;
  virtual int searchTime(unsigned int ms, const IChessGame& game) override;
  virtual int stopSearch() override;
  virtual const MoveInfo& lastFoundMove() const override;
};

}}      // namespace
#endif  // _CGC_TESTS_CHESS_ENGINE_MOCK_HPP_
