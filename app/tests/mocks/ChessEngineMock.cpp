/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessEngineMock.hpp"

#include <CppUTestExt/MockSupport.h>

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
ChessEngineMock::ChessEngineMock()
{
}

//------------------------------------------------------------------------------
ChessEngineMock::~ChessEngineMock()
{
}

//------------------------------------------------------------------------------
int ChessEngineMock::init()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessEngineMock::start()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
void ChessEngineMock::addObserver(IChessEngineObserver& observer)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("observer", &observer);
}

//------------------------------------------------------------------------------
void ChessEngineMock::removeObserver(IChessEngineObserver& observer)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("observer", &observer);
}

//------------------------------------------------------------------------------
bool ChessEngineMock::isRunning() const
{
  return mock().actualCall(__func__).onObject(this).
      returnBoolValue();
}

//------------------------------------------------------------------------------
bool ChessEngineMock::isReady() const
{
  return mock().actualCall(__func__).onObject(this).
      returnBoolValue();
}

//------------------------------------------------------------------------------
const std::string& ChessEngineMock::getName() const
{
  return *((std::string*)mock().actualCall(__func__).onObject(this).
      returnPointerValue());
}

//------------------------------------------------------------------------------
const std::string& ChessEngineMock::getAuthor() const
{
  return *((std::string*)mock().actualCall(__func__).onObject(this).
      returnPointerValue());
}

//------------------------------------------------------------------------------
int ChessEngineMock::searchDepth(unsigned int depth, const IChessGame& game)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("depth", depth).
      withParameter("game", &game).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessEngineMock::searchTime(unsigned int ms, const IChessGame& game)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("ms", ms).
      withParameter("game", &game).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessEngineMock::stopSearch()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
const MoveInfo& ChessEngineMock::lastFoundMove() const
{
  return *((const MoveInfo*)mock().actualCall(__func__).onObject(this).
      returnPointerValue());
}

}}      // namespace
