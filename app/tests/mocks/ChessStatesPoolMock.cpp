/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessStatesPoolMock.hpp"

#include <CppUTestExt/MockSupport.h>

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
ChessStatesPoolMock::ChessStatesPoolMock()
{
}

//------------------------------------------------------------------------------
ChessStatesPoolMock::~ChessStatesPoolMock()
{
}

//------------------------------------------------------------------------------
int ChessStatesPoolMock::init()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
void ChessStatesPoolMock::addState(
      State stateType, std::unique_ptr<IChessState>& state)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("stateType", (int)stateType).
      withParameter("state", state.get());
}

//------------------------------------------------------------------------------
void ChessStatesPoolMock::changeState(State state)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("state", (int)state);
}

}}      // namespace
