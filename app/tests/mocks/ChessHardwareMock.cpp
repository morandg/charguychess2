/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTestExt/MockSupport.h>

#include "ChessHardwareMock.hpp"

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
ChessHardwareMock::ChessHardwareMock()
{
}

//------------------------------------------------------------------------------
ChessHardwareMock::~ChessHardwareMock()
{
}

//------------------------------------------------------------------------------
int ChessHardwareMock::init()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessHardwareMock::turnRankLed(bool isOn, Rank rank)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("isOn", isOn).
      withParameter("rank", (int)rank).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessHardwareMock::turnFileLed(bool isOn, File file)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("isOn", isOn).
      withParameter("file", (int)file).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessHardwareMock::turnSquareLed(bool isOn, const Square& square)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("isOn", isOn).
      withParameterOfType("Square", "square", &square).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessHardwareMock::turnLed(bool isOn, Led led)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("isOn", isOn).
      withParameter("led", (int)led).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessHardwareMock::turnAllLeds(bool isOn)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("isOn", isOn).
      returnIntValue();
}

//------------------------------------------------------------------------------
int ChessHardwareMock::turnPlayerLed(bool isOn, Color color)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("isOn", isOn).
      withParameter("color", (int)color).
      returnIntValue();
}

//------------------------------------------------------------------------------
void ChessHardwareMock::addObserver(IChessHardwareObserver& observer)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("observer", &observer);
}

//------------------------------------------------------------------------------
void ChessHardwareMock::removeObserver(IChessHardwareObserver& observer)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("observer", &observer);
}

//------------------------------------------------------------------------------
BitBoard ChessHardwareMock::getPieces()
{
  return *((BitBoard*)mock().actualCall(__func__).onObject(this).
      returnPointerValue());
}

}}      // namespace
