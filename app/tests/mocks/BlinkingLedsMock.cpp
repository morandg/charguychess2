  /**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BlinkingLedsMock.hpp"

#include <CppUTestExt/MockSupport.h>

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
BlinkingLedsMock::BlinkingLedsMock()
{
}

//------------------------------------------------------------------------------
BlinkingLedsMock::~BlinkingLedsMock()
{
}

//------------------------------------------------------------------------------
void BlinkingLedsMock::start()
{
  mock().actualCall(__func__).onObject(this);
}

//------------------------------------------------------------------------------
void BlinkingLedsMock::setOnTimeout(unsigned int ms)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("ms", ms);
}

//------------------------------------------------------------------------------
void BlinkingLedsMock::setOffTimeout(unsigned int ms)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("ms", ms);
}

//------------------------------------------------------------------------------
void BlinkingLedsMock::addLed(Led led)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("led", (int)led);
}

//------------------------------------------------------------------------------
void BlinkingLedsMock::stop()
{
  mock().actualCall(__func__).onObject(this);
}

}}      // namespace
