/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_TESTS_CHESS_HARDWARE_MOCK_HPP_
#define _CGC_TESTS_CHESS_HARDWARE_MOCK_HPP_

#include "../../src/hardware/IChessHardware.hpp"

namespace charguychess {
namespace tests {

class ChessHardwareMock:
    public IChessHardware
{
public:
  ChessHardwareMock();
  virtual ~ChessHardwareMock();

  virtual int init() override;
  virtual int turnRankLed(bool isOn, Rank rank) override;
  virtual int turnFileLed(bool isOn, File file) override;
  virtual int turnSquareLed(bool isOn, const Square& square) override;
  virtual int turnLed(bool isOn, Led led) override;
  virtual int turnAllLeds(bool isOn) override;
  virtual int turnPlayerLed(bool isOn, Color color) override;
  virtual void addObserver(IChessHardwareObserver& observer) override;
  virtual void removeObserver(IChessHardwareObserver& observer) override;
  virtual BitBoard getPieces() override;
};

}}      // namespace
#endif  // _CGC_TESTS_CHESS_HARDWARE_MOCK_HPP_
