/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "../src/chess/History.hpp"

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(History)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(History, setMateBlackMove)
{
  Move move1(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move move2(Square(File::E, Rank::SEVEN), Square(File::E, Rank::FIVE));
  Turn lastTurn;
  History history;

  history.addMove(move1);
  history.addMove(move2);
  history.setMate();

  lastTurn = history.lastTurn();

  CHECK_FALSE(lastTurn.whiteMove().isMate());
  CHECK(lastTurn.blackMove().isMate());
}

//------------------------------------------------------------------------------
TEST(History, setMateWhiteMove)
{
  Move move(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Turn lastTurn;
  History history;

  history.addMove(move);
  history.setMate();

  lastTurn = history.lastTurn();

  CHECK(lastTurn.whiteMove().isMate());
}

//------------------------------------------------------------------------------
TEST(History, setMateNoMoveInHistory)
{
  Turn lastTurn;
  History history;

  history.setMate();

  lastTurn = history.lastTurn();

  CHECK_FALSE(lastTurn.whiteMove().isValid());
  CHECK_FALSE(lastTurn.blackMove().isValid());
}

//------------------------------------------------------------------------------
TEST(History, setCheckBlackMove)
{
  Move move1(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move move2(Square(File::E, Rank::SEVEN), Square(File::E, Rank::FIVE));
  Turn lastTurn;
  History history;

  history.addMove(move1);
  history.addMove(move2);
  history.setCheck();

  lastTurn = history.lastTurn();

  CHECK_FALSE(lastTurn.whiteMove().isCheck());
  CHECK(lastTurn.blackMove().isCheck());
}

//------------------------------------------------------------------------------
TEST(History, setCheckWhiteMove)
{
  Move move(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Turn lastTurn;
  History history;

  history.addMove(move);
  history.setCheck();

  lastTurn = history.lastTurn();

  CHECK(lastTurn.whiteMove().isCheck());
}

//------------------------------------------------------------------------------
TEST(History, setCheckNoMoveInHistory)
{
  Turn lastTurn;
  History history;

  history.setCheck();

  lastTurn = history.lastTurn();

  CHECK_FALSE(lastTurn.whiteMove().isValid());
  CHECK_FALSE(lastTurn.blackMove().isValid());
}

//------------------------------------------------------------------------------
TEST(History, lastMoveSecondWhiteMove)
{
  Move move1(Square(File::E, Rank::ONE), Square(File::E, Rank::FOUR));
  Move move2(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  Move move3(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  Move lastMove;
  History history;

  history.addMove(move1);
  history.addMove(move2);
  history.addMove(move3);
  lastMove = history.lastMove();

  CHECK(lastMove.isValid());
  CHECK(lastMove == move3);
}

//------------------------------------------------------------------------------
TEST(History, lastMoveFirsBlackMove)
{
  Move move1(Square(File::E, Rank::ONE), Square(File::E, Rank::FOUR));
  Move move2(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  Move lastMove;
  History history;

  history.addMove(move1);
  history.addMove(move2);
  lastMove = history.lastMove();

  CHECK(lastMove.isValid());
  CHECK(lastMove == move2);
}

//------------------------------------------------------------------------------
TEST(History, lastMoveFirstWhiteMove)
{
  Move move1(Square(File::E, Rank::ONE), Square(File::E, Rank::FOUR));
  Move lastMove;
  History history;

  history.addMove(move1);
  lastMove = history.lastMove();

  CHECK(lastMove.isValid());
  CHECK(lastMove == move1);
}

//------------------------------------------------------------------------------
TEST(History, lastMoveEmptyHistory)
{
  Move lastMove;
  History history;

  lastMove = history.lastMove();

  CHECK_FALSE(lastMove.isValid());
}

//------------------------------------------------------------------------------
TEST(History, pgnString)
{
  Move move1(Square(File::E, Rank::ONE), Square(File::E, Rank::FOUR));
  Move move2(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  Move move3(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  std::string pgnHistory;
  History history;

  move3.setCapture(true);
  history.addMove(move1);
  history.addMove(move2);
  history.addMove(move3);

  pgnHistory = history.pgnString();

  STRCMP_EQUAL("1. e4 d5 2. exd5", pgnHistory.c_str());
}

//------------------------------------------------------------------------------
TEST(History, lastTurnWhitMoveDone)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  Turn lastTurn;
  History history;

  history.addMove(move);

  lastTurn = history.lastTurn();

  CHECK(move == lastTurn.whiteMove());
  CHECK_FALSE(lastTurn.blackMove().isValid());
}

//------------------------------------------------------------------------------
TEST(History, lastTurnNoMoveDone)
{
  Turn lastTurn;
  History history;

  lastTurn = history.lastTurn();

  CHECK_FALSE(lastTurn.whiteMove().isValid());
  CHECK_FALSE(lastTurn.blackMove().isValid());
}

//------------------------------------------------------------------------------
TEST(History, uciStringWithMoves)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  std::string expectedHistory = "position startpos moves a1a2";
  std::string historyUci;
  History history;

  history.addMove(move);
  historyUci = history.uciString();

  STRCMP_EQUAL(expectedHistory.c_str(), historyUci.c_str());
}

//------------------------------------------------------------------------------
TEST(History, uciStringEmptyHistory)
{
  std::string expectedHistory = "position startpos";
  std::string historyUci;
  History history;

  historyUci = history.uciString();

  STRCMP_EQUAL(expectedHistory.c_str(), historyUci.c_str());
}

//------------------------------------------------------------------------------
TEST(History, currentPlayerFirstBlackMove)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  History history;

  history.addMove(move);
  history.addMove(move);

  CHECK(Color::WHITE == history.currentPlayer());
}

//------------------------------------------------------------------------------
TEST(History, currentPlayerFirstWhiteMove)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  History history;

  history.addMove(move);

  CHECK(Color::BLACK == history.currentPlayer());
}

//------------------------------------------------------------------------------
TEST(History, currentPlayerEmptyHistory)
{
  History history;

  CHECK(Color::WHITE == history.currentPlayer());
}

//------------------------------------------------------------------------------
TEST(History, turnNumberAfterBlackTurnDone)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  History history;

  history.addMove(move);
  history.addMove(move);

  CHECK_EQUAL(2, history.turnNumber());
}

//------------------------------------------------------------------------------
TEST(History, turnNumberAfterfirstWhiteMove)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  History history;

  history.addMove(move);

  CHECK_EQUAL(1, history.turnNumber());
}

//------------------------------------------------------------------------------
TEST(History, turnNumberEmpty)
{
  History history;

  CHECK_EQUAL(1, history.turnNumber());
}

//------------------------------------------------------------------------------
TEST(History, clear)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  History history;

  history.addMove(move);
  history.addMove(move);
  history.addMove(move);
  history.clear();

  CHECK_FALSE(history.lastTurn().whiteMove().isValid());
}

//------------------------------------------------------------------------------
TEST(History, addMoveWithSecondTurn)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  Move lastMove(Square(File::B, Rank::ONE), Square(File::C, Rank::TWO));
  History history;

  history.addMove(move);
  history.addMove(move);
  history.addMove(lastMove);

  CHECK_EQUAL(2, history.turnNumber());
  CHECK(history.lastTurn().whiteMove() == lastMove);
}

//------------------------------------------------------------------------------
TEST(History, addFirstBlackMove)
{
  Move move1(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  Move move2(Square(File::B, Rank::THREE), Square(File::C, Rank::FIVE));
  History history;

  history.addMove(move1);
  history.addMove(move2);

  CHECK_EQUAL(2, history.turnNumber());
  CHECK(history.lastTurn().blackMove() == move2);
}

//------------------------------------------------------------------------------
TEST(History, addFirstWhiteMoveAddedInTurnsList)
{
  Move move(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  History history;

  history.addMove(move);

  CHECK_EQUAL(1, history.turnNumber())
  CHECK(history.lastTurn().whiteMove() == move);
}

//------------------------------------------------------------------------------
TEST(History, constructor)
{
  History history;
}
