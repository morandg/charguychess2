/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../src/chess/BitBoard.hpp"
#include "../../src/telnet/ChessObjectsSerializer.hpp"
#include "BitBoardComparator.hpp"

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
BitBoardComparator::BitBoardComparator()
{
}

//------------------------------------------------------------------------------
BitBoardComparator::~BitBoardComparator()
{
}

//------------------------------------------------------------------------------
bool BitBoardComparator::isEqual(const void* object1, const void* object2)
{
  BitBoard* bb1 = (BitBoard*)object1;
  BitBoard* bb2 = (BitBoard*)object2;

  return *bb1 == *bb2;
}

//------------------------------------------------------------------------------
SimpleString BitBoardComparator::valueToString(const void* object)
{
  const BitBoard* bb = static_cast<const BitBoard*>(object);

  return (std::string("\n") + ChessObjectsSerializer::toString(*bb)).c_str();
}

}}      // namespace

//------------------------------------------------------------------------------
SimpleString StringFrom(charguychess::BitBoard& bb)
{
  charguychess::tests::BitBoardComparator bbComp;

  return bbComp.valueToString(&bb);
}

