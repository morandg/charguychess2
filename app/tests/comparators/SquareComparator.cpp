/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../src/chess/Square.hpp"
#include "SquareComparator.hpp"

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
SquareComparator::SquareComparator()
{
}

//------------------------------------------------------------------------------
SquareComparator::~SquareComparator()
{
}

//------------------------------------------------------------------------------
bool SquareComparator::isEqual(const void* object1, const void* object2)
{
  const Square* s1 = static_cast<const Square*>(object1);
  const Square* s2 = static_cast<const Square*>(object2);

  return *s1 == *s2;
}

//------------------------------------------------------------------------------
SimpleString SquareComparator::valueToString(const void* object)
{
  const Square* s = static_cast<const Square*>(object);

  return s->toString().c_str();
}

}}      // namespace
