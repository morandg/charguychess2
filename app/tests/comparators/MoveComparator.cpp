/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../src/chess/Move.hpp"

#include "MoveComparator.hpp"

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
MoveComparator::MoveComparator()
{
}

//------------------------------------------------------------------------------
MoveComparator::~MoveComparator()
{
}

//------------------------------------------------------------------------------
bool MoveComparator::isEqual(const void* object1, const void* object2)
{
  Move* m1 = (Move*)object1;
  Move* m2 = (Move*)object2;

  return m1->from().file() == m2->from().file() &&
      m1->from().rank() == m2->from().rank() &&
      m1->to().file() == m2->to().file() &&
      m1->to().rank() == m2->to().rank();
}

//------------------------------------------------------------------------------
SimpleString MoveComparator::valueToString(const void* object)
{
  (void)object;
  return "TODO: Move to string";
}

}}      // namespace
