/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/Square.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(Square)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Square, fromValidSquareStringTooLong)
{
  Square s;

  CHECK_FALSE(s.fromString("a8aaa"));
}

//------------------------------------------------------------------------------
TEST(Square, froma8String)
{
  Square s;

  CHECK(s.fromString("a8"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::EIGHT);
}

//------------------------------------------------------------------------------
TEST(Square, froma7String)
{
  Square s;

  CHECK(s.fromString("a7"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::SEVEN);
}

//------------------------------------------------------------------------------
TEST(Square, froma6String)
{
  Square s;

  CHECK(s.fromString("a6"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::SIX);
}

//------------------------------------------------------------------------------
TEST(Square, froma5String)
{
  Square s;

  CHECK(s.fromString("a5"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::FIVE);
}

//------------------------------------------------------------------------------
TEST(Square, froma4String)
{
  Square s;

  CHECK(s.fromString("a4"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::FOUR);
}

//------------------------------------------------------------------------------
TEST(Square, froma3String)
{
  Square s;

  CHECK(s.fromString("a3"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::THREE);
}

//------------------------------------------------------------------------------
TEST(Square, froma2String)
{
  Square s;

  CHECK(s.fromString("a2"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::TWO);
}

//------------------------------------------------------------------------------
TEST(Square, fromh1String)
{
  Square s;

  CHECK(s.fromString("h1"));
  CHECK(s.file() == File::H);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, fromg1String)
{
  Square s;

  CHECK(s.fromString("g1"));
  CHECK(s.file() == File::G);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, fromf1String)
{
  Square s;

  CHECK(s.fromString("f1"));
  CHECK(s.file() == File::F);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, frome1String)
{
  Square s;

  CHECK(s.fromString("e1"));
  CHECK(s.file() == File::E);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, fromd1String)
{
  Square s;

  CHECK(s.fromString("d1"));
  CHECK(s.file() == File::D);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, fromc1String)
{
  Square s;

  CHECK(s.fromString("c1"));
  CHECK(s.file() == File::C);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, fromb1String)
{
  Square s;

  CHECK(s.fromString("b1"));
  CHECK(s.file() == File::B);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, froma1StringUpperCase)
{
  Square s;

  CHECK(s.fromString("A1"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, froma1String)
{
  Square s;

  CHECK(s.fromString("a1"));
  CHECK(s.file() == File::A);
  CHECK(s.rank() == Rank::ONE);
}

//------------------------------------------------------------------------------
TEST(Square, fromEmptyString)
{
  Square s;

  CHECK_FALSE(s.fromString(""));
}

//------------------------------------------------------------------------------
TEST(Square, constructor)
{
  Square s;
}
