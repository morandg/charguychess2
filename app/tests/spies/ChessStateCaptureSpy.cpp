/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessStateCaptureSpy.hpp"

namespace charguychess {
namespace tests {

//------------------------------------------------------------------------------
ChessStateCaptureSpy::ChessStateCaptureSpy(
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame):
  ChessStateCapture(statesPool, hardware, chessGame)
{
}

//------------------------------------------------------------------------------
ChessStateCaptureSpy::~ChessStateCaptureSpy()
{
}

//------------------------------------------------------------------------------
const Square& ChessStateCaptureSpy::getDestination() const
{
  return m_destination;
}

//------------------------------------------------------------------------------
void ChessStateCaptureSpy::setDestination(const Square& dest)
{
  m_destination = dest;
}

//------------------------------------------------------------------------------
Piece ChessStateCaptureSpy::promotion() const
{
  return m_promotion.promotion();
}

//------------------------------------------------------------------------------
void ChessStateCaptureSpy::setPromotion(Piece piece)
{
  m_promotion = PromotionInformation(piece);
}

}}      // namespace
