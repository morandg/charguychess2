/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>

#include <CppUTest/TestHarness.h>

#include "../src/chess/BitBoard.hpp"

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(BitBoard)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(BitBoard, differeBitBoardNotEqual)
{
  BitBoard bb1(123);
  BitBoard bb2(321);

  CHECK(bb1 != bb2);
}

//------------------------------------------------------------------------------
TEST(BitBoard, sameBitBoardNotEqual)
{
  BitBoard bb1(0xF01);
  BitBoard bb2 = bb1;

  CHECK_FALSE(bb1 != bb2);
}

//------------------------------------------------------------------------------
TEST(BitBoard, xorOperator)
{
  BitBoard bb1(0xF01);
  BitBoard bb2(0xF23);

  CHECK_EQUAL(0x22, (bb1 ^ bb2).getValue());
}

//------------------------------------------------------------------------------
TEST(BitBoard, hasOneOccupiedSquareWhenOneAndLastBitSet)
{
  BitBoard bb;

  bb.set(3);
  bb.set(63);

  CHECK_EQUAL(2, bb.activeBits())
}

//------------------------------------------------------------------------------
TEST(BitBoard, hasOneOccupiedSquareWhenOneBitSet)
{
  BitBoard bb;

  bb.set(3);

  CHECK_EQUAL(1, bb.activeBits())
}

//------------------------------------------------------------------------------
TEST(BitBoard, emptyBitBoardHasZeroOccupiedSquare)
{
  BitBoard bb;

  CHECK_EQUAL(0, bb.activeBits())
}

//------------------------------------------------------------------------------
TEST(BitBoard, firstAndLastBitHasOccupiedSquare)
{
  SquaresList occupiedSquares;
  Square h1(File::H, Rank::ONE);
  Square a8(File::A, Rank::EIGHT);
  BitBoard bb;

  bb.set(0);
  bb.set(63);

  occupiedSquares = bb.activeSquares();

  CHECK(occupiedSquares.contains(h1));
  CHECK(occupiedSquares.contains(a8));
}

//------------------------------------------------------------------------------
TEST(BitBoard, firstBitHasOccupiedSquare)
{
  SquaresList occupiedSquares;
  Square h1(File::H, Rank::ONE);
  BitBoard bb;

  bb.set(0);

  occupiedSquares = bb.activeSquares();

  CHECK(occupiedSquares.contains(h1));
}

//------------------------------------------------------------------------------
TEST(BitBoard, emptyBitBoardHasEmptyOccupiedSquare)
{
  SquaresList occupiedSquares;
  Square square;
  BitBoard bb;

  occupiedSquares = bb.activeSquares();

  CHECK_FALSE(occupiedSquares.at(0, square));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toSquareBit18)
{
  CHECK(Square(File::F, Rank::THREE) == BitBoard::toSquare(18));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit63)
{
  CHECK(Rank::EIGHT == BitBoard::toRank(63));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit55)
{
  CHECK(Rank::SEVEN == BitBoard::toRank(55));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit47)
{
  CHECK(Rank::SIX == BitBoard::toRank(47));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit39)
{
  CHECK(Rank::FIVE == BitBoard::toRank(39));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit31)
{
  CHECK(Rank::FOUR == BitBoard::toRank(31));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit23)
{
  CHECK(Rank::THREE == BitBoard::toRank(23));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit15)
{
  CHECK(Rank::TWO == BitBoard::toRank(15));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toRankBit7)
{
  CHECK(Rank::ONE == BitBoard::toRank(7));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toFileBit7)
{
  CHECK(File::A == BitBoard::toFile(7));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toFileBit0)
{
  CHECK(File::H == BitBoard::toFile(0));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toggleSquareNotSet)
{
  Square square(File::A, Rank::ONE);
  BitBoard bb;

  bb.toggle(square);
  CHECK(bb.isSet(square));
}

//------------------------------------------------------------------------------
TEST(BitBoard, toggleBitSet)
{
  BitBoard bb(1);

  bb.toggle(0);
  CHECK_EQUAL(0, bb.getValue());
}

//------------------------------------------------------------------------------
TEST(BitBoard, toggleBitNotSet)
{
  BitBoard bb;

  bb.toggle(0);
  CHECK_EQUAL(1, bb.getValue());
}

//------------------------------------------------------------------------------
TEST(BitBoard, differentBitboardsAreNotEqual)
{
  BitBoard bb1(123);
  BitBoard bb2(321);

  CHECK_FALSE(bb1 == bb2);
}

//------------------------------------------------------------------------------
TEST(BitBoard, sameBitboardsAreEqual)
{
  BitBoard bb1(123);
  BitBoard bb2(123);

  CHECK(bb1 == bb2);
}

//------------------------------------------------------------------------------
TEST(BitBoard, orOpeartor)
{
  BitBoard bb1(0x8000000000000000);
  BitBoard bb2(0x0000000000000001);
  BitBoard bb3 = bb1 | bb2;

  CHECK_EQUAL((BitBoardValue)0x8000000000000001, bb3.getValue());
}

//------------------------------------------------------------------------------
TEST(BitBoard, H8IsSetWithGivenValue)
{
  BitBoardValue value = 0x0100000000000000;
  BitBoard bb(value);

  CHECK(bb.isSet(Square(File::H, Rank::EIGHT)));
}

//------------------------------------------------------------------------------
TEST(BitBoard, A8IsSetWithGivenValue)
{
  BitBoardValue value = 0x8000000000000000;
  BitBoard bb(value);

  CHECK(bb.isSet(Square(File::A, Rank::EIGHT)));
}

//------------------------------------------------------------------------------
TEST(BitBoard, H1IsSetWithGivenValue)
{
  BitBoardValue value = 0x0000000000000001;
  BitBoard bb(value);

  CHECK(bb.isSet(Square(File::H, Rank::ONE)));
}

//------------------------------------------------------------------------------
TEST(BitBoard, A1IsSetWithGivenValue)
{
  BitBoardValue value = 0x0000000000000080;
  BitBoard bb(value);

  CHECK(bb.isSet(Square(File::A, Rank::ONE)));
}

//------------------------------------------------------------------------------
TEST(BitBoard, A1IsNotSetByDefault)
{
  BitBoard bb;

  CHECK_FALSE(bb.isSet(Square(File::A, Rank::ONE)));
}

//------------------------------------------------------------------------------
TEST(BitBoard, constructoreWithValue)
{
  BitBoardValue value = 9873;
  BitBoard bb(value);

  CHECK_EQUAL(value, bb.getValue());
}

//------------------------------------------------------------------------------
TEST(BitBoard, defaultValueisZero)
{
  BitBoard bb;

  CHECK_EQUAL(0, bb.getValue());
}

//------------------------------------------------------------------------------
TEST(BitBoard, constructor)
{
  BitBoard bb;
}
