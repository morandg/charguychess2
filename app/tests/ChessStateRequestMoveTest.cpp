/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/ChessStateRequestMove.hpp"
#include "../src/chess/ChessGame.hpp"

#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "mocks/ChessEngineMock.hpp"
#include "comparators/SquareComparator.hpp"
#include "comparators/MoveComparator.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateRequestMove)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
    mock().installComparator("Square", m_squareComp);
    mock().installComparator("Move", m_moveComp);
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateRequestMove>& state)
  {
    state.reset(
        new ChessStateRequestMove(
            m_chessStatesPool,
            m_chessHardwareMock,
            m_chessGame,
            m_chessEngine));
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_chessHardwareMock;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_chessStatesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

  ChessEngineMock& getChessEngine()
  {
    return m_chessEngine;
  }

private:
  ChessStatesPoolMock m_chessStatesPool;
  ChessHardwareMock m_chessHardwareMock;
  ChessGame m_chessGame;
  ChessEngineMock m_chessEngine;
  SquareComparator m_squareComp;
  MoveComparator m_moveComp;
};

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedCapturedPieceLifted)
{
  MoveInfo moveInfo;
  Square from(File::E, Rank::FOUR);
  Square to(File::D, Rank::FIVE);
  Move computedMove(from, to);
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move d7d5(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  BitBoard newStatus;
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  getChessGame().applyMove(e2e4);
  getChessGame().applyMove(d7d5);
  newStatus = getChessGame().allPieces();
  newStatus.clear(from);
  newStatus.clear(to);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::REQUEST_DROP_PIECE);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedBackToOriginalPosition)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::TWO);
  Square to(File::A, Rank::THREE);
  Move computedMove(from, to);
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", false).
      withParameterOfType("Square", "square", &to);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &from);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedDropped)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::TWO);
  Square to(File::A, Rank::THREE);
  Move computedMove(from, to);
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  newStatus.clear(from);
  newStatus.set(to);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();

  // Move was applied in game
  CHECK(Color::BLACK == getChessGame().currentPlayer());
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedDroppedWrongPlace)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::ONE);
  Square to(File::A, Rank::TWO);
  Move computedMove(from, to);
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  newStatus.clear(from);
  newStatus.set(Square(File::B, Rank::FIVE));

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedRightPieceLiftedWithPromotion)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::ONE);
  Square to(File::A, Rank::TWO);
  Piece promotion = Piece::ROOK;
  Move computedMove(from, to);
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  computedMove.setPromotion(promotion);
  moveInfo.setMove(computedMove);
  newStatus.clear(from);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", false).
      withParameterOfType("Square", "square", &from);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &to);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::ROOK);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedRightPieceLifted)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::ONE);
  Square to(File::A, Rank::TWO);
  Move computedMove(from, to);
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  newStatus.clear(from);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", false).
      withParameterOfType("Square", "square", &from);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &to);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, boardChangedInvalidPieceLifted)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::ONE);
  Move computedMove(from, Square(File::A, Rank::TWO));
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  newStatus.clear(Square(File::B, Rank::TWO));

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, exit)
{
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  // cppvtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, enter)
{
  MoveInfo moveInfo;
  Square from(File::A, Rank::ONE);
  Move computedMove(from, Square(File::A, Rank::TWO));
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  // cppvtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&computedMove);
  mock().expectOneCall("turnAllLeds").onObject(&getChessHardware()).
      withParameter("isOn", false);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::WHITE);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &from);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, init)
{
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);

  CHECK_EQUAL(0, state->init());
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestMove, constructor)
{
  std::unique_ptr<ChessStateRequestMove> state;

  buildState(state);
}
