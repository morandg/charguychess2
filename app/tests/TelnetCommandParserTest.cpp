/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>

#include "mocks/TelnetCommandMock.hpp"

#include "../src/telnet/ITelnetClient.hpp"
#include "../src/telnet/commands/TelnetCommandParser.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
class TelnetClientNull:
    public ITelnetClient
{
public:
  TelnetClientNull()
  {
  }

  ~TelnetClientNull()
  {
  }

  virtual void sendMessage(const std::string& message) override
  {
    (void)message;
  }

  virtual void disconnect() override
  {
  }

  virtual void onMovePlayed(const Move& move) override
  {
    (void)move;
  }

  virtual void onNewGameStarted()
  {
  }

  virtual void onBoardChanged(const BitBoard& boardStatus) override
  {
    (void)boardStatus;
  }

  virtual void onButtonPressed(Button button) override
  {
    (void)button;
  }

  virtual void onButtonReleased(Button button) override
  {
    (void)button;
  }

  virtual void onBestMoveFound(const Move& move) override
  {
    (void)move;
  }
};

//------------------------------------------------------------------------------
class ArgumentsListComparator:
    public MockNamedValueComparator
{
public:
  virtual bool isEqual(const void* object1, const void* object2) override
  {
    ArgumentsList* list1 = (ArgumentsList*)object1;
    ArgumentsList* list2 = (ArgumentsList*)object2;

    while(1)
    {
      std::string arg1;
      std::string arg2;
      bool list1HasArg;
      bool list2HasArg;

      list1HasArg = list1->getNext(arg1);
      list2HasArg = list2->getNext(arg2);

      if(!list1HasArg && !list2HasArg)
        break;
      if(list1HasArg != list2HasArg)
        return false;
      if(arg1 != arg2)
        return false;
    }

    return true;
  }

  virtual SimpleString valueToString(const void* object) override
  {
    // TODO
    (void)object;
    return SimpleString("TODO ArgumentsList to string");
  }
};

//------------------------------------------------------------------------------
TEST_GROUP(TelnetCommandParser)
{
  TEST_SETUP()
  {
    mock().installComparator("ArgumentsList", m_argListComp);
  }

  TEST_TEARDOWN()
  {
    mock().clear();
    mock().removeAllComparatorsAndCopiers();
  }

private:
  ArgumentsListComparator m_argListComp;
};

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parserCleanupCarriageReturn)
{
  TelnetClientNull client;
  std::string cmd = "abc\rdef\r";
  TelnetCommandParser parser;

  parser.parseCommand(cmd, client);

  STRCMP_EQUAL("abcdef", cmd.c_str());
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parserCommandLeavRestOfBuffer)
{
  std::string cmd = "command";
  std::string cmdIn = cmd + "\nhello";
  TelnetClientNull client;
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  std::list<std::string> argsList;
  ArgumentsList args(argsList);
  TelnetCommandParser parser;

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);
  mock().expectOneCall("execute").onObject(pMockCmd).
      withParameterOfType("ArgumentsList", "args", &args).
      withParameter("client", &client);

  parser.addCommand(cmdMock);
  parser.parseCommand(cmdIn, client);

  mock().checkExpectations();

  CHECK(cmdIn == "hello")
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parseTwoCommands)
{
  std::string cmd = "command";
  std::string cmdIn = cmd + "\n" + cmd + "\n";
  TelnetClientNull client;
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  std::list<std::string> argsList;
  ArgumentsList args(argsList);
  TelnetCommandParser parser;

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);
  mock().expectNCalls(2, "execute").onObject(pMockCmd).
      withParameterOfType("ArgumentsList", "args", &args).
      withParameter("client", &client);

  parser.addCommand(cmdMock);
  parser.parseCommand(cmdIn, client);

  mock().checkExpectations();

  CHECK(cmdIn == "")
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parserCommandArgumentsWithTwoSpaces)
{
  std::string cmd = "command";
  std::string cmdIn = cmd + " a b   c\n";
  TelnetClientNull client;
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  std::list<std::string> argsList;
  TelnetCommandParser parser;

  argsList.push_back("a");
  argsList.push_back("b");
  argsList.push_back("c");
  ArgumentsList args(argsList);

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);
  mock().expectOneCall("execute").onObject(pMockCmd).
      withParameterOfType("ArgumentsList", "args", &args).
      withParameter("client", &client);

  parser.addCommand(cmdMock);
  parser.parseCommand(cmdIn, client);

  mock().checkExpectations();

  CHECK(cmdIn == "")
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parserCommandKnownWithReturnLineAndParameters)
{
  std::string cmd = "command";
  std::string cmdIn = cmd + " a b c\n";
  TelnetClientNull client;
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  std::list<std::string> argsList;
  TelnetCommandParser parser;

  argsList.push_back("a");
  argsList.push_back("b");
  argsList.push_back("c");
  ArgumentsList args(argsList);

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);
  mock().expectOneCall("execute").onObject(pMockCmd).
      withParameterOfType("ArgumentsList", "args", &args).
      withParameter("client", &client);

  parser.addCommand(cmdMock);
  parser.parseCommand(cmdIn, client);

  mock().checkExpectations();

  CHECK(cmdIn == "")
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parserCommandKnownWithReturnLine)
{
  std::string cmd = "command";
  std::string cmdIn = cmd + "\n";
  TelnetClientNull client;
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  std::list<std::string> argsList;
  ArgumentsList args(argsList);
  TelnetCommandParser parser;

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);
  mock().expectOneCall("execute").onObject(pMockCmd).
      withParameterOfType("ArgumentsList", "args", &args).
      withParameter("client", &client);

  parser.addCommand(cmdMock);
  parser.parseCommand(cmdIn, client);

  mock().checkExpectations();

  CHECK(cmdIn == "")
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, parserCommandKnownCommandNoReturnLine)
{
  std::string cmd = "command";
  std::string parsedCmd = cmd;
  TelnetClientNull client;
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  TelnetCommandParser parser;

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);

  parser.addCommand(cmdMock);
  parser.parseCommand(parsedCmd, client);

  mock().checkExpectations();

  CHECK(cmd == parsedCmd);
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, addCommand)
{
  std::string cmd = "command";
  TelnetCommandMock* pMockCmd = new TelnetCommandMock;
  std::unique_ptr<ITelnetCommand> cmdMock(pMockCmd);
  TelnetCommandParser parser;

  mock().expectOneCall("command").onObject(pMockCmd).
      andReturnValue(&cmd);

  parser.addCommand(cmdMock);

  mock().checkExpectations();

  CHECK(parser.getCommands().find(cmd) != parser.getCommands().end());
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, executeCommandNotFound)
{
  std::string cmd = "unkown\n";
  TelnetClientNull client;
  TelnetCommandParser parser;

  parser.parseCommand(cmd, client);
}

//------------------------------------------------------------------------------
TEST(TelnetCommandParser, constructor)
{
  TelnetCommandParser parser;
}
