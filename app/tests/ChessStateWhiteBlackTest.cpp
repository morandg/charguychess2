/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/ChessStateWhiteBlack.hpp"

#include "../src/chess/ChessGame.hpp"
#include "mocks/ChessStateMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateWhiteBlack)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateWhiteBlack>& state)
  {
    m_whiteState = new ChessStateMock();
    m_blackState = new ChessStateMock();
    std::unique_ptr<IChessState> white(m_whiteState);
    std::unique_ptr<IChessState> black(m_blackState);
    state.reset(new ChessStateWhiteBlack(m_chessGame, white, black));
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

  ChessStateMock* getWhiteState()
  {
    return m_whiteState;
  }

  ChessStateMock* getBlackState()
  {
    return m_blackState;
  }
private:
  ChessGame m_chessGame;
  ChessStateMock* m_whiteState;
  ChessStateMock* m_blackState;
};

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, enterExitAfterAMoveDone)
{
  std::unique_ptr<ChessStateWhiteBlack> state;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  buildState(state);

  mock().expectOneCall("enter").onObject(getWhiteState());
  mock().expectOneCall("exit").onObject(getWhiteState());

  state->enter();
  getChessGame().applyMove(move);
  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, enterExitBlackTurn)
{
  std::unique_ptr<ChessStateWhiteBlack> state;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  buildState(state);

  getChessGame().applyMove(move);

  mock().expectOneCall("enter").onObject(getBlackState());
  mock().expectOneCall("exit").onObject(getBlackState());

  state->enter();
  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, enterExitWhiteTurn)
{
  std::unique_ptr<ChessStateWhiteBlack> state;

  buildState(state);

  mock().expectOneCall("enter").onObject(getWhiteState());
  mock().expectOneCall("exit").onObject(getWhiteState());

  state->enter();
  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, enterBlackTurn)
{
  std::unique_ptr<ChessStateWhiteBlack> state;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  buildState(state);
  getChessGame().applyMove(move);

  mock().expectOneCall("enter").onObject(getBlackState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, enterWhiteTurn)
{
  std::unique_ptr<ChessStateWhiteBlack> state;

  buildState(state);

  mock().expectOneCall("enter").onObject(getWhiteState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, initSuccess)
{
  std::unique_ptr<ChessStateWhiteBlack> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getWhiteState()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getBlackState()).
      andReturnValue(0);

  CHECK_EQUAL(0, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, initBlackFails)
{
  std::unique_ptr<ChessStateWhiteBlack> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getWhiteState()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getBlackState()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, initWhiteFails)
{
  std::unique_ptr<ChessStateWhiteBlack> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getWhiteState()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateWhiteBlack, constructor)
{
  std::unique_ptr<ChessStateWhiteBlack> state;

  buildState(state);
}
