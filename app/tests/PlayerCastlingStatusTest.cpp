/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/PlayerCastlingStatus.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PlayerCastlingStatus)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, canCastleAfterReset)
{
  PlayerCastlingStatus castlingStatus;

  castlingStatus.kingSideRookMoved();
  castlingStatus.queenRookMoved();
  castlingStatus.kingMoved();
  castlingStatus.reset();

  CHECK(castlingStatus.isKingSideCastleAllowed());
  CHECK(castlingStatus.isQueenSideCastleAllowed());
}

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, cannotCastleKingSideAfterMovingKingSideRook)
{
  PlayerCastlingStatus castlingStatus;

  castlingStatus.kingSideRookMoved();

  CHECK_FALSE(castlingStatus.isKingSideCastleAllowed());
}

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, cannotCastleAfterMovingKing)
{
  PlayerCastlingStatus castlingStatus;

  castlingStatus.kingMoved();

  CHECK_FALSE(castlingStatus.isQueenSideCastleAllowed());
  CHECK_FALSE(castlingStatus.isKingSideCastleAllowed());
}

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, cannotCastleQueenSideAfterMovingQueenSideRook)
{
  PlayerCastlingStatus castlingStatus;

  castlingStatus.queenRookMoved();

  CHECK_FALSE(castlingStatus.isQueenSideCastleAllowed());
}

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, canCastleKingSide)
{
  PlayerCastlingStatus castlingStatus;

  CHECK(castlingStatus.isKingSideCastleAllowed());
}

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, canCastleQueenSide)
{
  PlayerCastlingStatus castlingStatus;

  CHECK(castlingStatus.isQueenSideCastleAllowed());
}

//------------------------------------------------------------------------------
TEST(PlayerCastlingStatus, constructor)
{
  PlayerCastlingStatus castlingStatus;
}
