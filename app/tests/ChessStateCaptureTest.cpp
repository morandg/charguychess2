/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"

#include "spies/ChessStateCaptureSpy.hpp"
#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "comparators/SquareComparator.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateCapture)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
    mock().installComparator("Square", m_squareComp);
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateCaptureSpy>& state)
  {
    state.reset(new ChessStateCaptureSpy(
        m_chessStatesPool, m_chessHardwareMock, m_chessGame));
  }

  void playE2e4D7d5()
  {
    Move move1(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
    Move move2(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
    Square from(File::E, Rank::FOUR);
    Square to(File::D, Rank::FIVE);

    m_chessGame.applyMove(move1);
    m_chessGame.applyMove(move2);
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_chessHardwareMock;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_chessStatesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

private:
  ChessStatesPoolMock m_chessStatesPool;
  ChessHardwareMock m_chessHardwareMock;
  ChessGame m_chessGame;
  SquareComparator m_squareComp;
};

//------------------------------------------------------------------------------
TEST(ChessStateCapture, buttonReleasedPromotionalMoveCanBeApplied)
{
  Button pressedButton = Button::ROOK;
  BitBoard hardwareState;
  Square from = Square(File::E, Rank::SEVEN);
  Square to = Square(File::D, Rank::EIGHT);
  Piece promotion = Piece::ROOK;
  BitBoard whitePawns;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  state->setDestination(to);
  state->setPromotion(promotion);
  whitePawns.set(Square(File::E, Rank::SEVEN));
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);

  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onButtonPressed(pressedButton);
  state->onButtonReleased(pressedButton);

  mock().checkExpectations();

  Move playedMove = getChessGame().lastTurn().whiteMove();
  CHECK(playedMove == Move(from, to));
  CHECK(playedMove.promotion() == promotion);
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, queenButtonPressed)
{
  Button pressedButton = Button::QUEEN;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);

  state->onButtonPressed(pressedButton);

  CHECK(Piece::QUEEN == state->promotion());
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, pieceDroppedBackToOrigin)
{
  BitBoard hardwareState;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  playE2e4D7d5();
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(Square(File::D, Rank::FIVE));

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PIECE_LIFTED);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, pieceDroppedAtCaptureLocationWithPromotion)
{
  BitBoard hardwareState;
  Square from = Square(File::E, Rank::SEVEN);
  Square to = Square(File::D, Rank::EIGHT);
  Piece promotion = Piece::KNIGHT;
  BitBoard whitePawns;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  state->setDestination(to);
  state->setPromotion(promotion);
  whitePawns.set(Square(File::E, Rank::SEVEN));
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();

  Move playedMove = getChessGame().lastTurn().whiteMove();
  CHECK(playedMove == Move(from, to));
  CHECK(playedMove.promotion() == promotion);
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, pieceDroppedAtCaptureLocationPromotionMissing)
{
  BitBoard hardwareState;
  Square from = Square(File::E, Rank::SEVEN);
  Square to = Square(File::D, Rank::EIGHT);
  BitBoard whitePawns;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  state->setDestination(to);
  whitePawns.set(Square(File::E, Rank::SEVEN));
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);

  state->onBoardChanged(hardwareState);
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, pieceDroppedAtCaptureLocation)
{
  BitBoard hardwareState;
  Square from(File::E, Rank::FOUR);
  Square to(File::D, Rank::FIVE);
  Move playedMove(from, to);
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  playE2e4D7d5();
  state->setDestination(to);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();

  CHECK(getChessGame().lastTurn().whiteMove() == playedMove);
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, pieceDroppedWrongPlace)
{
  BitBoard hardwareState;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  playE2e4D7d5();
  hardwareState = getChessGame().allPieces();

  hardwareState.clear(Square(File::E, Rank::FOUR));
  hardwareState.clear(Square(File::D, Rank::FIVE));
  hardwareState.set(Square(File::A, Rank::FOUR));

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, exit)
{
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  // cpp vtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, enterWithPromotion)
{
  BitBoard hardwareState;
  Square liftedPiece1 = Square(File::E, Rank::SEVEN);
  Square liftedPiece2 = Square(File::D, Rank::EIGHT);
  BitBoard whitePawns;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;
  whitePawns.set(Square(File::E, Rank::SEVEN));
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(liftedPiece1);
  hardwareState.clear(liftedPiece2);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedPiece1);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedPiece2);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::ROOK);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::KNIGHT);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::BISHOP);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::QUEEN);

  state->enter();

  mock().checkExpectations();

  CHECK(state->getDestination() == liftedPiece2);
  CHECK(Piece::PAWN == state->promotion());
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, enter)
{
  BitBoard hardwareState;
  Square liftedPiece1 = Square(File::E, Rank::FOUR);
  Square liftedPiece2 = Square(File::D, Rank::FIVE);
  Piece promotion = Piece::BISHOP;
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
  state->setPromotion(promotion);
  playE2e4D7d5();
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(liftedPiece1);
  hardwareState.clear(liftedPiece2);
  // cpp vtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedPiece1);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedPiece2);

  state->enter();

  mock().checkExpectations();

  CHECK(state->getDestination() == Square(File::D, Rank::FIVE));
  CHECK(Piece::PAWN == state->promotion());
}

//------------------------------------------------------------------------------
TEST(ChessStateCapture, constructor)
{
  std::unique_ptr<ChessStateCaptureSpy> state;

  buildState(state);
}
