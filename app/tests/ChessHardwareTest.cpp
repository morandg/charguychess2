/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/ChessHardware.hpp"

#include "mocks/ChessHardwareObserverMock.hpp"
#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
class ChessHardwareSpyMock:
  public ChessHardware
{
public:
  ChessHardwareSpyMock()
  {
  }

  ~ChessHardwareSpyMock()
  {
  }

  void callRaiseBoardChanged(const BitBoard& boardStatus)
  {
    raiseBoardChanged(boardStatus);
  }

  void callRaiseButtonPressed(Button button)
  {
    raiseButtonPressed(button);
  }

  void callRaiseButtonReleased(Button button)
  {
    raiseButtonReleased(button);
  }

  // IChessHardware
  virtual int init() override
  {
    return mock().actualCall(__func__).onObject(this).
        returnIntValue();
  }

  virtual int turnLed(bool isOn, Led led) override
  {
    return mock().actualCall(__func__).onObject(this).
        withParameter("isOn", isOn).
        withParameter("led", (int)led).
        returnIntValue();
  }

  virtual BitBoard getPieces() override
  {
    return *((BitBoard*)mock().actualCall(__func__).onObject(this).
        returnPointerValue());
  }
};

//------------------------------------------------------------------------------
class ObserverDeregister:
    public IChessHardwareObserver
{
public:
  ObserverDeregister(IChessHardware& hw):
    m_hw(hw)
  {
  }

  ~ObserverDeregister()
  {
  }

  void onBoardChanged(const BitBoard& boardStatus) override
  {
    (void)boardStatus;
    m_hw.removeObserver(*this);
  }

  void onButtonPressed(Button button) override
  {
    (void)button;
    m_hw.removeObserver(*this);
  }

  virtual void onButtonReleased(Button button) override
  {
    (void)button;
    m_hw.removeObserver(*this);
  }

private:
  IChessHardware& m_hw;
};

//------------------------------------------------------------------------------
TEST_GROUP(ChessHardware)
{
  TEST_SETUP()
  {
    mock().installComparator("BitBoard", m_bitBoardComp);
  }

  TEST_TEARDOWN()
  {
    mock().clear();
    mock().removeAllComparatorsAndCopiers();
  }

private:
  BitBoardComparator m_bitBoardComp;
};

//------------------------------------------------------------------------------
TEST(ChessHardware, removeObserverInButtonReleasedCallbackDoesntCrash)
{
  Button button = Button::BISHOP;
  ChessHardwareSpyMock hw;
  ObserverDeregister observer(hw);

  hw.addObserver(observer);
  hw.callRaiseButtonReleased(button);
}

//------------------------------------------------------------------------------
TEST(ChessHardware, removeObserverInButtonPressedCallbackDoesntCrash)
{
  Button button = Button::BISHOP;
  ChessHardwareSpyMock hw;
  ObserverDeregister observer(hw);

  hw.addObserver(observer);
  hw.callRaiseButtonPressed(button);
}

//------------------------------------------------------------------------------
TEST(ChessHardware, removeObserverInBitBoardCallbackDoesntCrash)
{
  BitBoard board(798);
  ChessHardwareSpyMock hw;
  ObserverDeregister observer(hw);

  hw.addObserver(observer);
  hw.callRaiseBoardChanged(board);
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnBlackPlayerLed)
{
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", true).
      withParameter("led", (int)Led::BLACK).
      andReturnValue(0);

  CHECK_EQUAL(0, hw.turnPlayerLed(true, Color::BLACK));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnWhitePlayerLedOff)
{
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", false).
      withParameter("led", (int)Led::WHITE).
      andReturnValue(0);

  CHECK_EQUAL(0, hw.turnPlayerLed(false, Color::WHITE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnWhitePlayerLedFails)
{
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", true).
      withParameter("led", (int)Led::WHITE).
      andReturnValue(-1);

  CHECK_EQUAL(-1, hw.turnPlayerLed(true, Color::WHITE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnWhitePlayerLedOn)
{
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", true).
      withParameter("led", (int)Led::WHITE).
      andReturnValue(0);

  CHECK_EQUAL(0, hw.turnPlayerLed(true, Color::WHITE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, raisButtonReleased)
{
  Button button = Button::QUEEN;
  ChessHardwareObserverMock observer;
  ChessHardwareSpyMock hw;

  hw.addObserver(observer);

  mock().expectOneCall("onButtonReleased").onObject(&observer).
      withParameter("button", (int)button);

  hw.callRaiseButtonReleased(button);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, raisButtonPressed)
{
  Button button = Button::BISHOP;
  ChessHardwareObserverMock observer;
  ChessHardwareSpyMock hw;

  hw.addObserver(observer);

  mock().expectOneCall("onButtonPressed").onObject(&observer).
      withParameter("button", (int)button);

  hw.callRaiseButtonPressed(button);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, raisBitobardChangeObserverRemoved)
{
  BitBoard board(798);
  ChessHardwareObserverMock observer;
  ChessHardwareSpyMock hw;

  hw.addObserver(observer);
  hw.removeObserver(observer);

  hw.callRaiseBoardChanged(board);
}

//------------------------------------------------------------------------------
TEST(ChessHardware, raisBitobardChangeObserverRegisteredTwice)
{
  BitBoard board(798);
  ChessHardwareObserverMock observer;
  ChessHardwareSpyMock hw;

  hw.addObserver(observer);
  hw.addObserver(observer);

  mock().expectOneCall("onBoardChanged").onObject(&observer).
      withParameterOfType("BitBoard", "boardStatus", &board);

  hw.callRaiseBoardChanged(board);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, raisBitobardChangeWithObserver)
{
  BitBoard board(798);
  ChessHardwareObserverMock observer;
  ChessHardwareSpyMock hw;

  hw.addObserver(observer);

  mock().expectOneCall("onBoardChanged").onObject(&observer).
      withParameterOfType("BitBoard", "boardStatus", &board);

  hw.callRaiseBoardChanged(board);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnAllLedsOffOnFails)
{
  bool isOn = false;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::A).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::B).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::C).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::E).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::F).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::G).
      andReturnValue(-1);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::H).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ONE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::TWO).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::THREE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FOUR).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SIX).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SEVEN).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::EIGHT).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ROOK).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::BISHOP).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::KNIGHT).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::QUEEN).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::WHITE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::BLACK).
      andReturnValue(0);

  CHECK_EQUAL(-1, hw.turnAllLeds(isOn));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnAllLedsOff)
{
  bool isOn = false;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::A).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::B).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::C).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::E).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::F).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::G).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::H).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ONE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::TWO).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::THREE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FOUR).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SIX).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SEVEN).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::EIGHT).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ROOK).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::BISHOP).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::KNIGHT).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::QUEEN).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::WHITE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::BLACK).
      andReturnValue(0);

  CHECK_EQUAL(0, hw.turnAllLeds(isOn));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnAllLedsOn)
{
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::A).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::B).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::C).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::E).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::F).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::G).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::H).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ONE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::TWO).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::THREE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FOUR).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SIX).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SEVEN).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::EIGHT).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ROOK).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::BISHOP).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::KNIGHT).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::QUEEN).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::WHITE).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::BLACK).
      andReturnValue(0);

  CHECK_EQUAL(0, hw.turnAllLeds(isOn));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnSquareD5RankFails)
{
  bool isOn = true;
  Square square(File::D, Rank::FIVE);
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(0);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(-1);

  CHECK_EQUAL(-1, hw.turnSquareLed(isOn, square));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnSquareD5FileFails)
{
  bool isOn = true;
  Square square(File::D, Rank::FIVE);
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(-1);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(0);

  CHECK_EQUAL(-1, hw.turnSquareLed(isOn, square));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnSquareD5)
{
  int ret = 22;
  bool isOn = true;
  Square square(File::D, Rank::FIVE);
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(ret);
  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(ret);

  CHECK_EQUAL(0, hw.turnSquareLed(isOn, square));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileInvalid)
{
  bool isOn = true;
  ChessHardwareSpyMock hw;

  CHECK_EQUAL(-1, hw.turnFileLed(isOn, File(999)));
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileH)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::H).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::H));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileG)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::G).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::G));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileF)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::F).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::F));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileE)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::E).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::E));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileD)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::D).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::D));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileC)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::C).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::C));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileB)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::B).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::B));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnFileA)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::A).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnFileLed(isOn, File::A));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRankInvalid)
{
  bool isOn = true;
  ChessHardwareSpyMock hw;

  CHECK_EQUAL(-1, hw.turnRankLed(isOn, (Rank)999));
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank8)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::EIGHT).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::EIGHT));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank7)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SEVEN).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::SEVEN));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank6)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::SIX).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::SIX));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank5)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FIVE).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::FIVE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank4)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::FOUR).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::FOUR));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank3)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::THREE).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::THREE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank2)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::TWO).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::TWO));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, turnLedOnRank1)
{
  int ret = 123;
  bool isOn = true;
  ChessHardwareSpyMock hw;

  mock().expectOneCall("turnLed").onObject(&hw).
      withParameter("isOn", isOn).
      withParameter("led", (int)Led::ONE).
      andReturnValue(ret);

  CHECK_EQUAL(ret, hw.turnRankLed(isOn, Rank::ONE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardware, constructor)
{
  ChessHardwareSpyMock hw;
}
