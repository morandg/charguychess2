/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/PromotionInformation.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PromotionInformation)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PromotionInformation, resetAfterbuttonPressed)
{
  PromotionInformation pi;

  pi.buttonPressed(Button::QUEEN);
  pi.reset();

  CHECK(Piece::PAWN == pi.promotion());
}

//------------------------------------------------------------------------------
TEST(PromotionInformation, buttonPressedQueen)
{
  PromotionInformation pi;

  pi.buttonPressed(Button::QUEEN);

  CHECK(Piece::QUEEN == pi.promotion());
}


//------------------------------------------------------------------------------
TEST(PromotionInformation, buttonPressedBishop)
{
  PromotionInformation pi;

  pi.buttonPressed(Button::BISHOP);

  CHECK(Piece::BISHOP == pi.promotion());
}

//------------------------------------------------------------------------------
TEST(PromotionInformation, buttonPressedKnight)
{
  PromotionInformation pi;

  pi.buttonPressed(Button::KNIGHT);

  CHECK(Piece::KNIGHT == pi.promotion());
}

//------------------------------------------------------------------------------
TEST(PromotionInformation, buttonPressedRook)
{
  PromotionInformation pi;

  pi.buttonPressed(Button::ROOK);

  CHECK(Piece::ROOK == pi.promotion());
}

//------------------------------------------------------------------------------
TEST(PromotionInformation, defaultPromotionIsPawn)
{
  PromotionInformation pi;

  CHECK(Piece::PAWN == pi.promotion());
}
