/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/ChessStatesPool.hpp"

#include "mocks/ChessStateMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStatesPool)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }
};

//------------------------------------------------------------------------------
TEST(ChessStatesPool, changeStateTwicePreviousExit)
{
  IChessState* pInvalidPosState = new ChessStateMock();
  IChessState* pPlayerThinkingState = new ChessStateMock();
  std::unique_ptr<IChessState> invalidPosState(pInvalidPosState);
  std::unique_ptr<IChessState> playerThinkingState(pPlayerThinkingState);
  ChessStatesPool statesPool;

  statesPool.addState(IChessStatesPool::INVALID_POSITION, invalidPosState);
  statesPool.addState(IChessStatesPool::PLAYER_THINKING, playerThinkingState);

  mock().expectOneCall("enter").onObject(pInvalidPosState);
  mock().expectOneCall("exit").onObject(pInvalidPosState);
  mock().expectOneCall("enter").onObject(pPlayerThinkingState);

  statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  statesPool.changeState(IChessStatesPool::PLAYER_THINKING);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStatesPool, changeStateEnters)
{
  IChessState* pAddedState = new ChessStateMock();
  std::unique_ptr<IChessState> addedState(pAddedState);
  ChessStatesPool statesPool;

  statesPool.addState(IChessStatesPool::INVALID_POSITION, addedState);

  mock().expectOneCall("enter").onObject(pAddedState);

  statesPool.changeState(IChessStatesPool::INVALID_POSITION);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStatesPool, initStateAddedInitFails)
{
  IChessState* pAddedState = new ChessStateMock();
  std::unique_ptr<IChessState> addedState(pAddedState);
  ChessStatesPool statesPool;

  statesPool.addState(IChessStatesPool::INVALID_POSITION, addedState);

  mock().expectOneCall("init").onObject(pAddedState).
      andReturnValue(-1);

  CHECK_EQUAL(-1, statesPool.init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStatesPool, initStateAddedInitSucceed)
{
  IChessState* pAddedState = new ChessStateMock();
  std::unique_ptr<IChessState> addedState(pAddedState);
  ChessStatesPool statesPool;

  statesPool.addState(IChessStatesPool::INVALID_POSITION, addedState);

  mock().expectOneCall("init").onObject(pAddedState).
      andReturnValue(0);

  CHECK_EQUAL(0, statesPool.init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStatesPool, initNoStateAdded)
{
  ChessStatesPool statesPool;

  CHECK_EQUAL(0, statesPool.init());
}

//------------------------------------------------------------------------------
TEST(ChessStatesPool, constructor)
{
  ChessStatesPool statesPool;
}
