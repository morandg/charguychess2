/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/chess/rules/PieceRulesQueen.hpp"

#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PieceRulesQueen)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PieceRulesQueen, atackMap)
{
  Square from(File::D, Rank::ONE);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  ChessGame cg;
  PieceRulesQueen prq;

  expectedAttackMap.set(Square(File::C, Rank::ONE));
  expectedAttackMap.set(Square(File::C, Rank::TWO));
  expectedAttackMap.set(Square(File::D, Rank::TWO));
  expectedAttackMap.set(Square(File::E, Rank::ONE));
  expectedAttackMap.set(Square(File::E, Rank::TWO));

  attackMap = prq.attackMap(from, cg);

  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesQueen, moveMapBlockedByCurrent)
{
  ChessGame cg;
  Square from(File::G, Rank::FOUR);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesQueen prq;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  expectedMoveMap.set(Square(File::G, Rank::SEVEN));
  expectedMoveMap.set(Square(File::G, Rank::EIGHT));
  expectedMoveMap.set(Square(File::A, Rank::FOUR));
  expectedMoveMap.set(Square(File::B, Rank::FOUR));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::FOUR));
  expectedMoveMap.set(Square(File::H, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::H, Rank::FIVE));
  expectedMoveMap.set(Square(File::C, Rank::EIGHT));
  expectedMoveMap.set(Square(File::D, Rank::SEVEN));
  expectedMoveMap.set(Square(File::E, Rank::SIX));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  expectedMoveMap.set(Square(File::H, Rank::THREE));

  moveMap = prq.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesQueen, moveMapCanBlockedByOpponent)
{
  ChessGame cg;
  Square from(File::G, Rank::FOUR);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesQueen prq;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  expectedMoveMap.set(Square(File::G, Rank::ONE));
  expectedMoveMap.set(Square(File::G, Rank::TWO));
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  expectedMoveMap.set(Square(File::G, Rank::SEVEN));
  expectedMoveMap.set(Square(File::A, Rank::FOUR));
  expectedMoveMap.set(Square(File::B, Rank::FOUR));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::FOUR));
  expectedMoveMap.set(Square(File::H, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::ONE));
  expectedMoveMap.set(Square(File::E, Rank::TWO));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::H, Rank::FIVE));
  expectedMoveMap.set(Square(File::D, Rank::SEVEN));
  expectedMoveMap.set(Square(File::E, Rank::SIX));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  expectedMoveMap.set(Square(File::H, Rank::THREE));

  moveMap = prq.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesQueen, moveMapCanGoAnywhere)
{
  ChessGame cg;
  Square from(File::G, Rank::FOUR);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesQueen prq;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  expectedMoveMap.set(Square(File::G, Rank::ONE));
  expectedMoveMap.set(Square(File::G, Rank::TWO));
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  expectedMoveMap.set(Square(File::G, Rank::SEVEN));
  expectedMoveMap.set(Square(File::G, Rank::EIGHT));
  expectedMoveMap.set(Square(File::A, Rank::FOUR));
  expectedMoveMap.set(Square(File::B, Rank::FOUR));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::FOUR));
  expectedMoveMap.set(Square(File::H, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::ONE));
  expectedMoveMap.set(Square(File::E, Rank::TWO));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::H, Rank::FIVE));
  expectedMoveMap.set(Square(File::C, Rank::EIGHT));
  expectedMoveMap.set(Square(File::D, Rank::SEVEN));
  expectedMoveMap.set(Square(File::E, Rank::SIX));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  expectedMoveMap.set(Square(File::H, Rank::THREE));

  moveMap = prq.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesQueen, piece)
{
  PieceRulesQueen prq;

  CHECK(Piece::QUEEN == prq.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesQueen, constructor)
{
  PieceRulesQueen prq;
}
