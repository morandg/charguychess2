/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/driver/CgcProtocolParser.hpp"

#include <CppUTestExt/MockSupport.h>
#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
class ProtocolParserEventMock:
    public ICgcProtocolParserEvent
{
public:
  ProtocolParserEventMock(){}
  ~ProtocolParserEventMock(){}

  virtual void onRegisterChanged(uint8_t reg, uint8_t value) override
  {
    mock().actualCall(__func__).onObject(this).
        withParameter("reg", reg).
        withParameter("value", value);
  }
};


//------------------------------------------------------------------------------
TEST_GROUP(CgcProtocolParser)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }
};

//------------------------------------------------------------------------------
TEST(CgcProtocolParser, readCallback)
{
  ProtocolParserEventMock eventCb;
  CgcProtocolParser parser;

  parser.init(eventCb);

  mock().expectOneCall("onRegisterChanged").onObject(&eventCb).
      withParameter("reg", 0x01).
      withParameter("value", 0x23);

  parser.parseBuffer("0123\n", 5);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(CgcProtocolParser, constructor)
{
  CgcProtocolParser parser;
}
