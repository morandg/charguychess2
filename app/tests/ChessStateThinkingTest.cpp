/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/hardware/states/ChessStateThinking.hpp"

#include "mocks/ChessStateMock.hpp"
#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"

#include "comparators/SquareComparator.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateThinking)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
    mock().installComparator("Square", m_squareComp);
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateThinking>& state)
  {
    m_humanComputerState = new ChessStateMock();
    std::unique_ptr<IChessState> humanComputerState(m_humanComputerState);
    state.reset(new ChessStateThinking(
        m_statesPool, m_chessGame, m_chessHardware, humanComputerState));
  }

  ChessStateMock* getHumanComputerState()
  {
    return m_humanComputerState;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_statesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

  ChessHardwareMock& getHardware()
  {
    return m_chessHardware;
  }

private:
  ChessGame m_chessGame;
  ChessStatesPoolMock m_statesPool;
  ChessHardwareMock m_chessHardware;
  ChessStateMock* m_humanComputerState;
  SquareComparator m_squareComp;
};



//------------------------------------------------------------------------------
TEST(ChessStateThinking, enterWhenChecked)
{
  BitBoard whiteKing;
  BitBoard blackKing;
  BitBoard blackRooks;
  Square whiteKingPosition(File::H, Rank::ONE);
  ChessGame& cg = getChessGame();
  std::unique_ptr<ChessStateThinking> state;

  whiteKing.set(whiteKingPosition);
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::KING, whiteKing);
  blackRooks.set(Square(File::A, Rank::TWO));
  blackRooks.set(Square(File::H, Rank::EIGHT));
  blackKing.set(Square(File::B, Rank::SEVEN));
  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::QUEEN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, blackRooks);
  cg.setOpponentPieces(Piece::KING, blackKing);

  buildState(state);

  mock().expectOneCall("turnAllLeds").onObject(&getHardware()).
      withParameter("isOn", false).
      andReturnValue(0);
  mock().expectOneCall("turnSquareLed").onObject(&getHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &whiteKingPosition);
  mock().expectOneCall("enter").onObject(getHumanComputerState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateThinking, enterWhenMate)
{
  BitBoard whiteKing;
  BitBoard blackKing;
  BitBoard blackRooks;
  ChessGame& cg = getChessGame();
  std::unique_ptr<ChessStateThinking> state;

  whiteKing.set(Square(File::H, Rank::ONE));
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::KING, whiteKing);
  blackRooks.set(Square(File::A, Rank::TWO));
  blackRooks.set(Square(File::G, Rank::EIGHT));
  blackRooks.set(Square(File::H, Rank::EIGHT));
  blackKing.set(Square(File::B, Rank::SEVEN));
  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::QUEEN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, blackRooks);
  cg.setOpponentPieces(Piece::KING, blackKing);

  buildState(state);

  mock().expectOneCall("turnAllLeds").onObject(&getHardware()).
      withParameter("isOn", false).
      andReturnValue(0);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::END_OF_GAME);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateThinking, enterEnterHumanComputer)
{
  std::unique_ptr<ChessStateThinking> state;

  buildState(state);

  mock().expectOneCall("turnAllLeds").onObject(&getHardware()).
      withParameter("isOn", false).
      andReturnValue(0);
  mock().expectOneCall("enter").onObject(getHumanComputerState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateThinking, exit)
{
  std::unique_ptr<ChessStateThinking> state;

  buildState(state);

  mock().expectOneCall("exit").onObject(getHumanComputerState());

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateThinking, initSuccess)
{
  std::unique_ptr<ChessStateThinking> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getHumanComputerState()).
      andReturnValue(0);

  CHECK_EQUAL(0, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateThinking, initFails)
{
  std::unique_ptr<ChessStateThinking> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getHumanComputerState()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateThinking, constructor)
{
  std::unique_ptr<ChessStateThinking> state;

  buildState(state);
}
