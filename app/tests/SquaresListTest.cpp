/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "../src/chess/SquaresList.hpp"

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(SquaresList)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(SquaresList, atFound)
{
  Square square(File::B, Rank::SIX);
  Square at;
  SquaresList squares;

  squares.add(square);

  CHECK(squares.at(0, at));
  CHECK(at == square);
}

//------------------------------------------------------------------------------
TEST(SquaresList, atNotFound)
{
  Square at;
  SquaresList squares;

  CHECK_FALSE(squares.at(0, at));
}

//------------------------------------------------------------------------------
TEST(SquaresList, doesntContainsMove)
{
  Square from(File::A, Rank::ONE);
  Square to(File::A, Rank::TWO);
  Move move(from, to);
  SquaresList squares;

  squares.add(from);
  squares.add(to);

  CHECK(squares.contains(move));
}

//------------------------------------------------------------------------------
TEST(SquaresList, doesntContainMove)
{
  Square from(File::A, Rank::ONE);
  Square to(File::A, Rank::TWO);
  Move move(from, to);
  SquaresList squares;

  squares.add(from);

  CHECK_FALSE(squares.contains(move));
}

//------------------------------------------------------------------------------
TEST(SquaresList, containsAfterAdd)
{
  Square square(File::A, Rank::ONE);
  SquaresList squares;

  squares.add(square);

  CHECK(squares.contains(square));
}

//------------------------------------------------------------------------------
TEST(SquaresList, doesntContainWhenNotAdded)
{
  Square square(File::A, Rank::ONE);
  SquaresList squares;

  CHECK_FALSE(squares.contains(square));
}

//------------------------------------------------------------------------------
TEST(SquaresList, constructor)
{
  SquaresList squares;
}
