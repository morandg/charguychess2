/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/rules/PieceRulesKing.hpp"

#include "spies/ChessGameSpy.hpp"
#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(PieceRulesKing)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }

  void clearCurrentPlayerCastlingPath(ChessGame& cg)
  {
    cg.setPlayerPieces(Piece::BISHOP, BitBoard());
    cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
    cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  }
};

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveKBlackKingSideCastlingInMove)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  Square to(File::G, Rank::EIGHT);
  Move move(from, to);
  PlayerPiece rook;
  ChessGame cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK(move.isKingSideCastling());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveKBlackQueenSideCastlingInMove)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  Square to(File::C, Rank::EIGHT);
  Move move(from, to);
  ChessGame cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK(move.isQueenSideCastling());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveKWhiteKingSideCastlingInMove)
{
  Square from(File::E, Rank::ONE);
  Square to(File::G, Rank::ONE);
  Move move(from, to);
  PlayerPiece rook;
  ChessGame cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK(move.isKingSideCastling());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveKWhiteQueenSideCastlingInMove)
{
  Square from(File::E, Rank::ONE);
  Square to(File::C, Rank::ONE);
  Move move(from, to);
  ChessGame cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK(move.isQueenSideCastling());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapCannotMoveIntoChecked)
{
  Square from(File::E, Rank::ONE);
  BitBoard expectedMoveMap;
  BitBoard moveMap;
  BitBoard blackQueen;
  ChessGame cg;
  PieceRulesKing prk;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  blackQueen.set(Square(File::F, Rank::SIX));
  cg.setOpponentPieces(Piece::QUEEN, blackQueen);
  expectedMoveMap.set(Square(File::D, Rank::TWO));
  expectedMoveMap.set(Square(File::E, Rank::TWO));

  moveMap = prk.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, atackMap)
{
  Square from(File::E, Rank::ONE);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  ChessGame cg;
  PieceRulesKing prk;

  expectedAttackMap.set(Square(File::D, Rank::ONE));
  expectedAttackMap.set(Square(File::D, Rank::TWO));
  expectedAttackMap.set(Square(File::E, Rank::TWO));
  expectedAttackMap.set(Square(File::F, Rank::ONE));
  expectedAttackMap.set(Square(File::F, Rank::TWO));

  attackMap = prk.attackMap(from, cg);

  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveBlackKingCastleKingSize)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  Square to(File::G, Rank::EIGHT);
  Move move(from, to);
  PlayerPiece rook;
  ChessGame cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK_FALSE(cg.pieceAt(Square(File::H, Rank::EIGHT), rook));
  CHECK(cg.pieceAt(Square(File::F, Rank::EIGHT), rook));
  CHECK(Piece::ROOK == rook.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveBlackKingCastleQueenSize)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  Square to(File::C, Rank::EIGHT);
  Move move(from, to);
  PlayerPiece rook;
  ChessGame cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK_FALSE(cg.pieceAt(Square(File::A, Rank::EIGHT), rook));
  CHECK(cg.pieceAt(Square(File::D, Rank::EIGHT), rook));
  CHECK(Piece::ROOK == rook.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveWhiteKingCastleKingSize)
{
  Square from(File::E, Rank::ONE);
  Square to(File::G, Rank::ONE);
  Move move(from, to);
  PlayerPiece rook;
  ChessGame cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK_FALSE(cg.pieceAt(Square(File::H, Rank::ONE), rook));
  CHECK(cg.pieceAt(Square(File::F, Rank::ONE), rook));
  CHECK(Piece::ROOK == rook.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMoveWhiteKingCastleQueenSize)
{
  Square from(File::E, Rank::ONE);
  Square to(File::C, Rank::ONE);
  Move move(from, to);
  PlayerPiece rook;
  ChessGame cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);

  CHECK(prk.applyMove(move, cg));
  CHECK_FALSE(cg.pieceAt(Square(File::A, Rank::ONE), rook));
  CHECK(cg.pieceAt(Square(File::D, Rank::ONE), rook));
  CHECK(Piece::ROOK == rook.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapCanCastleBlackKnightOnBoard)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGame cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  expectedMoveMap.set(Square(File::D, Rank::EIGHT));
  expectedMoveMap.set(Square(File::F, Rank::EIGHT));

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapBlackKingCanCastleKingSideNoMoreAllowed)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGameSpy cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);
  expectedMoveMap.set(Square(File::C, Rank::EIGHT));
  expectedMoveMap.set(Square(File::D, Rank::EIGHT));
  expectedMoveMap.set(Square(File::F, Rank::EIGHT));
  cg.getCastlingStatus().kingSideRookMoved(Color::BLACK);

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapBlackKingCanCastleQueenSideNoMoreAllowed)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGameSpy cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);
  expectedMoveMap.set(Square(File::D, Rank::EIGHT));
  expectedMoveMap.set(Square(File::F, Rank::EIGHT));
  expectedMoveMap.set(Square(File::G, Rank::EIGHT));
  cg.getCastlingStatus().queenRookMoved(Color::BLACK);

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapBlackKingCanCastleBothSides)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::E, Rank::EIGHT);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGame cg;
  PieceRulesKing prk;

  cg.applyMove(e2e4);
  clearCurrentPlayerCastlingPath(cg);
  expectedMoveMap.set(Square(File::C, Rank::EIGHT));
  expectedMoveMap.set(Square(File::D, Rank::EIGHT));
  expectedMoveMap.set(Square(File::F, Rank::EIGHT));
  expectedMoveMap.set(Square(File::G, Rank::EIGHT));

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapCanCastleWithKnightOnBoard)
{
  Square from(File::E, Rank::ONE);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGame cg;
  PieceRulesKing prk;

  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  expectedMoveMap.set(Square(File::D, Rank::ONE));
  expectedMoveMap.set(Square(File::F, Rank::ONE));

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapWhiteKingCanCastleKingSideNoMoreAllowed)
{
  Square from(File::E, Rank::ONE);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGameSpy cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);
  expectedMoveMap.set(Square(File::C, Rank::ONE));
  expectedMoveMap.set(Square(File::D, Rank::ONE));
  expectedMoveMap.set(Square(File::F, Rank::ONE));
  cg.getCastlingStatus().kingSideRookMoved(Color::WHITE);

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapWhiteKingCanCastleQueenSideNoMoreAllowed)
{
  Square from(File::E, Rank::ONE);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGameSpy cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);
  expectedMoveMap.set(Square(File::D, Rank::ONE));
  expectedMoveMap.set(Square(File::F, Rank::ONE));
  expectedMoveMap.set(Square(File::G, Rank::ONE));
  cg.getCastlingStatus().queenRookMoved(Color::WHITE);

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapWhiteKingCanCastleBothSides)
{
  Square from(File::E, Rank::ONE);
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGame cg;
  PieceRulesKing prk;

  clearCurrentPlayerCastlingPath(cg);
  expectedMoveMap.set(Square(File::C, Rank::ONE));
  expectedMoveMap.set(Square(File::D, Rank::ONE));
  expectedMoveMap.set(Square(File::F, Rank::ONE));
  expectedMoveMap.set(Square(File::G, Rank::ONE));

  moveMap = prk.moveMap(from, cg);
  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, applyMove)
{
  Square from(File::E, Rank::ONE);
  Square to(File::F, Rank::ONE);
  Move move(from, to);
  ChessGame cg;
  BitBoard allPieces;
  PieceRulesKing prk;

  cg.setPlayerPieces(Piece::BISHOP, BitBoard());

  CHECK(prk.applyMove(move, cg));

  allPieces = cg.allPieces();
  CHECK_FALSE(allPieces.isSet(from));
  CHECK(allPieces.isSet(to));
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapBlockedByOpponentPiece)
{
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  BitBoard blackPawns;
  Square from(File::F, Rank::FOUR);
  ChessGame cg;
  PieceRulesKing prk;

  blackPawns = cg.opponentPieces(Piece::PAWN);
  blackPawns.set(Square(File::E, Rank::THREE));
  blackPawns.set(Square(File::F, Rank::THREE));
  blackPawns.set(Square(File::G, Rank::THREE));
  cg.setOpponentPieces(Piece::PAWN, blackPawns);
  expectedMoveMap.set(Square(File::E, Rank::THREE));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FIVE));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FOUR));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));

  moveMap = prk.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapBlockedBySameColorPiece)
{
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  BitBoard whitePawns;
  Square from(File::F, Rank::FOUR);
  ChessGame cg;
  PieceRulesKing prk;

  whitePawns.set(Square(File::E, Rank::THREE));
  whitePawns.set(Square(File::E, Rank::FOUR));
  whitePawns.set(Square(File::E, Rank::FIVE));
  whitePawns.set(Square(File::F, Rank::THREE));
  whitePawns.set(Square(File::F, Rank::FIVE));
  whitePawns.set(Square(File::G, Rank::THREE));
  whitePawns.set(Square(File::G, Rank::FOUR));
  whitePawns.set(Square(File::G, Rank::FIVE));
  cg.setPlayerPieces(Piece::PAWN, whitePawns);

  moveMap = prk.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, moveMapCanGoAnywhere)
{
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  Square from(File::F, Rank::FOUR);
  ChessGame cg;
  PieceRulesKing prk;

  expectedMoveMap.set(Square(File::E, Rank::THREE));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FIVE));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FOUR));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));

  moveMap = prk.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, piece)
{
  PieceRulesKing prk;

  CHECK(Piece::KING == prk.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKing, constructor)
{
  PieceRulesKing prk;
}
