/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/ChessStateComputerThinking.hpp"
#include "../src/chess/ChessGame.hpp"

#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "mocks/ChessEngineMock.hpp"
#include "mocks/BlinkingLedsMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateComputerThinking)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateComputerThinking>& state)
  {
    state.reset(
        new ChessStateComputerThinking(
            m_blinkingLeds,
            m_chessStatesPool,
            m_chessHardwareMock,
            m_chessGame,
            m_aiSettings,
            m_engine));
  }

  BlinkingLedsMock& getBlinkingLeds()
  {
    return m_blinkingLeds;
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_chessHardwareMock;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_chessStatesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

  AiSettings& getAiSettings()
  {
    return m_aiSettings;
  }

  ChessEngineMock& getEngine()
  {
    return m_engine;
  }

private:
  BlinkingLedsMock m_blinkingLeds;
  ChessStatesPoolMock m_chessStatesPool;
  ChessHardwareMock m_chessHardwareMock;
  ChessGame m_chessGame;
  AiSettings m_aiSettings;
  ChessEngineMock m_engine;
};

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, onBestMoveFound)
{
  Move bestMove;
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::REQUEST_MOVE);

  state->onBestMoveFound(bestMove);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, onBoardChange)
{
  BitBoard newBoardStatus;
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newBoardStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, exit)
{
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);
  // cppvtable
  IChessHardwareObserver& hwObserver = *state;
  IChessEngineObserver& engineObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("removeObserver").onObject(&getEngine()).
      withParameter("observer", &engineObserver);
  mock().expectOneCall("stop").onObject(&getBlinkingLeds());
  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, enterMoveAlreadyFound)
{
  MoveInfo lastFoundMove;
  Move validMove(Square(File::A, Rank::ONE), Square(File::A, Rank::TWO));
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);

  lastFoundMove.setColor(Color::WHITE);
  lastFoundMove.setTurnNumber(1);
  lastFoundMove.setMove(validMove);

  mock().expectOneCall("lastFoundMove").onObject(&getEngine()).
      andReturnValue(&lastFoundMove);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::REQUEST_MOVE);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, enterAlreadySearching)
{
  unsigned int searchTime = 321;
  AiSettings whiteAiSettings;
  MoveInfo lastFoundMove;
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);
  whiteAiSettings.setSearchTime(searchTime);
  getAiSettings() = whiteAiSettings;
  // cppvtable
  IChessHardwareObserver& hwObserver = *state;
  IChessEngineObserver& engineObserver = *state;
  lastFoundMove.setColor(Color::WHITE);
  lastFoundMove.setTurnNumber(1);

  mock().expectOneCall("lastFoundMove").onObject(&getEngine()).
      andReturnValue(&lastFoundMove);
  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("addObserver").onObject(&getEngine()).
      withParameter("observer", &engineObserver);
  mock().expectOneCall("setOnTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateComputerThinking::BLINK_ON_TIMEOUT);
  mock().expectOneCall("setOffTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateComputerThinking::BLINK_OFF_TIMEOUT);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::WHITE);
  mock().expectOneCall("start").onObject(&getBlinkingLeds());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, enterSearchTime)
{
  unsigned int searchTime = 321;
  AiSettings whiteAiSettings;
  MoveInfo lastFoundMove;
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);
  whiteAiSettings.setSearchTime(searchTime);
  getAiSettings() = whiteAiSettings;
  // cppvtable
  IChessHardwareObserver& hwObserver = *state;
  IChessEngineObserver& engineObserver = *state;
  const IChessGame& game = getChessGame();

  mock().expectOneCall("lastFoundMove").onObject(&getEngine()).
      andReturnValue(&lastFoundMove);
  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("addObserver").onObject(&getEngine()).
      withParameter("observer", &engineObserver);
  mock().expectOneCall("setOnTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateComputerThinking::BLINK_ON_TIMEOUT);
  mock().expectOneCall("setOffTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateComputerThinking::BLINK_OFF_TIMEOUT);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::WHITE);
  mock().expectOneCall("searchTime").onObject(&getEngine()).
      withParameter("ms", searchTime).
      withParameter("game", &game);
  mock().expectOneCall("start").onObject(&getBlinkingLeds());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, enterSearchDepth)
{
  unsigned int searchDepth = 123;
  AiSettings whiteAiSettings;
  MoveInfo lastFoundMove;
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);
  whiteAiSettings.setSearcDepth(searchDepth);
  getAiSettings() = whiteAiSettings;
  // cppvtable
  IChessHardwareObserver& hwObserver = *state;
  IChessEngineObserver& engineObserver = *state;
  const IChessGame& game = getChessGame();

  mock().expectOneCall("lastFoundMove").onObject(&getEngine()).
      andReturnValue(&lastFoundMove);
  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("addObserver").onObject(&getEngine()).
      withParameter("observer", &engineObserver);
  mock().expectOneCall("setOnTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateComputerThinking::BLINK_ON_TIMEOUT);
  mock().expectOneCall("setOffTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateComputerThinking::BLINK_OFF_TIMEOUT);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::WHITE);
  mock().expectOneCall("searchDepth").onObject(&getEngine()).
      withParameter("depth", searchDepth).
      withParameter("game", &game);
  mock().expectOneCall("start").onObject(&getBlinkingLeds());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, init)
{
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);

  CHECK_EQUAL(0, state->init());
}

//------------------------------------------------------------------------------
TEST(ChessStateComputerThinking, constructor)
{
  std::unique_ptr<ChessStateComputerThinking> state;

  buildState(state);
}
