/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/ChessStateRequestDropPiece.hpp"

#include "../src/chess/ChessGame.hpp"
#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "mocks/ChessEngineMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateRequestDropPiece)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateRequestDropPiece>& state)
  {
    state.reset(new ChessStateRequestDropPiece(
        m_statesPool, m_hardware, m_chessGame, m_engine));
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_hardware;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_statesPool;
  }

  ChessEngineMock& getChessEngine()
  {
    return m_engine;
  }

private:
  ChessStatesPoolMock m_statesPool;
  ChessHardwareMock m_hardware;
  ChessGame m_chessGame;
  ChessEngineMock m_engine;
};

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, boardChangedPieceDroppedOnOriginalSquare)
{
  MoveInfo moveInfo;
  Square from(File::E, Rank::FOUR);
  Square to(File::D, Rank::FIVE);
  Move computedMove(from, to);
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move d7d5(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  BitBoard newStatus;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  getChessGame().applyMove(e2e4);
  getChessGame().applyMove(d7d5);
  newStatus = getChessGame().allPieces();
  newStatus.clear(to);
  newStatus.set(from);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, boardChangedPieceDropped)
{
  MoveInfo moveInfo;
  Square from(File::E, Rank::FOUR);
  Square to(File::D, Rank::FIVE);
  Move computedMove(from, to);
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move d7d5(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  BitBoard newStatus;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  getChessGame().applyMove(e2e4);
  getChessGame().applyMove(d7d5);
  newStatus = getChessGame().allPieces();
  newStatus.clear(from);
  newStatus.set(to);

  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();

  // Move was played
  CHECK(Color::BLACK == getChessGame().currentPlayer());
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, boardChangedWrongPieceLifted)
{
  MoveInfo moveInfo;
  Square from(File::E, Rank::FOUR);
  Square to(File::D, Rank::FIVE);
  Move computedMove(from, to);
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move d7d5(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  BitBoard newStatus;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  moveInfo.setMove(computedMove);
  getChessGame().applyMove(e2e4);
  getChessGame().applyMove(d7d5);
  newStatus = getChessGame().allPieces();
  newStatus.clear(from);
  newStatus.clear(Square(File::A, Rank::EIGHT));


  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, exit)
{
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, enterWithQueenPromotion)
{
  MoveInfo moveInfo;
  Move computedMove(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  Piece promotion = Piece::QUEEN;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;
  computedMove.setPromotion(promotion);
  moveInfo.setMove(computedMove);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::QUEEN);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, enterWithBishopPromotion)
{
  MoveInfo moveInfo;
  Move computedMove(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  Piece promotion = Piece::BISHOP;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;
  computedMove.setPromotion(promotion);
  moveInfo.setMove(computedMove);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::BISHOP);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, enterWithKnighPromotion)
{
  MoveInfo moveInfo;
  Move computedMove(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  Piece promotion = Piece::KNIGHT;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;
  computedMove.setPromotion(promotion);
  moveInfo.setMove(computedMove);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::KNIGHT);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, enterWithRookPromotion)
{
  MoveInfo moveInfo;
  Move computedMove(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  Piece promotion = Piece::ROOK;
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;
  computedMove.setPromotion(promotion);
  moveInfo.setMove(computedMove);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::ROOK);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, enter)
{
  MoveInfo moveInfo;
  Move computedMove(Square(File::E, Rank::FOUR), Square(File::D, Rank::FIVE));
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
  IChessHardwareObserver& hwObserver = *state;
  moveInfo.setMove(computedMove);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("lastFoundMove").onObject(&getChessEngine()).
      andReturnValue(&moveInfo);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, init)
{
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);

  CHECK_EQUAL(0, state->init());
}

//------------------------------------------------------------------------------
TEST(ChessStateRequestDropPiece, constructor)
{
  std::unique_ptr<ChessStateRequestDropPiece> state;

  buildState(state);
}
