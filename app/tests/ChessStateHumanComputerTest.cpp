/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/states/ChessStateHumanComputer.hpp"

#include "../src/chess/ChessGame.hpp"
#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "mocks/ChessStateMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateHumanComputer)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateHumanComputer>& state)
  {
    m_humanState = new ChessStateMock();
    m_computerState = new ChessStateMock();
    std::unique_ptr<IChessState> humanState(m_humanState);
    std::unique_ptr<IChessState> computerState(m_computerState);
    state.reset(new ChessStateHumanComputer(
        m_chessGame, m_aisSettings, humanState, computerState));
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

  ChessStateMock* getHumanState()
  {
    return m_humanState;
  }

  ChessStateMock* getComputerState()
  {
    return m_computerState;
  }

  AisSettings& getAisSettings()
  {
    return m_aisSettings;
  }

private:
  ChessGame m_chessGame;
  AisSettings m_aisSettings;
  ChessStateMock* m_humanState;
  ChessStateMock* m_computerState;
};

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, exitComputerBlackTurnTurn)
{
  AiSettings blackAi;
  std::unique_ptr<ChessStateHumanComputer> state;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  buildState(state);
  blackAi.setEngineName("CPU");
  getAisSettings().setBlackAiSettings(blackAi);
  getChessGame().applyMove(move);

  mock().expectOneCall("exit").onObject(getComputerState());

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, exitHumanTurn)
{
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);

  mock().expectOneCall("exit").onObject(getHumanState());

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, enterHumanBlackTurn)
{
  AiSettings whiteAi;
  std::unique_ptr<ChessStateHumanComputer> state;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  buildState(state);
  whiteAi.setEngineName("CPU");
  getAisSettings().setWhiteAiSettings(whiteAi);
  getChessGame().applyMove(move);

  mock().expectOneCall("enter").onObject(getHumanState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, enterComputerWhiteTurn)
{
  AiSettings whiteAi;
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);
  whiteAi.setEngineName("CPU");
  getAisSettings().setWhiteAiSettings(whiteAi);

  mock().expectOneCall("enter").onObject(getComputerState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, enterHumanWhiteTurn)
{
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);

  mock().expectOneCall("enter").onObject(getHumanState());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, initSuccess)
{
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getHumanState()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getComputerState()).
      andReturnValue(0);

  CHECK_EQUAL(0, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, initComputerFails)
{
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getHumanState()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getComputerState()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, initHumanStateFails)
{
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);

  mock().expectOneCall("init").onObject(getHumanState()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, state->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanComputer, constructor)
{
  std::unique_ptr<ChessStateHumanComputer> state;

  buildState(state);
}
