/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/PlayerPiece.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PlayerPiece)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharInvalidColor)
{
  PlayerPiece pp(Color::COUNT, Piece::PAWN);

  CHECK_EQUAL(0, pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharInvalidPiece)
{
  PlayerPiece pp(Color::WHITE, Piece::COUNT);

  CHECK_EQUAL(0, pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharWhiteKing)
{
  PlayerPiece pp(Color::WHITE, Piece::KING);

  CHECK_EQUAL('K', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharWhiteQueen)
{
  PlayerPiece pp(Color::WHITE, Piece::QUEEN);

  CHECK_EQUAL('Q', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharWhiteBishop)
{
  PlayerPiece pp(Color::WHITE, Piece::BISHOP);

  CHECK_EQUAL('B', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharWhiteKnight)
{
  PlayerPiece pp(Color::WHITE, Piece::KNIGHT);

  CHECK_EQUAL('N', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharWhiteRook)
{
  PlayerPiece pp(Color::WHITE, Piece::ROOK);

  CHECK_EQUAL('R', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharBlackPawn)
{
  PlayerPiece pp(Color::BLACK, Piece::PAWN);

  CHECK_EQUAL('p', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, toCharWhitePawn)
{
  PlayerPiece pp(Color::WHITE, Piece::PAWN);

  CHECK_EQUAL('P', pp.toChar());
}

//------------------------------------------------------------------------------
TEST(PlayerPiece, constructor)
{
  PlayerPiece pp;
}
