/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/chess/rules/PieceRulesPawn.hpp"

#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PieceRulesPawn)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, pawnMoveIsNeverAmbiguous)
{
  BitBoard whitePawns;
  BitBoard blackPawns;
  ChessGame cg;
  Move move(Square(File::B, Rank::FOUR), Square(File::C, Rank::FIVE));
  PieceRulesPawn prp;

  whitePawns.set(Square(File::B, Rank::FOUR));
  whitePawns.set(Square(File::D, Rank::FOUR));
  blackPawns.set(Square(File::C, Rank::FIVE));
  cg.setPlayerPieces(Piece::PAWN, whitePawns);
  cg.setOpponentPieces(Piece::PAWN, blackPawns);

  prp.applyMove(move, cg);

  CHECK_FALSE(move.isFileAmbiguous());
  CHECK_FALSE(move.isRankAmbiguous());
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, promotionalMove)
{
  Square from(File::E, Rank::SEVEN);
  Square to(File::F, Rank::EIGHT);
  Piece promotion = Piece::BISHOP;
  Move move(from, to);
  BitBoard whitePawns;
  BitBoard expectedWhitePawns;
  BitBoard expectedBishops;
  ChessGame cg;
  PieceRulesPawn pmp;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  whitePawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, whitePawns);
  move.setPromotion(promotion);
  expectedBishops = cg.playerPieces(Piece::BISHOP);
  expectedBishops.set(to);

  CHECK(pmp.applyMove(move, cg));

  BitBoard whiteBishop = cg.playerPieces(Piece::BISHOP);
  whitePawns = cg.playerPieces(Piece::PAWN);
  CHECK_EQUAL(expectedBishops, whiteBishop);
  CHECK_EQUAL(expectedWhitePawns, whitePawns);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, promotionalMoveMissingPromotion)
{
  Square from(File::E, Rank::SEVEN);
  Square to(File::F, Rank::EIGHT);
  Move move(from, to);
  BitBoard whitePawns;
  ChessGame cg;
  PieceRulesPawn pmp;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  whitePawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, whitePawns);

  CHECK_FALSE(pmp.applyMove(move, cg));
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapWithDiscoveredCheck)
{
  ChessGame cg;
  Square from(File::D, Rank::TWO);
  BitBoard blackQueen;
  BitBoard expecteMoveMap;
  BitBoard moveMap;
  PieceRulesPawn pmp;

  blackQueen.set(Square(File::B, Rank::FOUR));
  cg.setOpponentPieces(Piece::QUEEN, blackQueen);

  moveMap = pmp.moveMap(from, cg);
  CHECK_EQUAL(expecteMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, blackAttackMap)
{
  ChessGame cg;
  Square from(File::B, Rank::SEVEN);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  PieceRulesPawn pmp;

  expectedAttackMap.set(Square(File::A, Rank::SIX));
  expectedAttackMap.set(Square(File::C, Rank::SIX));

  attackMap = pmp.attackMap(from, cg);
  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, whiteAttackMapOnRankH)
{
  ChessGame cg;
  Square from(File::H, Rank::TWO);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  PieceRulesPawn pmp;

  expectedAttackMap.set(Square(File::G, Rank::THREE));

  attackMap = pmp.attackMap(from, cg);
  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, whiteAttackMapOnRankA)
{
  ChessGame cg;
  Square from(File::A, Rank::TWO);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  PieceRulesPawn pmp;

  expectedAttackMap.set(Square(File::B, Rank::THREE));

  attackMap = pmp.attackMap(from, cg);
  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, whiteAttackMap)
{
  ChessGame cg;
  Square from(File::B, Rank::TWO);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  PieceRulesPawn pmp;

  expectedAttackMap.set(Square(File::A, Rank::THREE));
  expectedAttackMap.set(Square(File::C, Rank::THREE));

  attackMap = pmp.attackMap(from, cg);
  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapBlackInitialWithCaptureLeft)
{
  ChessGame cg;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));
  Square from(File::B, Rank::SEVEN);
  BitBoard moveMap;
  BitBoard whitePawns;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  cg.applyMove(move);
  whitePawns.set(Square(File::A, Rank::SIX));
  cg.setOpponentPieces(Piece::PAWN, whitePawns);

  expectedMoves.set(Square(File::B, Rank::SIX));
  expectedMoves.set(Square(File::B, Rank::FIVE));
  expectedMoves.set(Square(File::A, Rank::SIX));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapBlackInitialWithCaptureRight)
{
  ChessGame cg;
  Square from(File::B, Rank::SEVEN);
  BitBoard moveMap;
  BitBoard whitePawns;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  cg.applyMove(move);
  whitePawns.set(Square(File::C, Rank::SIX));
  cg.setOpponentPieces(Piece::PAWN, whitePawns);

  expectedMoves.set(Square(File::B, Rank::SIX));
  expectedMoves.set(Square(File::B, Rank::FIVE));
  expectedMoves.set(Square(File::C, Rank::SIX));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapBlackInitialCanAdvanceTwo)
{
  ChessGame cg;
  Square from(File::B, Rank::SEVEN);
  BitBoard moveMap;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  cg.applyMove(move);

  expectedMoves.set(Square(File::B, Rank::SIX));
  expectedMoves.set(Square(File::B, Rank::FIVE));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapBlackCanAdvanceOnlyOneForward)
{
  ChessGame cg;
  Square from(File::B, Rank::SIX);
  BitBoard currentPlayerPawns;
  BitBoard moveMap;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));

  cg.applyMove(move);
  currentPlayerPawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, currentPlayerPawns);
  expectedMoves.set(Square(File::B, Rank::FIVE));

  moveMap = pmp.moveMap(from, cg);

  CHECK_EQUAL(expectedMoves, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, applyMoveValidWhiteMoveCaptureOpponentPiece)
{
  Square from(File::A, Rank::TWO);
  Square to(File::B, Rank::THREE);
  Move move(from, to);
  ChessGame game;
  BitBoard blackPawns;
  PieceRulesPawn pmp;

  blackPawns.set(to);
  game.setOpponentPieces(Piece::PAWN, blackPawns);

  CHECK(pmp.applyMove(move, game));

  blackPawns = game.opponentPieces();

  CHECK_FALSE(blackPawns.isSet(to));
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, applyMoveValidWhitePawnMove)
{
  Square from(File::A, Rank::TWO);
  Square to(File::A, Rank::FOUR);
  Move move(from, to);
  ChessGame game;
  BitBoard whitePieces;
  PieceRulesPawn pmp;

  CHECK(pmp.applyMove(move, game));

  whitePieces = game.playerPieces();
  CHECK_FALSE(whitePieces.isSet(from));
  CHECK(whitePieces.isSet(to));
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteInitialWhiteCannotCaptureLeftOverTheBoard)
{
  ChessGame cg;
  Square from(File::A, Rank::TWO);
  BitBoard moveMap;
  BitBoard blackPawns;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  blackPawns.set(Square(File::H, Rank::FOUR));
  cg.setOpponentPieces(Piece::PAWN, blackPawns);

  expectedMoves.set(Square(File::A, Rank::THREE));
  expectedMoves.set(Square(File::A, Rank::FOUR));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteInitialWithCaptureLeft)
{
  ChessGame cg;
  Square from(File::B, Rank::TWO);
  BitBoard moveMap;
  BitBoard blackPawns;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  blackPawns.set(Square(File::A, Rank::THREE));
  cg.setOpponentPieces(Piece::PAWN, blackPawns);

  expectedMoves.set(Square(File::B, Rank::THREE));
  expectedMoves.set(Square(File::B, Rank::FOUR));
  expectedMoves.set(Square(File::A, Rank::THREE));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteInitialWithCannotCaptureRightOverTheBoard)
{
  ChessGame cg;
  Square from(File::H, Rank::TWO);
  BitBoard moveMap;
  BitBoard blackPawns;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  blackPawns.set(Square(File::A, Rank::TWO));
  cg.setOpponentPieces(Piece::PAWN, blackPawns);

  expectedMoves.set(Square(File::H, Rank::THREE));
  expectedMoves.set(Square(File::H, Rank::FOUR));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteInitialWithCaptureRight)
{
  ChessGame cg;
  Square from(File::B, Rank::TWO);
  BitBoard moveMap;
  BitBoard blackPawns;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  blackPawns.set(Square(File::C, Rank::THREE));
  cg.setOpponentPieces(Piece::PAWN, blackPawns);

  expectedMoves.set(Square(File::B, Rank::THREE));
  expectedMoves.set(Square(File::B, Rank::FOUR));
  expectedMoves.set(Square(File::C, Rank::THREE));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteInitialCannotAdvanceTwoWhenOccupied)
{
  ChessGame cg;
  Square from(File::B, Rank::TWO);
  BitBoard blackPawns;
  BitBoard moveMap;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  blackPawns.set(Square(File::B, Rank::FOUR));
  cg.setOpponentPieces(Piece::PAWN, blackPawns);
  expectedMoves.set(Square(File::B, Rank::THREE));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteInitialCanAdvanceTwo)
{
  ChessGame cg;
  Square from(File::B, Rank::TWO);
  BitBoard moveMap;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  expectedMoves.set(Square(File::B, Rank::THREE));
  expectedMoves.set(Square(File::B, Rank::FOUR));

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteCannotAdvanceIfWhitwPieceToFront)
{
  ChessGame cg;
  Square from(File::B, Rank::THREE);
  BitBoard moveMap;
  BitBoard expectedMoves;
  BitBoard whitePawnBitboard;
  PieceRulesPawn pmp;

  whitePawnBitboard.set(from);
  whitePawnBitboard.set(Square(File::B, Rank::FOUR));
  cg.setPlayerPieces(Piece::PAWN, whitePawnBitboard);

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteCannotAdvanceIfBlackPieceToFront)
{
  ChessGame cg;
  Square from(File::B, Rank::THREE);
  BitBoard currentPlayerPawns;
  BitBoard moveMap;
  BitBoard expectedMoves;
  BitBoard blackPawnsBitBoard;
  PieceRulesPawn pmp;

  currentPlayerPawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, currentPlayerPawns);
  blackPawnsBitBoard.set(Square(File::B, Rank::FOUR));
  cg.setOpponentPieces(Piece::PAWN, blackPawnsBitBoard);

  moveMap = pmp.moveMap(from, cg);

  CHECK(expectedMoves == moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, moveMapwhiteCanAdvanceOnlyOneForward)
{
  ChessGame cg;
  Square from(File::B, Rank::THREE);
  BitBoard currentPlayerPawns;
  BitBoard moveMap;
  BitBoard expectedMoves;
  PieceRulesPawn pmp;

  currentPlayerPawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, currentPlayerPawns);
  expectedMoves.set(Square(File::B, Rank::FOUR));
  moveMap = pmp.moveMap(from, cg);

  CHECK(moveMap == expectedMoves);
}

//------------------------------------------------------------------------------
TEST(PieceRulesPawn, constructor)
{
  PieceRulesPawn pmp;
}
