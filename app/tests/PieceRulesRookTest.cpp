/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/chess/rules/PieceRulesRook.hpp"

#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PieceRulesRook)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PieceRulesRook, moveMapWithDiscoveredCheck)
{
  ChessGame cg;
  Square from(File::D, Rank::TWO);
  BitBoard blackQueen;
  BitBoard whiteRook;
  BitBoard expecteMoveMap;
  BitBoard moveMap;
  PieceRulesRook prr;

  whiteRook.set(from);
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, whiteRook);
  blackQueen.set(Square(File::B, Rank::FOUR));
  cg.setOpponentPieces(Piece::QUEEN, blackQueen);

  moveMap = prr.moveMap(from, cg);
  CHECK_EQUAL(expecteMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesRook, atackMap)
{
  Square from(File::A, Rank::ONE);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  ChessGame cg;
  PieceRulesRook prr;

  expectedAttackMap.set(Square(File::A, Rank::TWO));
  expectedAttackMap.set(Square(File::B, Rank::ONE));

  attackMap = prr.attackMap(from, cg);

  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesRook, moveMapIsBlockByPlayePieces)
{
  Square from(File::G, Rank::FOUR);
  ChessGame cg;
  BitBoard expectedMoveMap;
  BitBoard moveMap;
  PieceRulesRook prr;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  expectedMoveMap.set(Square(File::G, Rank::SEVEN));
  expectedMoveMap.set(Square(File::G, Rank::EIGHT));
  expectedMoveMap.set(Square(File::A, Rank::FOUR));
  expectedMoveMap.set(Square(File::B, Rank::FOUR));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::FOUR));
  expectedMoveMap.set(Square(File::H, Rank::FOUR));

  moveMap = prr.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesRook, moveMapBlockedByOpponent)
{
  Square from(File::G, Rank::FOUR);
  ChessGame cg;
  BitBoard expectedMoveMap;
  BitBoard moveMap;
  PieceRulesRook prr;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  expectedMoveMap.set(Square(File::G, Rank::ONE));
  expectedMoveMap.set(Square(File::G, Rank::TWO));
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  expectedMoveMap.set(Square(File::G, Rank::SEVEN));
  expectedMoveMap.set(Square(File::A, Rank::FOUR));
  expectedMoveMap.set(Square(File::B, Rank::FOUR));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::FOUR));
  expectedMoveMap.set(Square(File::H, Rank::FOUR));

  moveMap = prr.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesRook, moveMapCanGoAnyWhere)
{
  Square from(File::G, Rank::FOUR);
  ChessGame cg;
  BitBoard expectedMoveMap;
  BitBoard moveMap;
  PieceRulesRook prr;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  expectedMoveMap.set(Square(File::G, Rank::ONE));
  expectedMoveMap.set(Square(File::G, Rank::TWO));
  expectedMoveMap.set(Square(File::G, Rank::THREE));
  expectedMoveMap.set(Square(File::G, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  expectedMoveMap.set(Square(File::G, Rank::SEVEN));
  expectedMoveMap.set(Square(File::G, Rank::EIGHT));
  expectedMoveMap.set(Square(File::A, Rank::FOUR));
  expectedMoveMap.set(Square(File::B, Rank::FOUR));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::D, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::FOUR));
  expectedMoveMap.set(Square(File::H, Rank::FOUR));

  moveMap = prr.moveMap(from, cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesRook, piece)
{
  PieceRulesRook prr;

  CHECK(Piece::ROOK == prr.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesRook, constructor)
{
  PieceRulesRook prr;
}
