/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/chess/rules/PieceRulesKnight.hpp"

#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PieceRulesKnight)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }

  void clearPawns(ChessGame& cg)
  {
    cg.setPlayerPieces(Piece::PAWN, BitBoard());
    cg.setOpponentPieces(Piece::PAWN, BitBoard());
  }
};

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyBothAmbiguous)
{
  BitBoard whiteKnights;
  ChessGame cg;
  Move move(Square(File::B, Rank::ONE), Square(File::C, Rank::THREE));
  PieceRulesKnight prb;

  whiteKnights.set(Square(File::A, Rank::FOUR));
  whiteKnights.set(Square(File::B, Rank::ONE));
  whiteKnights.set(Square(File::D, Rank::FIVE));
  cg.setPlayerPieces(Piece::KNIGHT, whiteKnights);

  prb.applyMove(move, cg);

  CHECK(move.isFileAmbiguous());
  CHECK(move.isRankAmbiguous());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMoveAmbiguousNotStartingFromSameRankNeitherFile)
{
  BitBoard whiteKnights;
  ChessGame cg;
  Move move(Square(File::C, Rank::THREE), Square(File::D, Rank::FIVE));
  PieceRulesKnight prb;

  whiteKnights.set(Square(File::B, Rank::FOUR));
  whiteKnights.set(Square(File::C, Rank::THREE));
  cg.setPlayerPieces(Piece::KNIGHT, whiteKnights);

  prb.applyMove(move, cg);

  CHECK(move.isFileAmbiguous());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMoveRankAmbiguous)
{
  BitBoard whiteKnights;
  ChessGame cg;
  Move move(Square(File::C, Rank::THREE), Square(File::E, Rank::FOUR));
  PieceRulesKnight prb;

  whiteKnights.set(Square(File::C, Rank::THREE));
  whiteKnights.set(Square(File::C, Rank::FIVE));
  cg.setPlayerPieces(Piece::KNIGHT, whiteKnights);

  prb.applyMove(move, cg);

  CHECK(move.isRankAmbiguous());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMoveFileAmbiguous)
{
  BitBoard whiteKnights;
  ChessGame cg;
  Move move(Square(File::C, Rank::THREE), Square(File::D, Rank::FIVE));
  PieceRulesKnight prb;

  whiteKnights.set(Square(File::C, Rank::THREE));
  whiteKnights.set(Square(File::E, Rank::THREE));
  cg.setPlayerPieces(Piece::KNIGHT, whiteKnights);

  prb.applyMove(move, cg);

  CHECK(move.isFileAmbiguous());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMoveCaptureUpdated)
{
  Square to(File::C, Rank::THREE);
  Move move(Square(File::B, Rank::ONE), to);
  BitBoard blackPawns;
  ChessGame cg;
  PieceRulesKnight prb;

  blackPawns.set(to);
  cg.setOpponentPieces(Piece::PAWN, blackPawns);

  prb.applyMove(move, cg);

  CHECK(move.isCapture());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMoveSourceUpdated)
{
  Move move(Square(File::B, Rank::ONE), Square(File::C, Rank::THREE));
  ChessGame cg;
  PieceRulesKnight prb;

  prb.applyMove(move, cg);

  CHECK(Piece::KNIGHT == move.getMovedPiece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapWithDiscoveredCheck)
{
  ChessGame cg;
  Square from(File::D, Rank::TWO);
  BitBoard blackQueen;
  BitBoard whiteKnight;
  BitBoard expecteMoveMap;
  BitBoard moveMap;
  PieceRulesKnight prb;

  whiteKnight.set(from);
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, whiteKnight);
  blackQueen.set(Square(File::B, Rank::FOUR));
  cg.setOpponentPieces(Piece::QUEEN, blackQueen);

  moveMap = prb.moveMap(from, cg);
  CHECK_EQUAL(expecteMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, attackMap)
{
  ChessGame cg;
  Square from(File::B, Rank::ONE);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  PieceRulesKnight prk;

  expectedAttackMap.set(Square(File::A, Rank::THREE));
  expectedAttackMap.set(Square(File::C, Rank::THREE));
  expectedAttackMap.set(Square(File::D, Rank::TWO));

  attackMap = prk.attackMap(from, cg);
  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMoveIllegal)
{
  ChessGame cg;
  Square from(File::B, Rank::ONE);
  Square to(File::C, Rank::FOUR);
  Move move(from, to);
  BitBoard allPieces;
  PieceRulesKnight prk;

  CHECK_FALSE(prk.applyMove(move, cg));
  allPieces = cg.allPieces();
  CHECK(allPieces.isSet(from));
  CHECK_FALSE(allPieces.isSet(to));
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, applyMove)
{
  ChessGame cg;
  Square from(File::B, Rank::ONE);
  Square to(File::C, Rank::THREE);
  Move move(from, to);
  BitBoard allPieces;
  PieceRulesKnight prk;

  CHECK(prk.applyMove(move, cg));
  allPieces = cg.allPieces();
  CHECK_FALSE(allPieces.isSet(from));
  CHECK(allPieces.isSet(to));
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoSouth)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::B, Rank::TWO));
  expectedMoveMap.set(Square(File::C, Rank::THREE));
  expectedMoveMap.set(Square(File::E, Rank::THREE));
  expectedMoveMap.set(Square(File::F, Rank::TWO));
  clearPawns(cg);

  moveMap = prk.moveMap(Square(File::D, Rank::ONE), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoSouthSouth)
{
  ChessGame cg;
  BitBoard empty;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::B, Rank::ONE));
  expectedMoveMap.set(Square(File::B, Rank::THREE));
  expectedMoveMap.set(Square(File::C, Rank::FOUR));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::F, Rank::ONE));
  clearPawns(cg);
  cg.setPlayerPieces(Piece::BISHOP, empty);
  cg.setPlayerPieces(Piece::KNIGHT, empty);

  moveMap = prk.moveMap(Square(File::D, Rank::TWO), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoEast)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  expectedMoveMap.set(Square(File::G, Rank::TWO));
  expectedMoveMap.set(Square(File::G, Rank::SIX));
  clearPawns(cg);

  moveMap = prk.moveMap(Square(File::H, Rank::FOUR), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCanGoAnywhereButHasPanws)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard whitePawns;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  whitePawns.set(Square(File::B, Rank::THREE));
  whitePawns.set(Square(File::B, Rank::FIVE));
  whitePawns.set(Square(File::C, Rank::TWO));
  whitePawns.set(Square(File::C, Rank::SIX));
  whitePawns.set(Square(File::E, Rank::TWO));
  whitePawns.set(Square(File::E, Rank::SIX));
  whitePawns.set(Square(File::F, Rank::THREE));
  whitePawns.set(Square(File::F, Rank::FIVE));
  clearPawns(cg);
  cg.setPlayerPieces(Piece::PAWN, whitePawns);

  moveMap = prk.moveMap(Square(File::D, Rank::FOUR), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoEastEast)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::E, Rank::THREE));
  expectedMoveMap.set(Square(File::E, Rank::FIVE));
  expectedMoveMap.set(Square(File::F, Rank::TWO));
  expectedMoveMap.set(Square(File::F, Rank::SIX));
  expectedMoveMap.set(Square(File::H, Rank::SIX));
  expectedMoveMap.set(Square(File::H, Rank::TWO));
  clearPawns(cg);

  moveMap = prk.moveMap(Square(File::G, Rank::FOUR), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoNorth)
{
  ChessGame cg;
  BitBoard blackKing;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::A, Rank::SEVEN));
  expectedMoveMap.set(Square(File::B, Rank::SIX));
  expectedMoveMap.set(Square(File::D, Rank::SIX));
  expectedMoveMap.set(Square(File::E, Rank::SEVEN));
  clearPawns(cg);
  // No check!
  blackKing.set(Square(File::G, Rank::THREE));
  cg.setOpponentPieces(Piece::KING, blackKing);

  moveMap = prk.moveMap(Square(File::C, Rank::EIGHT), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoNorthNorth)
{
  ChessGame cg;
  BitBoard blackKing;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::A, Rank::EIGHT));
  expectedMoveMap.set(Square(File::A, Rank::SIX));
  expectedMoveMap.set(Square(File::B, Rank::FIVE));
  expectedMoveMap.set(Square(File::D, Rank::FIVE));
  expectedMoveMap.set(Square(File::E, Rank::SIX));
  expectedMoveMap.set(Square(File::E, Rank::EIGHT));
  clearPawns(cg);
  // No check!
  blackKing.set(Square(File::G, Rank::THREE));
  cg.setOpponentPieces(Piece::KING, blackKing);

  moveMap = prk.moveMap(Square(File::C, Rank::SEVEN), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoWest)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::B, Rank::TWO));
  expectedMoveMap.set(Square(File::B, Rank::SIX));
  expectedMoveMap.set(Square(File::C, Rank::THREE));
  expectedMoveMap.set(Square(File::C, Rank::FIVE));
  clearPawns(cg);

  moveMap = prk.moveMap(Square(File::A, Rank::FOUR), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCannotGoWestWest)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::A, Rank::TWO));
  expectedMoveMap.set(Square(File::A, Rank::SIX));
  expectedMoveMap.set(Square(File::C, Rank::SIX));
  expectedMoveMap.set(Square(File::C, Rank::TWO));
  expectedMoveMap.set(Square(File::D, Rank::FIVE));
  expectedMoveMap.set(Square(File::D, Rank::THREE));
  clearPawns(cg);

  moveMap = prk.moveMap(Square(File::B, Rank::FOUR), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, moveMapCanGoAnywhere)
{
  ChessGame cg;
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  PieceRulesKnight prk;

  expectedMoveMap.set(Square(File::B, Rank::THREE));
  expectedMoveMap.set(Square(File::B, Rank::FIVE));
  expectedMoveMap.set(Square(File::C, Rank::TWO));
  expectedMoveMap.set(Square(File::C, Rank::SIX));
  expectedMoveMap.set(Square(File::E, Rank::TWO));
  expectedMoveMap.set(Square(File::E, Rank::SIX));
  expectedMoveMap.set(Square(File::F, Rank::THREE));
  expectedMoveMap.set(Square(File::F, Rank::FIVE));
  clearPawns(cg);

  moveMap = prk.moveMap(Square(File::D, Rank::FOUR), cg);

  CHECK_EQUAL(expectedMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, pieceIsKnight)
{
  PieceRulesKnight prk;

  CHECK(Piece::KNIGHT == prk.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesKnight, constructor)
{
  PieceRulesKnight prk;
}
