/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/driver/CgcProtocolSerializer.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(CgcProtocolSerializer)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(CgcProtocolSerializer, serializeWrite)
{
  unsigned int buffSize = 32;
  char buffer[buffSize];
  uint8_t reg = 0x34;
  uint8_t value = 0x56;
  unsigned int serializedSize;
  CgcProtocolSerializer serializer;

  CHECK_EQUAL(0,
      serializer.serializeWriteRequest(
          buffer, buffSize, reg, value, serializedSize));
  MEMCMP_EQUAL("3456\n", buffer, 5);
  CHECK_EQUAL(5, serializedSize);
}

//------------------------------------------------------------------------------
TEST(CgcProtocolSerializer, serializerRead)
{
  unsigned int buffSize = 32;
  char buffer[buffSize];
  uint8_t reg = 0x34;
  unsigned int serializedSize;
  CgcProtocolSerializer serializer;

  CHECK_EQUAL(0,
      serializer.serializeReadRequest(buffer, buffSize, reg, serializedSize));
  MEMCMP_EQUAL("34\n", buffer, 3);
  CHECK_EQUAL(3, serializedSize);
}

//------------------------------------------------------------------------------
TEST(CgcProtocolSerializer, constructor)
{
  CgcProtocolSerializer serializer;
}
