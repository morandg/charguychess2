/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "spies/BlinkingLedsSpy.hpp"

#include "mocks/ChessHardwareMock.hpp"

#include <rglib-mocks/comparators/StdStringComparator.hpp>
#include <rglib-mocks/copiers/TimerUniquePtrCopier.hpp>
#include <rglib-mocks/mocks/event-loop/EventLoopMock.hpp>
#include <rglib-mocks/mocks/event-loop/TimerMock.hpp>

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;
using namespace rglib::mocks;

//------------------------------------------------------------------------------
TEST_GROUP(BlinkingLeds)
{
  TEST_SETUP()
  {
    mock().installComparator("std::string", m_strComp);
    mock().installCopier("std::unique_ptr<ITimer>", m_timerCop);
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildBlinkingLeds(std::unique_ptr<BlinkingLedsSpy>& blinkingLeds)
  {
    blinkingLeds.reset(new BlinkingLedsSpy(m_hardware));
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_hardware;
  }

private:
  ChessHardwareMock m_hardware;
  StdStringComparator m_strComp;
  TimerUniquePtrCopier m_timerCop;
};

//------------------------------------------------------------------------------
TEST(BlinkingLeds, stopBlinkingLedsAreCleared)
{
  TimerMock* timer = new TimerMock();
  Led blinkLed = Led::A;
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  mock().expectOneCall("stop").onObject(timer);

  blinkingLeds->addLed(blinkLed);
  blinkingLeds->stop();
  blinkingLeds->start();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, stopBlinking)
{
  TimerMock* timer = new TimerMock();
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  mock().expectOneCall("stop").onObject(timer);

  blinkingLeds->stop();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, LedsAreOnAgainAfterSecondTimeoutCallBack)
{
  unsigned int onTimeout = 123;
  unsigned int offTimeout = 321;
  TimerMock* timer = new TimerMock();
  Led blinkLed = Led::A;
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  mock().strictOrder();
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", false).
      withParameter("led", (int)blinkLed);
  mock().expectOneCall("setTimeout").onObject(timer).
      withParameter("ms", offTimeout);
  mock().expectOneCall("start").onObject(timer);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)blinkLed);
  mock().expectOneCall("setTimeout").onObject(timer).
      withParameter("ms", onTimeout);
  mock().expectOneCall("start").onObject(timer);

  blinkingLeds->setOnTimeout(onTimeout);
  blinkingLeds->setOffTimeout(offTimeout);
  blinkingLeds->addLed(blinkLed);
  blinkingLeds->onTimeout(BlinkingLeds::BLINK_TIMER_NAME);
  blinkingLeds->onTimeout(BlinkingLeds::BLINK_TIMER_NAME);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, LedsAreOffOnTimeoutCallBack)
{
  unsigned int offTimeout = 321;
  TimerMock* timer = new TimerMock();
  Led blinkLed = Led::A;
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", false).
      withParameter("led", (int)blinkLed);
  mock().expectOneCall("setTimeout").onObject(timer).
      withParameter("ms", offTimeout);
  mock().expectOneCall("start").onObject(timer);

  blinkingLeds->setOffTimeout(offTimeout);
  blinkingLeds->addLed(blinkLed);
  blinkingLeds->onTimeout(BlinkingLeds::BLINK_TIMER_NAME);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, startAddTwiceSameLed)
{
  unsigned int onTimeout = 123;
  TimerMock* timer = new TimerMock();
  Led blinkLed = Led::A;
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)blinkLed);
  mock().expectOneCall("setTimeout").onObject(timer).
      withParameter("ms", onTimeout);
  mock().expectOneCall("start").onObject(timer);

  blinkingLeds->setOnTimeout(onTimeout);
  blinkingLeds->addLed(blinkLed);
  blinkingLeds->addLed(blinkLed);
  blinkingLeds->start();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, startHasLedToBlink)
{
  unsigned int onTimeout = 123;
  TimerMock* timer = new TimerMock();
  Led blinkLed = Led::A;
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)blinkLed);
  mock().expectOneCall("setTimeout").onObject(timer).
      withParameter("ms", onTimeout);
  mock().expectOneCall("start").onObject(timer);

  blinkingLeds->setOnTimeout(onTimeout);
  blinkingLeds->addLed(blinkLed);
  blinkingLeds->start();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, startNoLedToBlinkNothingHappen)
{
  TimerMock* timer = new TimerMock();
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  blinkingLeds->setTimer(timer);

  blinkingLeds->start();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, initSuccess)
{
  EventLoopMock eventLoop;
  TimerMock* timer = new TimerMock();
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  // cpp vtable
  rglib::ITimedOut& timedOut = *blinkingLeds;

  mock().expectOneCall("createTimer").onObject(&eventLoop).
      withOutputParameterOfTypeReturning(
          "std::unique_ptr<ITimer>", "timer", timer).
      withParameterOfType(
          "std::string", "name", &BlinkingLeds::BLINK_TIMER_NAME).
      withParameter("timedOut", &timedOut).
      andReturnValue(0);

  CHECK_EQUAL(0, blinkingLeds->init(eventLoop));

  mock().checkExpectations();

  POINTERS_EQUAL(timer, blinkingLeds->getTimer());
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, initCannotCreateTimer)
{
  EventLoopMock eventLoop;
  TimerMock* timer = new TimerMock();
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
  // cpp vtable
  rglib::ITimedOut& timedOut = *blinkingLeds;

  mock().expectOneCall("createTimer").onObject(&eventLoop).
      withOutputParameterOfTypeReturning(
          "std::unique_ptr<ITimer>", "timer", timer).
      withParameterOfType(
          "std::string", "name", &BlinkingLeds::BLINK_TIMER_NAME).
      withParameter("timedOut", &timedOut).
      andReturnValue(-1);

  CHECK_EQUAL(-1, blinkingLeds->init(eventLoop));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(BlinkingLeds, constructor)
{
  std::unique_ptr<BlinkingLedsSpy> blinkingLeds;

  buildBlinkingLeds(blinkingLeds);
}
