/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/Move.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(Move)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Move, toPGNPawnCapture)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setCapture(true);

  STRCMP_EQUAL("axa4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNToQueenPromotion)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setPromotion(Piece::QUEEN);

  STRCMP_EQUAL("a4=Q", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNQueenSideWithMate)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMate(true);
  move.setQueenSideCastling(true);

  STRCMP_EQUAL("0-0-0#", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNKingSideWithCheck)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setCheck(true);
  move.setKingSideCastling(true);

  STRCMP_EQUAL("0-0+", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNMateAndChecked)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);
  move.setMate(true);
  move.setCheck(true);

  STRCMP_EQUAL("Ra4#", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNMate)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);
  move.setMate(true);

  STRCMP_EQUAL("Ra4#", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNCheck)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);
  move.setCheck(true);

  STRCMP_EQUAL("Ra4+", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNKingQueenSideCastling)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setQueenSideCastling(true);

  STRCMP_EQUAL("0-0-0", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNKingSideCastling)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setKingSideCastling(true);

  STRCMP_EQUAL("0-0", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNCapture)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);
  move.setCapture(true);

  STRCMP_EQUAL("Rxa4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNRankAmbiguous)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);
  move.setRankAmbiguous(true);

  STRCMP_EQUAL("R2a4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNFileAmbiguous)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);
  move.setFileAmbiguous(true);

  STRCMP_EQUAL("Raa4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNRookMoved)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::ROOK);

  STRCMP_EQUAL("Ra4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNKingMoved)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::KING);

  STRCMP_EQUAL("Ka4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNKnightMoved)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::KNIGHT);

  STRCMP_EQUAL("Na4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNBishopMoved)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::BISHOP);

  STRCMP_EQUAL("Ba4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNQueenMoved)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  move.setMovedPiece(Piece::QUEEN);

  STRCMP_EQUAL("Qa4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toPGNNoPieceSpecified)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::FOUR));

  STRCMP_EQUAL("a4", move.toPGN().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toStringBishopPromotion)
{
  Piece promotion = Piece::BISHOP;
  Move move(Square(File::A, Rank::TWO), Square(File::B, Rank::TWO));

  move.setPromotion(promotion);

  STRCMP_EQUAL("a2b2b", move.toString().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toStringKinghtPromotion)
{
  Piece promotion = Piece::KNIGHT;
  Move move(Square(File::A, Rank::TWO), Square(File::B, Rank::TWO));

  move.setPromotion(promotion);

  STRCMP_EQUAL("a2b2k", move.toString().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, toStringRookPromotion)
{
  Piece promotion = Piece::ROOK;
  Move move(Square(File::A, Rank::TWO), Square(File::B, Rank::TWO));

  move.setPromotion(promotion);

  STRCMP_EQUAL("a2b2r", move.toString().c_str());
}

//------------------------------------------------------------------------------
TEST(Move, setPromotion)
{
  Piece promotion = Piece::BISHOP;
  Move move;

  move.setPromotion(promotion);

  CHECK(promotion == move.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, constructorFromSquaredefaultPromotionIsPawn)
{
  Move move(Square(File::A, Rank::TWO), Square(File::B, Rank::TWO));

  CHECK(Piece::PAWN == move.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, defaultPromotionIsPawn)
{
  Move move;

  CHECK(Piece::PAWN == move.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, towDifferentMovesAreNotEqual)
{
  Move move1(Square(File::A, Rank::TWO), Square(File::B, Rank::TWO));
  Move move2(Square(File::A, Rank::TWO), Square(File::C, Rank::TWO));

  CHECK(move1 != move2);
}

//------------------------------------------------------------------------------
TEST(Move, fromValidStringTooLong)
{
  Move m;

  CHECK_FALSE(m.fromString("e2e4asdf"));
}

//------------------------------------------------------------------------------
TEST(Move, fromStringWithQueenPromotion)
{
  Move m;

  CHECK(m.fromString("e7f8q"));
  CHECK(m.from().file() == File::E);
  CHECK(m.from().rank() == Rank::SEVEN);
  CHECK(m.to().file() == File::F);
  CHECK(m.to().rank() == Rank::EIGHT);
  CHECK(Piece::QUEEN == m.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, fromStringWithBishopPromotion)
{
  Move m;

  CHECK(m.fromString("e7f8b"));
  CHECK(m.from().file() == File::E);
  CHECK(m.from().rank() == Rank::SEVEN);
  CHECK(m.to().file() == File::F);
  CHECK(m.to().rank() == Rank::EIGHT);
  CHECK(Piece::BISHOP == m.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, fromStringWithKnightPromotion)
{
  Move m;

  CHECK(m.fromString("e7f8k"));
  CHECK(m.from().file() == File::E);
  CHECK(m.from().rank() == Rank::SEVEN);
  CHECK(m.to().file() == File::F);
  CHECK(m.to().rank() == Rank::EIGHT);
  CHECK(Piece::KNIGHT == m.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, fromStringWithRookPromotion)
{
  Move m;

  CHECK(m.fromString("e7f8r"));
  CHECK(m.from().file() == File::E);
  CHECK(m.from().rank() == Rank::SEVEN);
  CHECK(m.to().file() == File::F);
  CHECK(m.to().rank() == Rank::EIGHT);
  CHECK(Piece::ROOK == m.promotion());
}

//------------------------------------------------------------------------------
TEST(Move, fromValidString)
{
  Move m;

  CHECK(m.fromString("e2e4"));
  CHECK(m.from().file() == File::E);
  CHECK(m.from().rank() == Rank::TWO);
  CHECK(m.to().file() == File::E);
  CHECK(m.to().rank() == Rank::FOUR);
}

//------------------------------------------------------------------------------
TEST(Move, fromEmptyString)
{
  Move m;

  CHECK_FALSE(m.fromString(""));
}

//------------------------------------------------------------------------------
TEST(Move, constructor)
{
  Move m;
}
