/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"

#include "spies/ChessStatePieceLiftedSpy.hpp"
#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "comparators/SquareComparator.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(PieceLifted)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
    mock().installComparator("Square", m_squareComp);
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStatePieceLiftedSpy>& state)
  {
    state.reset(new ChessStatePieceLiftedSpy(
        m_chessStatesPool, m_chessHardwareMock, m_chessGame));
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_chessHardwareMock;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_chessStatesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

private:
  ChessStatesPoolMock m_chessStatesPool;
  ChessHardwareMock m_chessHardwareMock;
  ChessGame m_chessGame;
  SquareComparator m_squareComp;
};

//------------------------------------------------------------------------------
TEST(PieceLifted, promotionalMoveAppliedOnButtonReleased)
{
  BitBoard hardwareState;
  BitBoard whitePawns;
  Square from(File::B, Rank::SEVEN);
  Square to(File::B, Rank::EIGHT);
  Piece promotion = Piece::ROOK;
  Button pressedButton = Button::ROOK;
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setOpponentPieces(Piece::KNIGHT, BitBoard());
  whitePawns.set(from);
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);
  hardwareState.set(to);
  state->setPromotion(promotion);

  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onButtonPressed(pressedButton);
  state->onButtonReleased(pressedButton);

  mock().checkExpectations();

  Move playedMove = getChessGame().lastTurn().whiteMove();
  CHECK(playedMove == Move(from, to));
  CHECK(playedMove.promotion() == promotion);
}

//------------------------------------------------------------------------------
TEST(PieceLifted, boardChangedHasPromotion)
{
  BitBoard hardwareState;
  BitBoard whitePawns;
  Square from(File::B, Rank::SEVEN);
  Square to(File::B, Rank::EIGHT);
  Piece promotion = Piece::ROOK;
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setOpponentPieces(Piece::KNIGHT, BitBoard());
  whitePawns.set(from);
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);
  hardwareState.set(to);
  state->setPromotion(promotion);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();

  Move playedMove = getChessGame().lastTurn().whiteMove();
  CHECK(playedMove == Move(from, to));
  CHECK(playedMove.promotion() == promotion);
}

//------------------------------------------------------------------------------
TEST(PieceLifted, boardChangedMissingPromotion)
{
  BitBoard hardwareState;
  BitBoard whitePawns;
  Square from(File::B, Rank::SEVEN);
  Square to(File::B, Rank::EIGHT);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  getChessGame().setOpponentPieces(Piece::PAWN, BitBoard());
  getChessGame().setOpponentPieces(Piece::KNIGHT, BitBoard());
  whitePawns.set(from);
  getChessGame().setPlayerPieces(Piece::PAWN, whitePawns);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);
  hardwareState.set(to);

  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::ROOK);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::KNIGHT);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::BISHOP);
  mock().expectOneCall("turnLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("led", (int)Led::QUEEN);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, buttonPressedPromotionIsUpdated)
{
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);

  state->onButtonPressed(Button::QUEEN);

  CHECK(Piece::QUEEN == state->promotion());
}

//------------------------------------------------------------------------------
TEST(PieceLifted, twoDifferentColorPiecesLiftedCanBeCaptured)
{
  BitBoard hardwareState;
  Move move1(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move move2(Square(File::D, Rank::SEVEN), Square(File::D, Rank::FIVE));
  Square from(File::E, Rank::FOUR);
  Square to(File::D, Rank::FIVE);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  getChessGame().applyMove(move1);
  getChessGame().applyMove(move2);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(from);
  hardwareState.clear(to);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::CAPTURE);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, twoDifferentColorPiecesLiftedCannotBeCaptured)
{
  BitBoard hardwareState = getChessGame().allPieces();
  Square from(File::E, Rank::TWO);
  Square to(File::E, Rank::SEVEN);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  hardwareState.clear(from);
  hardwareState.clear(to);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, twoWhitePicesLiftedInvalidState)
{
  BitBoard hardwareState = getChessGame().allPieces();
  Square from(File::E, Rank::TWO);
  Square to(File::D, Rank::TWO);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  hardwareState.clear(from);
  hardwareState.clear(to);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, boardChangedInValidMove)
{
  BitBoard hardwareState = getChessGame().allPieces();
  Square from(File::E, Rank::TWO);
  Square to(File::E, Rank::FIVE);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  hardwareState.clear(from);
  hardwareState.set(to);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, boardChangedValidMove)
{
  BitBoard hardwareState = getChessGame().allPieces();
  Square from(File::E, Rank::TWO);
  Square to(File::E, Rank::FOUR);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  hardwareState.clear(from);
  hardwareState.set(to);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();

  CHECK(getChessGame().lastTurn().whiteMove() == Move(from, to));
}

//------------------------------------------------------------------------------
TEST(PieceLifted, boardChangedBackToGameStatus)
{
  BitBoard hardwareState = getChessGame().allPieces();
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(hardwareState);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, exit)
{
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  // cpp vtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, enterTurnLiftedPieceOnBlackPlayer)
{
  BitBoard hardwareState;
  Square liftedSquare(File::E, Rank::SEVEN);
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  // cpp vtable
  IChessHardwareObserver& hwObserver = *state;
  getChessGame().applyMove(e2e4);
  hardwareState = getChessGame().allPieces();
  hardwareState.clear(liftedSquare);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("turnAllLeds").onObject(&getChessHardware()).
      withParameter("isOn", false);
  mock().expectOneCall("turnPlayerLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("color", (int)Color::BLACK);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedSquare);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, enterPromotionIsReseted)
{
  BitBoard hardwareState = getChessGame().allPieces();
  Square liftedSquare(File::E, Rank::TWO);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  // cpp vtable
  IChessHardwareObserver& hwObserver = *state;
  state->setPromotion(Piece::QUEEN);
  hardwareState.clear(liftedSquare);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("turnAllLeds").onObject(&getChessHardware()).
      withParameter("isOn", false);
  mock().expectOneCall("turnPlayerLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("color", (int)Color::WHITE);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedSquare);

  state->enter();

  mock().checkExpectations();

  CHECK(Piece::PAWN == state->promotion());
}

//------------------------------------------------------------------------------
TEST(PieceLifted, enterTurnLiftedPieceOn)
{
  BitBoard hardwareState = getChessGame().allPieces();
  Square liftedSquare(File::E, Rank::TWO);
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
  // cpp vtable
  IChessHardwareObserver& hwObserver = *state;
  hardwareState.clear(liftedSquare);

  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hardwareState);
  mock().expectOneCall("turnAllLeds").onObject(&getChessHardware()).
      withParameter("isOn", false);
  mock().expectOneCall("turnPlayerLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("color", (int)Color::WHITE);
  mock().expectOneCall("turnSquareLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameterOfType("Square", "square", &liftedSquare);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(PieceLifted, constructor)
{
  std::unique_ptr<ChessStatePieceLiftedSpy> state;

  buildState(state);
}
