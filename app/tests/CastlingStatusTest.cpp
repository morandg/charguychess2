/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/CastlingStatus.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(CastlingStatus)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(CastlingStatus, canCastleAfterReset)
{
  CastlingStatus castlingStatus;

  castlingStatus.kingSideRookMoved(Color::WHITE);
  castlingStatus.queenRookMoved(Color::WHITE);
  castlingStatus.kingMoved(Color::WHITE);
  castlingStatus.kingSideRookMoved(Color::BLACK);
  castlingStatus.queenRookMoved(Color::BLACK);
  castlingStatus.kingMoved(Color::BLACK);
  castlingStatus.reset();

  CHECK(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, cannotCastleKingSideAfterBlackKingRookMove)
{
  CastlingStatus castlingStatus;

  castlingStatus.kingSideRookMoved(Color::BLACK);

  CHECK(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK_FALSE(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, cannotCastleKingSideAfterWhiteKingRookMove)
{
  CastlingStatus castlingStatus;

  castlingStatus.kingSideRookMoved(Color::WHITE);

  CHECK_FALSE(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, cannotCastleQueenSideAfterBlackQueenRookMove)
{
  CastlingStatus castlingStatus;

  castlingStatus.queenRookMoved(Color::BLACK);

  CHECK(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK_FALSE(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, cannotCastleQueenSideAfterWhiteQueenRookMove)
{
  CastlingStatus castlingStatus;

  castlingStatus.queenRookMoved(Color::WHITE);

  CHECK(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK_FALSE(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, cannotCastleAfterBlackKingMove)
{
  CastlingStatus castlingStatus;

  castlingStatus.kingMoved(Color::BLACK);

  CHECK(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK_FALSE(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK_FALSE(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, cannotCastleAfterWhiteKingMove)
{
  CastlingStatus castlingStatus;

  castlingStatus.kingMoved(Color::WHITE);

  CHECK_FALSE(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK_FALSE(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, allCanCastle)
{
  CastlingStatus castlingStatus;

  CHECK(castlingStatus.isKingSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::WHITE));
  CHECK(castlingStatus.isKingSideCastleAllowed(Color::BLACK));
  CHECK(castlingStatus.isQueenSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(CastlingStatus, constructor)
{
  CastlingStatus castlingStatus;
}
