/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/hardware/states/ChessStateHumanThinking.hpp"

#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateHumanThinking)
{
  TEST_SETUP()
  {
    m_chessGame.newGame();
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateHumanThinking>& state)
  {
    state.reset(new ChessStateHumanThinking(
        m_chessStatesPool, m_chessHardwareMock, m_chessGame));
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_chessHardwareMock;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_chessStatesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

private:
  ChessStatesPoolMock m_chessStatesPool;
  ChessHardwareMock m_chessHardwareMock;
  ChessGame m_chessGame;
};

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, piecesLiftedNotMovable)
{
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
  newStatus.clear(Square(File::A, Rank::ONE));

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, boardChangedTooManyPieces)
{
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
  newStatus.set(Square(File::A, Rank::THREE));

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::INVALID_POSITION);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, boardChangedMovablePieceLifted)
{
  BitBoard newStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
  newStatus.clear(Square(File::E, Rank::TWO));

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", IChessStatesPool::PIECE_LIFTED);

  state->onBoardChanged(newStatus);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, exit)
{
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
  // ccp vtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, enterBlackPlayerTurn)
{
  Move m(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
  // ccp vtable
  IChessHardwareObserver& hwObserver = *state;

  getChessGame().applyMove(m);

  mock().expectOneCall("turnPlayerLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("color", (int)Color::BLACK);
  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, enterWhitePlayerTurn)
{
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
  // ccp vtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("turnPlayerLed").onObject(&getChessHardware()).
      withParameter("isOn", true).
      withParameter("color", (int)Color::WHITE);
  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateHumanThinking, constructor)
{
  std::unique_ptr<ChessStateHumanThinking> state;

  buildState(state);
}
