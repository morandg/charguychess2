/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/chess/rules/PieceRulesBishop.hpp"

#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(PieceRulesBishop)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, moveMapWithDiscoveredCheck)
{
  ChessGame cg;
  Square from(File::D, Rank::TWO);
  BitBoard blackQueen;
  BitBoard whiteBishop;
  BitBoard expecteMoveMap;
  BitBoard moveMap;
  PieceRulesBishop prb;

  whiteBishop.set(from);
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::BISHOP, whiteBishop);
  blackQueen.set(Square(File::B, Rank::FOUR));
  cg.setOpponentPieces(Piece::QUEEN, blackQueen);
  expecteMoveMap.set(Square(File::C, Rank::THREE));
  expecteMoveMap.set(Square(File::B, Rank::FOUR));

  moveMap = prb.moveMap(from, cg);
  CHECK_EQUAL(expecteMoveMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, attackMap)
{
  ChessGame cg;
  Square from(File::C, Rank::ONE);
  BitBoard expectedAttackMap;
  BitBoard attackMap;
  PieceRulesBishop prb;

  expectedAttackMap.set(Square(File::B, Rank::TWO));
  expectedAttackMap.set(Square(File::D, Rank::TWO));

  attackMap = prb.attackMap(from, cg);
  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, moveMapBlockedByCurrentPlayerPieces)
{
  ChessGame cg;
  Square from(File::E, Rank::FOUR);
  BitBoard expectedMap;
  BitBoard moveMap;
  PieceRulesBishop prb;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, BitBoard());
  expectedMap.set(Square(File::D, Rank::FIVE));
  expectedMap.set(Square(File::C, Rank::SIX));
  expectedMap.set(Square(File::B, Rank::SEVEN));
  expectedMap.set(Square(File::A, Rank::EIGHT));
  expectedMap.set(Square(File::D, Rank::THREE));
  expectedMap.set(Square(File::F, Rank::FIVE));
  expectedMap.set(Square(File::G, Rank::SIX));
  expectedMap.set(Square(File::H, Rank::SEVEN));
  expectedMap.set(Square(File::F, Rank::THREE));

  moveMap = prb.moveMap(from, cg);
  CHECK_EQUAL(expectedMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, moveMapStoppedAtOponentPiece)
{
  ChessGame cg;
  Square from(File::E, Rank::FOUR);
  BitBoard expectedMap;
  BitBoard moveMap;
  PieceRulesBishop prb;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  expectedMap.set(Square(File::D, Rank::FIVE));
  expectedMap.set(Square(File::C, Rank::SIX));
  expectedMap.set(Square(File::B, Rank::SEVEN));
  expectedMap.set(Square(File::D, Rank::THREE));
  expectedMap.set(Square(File::C, Rank::TWO));
  expectedMap.set(Square(File::B, Rank::ONE));
  expectedMap.set(Square(File::F, Rank::FIVE));
  expectedMap.set(Square(File::G, Rank::SIX));
  expectedMap.set(Square(File::H, Rank::SEVEN));
  expectedMap.set(Square(File::F, Rank::THREE));
  expectedMap.set(Square(File::G, Rank::TWO));
  expectedMap.set(Square(File::H, Rank::ONE));

  moveMap = prb.moveMap(from, cg);
  CHECK_EQUAL(expectedMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, moveFromA8CanGoAnyWhere)
{
  ChessGame cg;
  Square from(File::A, Rank::EIGHT);
  BitBoard expectedMap;
  BitBoard moveMap;
  PieceRulesBishop prb;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  expectedMap.set(Square(File::B, Rank::SEVEN));
  expectedMap.set(Square(File::C, Rank::SIX));
  expectedMap.set(Square(File::D, Rank::FIVE));
  expectedMap.set(Square(File::E, Rank::FOUR));
  expectedMap.set(Square(File::F, Rank::THREE));
  expectedMap.set(Square(File::G, Rank::TWO));
  expectedMap.set(Square(File::H, Rank::ONE));

  moveMap = prb.moveMap(from, cg);
  CHECK_EQUAL(expectedMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, moveMapCanGoAnyWhere)
{
  ChessGame cg;
  Square from(File::E, Rank::FOUR);
  BitBoard expectedMap;
  BitBoard moveMap;
  PieceRulesBishop prb;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  expectedMap.set(Square(File::D, Rank::FIVE));
  expectedMap.set(Square(File::C, Rank::SIX));
  expectedMap.set(Square(File::B, Rank::SEVEN));
  expectedMap.set(Square(File::A, Rank::EIGHT));
  expectedMap.set(Square(File::D, Rank::THREE));
  expectedMap.set(Square(File::C, Rank::TWO));
  expectedMap.set(Square(File::B, Rank::ONE));
  expectedMap.set(Square(File::F, Rank::FIVE));
  expectedMap.set(Square(File::G, Rank::SIX));
  expectedMap.set(Square(File::H, Rank::SEVEN));
  expectedMap.set(Square(File::F, Rank::THREE));
  expectedMap.set(Square(File::G, Rank::TWO));
  expectedMap.set(Square(File::H, Rank::ONE));

  moveMap = prb.moveMap(from, cg);
  CHECK_EQUAL(expectedMap, moveMap);
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, pice)
{
  PieceRulesBishop prb;

  CHECK(Piece::BISHOP == prb.piece());
}

//------------------------------------------------------------------------------
TEST(PieceRulesBishop, constructor)
{
  PieceRulesBishop prb;
}
