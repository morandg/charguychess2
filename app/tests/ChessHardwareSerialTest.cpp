/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cgc-protocol/registers_defs.h>

#include "../src/hardware/driver/ChessHardwareSerial.hpp"

#include "mocks/RegistersMock.hpp"
#include "mocks/CgcProtocolParserMock.hpp"
#include "mocks/CgcProtocolSerializerMock.hpp"
#include "mocks/ChessHardwareObserverMock.hpp"

#include "comparators/BitBoardComparator.hpp"

#include <rglib-mocks/mocks/io/FileMock.hpp>
#include <rglib-mocks/mocks/event-loop/EventLoopMock.hpp>
#include <rglib-mocks/comparators/StdStringComparator.hpp>

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;
using namespace rglib;
using namespace rglib::mocks;

//------------------------------------------------------------------------------
TEST_GROUP(ChessHardwareSerial)
{
  TEST_SETUP()
  {
    mock().installComparator("std::string", m_strComp);
    mock().installComparator("BitBoard", m_bbComp);
  }

  TEST_TEARDOWN()
  {
    mock().removeAllComparatorsAndCopiers();
    mock().clear();
  }

  void buildHw(std::unique_ptr<ChessHardwareSerial>& hw)
  {
    m_serialPort = new FileMock();
    m_registers = new RegistersMock();
    m_parser = new CgcProtocolParserMock();
    m_serializer = new CgcProtocolSerializerMock();
    std::unique_ptr<IFile> serialPort(m_serialPort);
    std::unique_ptr<IRegisters> registers(m_registers);
    std::unique_ptr<ICgcProtocolParser> parser(m_parser);
    std::unique_ptr<ICgcProtocolSerializer> serializer(m_serializer);

    hw.reset(new ChessHardwareSerial(
        serialPort, m_el, registers, parser, serializer));
  }

  FileMock* getSerialPort()
  {
    return m_serialPort;
  }

  RegistersMock* getRegisters()
  {
    return m_registers;
  }

  CgcProtocolParserMock* getParser()
  {
    return m_parser;
  }

  CgcProtocolSerializerMock* getSerializer()
  {
    return m_serializer;
  }

  EventLoopMock& getEventLoop()
  {
    return m_el;
  }

private:
  StdStringComparator m_strComp;
  FileMock* m_serialPort;
  EventLoopMock m_el;
  RegistersMock* m_registers;
  CgcProtocolParserMock* m_parser;
  CgcProtocolSerializerMock* m_serializer;
  BitBoardComparator m_bbComp;
};

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOffQueenRookWasAlreadyOn)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue =
      CGC_REGISTER_MASK_LED_ROOK | CGC_REGISTER_MASK_LED_QUEEN;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_ROOK;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(false, Led::QUEEN);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnQueenRookWasAlreadyOn)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = CGC_REGISTER_MASK_LED_ROOK;
  uint8_t newRegisterValue = oldRegisterValue | CGC_REGISTER_MASK_LED_QUEEN;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::QUEEN);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnQueen)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_QUEEN;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::QUEEN);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnBishop)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_BISHOP;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::BISHOP);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnKnight)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_KNIGHT;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::KNIGHT);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnRook)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_ROOK;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::ROOK);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnBlack)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_BLACK;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::BLACK);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnWhite)
{
  uint8_t reg = CGC_REGISTER_PIECES_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_LED_WHITE;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::WHITE);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnEight)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_EIGHT;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::EIGHT);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnSeven)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_SEVEN;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::SEVEN);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnSix)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_SIX;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::SIX);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnFive)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_FIVE;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::FIVE);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnFour)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_FOUR;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::FOUR);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnThree)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_THREE;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::THREE);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnTwo)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_TWO;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::TWO);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedOnOne)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_RANK_ONE;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::ONE);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedHOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_H;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::H);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedGOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_G;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::G);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedFOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_F;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::F);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedEOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_E;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::E);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedDOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_D;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::D);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedCOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_C;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::C);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedBOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_B;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::B);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedAOff)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = CGC_REGISTER_MASK_FILE_A;
  uint8_t newRegisterValue = 0;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(false, Led::A);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, turnLedAOn)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_FILE_A;
  char serializedBuffer = '1';
  unsigned int serializedSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("serializeWriteRequest").onObject(getSerializer()).
      withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
      withParameter("reg", reg).
      withParameter("value", newRegisterValue).
      withOutputParameterReturning(
          "serializedSize", &serializedSize, sizeof(serializedSize)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
      withParameter(
          "buffer", (const unsigned char*)&serializedBuffer, serializedSize).
      andReturnValue(0);

  hw->turnLed(true, Led::A);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRankLedChanged)
{
  uint8_t reg = CGC_REGISTER_RANK_LEDS;
  uint8_t registerValue = 0xab;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value", registerValue).
      andReturnValue(0);

  hw->onRegisterChanged(reg, registerValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerButtonChangedAllButtonPressed)
{
  uint8_t reg = CGC_REGISTER_BUTTONS_STATUS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue =
      CGC_REGISTER_MASK_BUTTON_ROOK | CGC_REGISTER_MASK_BUTTON_KNIGHT |
      CGC_REGISTER_MASK_BUTTON_BISHOP | CGC_REGISTER_MASK_BUTTON_QUEEN;
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::ROOK);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::BISHOP);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::KNIGHT);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::QUEEN);

  hw->onRegisterChanged(reg, newRegisterValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerButtonChangedWithButtonRookPressed)
{
  uint8_t reg = CGC_REGISTER_BUTTONS_STATUS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_BUTTON_ROOK;
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::ROOK);

  hw->onRegisterChanged(reg, newRegisterValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerButtonChangedWithButtonKnightPressed)
{
  uint8_t reg = CGC_REGISTER_BUTTONS_STATUS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_BUTTON_KNIGHT;
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::KNIGHT);

  hw->onRegisterChanged(reg, newRegisterValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerButtonChangedWithButtonBishopPressed)
{
  uint8_t reg = CGC_REGISTER_BUTTONS_STATUS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_BUTTON_BISHOP;
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::BISHOP);

  hw->onRegisterChanged(reg, newRegisterValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerButtonChangedWithButtonQueenReleased)
{
  uint8_t reg = CGC_REGISTER_BUTTONS_STATUS;
  uint8_t oldRegisterValue = CGC_REGISTER_MASK_BUTTON_QUEEN;
  uint8_t newRegisterValue = 0;
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("onButtonReleased").onObject(&hwObserver).
      withParameter("button", (int)Button::QUEEN);

  hw->onRegisterChanged(reg, newRegisterValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerButtonChangedWithButtonQueenPressed)
{
  uint8_t reg = CGC_REGISTER_BUTTONS_STATUS;
  uint8_t oldRegisterValue = 0;
  uint8_t newRegisterValue = CGC_REGISTER_MASK_BUTTON_QUEEN;
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", reg).
      withOutputParameterReturning(
          "value", &oldRegisterValue, sizeof(oldRegisterValue)).
      andReturnValue(0);
  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", reg).
      withParameter("value",newRegisterValue).
      andReturnValue(0);
  mock().expectOneCall("onButtonPressed").onObject(&hwObserver).
      withParameter("button", (int)Button::QUEEN);

  hw->onRegisterChanged(reg, newRegisterValue);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, getAllPieces)
{
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard expectedBbValue(0xFEDEBC9A78563412);
  BitBoard bbValue;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);

  bbValue = hw->getPieces();

  mock().checkExpectations();

  CHECK_EQUAL(expectedBbValue, bbValue);
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank8ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK8;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank8Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank8Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank7ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK7;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank7Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank7Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank6ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK6;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank6Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank6Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank5ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK5;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank5Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank5Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank4ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK4;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank4Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank4Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank3ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK3;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank3Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank3Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank2ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK2;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank2Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank2Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, registerRank1ChangedBoardChangedCallback)
{
  uint8_t changedReg = CGC_REGISTER_RANK1;
  uint8_t rank1Value = 0x12;
  uint8_t rank2Value = 0x34;
  uint8_t rank3Value = 0x56;
  uint8_t rank4Value = 0x78;
  uint8_t rank5Value = 0x9A;
  uint8_t rank6Value = 0xBC;
  uint8_t rank7Value = 0xDE;
  uint8_t rank8Value = 0xFE;
  BitBoard bbValue(0xFEDEBC9A78563412);
  ChessHardwareObserverMock hwObserver;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  hw->addObserver(hwObserver);

  mock().expectOneCall("write").onObject(getRegisters()).
      withParameter("reg", changedReg).
      withParameter("value", rank1Value).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK1).
      withOutputParameterReturning("value", &rank1Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK2).
      withOutputParameterReturning("value", &rank2Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK3).
      withOutputParameterReturning("value", &rank3Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK4).
      withOutputParameterReturning("value", &rank4Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK5).
      withOutputParameterReturning("value", &rank5Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK6).
      withOutputParameterReturning("value", &rank6Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK7).
      withOutputParameterReturning("value", &rank7Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("read").onObject(getRegisters()).
      withParameter("reg", CGC_REGISTER_RANK8).
      withOutputParameterReturning("value", &rank8Value, sizeof(rank1Value)).
      andReturnValue(0);
  mock().expectOneCall("onBoardChanged").onObject(&hwObserver).
      withParameterOfType("BitBoard", "boardStatus", &bbValue);

  hw->onRegisterChanged(changedReg, rank1Value);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, readReady)
{
  unsigned char buffer = 'a';
  int readSize = sizeof(buffer);
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getSerialPort()).
      withOutputParameterReturning("buffer", &buffer, sizeof(buffer)).
      andReturnValue(readSize);
  mock().expectOneCall("parseBuffer").onObject(getParser()).
      withParameter("buffer", &buffer, readSize);

  hw->onReadReady();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, readReadyCannotReadFile)
{
  unsigned char buffer = 'a';
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("read").onObject(getSerialPort()).
      withOutputParameterReturning("buffer", &buffer, sizeof(buffer)).
      andReturnValue(-23);

  hw->onReadReady();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, getFileDescriptor)
{
  FileDescriptor fd = 23;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("getFileDescriptor").onObject(getSerialPort()).
      andReturnValue(fd);

  CHECK_EQUAL(fd, hw->getFileDescriptor());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initSuccess)
{
  char serializedBuffer = 'a';
  unsigned int serializedBufferSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  // cpp vtable
  ICgcProtocolParserEvent& eventCb = *hw;
  IHandledIo& ioHandler = *hw;

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getRegisters()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getParser()).
    withParameter("parserEvent", &eventCb).
    andReturnValue(0);
  mock().expectOneCall("serializeReadRequest").onObject(getSerializer()).
    withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
    withParameter("reg", CGC_REGISTER_ALL_STATUS).
    withOutputParameterReturning(
        "serializedSize", &serializedBufferSize, sizeof(serializedBufferSize)).
    andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
    withParameter(
        "buffer", (const unsigned char*)&serializedBuffer, serializedBufferSize).
    andReturnValue(0);
  mock().expectOneCall("addHandledIo").onObject(&getEventLoop()).
    withParameter("handler", &ioHandler).
    withParameter("what", IEventLoop::READ | IEventLoop::PERSIST).
    andReturnValue(0);

  CHECK_EQUAL(0, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initCannnotRegisterToEventLoop)
{
  char serializedBuffer = 'a';
  unsigned int serializedBufferSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  // cpp vtable
  ICgcProtocolParserEvent& eventCb = *hw;
  IHandledIo& ioHandler = *hw;

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getRegisters()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getParser()).
    withParameter("parserEvent", &eventCb).
    andReturnValue(0);
  mock().expectOneCall("serializeReadRequest").onObject(getSerializer()).
    withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
    withParameter("reg", CGC_REGISTER_ALL_STATUS).
    withOutputParameterReturning(
        "serializedSize", &serializedBufferSize, sizeof(serializedBufferSize)).
    andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
    withParameter(
        "buffer", (const unsigned char*)&serializedBuffer, serializedBufferSize).
    andReturnValue(0);
  mock().expectOneCall("addHandledIo").onObject(&getEventLoop()).
    withParameter("handler", &ioHandler).
    withParameter("what", IEventLoop::READ | IEventLoop::PERSIST).
    andReturnValue(-1);

  CHECK_EQUAL(-1, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initCannnotWriteToSerialPort)
{
  char serializedBuffer = 'a';
  unsigned int serializedBufferSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  // cpp vtable
  ICgcProtocolParserEvent& eventCb = *hw;

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getRegisters()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getParser()).
    withParameter("parserEvent", &eventCb).
    andReturnValue(0);
  mock().expectOneCall("serializeReadRequest").onObject(getSerializer()).
    withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
    withParameter("reg", CGC_REGISTER_ALL_STATUS).
    withOutputParameterReturning(
        "serializedSize", &serializedBufferSize, sizeof(serializedBufferSize)).
    andReturnValue(0);
  mock().expectOneCall("write").onObject(getSerialPort()).
    withParameter(
        "buffer", (const unsigned char*)&serializedBuffer, serializedBufferSize).
    andReturnValue(-34);

  CHECK_EQUAL(-1, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initCannnotSerializeReadAll)
{
  char serializedBuffer = 'a';
  unsigned int serializedBufferSize = 1;
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  // cpp vtable
  ICgcProtocolParserEvent& eventCb = *hw;

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getRegisters()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getParser()).
    withParameter("parserEvent", &eventCb).
    andReturnValue(0);
  mock().expectOneCall("serializeReadRequest").onObject(getSerializer()).
    withOutputParameterReturning(
          "buffer", &serializedBuffer, sizeof(serializedBuffer)).
    withParameter("reg", CGC_REGISTER_ALL_STATUS).
    withOutputParameterReturning(
        "serializedSize", &serializedBufferSize, sizeof(serializedBufferSize)).
    andReturnValue(-1);

  CHECK_EQUAL(-1, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initCannotInitializeParser)
{
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
  // cpp vtable
  ICgcProtocolParserEvent& eventCb = *hw;

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getRegisters()).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getParser()).
    withParameter("parserEvent", &eventCb).
    andReturnValue(-1);

  CHECK_EQUAL(-1, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initCannotInitializeRegisters)
{
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(0);
  mock().expectOneCall("init").onObject(getRegisters()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, initCannotOpenFile)
{
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);

  mock().expectOneCall("open").onObject(getSerialPort()).
      withParameterOfType(
          "std::string", "path", &ChessHardwareSerial::DEFAULT_SERIAL_PORT).
      withParameter("flags", OpenFlag::READ_WRITE).
      andReturnValue(-34);

  CHECK_EQUAL(-1, hw->init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessHardwareSerial, constructor)
{
  std::unique_ptr<ChessHardwareSerial> hw;

  buildHw(hw);
}
