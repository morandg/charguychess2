/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/chess/ChessGame.hpp"
#include "../src/hardware/states/ChessStateInvalidPosition.hpp"

#include "mocks/ChessStatesPoolMock.hpp"
#include "mocks/ChessHardwareMock.hpp"
#include "mocks/BlinkingLedsMock.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessStateInvalidPosition)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }

  void buildState(std::unique_ptr<ChessStateInvalidPosition>& state)
  {
    state.reset(
        new ChessStateInvalidPosition(
            m_blinkingLeds,
            m_chessStatesPool,
            m_chessHardwareMock,
            m_chessGame));
  }

  BlinkingLedsMock& getBlinkingLeds()
  {
    return m_blinkingLeds;
  }

  ChessHardwareMock& getChessHardware()
  {
    return m_chessHardwareMock;
  }

  ChessStatesPoolMock& getStatesPool()
  {
    return m_chessStatesPool;
  }

  ChessGame& getChessGame()
  {
    return m_chessGame;
  }

private:
  BlinkingLedsMock m_blinkingLeds;
  ChessStatesPoolMock m_chessStatesPool;
  ChessHardwareMock m_chessHardwareMock;
  ChessGame m_chessGame;
};

//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, enterPositionIsCorrect)
{
  BitBoard hwStatus = getChessGame().allPieces();
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);

  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hwStatus);
  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, onBoardChagedSameAsGame)
{
  BitBoard newPosition(0xFFFF00000000FFFF);
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);

  mock().expectOneCall("changeState").onObject(&getStatesPool()).
      withParameter("state", (int)IChessStatesPool::PLAYER_THINKING);

  state->onBoardChanged(newPosition);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, onBoardChagedNotSameAsGame)
{
  BitBoard newPosition;
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);

  state->onBoardChanged(newPosition);
}


//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, exit)
{
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);

  // cppvtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("removeObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("stop").onObject(&getBlinkingLeds());

  state->exit();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, enter)
{
  BitBoard hwStatus;
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);

  // cppvtable
  IChessHardwareObserver& hwObserver = *state;

  mock().expectOneCall("getPieces").onObject(&getChessHardware()).
      andReturnValue(&hwStatus);
  mock().expectOneCall("addObserver").onObject(&getChessHardware()).
      withParameter("observer", &hwObserver);
  mock().expectOneCall("setOnTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateInvalidPosition::BLINK_TIMEOUT);
  mock().expectOneCall("setOffTimeout").onObject(&getBlinkingLeds()).
      withParameter("ms", ChessStateInvalidPosition::BLINK_TIMEOUT);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::A);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::B);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::C);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::D);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::E);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::F);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::G);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::H);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::ONE);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::TWO);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::THREE);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::FOUR);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::FIVE);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::SIX);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::SEVEN);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::EIGHT);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::WHITE);
  mock().expectOneCall("addLed").onObject(&getBlinkingLeds()).
      withParameter("led", (int)Led::BLACK);
  mock().expectOneCall("start").onObject(&getBlinkingLeds());

  state->enter();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, init)
{
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);

  CHECK_EQUAL(0, state->init());
}

//------------------------------------------------------------------------------
TEST(ChessStateInvalidPosition, constructor)
{
  std::unique_ptr<ChessStateInvalidPosition> state;

  buildState(state);
}
