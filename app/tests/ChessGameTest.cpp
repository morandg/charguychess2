/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "spies/ChessGameSpy.hpp"
#include "comparators/MoveComparator.hpp"
#include "comparators/BitBoardComparator.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>
#include "mocks/GameObserverMock.hpp"

using namespace charguychess;
using namespace charguychess::tests;

//------------------------------------------------------------------------------
TEST_GROUP(ChessGame)
{
  TEST_SETUP()
  {
    mock().installComparator("Move", m_moveComp);
  }

  TEST_TEARDOWN()
  {
    mock().clear();
    mock().removeAllComparatorsAndCopiers();
  }

  void clearCastlingPieces(ChessGame& cg)
  {
    cg.setPlayerPieces(Piece::BISHOP, BitBoard());
    cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
    cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  }

private:
  MoveComparator m_moveComp;
};

//------------------------------------------------------------------------------
TEST(ChessGame, applyMoveMateIsIsUpdated)
{
  BitBoard whiteQueen;
  BitBoard whiteBishop;
  Move move(Square(File::F, Rank::THREE), Square(File::F, Rank::SEVEN));
  Turn lastTurn;
  ChessGame cg;

  whiteQueen.set(Square(File::F, Rank::THREE));
  whiteBishop.set(Square(File::H, Rank::FIVE));
  cg.setPlayerPieces(Piece::QUEEN, whiteQueen);
  cg.setPlayerPieces(Piece::BISHOP, whiteBishop);

  cg.applyMove(move);

  lastTurn = cg.lastTurn();

  CHECK(move.isMate());
  CHECK(lastTurn.whiteMove().isMate());
}

//------------------------------------------------------------------------------
TEST(ChessGame, applyMoveCheckedIsUpdated)
{
  BitBoard whiteQueen;
  Move move(Square(File::F, Rank::THREE), Square(File::F, Rank::SEVEN));
  Turn lastTurn;
  ChessGame cg;

  whiteQueen.set(Square(File::F, Rank::THREE));
  cg.setPlayerPieces(Piece::QUEEN, whiteQueen);

  cg.applyMove(move);

  lastTurn = cg.lastTurn();

  CHECK(move.isCheck());
  CHECK(lastTurn.whiteMove().isCheck());
}

//------------------------------------------------------------------------------
TEST(ChessGame, blackPawnIsPromotional)
{
  Square from(File::E, Rank::TWO);
  BitBoard blackPawns;
  ChessGame cg;
  Move move(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));

  cg.applyMove(move);
  blackPawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, blackPawns);

  CHECK(cg.isPromotionalMove(from));
}

//------------------------------------------------------------------------------
TEST(ChessGame, blackPawnIsNotPromotional)
{
  Square from(File::E, Rank::SEVEN);
  Move move(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  ChessGame cg;

  cg.applyMove(move);

  CHECK_FALSE(cg.isPromotionalMove(from));
}

//------------------------------------------------------------------------------
TEST(ChessGame, whiteBishopMoveOnSeventhRankIsNotPromotional)
{
  Square from(File::E, Rank::SEVEN);
  BitBoard whiteBishops;
  ChessGame cg;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  whiteBishops.set(from);
  cg.setPlayerPieces(Piece::BISHOP, whiteBishops);

  CHECK_FALSE(cg.isPromotionalMove(from));
}

//------------------------------------------------------------------------------
TEST(ChessGame, promotionalWhitePawnMove)
{
  Square from(File::E, Rank::SEVEN);
  BitBoard whitePawns;
  ChessGame cg;

  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  whitePawns.set(from);
  cg.setPlayerPieces(Piece::PAWN, whitePawns);

  CHECK(cg.isPromotionalMove(from));
}

//------------------------------------------------------------------------------
TEST(ChessGame, notAPromotionalWhitePawnMove)
{
  Square from(File::E, Rank::TWO);
  ChessGame cg;

  CHECK_FALSE(cg.isPromotionalMove(from));
}

//------------------------------------------------------------------------------
TEST(ChessGame, isMate)
{
  BitBoard whiteKing;
  BitBoard blackKing;
  BitBoard blackRooks;
  ChessGame cg;

  whiteKing.set(Square(File::H, Rank::ONE));
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::KING, whiteKing);
  blackRooks.set(Square(File::A, Rank::TWO));
  blackRooks.set(Square(File::G, Rank::EIGHT));
  blackRooks.set(Square(File::H, Rank::EIGHT));
  blackKing.set(Square(File::B, Rank::SEVEN));
  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::QUEEN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, blackRooks);
  cg.setOpponentPieces(Piece::KING, blackKing);

  CHECK(cg.isMate());
}

//------------------------------------------------------------------------------
TEST(ChessGame, isNotMateInitialPosition)
{
  ChessGame cg;

  CHECK_FALSE(cg.isMate());
}

//------------------------------------------------------------------------------
TEST(ChessGame, isNotStaleMateWhenChecked)
{
  BitBoard whiteKing;
  BitBoard blackKing;
  BitBoard blackRooks;
  ChessGame cg;

  whiteKing.set(Square(File::H, Rank::ONE));
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::KING, whiteKing);
  blackRooks.set(Square(File::A, Rank::TWO));
  blackRooks.set(Square(File::G, Rank::EIGHT));
  blackRooks.set(Square(File::H, Rank::EIGHT));
  blackKing.set(Square(File::B, Rank::SEVEN));
  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::QUEEN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, blackRooks);
  cg.setOpponentPieces(Piece::KING, blackKing);

  CHECK_FALSE(cg.isStaleMate());
}

//------------------------------------------------------------------------------
TEST(ChessGame, isStaleMateWhenNotChecked)
{
  BitBoard whiteKing;
  BitBoard blackKing;
  BitBoard blackRooks;
  ChessGame cg;

  whiteKing.set(Square(File::H, Rank::ONE));
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  cg.setPlayerPieces(Piece::ROOK, BitBoard());
  cg.setPlayerPieces(Piece::KING, whiteKing);
  blackRooks.set(Square(File::A, Rank::TWO));
  blackRooks.set(Square(File::G, Rank::EIGHT));
  blackKing.set(Square(File::B, Rank::SEVEN));
  cg.setOpponentPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::BISHOP, BitBoard());
  cg.setOpponentPieces(Piece::KNIGHT, BitBoard());
  cg.setOpponentPieces(Piece::QUEEN, BitBoard());
  cg.setOpponentPieces(Piece::ROOK, blackRooks);
  cg.setOpponentPieces(Piece::KING, blackKing);

  CHECK(cg.isStaleMate());
}

//------------------------------------------------------------------------------
TEST(ChessGame, initialPositionIsNotStalemate)
{
  ChessGame cg;

  CHECK_FALSE(cg.isStaleMate());
}

//------------------------------------------------------------------------------
TEST(ChessGame, applyKingSideBlackRookMoveCannotCastleAnymore)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::H, Rank::EIGHT);
  Square to(File::G, Rank::EIGHT);
  Move move(from, to);
  ChessGameSpy cg;

  cg.applyMove(e2e4);
  clearCastlingPieces(cg);

  cg.applyMove(move);
  CHECK(cg.getCastlingStatus().isQueenSideCastleAllowed(Color::BLACK));
  CHECK_FALSE(cg.getCastlingStatus().isKingSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(ChessGame, applyQueenSideBlackeRookMoveCannotCastleAnymore)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Square from(File::A, Rank::EIGHT);
  Square to(File::B, Rank::EIGHT);
  Move move(from, to);
  ChessGameSpy cg;

  cg.applyMove(e2e4);
  clearCastlingPieces(cg);

  cg.applyMove(move);
  CHECK_FALSE(cg.getCastlingStatus().isQueenSideCastleAllowed(Color::BLACK));
  CHECK(cg.getCastlingStatus().isKingSideCastleAllowed(Color::BLACK));
}

//------------------------------------------------------------------------------
TEST(ChessGame, applyKingSideWhiteRookMoveCannotCastleAnymore)
{
  Square from(File::H, Rank::ONE);
  Square to(File::G, Rank::ONE);
  Move move(from, to);
  ChessGameSpy cg;

  clearCastlingPieces(cg);

  cg.applyMove(move);
  CHECK(cg.getCastlingStatus().isQueenSideCastleAllowed(Color::WHITE));
  CHECK_FALSE(cg.getCastlingStatus().isKingSideCastleAllowed(Color::WHITE));
}

//------------------------------------------------------------------------------
TEST(ChessGame, applyQueenSideWhiteRookMoveCannotCastleAnymore)
{
  Square from(File::A, Rank::ONE);
  Square to(File::B, Rank::ONE);
  Move move(from, to);
  ChessGameSpy cg;

  clearCastlingPieces(cg);

  cg.applyMove(move);
  CHECK_FALSE(cg.getCastlingStatus().isQueenSideCastleAllowed(Color::WHITE));
  CHECK(cg.getCastlingStatus().isKingSideCastleAllowed(Color::WHITE));
}

//------------------------------------------------------------------------------
TEST(ChessGame, applyWhiteKingMoveCannotCastleAnymore)
{
  Square from(File::E, Rank::ONE);
  Square to(File::F, Rank::ONE);
  Move move(from, to);
  ChessGameSpy cg;

  clearCastlingPieces(cg);

  cg.applyMove(move);
  CHECK_FALSE(cg.getCastlingStatus().isKingSideCastleAllowed(Color::WHITE));
  CHECK_FALSE(cg.getCastlingStatus().isQueenSideCastleAllowed(Color::WHITE));
}

//------------------------------------------------------------------------------
TEST(ChessGame, isChecked)
{
  BitBoard blackQueen;
  ChessGame cg;

  blackQueen.set(Square(File::E, Rank::THREE));
  cg.setPlayerPieces(Piece::PAWN, BitBoard());
  cg.setOpponentPieces(Piece::QUEEN, blackQueen);

  CHECK(cg.isChecked());
}

//------------------------------------------------------------------------------
TEST(ChessGame, isCheckedNotChcked)
{
  ChessGame cg;

  CHECK_FALSE(cg.isChecked());
}

//------------------------------------------------------------------------------
TEST(ChessGame, opponentAttackMap)
{
  BitBoard attackMap;
  BitBoard expectedAttackMap;
  ChessGame cg;

  expectedAttackMap.set(Square(File::B, Rank::EIGHT));
  expectedAttackMap.set(Square(File::C, Rank::EIGHT));
  expectedAttackMap.set(Square(File::D, Rank::EIGHT));
  expectedAttackMap.set(Square(File::E, Rank::EIGHT));
  expectedAttackMap.set(Square(File::F, Rank::EIGHT));
  expectedAttackMap.set(Square(File::G, Rank::EIGHT));
  expectedAttackMap.set(Square(File::A, Rank::SEVEN));
  expectedAttackMap.set(Square(File::B, Rank::SEVEN));
  expectedAttackMap.set(Square(File::C, Rank::SEVEN));
  expectedAttackMap.set(Square(File::D, Rank::SEVEN));
  expectedAttackMap.set(Square(File::E, Rank::SEVEN));
  expectedAttackMap.set(Square(File::F, Rank::SEVEN));
  expectedAttackMap.set(Square(File::G, Rank::SEVEN));
  expectedAttackMap.set(Square(File::H, Rank::SEVEN));
  expectedAttackMap.set(Square(File::A, Rank::SIX));
  expectedAttackMap.set(Square(File::B, Rank::SIX));
  expectedAttackMap.set(Square(File::C, Rank::SIX));
  expectedAttackMap.set(Square(File::D, Rank::SIX));
  expectedAttackMap.set(Square(File::E, Rank::SIX));
  expectedAttackMap.set(Square(File::F, Rank::SIX));
  expectedAttackMap.set(Square(File::G, Rank::SIX));
  expectedAttackMap.set(Square(File::H, Rank::SIX));

  attackMap = cg.opponentAttackMap();

  CHECK_EQUAL(expectedAttackMap, attackMap);
}

//------------------------------------------------------------------------------
TEST(ChessGame, cloneHistoryIsCloned)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  BitBoard whitePlayer;
  std::unique_ptr<IChessGame> clone;
  ChessGame cg;

  cg.applyMove(e2e4);
  cg.clone(clone);

  CHECK(cg.currentPlayer() == clone->currentPlayer());
}

//------------------------------------------------------------------------------
TEST(ChessGame, cloneCastlingStatusIsCloned)
{
  Square kingLocation(File::E, Rank::ONE);
  BitBoard whitePlayer;
  std::unique_ptr<IChessGame> clone;
  ChessGameSpy cg;

  cg.setPlayerPieces(Piece::BISHOP, BitBoard());
  cg.setPlayerPieces(Piece::KNIGHT, BitBoard());
  cg.setPlayerPieces(Piece::QUEEN, BitBoard());
  cg.getCastlingStatus().kingMoved(Color::WHITE);

  cg.clone(clone);

  CHECK_FALSE(clone->isQuenSideCastlingAllowed(kingLocation));
  CHECK_FALSE(clone->isKingSideCastlingAllowed(kingLocation));
}

//------------------------------------------------------------------------------
TEST(ChessGame, clonePlayerPiecesAreCloned)
{
  BitBoard whitePlayer;
  std::unique_ptr<IChessGame> clone;
  BitBoard newWhitePawns;
  BitBoard clonedWhitePawns;
  ChessGame cg;

  newWhitePawns.set(Square(File::A, Rank::THREE));
  cg.setPlayerPieces(Piece::PAWN, newWhitePawns);

  cg.clone(clone);
  clonedWhitePawns = clone->playerPieces(Piece::PAWN);
  CHECK_EQUAL(newWhitePawns, clonedWhitePawns);
}

//------------------------------------------------------------------------------
TEST(ChessGame, cloneCreatesNewGame)
{
  BitBoard whitePlayer;
  std::unique_ptr<IChessGame> clone;
  ChessGame cg;

  cg.clone(clone);

  CHECK(clone);
}

//------------------------------------------------------------------------------
TEST(ChessGame, getWhitePlayerBitBoard)
{
  BitBoard whitePlayer;
  ChessGame cg;

  whitePlayer = cg.playerPieces();

  CHECK_EQUAL(0xFFFF, whitePlayer.getValue());
}

//------------------------------------------------------------------------------
TEST(ChessGame, trunNumberAfterFirstMove)
{
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));
  ChessGame cg;

  cg.applyMove(move);

  CHECK_EQUAL(1, cg.turnNumber());
}

//------------------------------------------------------------------------------
TEST(ChessGame, trunNumberFromNewGame)
{
  ChessGame cg;

  CHECK_EQUAL(1, cg.turnNumber());
}

//------------------------------------------------------------------------------
TEST(ChessGame, getMoveMapPawn)
{
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGame cg;

  expectedMoveMap.set(Square(File::E, Rank::THREE));
  expectedMoveMap.set(Square(File::E, Rank::FOUR));

  moveMap = cg.moveMap(Square(File::E, Rank::TWO));
  CHECK_EQUAL(moveMap.getValue(), expectedMoveMap.getValue());
}

//------------------------------------------------------------------------------
TEST(ChessGame, getMoveMapNoPieces)
{
  BitBoard moveMap;
  BitBoard expectedMoveMap;
  ChessGame cg;

  moveMap = cg.moveMap(Square(File::E, Rank::THREE));
  CHECK_EQUAL(moveMap.getValue(), expectedMoveMap.getValue());
}

//------------------------------------------------------------------------------
TEST(ChessGame, newGameBlackTurn)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  ChessGame cg;

  cg.applyMove(e2e4);
  cg.newGame();

  CHECK(Color::WHITE == cg.currentPlayer());
}

//------------------------------------------------------------------------------
TEST(ChessGame, newGameWhiteTurn)
{
  Move e2e4(Square(File::E, Rank::TWO), Square(File::E, Rank::FOUR));
  Move e7e5(Square(File::E, Rank::SEVEN), Square(File::E, Rank::FIVE));
  BitBoard initialPosition(0xFFFF00000000FFFF);
  ChessGame cg;

  cg.applyMove(e2e4);
  cg.applyMove(e7e5);
  cg.newGame();

  CHECK_EQUAL(initialPosition.getValue(), cg.allPieces().getValue());
  CHECK_FALSE(cg.lastTurn().whiteMove().isValid());
}

//------------------------------------------------------------------------------
TEST(ChessGame, allPieces)
{
  BitBoard initialPosition(0xFFFF00000000FFFF);
  ChessGame cg;

  CHECK_EQUAL(initialPosition.getValue(), cg.allPieces().getValue());
}

//------------------------------------------------------------------------------
TEST(ChessGame, validMoveAddedInHistory)
{
  Square from(File::A, Rank::TWO);
  Square to(File::A, Rank::FOUR);
  Move move(from, to);
  ChessGame cg;

  cg.applyMove(move);

  CHECK(cg.lastTurn().whiteMove() == move);
}

//------------------------------------------------------------------------------
TEST(ChessGame, invalidMoveNotAddedInHistory)
{
  Square from(File::A, Rank::FIVE);
  Square to(File::A, Rank::FOUR);
  Move move(from, to);
  ChessGame cg;

  cg.applyMove(move);
  CHECK_FALSE(cg.lastTurn().whiteMove().isValid());
}

//------------------------------------------------------------------------------
TEST(ChessGame, newGameObserverCallback)
{
  GameObserverMock observer;
  ChessGame cg;

  cg.addObserver(observer);

  mock().expectOneCall("onNewGameStarted").onObject(&observer);

  cg.newGame();

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessGame, validMoveRegisterTwoObservers)
{
  GameObserverMock observer1;
  GameObserverMock observer2;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));
  ChessGame cg;

  cg.addObserver(observer1);
  cg.addObserver(observer2);

  mock().expectOneCall("onMovePlayed").onObject(&observer1).
      withParameterOfType("Move", "move", &move);
  mock().expectOneCall("onMovePlayed").onObject(&observer2).
      withParameterOfType("Move", "move", &move);

  cg.applyMove(move);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessGame, validMoveObserverRemoved)
{
  GameObserverMock observer;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));
  ChessGame cg;

  cg.addObserver(observer);
  cg.removeObserver(observer);

  cg.applyMove(move);
}

//------------------------------------------------------------------------------
TEST(ChessGame, validMoveRegisterObserverTwice)
{
  GameObserverMock observer;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));
  ChessGame cg;

  cg.addObserver(observer);
  cg.addObserver(observer);

  mock().expectOneCall("onMovePlayed").onObject(&observer).
      withParameterOfType("Move", "move", &move);

  cg.applyMove(move);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessGame, validMoveObserverCallBack)
{
  GameObserverMock observer;
  Move move(Square(File::A, Rank::TWO), Square(File::A, Rank::THREE));
  ChessGame cg;

  cg.addObserver(observer);

  mock().expectOneCall("onMovePlayed").onObject(&observer).
      withParameterOfType("Move", "move", &move);

  cg.applyMove(move);

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(ChessGame, invalidNoObserverCallBack)
{
  GameObserverMock observer;
  Move move(Square(File::A, Rank::FIVE), Square(File::A, Rank::FIVE));
  ChessGame cg;

  cg.addObserver(observer);

  cg.applyMove(move);
}

//------------------------------------------------------------------------------
TEST(ChessGame, pieceAtBlackPosition)
{
  PlayerPiece piece;
  ChessGame cg;

  CHECK(cg.pieceAt(Square(File::A, Rank::EIGHT), piece));
  CHECK_EQUAL((int)Color::BLACK, (int)piece.color());
  CHECK_EQUAL((int)Piece::ROOK, (int)piece.piece());
}

//------------------------------------------------------------------------------
TEST(ChessGame, pieceAtWhitePosition)
{
  PlayerPiece piece;
  ChessGame cg;

  CHECK(cg.pieceAt(Square(File::A, Rank::TWO), piece));
  CHECK_EQUAL((int)Color::WHITE, (int)piece.color());
  CHECK_EQUAL((int)Piece::PAWN, (int)piece.piece());
}

//------------------------------------------------------------------------------
TEST(ChessGame, pieceAtEmptySquare)
{
  PlayerPiece piece;
  ChessGame cg;

  CHECK_FALSE(cg.pieceAt(Square(File::D, Rank::FOUR), piece));
}

//------------------------------------------------------------------------------
TEST(ChessGame, moveBlackPieceWhenBlackToMove)
{
  Move moveWhite(
      Square(File::A, Rank::TWO),
      Square(File::A, Rank::THREE));
  Move moveBlack(
      Square(File::A, Rank::SEVEN),
      Square(File::A, Rank::SIX));
  ChessGame cg;

  CHECK(cg.applyMove(moveWhite));
  CHECK(cg.applyMove(moveBlack));
}

//------------------------------------------------------------------------------
TEST(ChessGame, validKingMove)
{
  Square from(File::E, Rank::ONE);
  Square to(File::D, Rank::ONE);
  Move move(from, to);
  ChessGame cg;

  cg.clear(Square(File::D, Rank::ONE));

  CHECK(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, validQueenMove)
{
  Square from(File::D, Rank::ONE);
  Square to(File::D, Rank::THREE);
  Move move(from, to);
  ChessGame cg;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());

  CHECK(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, validRookMove)
{
  Square from(File::A, Rank::ONE);
  Square to(File::A, Rank::THREE);
  Move move(from, to);
  ChessGame cg;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());

  CHECK(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, validBishopMove)
{
  Square from(File::C, Rank::ONE);
  Square to(File::E, Rank::THREE);
  Move move(from, to);
  ChessGame cg;

  cg.setPlayerPieces(Piece::PAWN, BitBoard());

  CHECK(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, validKnightMove)
{
  Square from(File::B, Rank::ONE);
  Square to(File::C, Rank::THREE);
  Move move(from, to);
  ChessGame cg;

  CHECK(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, validPawnMove)
{
  Square from(File::A, Rank::TWO);
  Square to(File::A, Rank::FOUR);
  PlayerPiece movedPiece;
  Move move(from, to);
  ChessGame cg;

  CHECK(cg.applyMove(move));
  CHECK_FALSE(cg.pieceAt(from, movedPiece));
  CHECK(cg.pieceAt(to, movedPiece));
  CHECK(Color::WHITE == movedPiece.color());
  CHECK(Piece::PAWN== movedPiece.piece());
}

//------------------------------------------------------------------------------
TEST(ChessGame, invalidMove)
{
  Square from(File::A, Rank::TWO);
  Square to(File::A, Rank::FIVE);
  Move move(from, to);
  ChessGame cg;

  CHECK_FALSE(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, invalidMoveFromEmptySquare)
{
  Move move(
      Square(File::A, Rank::THREE),
      Square(File::A, Rank::FOUR));
  ChessGame cg;

  CHECK_FALSE(cg.applyMove(move));
}

//------------------------------------------------------------------------------
TEST(ChessGame, constructor)
{
  ChessGame cg;
}
