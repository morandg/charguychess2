/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/hardware/driver/Registers.hpp"

#include <CppUTest/TestHarness.h>

using namespace charguychess;

//------------------------------------------------------------------------------
TEST_GROUP(Registers)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Registers, resetAfterInit)
{
  uint8_t reg = 0x03;
  uint8_t value = 0xAB;
  uint8_t readValue;
  Registers registers;

  registers.write(reg, value);
  CHECK_EQUAL(0, registers.init());
  registers.read(reg, readValue);

  CHECK_EQUAL(0, readValue);
}

//------------------------------------------------------------------------------
TEST(Registers, readWrite)
{
  uint8_t reg = 0x03;
  uint8_t value = 0xAB;
  uint8_t readValue;
  Registers registers;

  CHECK_EQUAL(0, registers.write(reg, value));
  CHECK_EQUAL(0, registers.read(reg, readValue));
  CHECK_EQUAL(value, readValue);
}

//------------------------------------------------------------------------------
TEST(Registers, constructor)
{
  Registers registers;
}
