/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <signal.h>

#include <rglib/logging/LogMacros.hpp>

#include "chess/engine/ChessEngines.hpp"
#include "hardware/states/ChessStatesPool.hpp"
#include "hardware/states/ChessStateHumanComputer.hpp"
#include "hardware/states/ChessStateWhiteBlack.hpp"
#include "hardware/states/ChessStateInvalidPosition.hpp"
#include "hardware/states/ChessStateThinking.hpp"
#include "hardware/states/ChessStateHumanThinking.hpp"
#include "hardware/states/ChessStateComputerThinking.hpp"
#include "hardware/states/ChessStatePieceLifted.hpp"
#include "hardware/states/ChessStateCapture.hpp"
#include "hardware/states/ChessStateRequestMove.hpp"
#include "hardware/states/ChessStateRequestDropPiece.hpp"
#include "hardware/states/ChessStateEndOfGame.hpp"
#include "CharGuyChess.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
CharGuyChess::CharGuyChess()
{
}

//------------------------------------------------------------------------------
CharGuyChess::~CharGuyChess()
{
}

//------------------------------------------------------------------------------
int CharGuyChess::init(const std::string& configFile)
{
  if(configFile != "")
  {
    if(m_settings.parse(configFile))
      return -1;
  }

  if(initEventLoop())
    return -1;
  if(initQuitHandler())
    return -1;
  if(initHardware())
    return -1;
  if(initChessEngines())
    return -1;
  if(initStatesPool())
    return -1;
  if(initTelnetServer())
    return -1;
  if(initPgnSerializer())
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int CharGuyChess::reinit()
{
  return m_el->reinit();
}

//------------------------------------------------------------------------------
int CharGuyChess::run()
{
  // Start chess engine after "fork" to avoid ECHILD error when checking if they
  // are running
  LOGDB() << "Starting chess engines ...";
  if(m_chessEngines->startEngines())
    return -1;

  LOGDB() << "Starting event loop";
  return m_el->run();
}

//------------------------------------------------------------------------------
int CharGuyChess::initEventLoop()
{
  rglib::IEventLoop::create(m_el);

  if(!m_el)
  {
    LOGER() << "Cannot create event loop instance, " <<
        "make sure you have compiled rglib with libevent support " <<
        "(libevent-dev)";
    return -1;
  }
  if(m_el->init())
  {
    LOGER() << "Cannot initialize event loop";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int CharGuyChess::initQuitHandler()
{
  m_quitHandler.reset(new QuitHandler(*m_el));

  if(m_el->addHandledSignal(*m_quitHandler, SIGTERM))
  {
    LOGER() << "Cannot register to SIGTERM signal";
    return -1;
  }

  if(m_el->addHandledSignal(*m_quitHandler, SIGINT))
  {
    LOGER() << "Cannot register to SIGINT signal";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int CharGuyChess::initHardware()
{
  IChessHardware::create(*m_el, m_hardware);
  if(!m_hardware)
    return -1;
  if(m_hardware->init())
    return -1;

  m_blinkingLeds.reset(new BlinkingLeds(*m_hardware));
  return m_blinkingLeds->init(*m_el);
}

//------------------------------------------------------------------------------
int CharGuyChess::initTelnetServer()
{
  m_telnetServer.reset(
      new TelnetServer(*m_el, m_chessGame, *m_hardware, *m_chessEngines));

  return m_telnetServer->init(m_settings.getTelnetSettings());
}

//------------------------------------------------------------------------------
int CharGuyChess::initChessEngines()
{
  std::unique_ptr<ChessEngines> chessEngines(new ChessEngines());

  if(chessEngines->init(*m_el, m_settings))
    return -1;

  m_chessEngines = std::move(chessEngines);

  return 0;
}

//------------------------------------------------------------------------------
int CharGuyChess::initStatesPool()
{
  std::unique_ptr<IChessState> state;
  std::unique_ptr<IChessState> substate;
  std::unique_ptr<IChessState> humanState;
  std::unique_ptr<IChessState> computerState;
  std::unique_ptr<IChessState> blackState;
  std::unique_ptr<IChessState> whiteState;
  m_statesPool.reset(new ChessStatesPool());

  // Invalid position
  state.reset(new ChessStateInvalidPosition(
      *m_blinkingLeds, *m_statesPool, *m_hardware, m_chessGame));
  m_statesPool->addState(IChessStatesPool::INVALID_POSITION, state);

  // Thinking
  humanState.reset(new ChessStateHumanThinking(
      *m_statesPool, *m_hardware, m_chessGame));
  whiteState.reset(
      new ChessStateComputerThinking(
          *m_blinkingLeds,
          *m_statesPool,
          *m_hardware,
          m_chessGame,
          m_settings.getAisSettings().getWhiteAiSettings(),
          m_chessEngines->getEngine(Color::WHITE)));
  blackState.reset(
      new ChessStateComputerThinking(
          *m_blinkingLeds,
          *m_statesPool,
          *m_hardware,
          m_chessGame,
          m_settings.getAisSettings().getBlackAiSettings(),
          m_chessEngines->getEngine(Color::BLACK)));
  computerState.reset(
      new ChessStateWhiteBlack(m_chessGame, whiteState, blackState));
  substate.reset(new ChessStateHumanComputer(
      m_chessGame, m_settings.getAisSettings(), humanState, computerState));
  state.reset(new ChessStateThinking(
      *m_statesPool, m_chessGame, *m_hardware, substate));
  m_statesPool->addState(IChessStatesPool::PLAYER_THINKING, state);

  // Piece lifted
  state.reset(new ChessStatePieceLifted(
      *m_statesPool, *m_hardware, m_chessGame));
  m_statesPool->addState(IChessStatesPool::PIECE_LIFTED, state);

  // Capture
  state.reset(new ChessStateCapture(
      *m_statesPool, *m_hardware, m_chessGame));
  m_statesPool->addState(IChessStatesPool::CAPTURE, state);

  // Request move
  whiteState.reset(new ChessStateRequestMove(
      *m_statesPool,
      *m_hardware,
      m_chessGame,
      m_chessEngines->getEngine(Color::WHITE)));
  blackState.reset(new ChessStateRequestMove(
      *m_statesPool,
      *m_hardware,
      m_chessGame,
      m_chessEngines->getEngine(Color::BLACK)));
  state.reset(new ChessStateWhiteBlack(m_chessGame, whiteState, blackState));
  m_statesPool->addState(IChessStatesPool::REQUEST_MOVE, state);

  // Request drop piece
  whiteState.reset(new ChessStateRequestDropPiece(
      *m_statesPool,
      *m_hardware,
      m_chessGame,
      m_chessEngines->getEngine(Color::WHITE)));
  blackState.reset(new ChessStateRequestDropPiece(
      *m_statesPool,
      *m_hardware,
      m_chessGame,
      m_chessEngines->getEngine(Color::BLACK)));
  state.reset(new ChessStateWhiteBlack(m_chessGame, whiteState, blackState));
  m_statesPool->addState(IChessStatesPool::REQUEST_DROP_PIECE, state);

  // Mate
  state.reset(
      new ChessStateEndOfGame(*m_el, *m_statesPool, *m_hardware, m_chessGame));
  m_statesPool->addState(IChessStatesPool::END_OF_GAME, state);

  if(m_statesPool->init())
    return -1;

  m_statesPool->changeState(IChessStatesPool::INVALID_POSITION);

  return 0;
}

//------------------------------------------------------------------------------
int CharGuyChess::initPgnSerializer()
{
  m_pgnSerializer.reset(new PgnSerializer(m_chessGame));

  return m_pgnSerializer->init(m_settings.getPgnSettings());
}

}       // namespace
