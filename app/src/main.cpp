/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <getopt.h>

#include <iostream>

#include <rglib/logging/LogMacros.hpp>
#include <rglib/process/IDaemonizer.hpp>
#include <rglib/Version.hpp>

#include "CharGuyChess.hpp"
#include "Version.hpp"

/* Constants */
static const rglib::ILogSink::LogLevel DEFAULT_LOG_LEVEL =
    rglib::ILogSink::INFO;
static const std::string DEFAULT_PID_FILE = "/var/run/charguychess.pid";

/* locals */
rglib::ILogSink::LogLevel maxLogLevel = DEFAULT_LOG_LEVEL;
std::string configFile;
std::string log4CplusConfig;
bool isDaemonize = false;
std::string pidFile = DEFAULT_PID_FILE;

//------------------------------------------------------------------------------
void showHelp(const char* progName)
{
  std::cout << progName << std::endl;
  std::cout << std::endl;
  std::cout << "-h|--help           Show this help" << std::endl;
  std::cout << "-v|--version        Show version" << std::endl;
  std::cout << "-c|--config         Path to configuration file" << std::endl;
  std::cout << "-l|--loglevel       Max log level " <<
      "(" <<(int)DEFAULT_LOG_LEVEL << ") " << std::endl <<
      "                      debug:" << (int)rglib::ILogSink::DEBUG <<
      std::endl <<
      "                      error:" << (int)rglib::ILogSink::ERROR <<
      std::endl;
  std::cout << "-L|--log-config     Path to log4cplus configuration" <<
      std::endl;
  std::cout << "-d|--daemonize      Run as daemon" << std::endl;
  std::cout << "-p|--pidfile        PID file (" << DEFAULT_PID_FILE << ")" <<
      std::endl;
}

//------------------------------------------------------------------------------
void showVersion()
{
  std::cout << charguychess::VERSION_MAJOR << "." <<
      charguychess::VERSION_MINOR << "." <<
      charguychess::VERSION_BUGFIX <<
      " (" << charguychess::VERSION_GIT << ")" << std::endl;

  std::cout << "r4nd0m6uy lib: " <<
      rglib::VERSION_MAJOR << "." <<
      rglib::VERSION_MINOR << "." <<
      rglib::VERSION_BUGFIX <<
      " (" << rglib::VERSION_GIT << ")" << std::endl;
}

//------------------------------------------------------------------------------
int parseArgs(int argc, char* argv[])
{
  int opt;
  static struct option long_options[] = {
      {"help",          no_argument,        0, 'h'},
      {"version",       no_argument,        0, 'v'},
      {"loglevel",      required_argument,  0, 'l'},
      {"config",        required_argument,  0, 'c'},
      {"log-config",    required_argument,  0, 'L'},
      {"daemonize",     no_argument,        0, 'd'},
      {"pidfile",       required_argument,  0, 'p'},
      {0,               0,                  0, 0}
  };

  while ((opt = getopt_long(argc, argv, "hvc:l:L:dp:", long_options, 0)) != -1)
  {
    switch (opt)
    {
    case 'h':
      showHelp(argv[0]);
      return 1;
    case 'v':
      showVersion();
      return 1;
    case 'c':
      configFile = optarg;
      break;
    case 'l':
      maxLogLevel = (rglib::ILogSink::LogLevel)atoi(optarg);
      break;
    case 'L':
      log4CplusConfig = optarg;
      break;
    case 'd':
      isDaemonize = true;
      break;
    case 'p':
      pidFile = optarg;
      break;
    default:
      showHelp(argv[0]);
      return -1;
    }
  }

  return 0;
}

//------------------------------------------------------------------------------
int initLogging()
{
  int err = 0;

  if(log4CplusConfig != "")
    err = rglib::LoggerInstance::getInstance().log4cplus(log4CplusConfig);
  else
  {
    err = rglib::LoggerInstance::getInstance().consoleLogger();
    rglib::LoggerInstance::getInstance().setMaxLevel(maxLogLevel);
  }

  return err;
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  int ret;
  charguychess::CharGuyChess charguychess;
  std::unique_ptr<rglib::IDaemonizer> daemonizer;
  int retCode;

  retCode = parseArgs(argc, argv);
  if(retCode < 0)
    return retCode;
  else if(retCode > 0)
    return 0;

  if(initLogging())
    return -1;
  if(charguychess.init(configFile))
    return -1;

  if(isDaemonize)
  {
    int daemonizeStatus;

    rglib::IDaemonizer::create(daemonizer);
    daemonizer->setPidFile(pidFile);
    daemonizeStatus = daemonizer->daemonize();

    if(daemonizeStatus < 0)
      return daemonizeStatus;
    else if(daemonizeStatus > 0)
      return 0;

    // Must survive to fork!
    if(charguychess.reinit())
    {
      daemonizer->removePidFile();
      return -1;
    }
  }

  ret = charguychess.run();

  if(daemonizer)
    daemonizer->removePidFile();

  LOGIN() << "Exiting application with code " << ret;

  return ret;
}
