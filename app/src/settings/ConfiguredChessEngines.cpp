/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../settings/ConfiguredChessEngines.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ConfiguredChessEngines::ConfiguredChessEngines()
{
}

//------------------------------------------------------------------------------
ConfiguredChessEngines::~ConfiguredChessEngines()
{
}

//------------------------------------------------------------------------------
void ConfiguredChessEngines::clear()
{
  m_enginesOption.clear();
}

//------------------------------------------------------------------------------
void ConfiguredChessEngines::addChessEngine(const ChessEngineSettings& engine)
{
  m_enginesOption.emplace(engine.getName(), engine);
}

//------------------------------------------------------------------------------
bool ConfiguredChessEngines::getChessEngineSettings(
      const std::string name, ChessEngineSettings& settings) const
{
  auto found = m_enginesOption.find(name);

  if(found == m_enginesOption.end())
    return false;

  settings = found->second;

  return true;
}

}       // namespace
