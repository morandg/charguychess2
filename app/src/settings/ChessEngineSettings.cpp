/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessEngineSettings.hpp"

namespace charguychess {

const std::string ChessEngineSettings::DEFAULT_NAME  = "DEFAULT";
static const std::string DEFAULT_COMMAND            = "/bin/stockfish";

//------------------------------------------------------------------------------
ChessEngineSettings::ChessEngineSettings():
    m_name(DEFAULT_NAME),
    m_command(DEFAULT_COMMAND)
{
}

//------------------------------------------------------------------------------
ChessEngineSettings::~ChessEngineSettings()
{
}

//------------------------------------------------------------------------------
const std::string& ChessEngineSettings::getName() const
{
  return m_name;
}

//------------------------------------------------------------------------------
void ChessEngineSettings::setName(const std::string& name)
{
  m_name = name;
}

//------------------------------------------------------------------------------
const std::string& ChessEngineSettings::getCommand() const
{
  return m_command;
}

//------------------------------------------------------------------------------
void ChessEngineSettings::setCommand(const std::string& command)
{
  m_command = command;
}

}       // namespace
