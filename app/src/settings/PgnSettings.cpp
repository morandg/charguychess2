/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PgnSettings.hpp"

namespace charguychess {

static const std::string DEFAULT_DEST_FOLDER    = "/tmp";
static const std::string DEFAULT_EVENT          = "???";
static const std::string DEFAULT_SITE           = "???";

//------------------------------------------------------------------------------
PgnSettings::PgnSettings():
    m_destFolder(DEFAULT_DEST_FOLDER),
    m_event(DEFAULT_EVENT),
    m_site(DEFAULT_SITE)
{
}

//------------------------------------------------------------------------------
PgnSettings::~PgnSettings()
{
}

//------------------------------------------------------------------------------
std::string PgnSettings::getDestFolder() const
{
  return m_destFolder;
}

//------------------------------------------------------------------------------
void PgnSettings::setDestFolder(const std::string& destFolder)
{
  m_destFolder = destFolder;
}

//------------------------------------------------------------------------------
std::string PgnSettings::getEvent() const
{
  return m_event;
}

//------------------------------------------------------------------------------
void PgnSettings::setEvent(const std::string& event)
{
  m_event = event;
}

//------------------------------------------------------------------------------
std::string PgnSettings::getSite() const
{
  return m_site;
}

//------------------------------------------------------------------------------
void PgnSettings::setSite(const std::string& site)
{
  m_site = site;
}

}       // namespace
