/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Settings.hpp"

#include <rglib/logging/LogMacros.hpp>


namespace charguychess {

//------------------------------------------------------------------------------
Settings::Settings()
{
  // Create default valid configuration
  m_configuredChessengines.addChessEngine(ChessEngineSettings());
}

//------------------------------------------------------------------------------
Settings::~Settings()
{
}

//------------------------------------------------------------------------------
int Settings::parse(const std::string& file)
{
  libconfig::Config config;

  LOGDB() << "Parsing configuration " << file;
  try
  {
    config.readFile(file.c_str());
  }
  catch(const libconfig::ParseException& ex)
  {
    LOGER() << "Cannot parse configuration " << file << " on line " <<
        ex.getLine() << ": " << ex.getError();
    return -1;
  }
  catch(const std::exception& ex)
  {
    LOGER() << "Cannot parse configuration " << file << ": " << ex.what();
    return -1;
  }

  // Parse all you can!
  if(parseTelnetSettings(config) |
      parseChessEngineSettings(config) |
      parseAiSettings(config) |
      parsePgnSettings(config))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
const TelnetSettings& Settings::getTelnetSettings() const
{
  return m_telnetSettings;
}

//------------------------------------------------------------------------------
const ConfiguredChessEngines& Settings::getConfiguredEngines() const
{
  return m_configuredChessengines;
}

//------------------------------------------------------------------------------
const AisSettings& Settings::getAisSettings() const
{
  return m_aisSettings;
}

//------------------------------------------------------------------------------
void Settings::setAisSettings(const AisSettings& aisSettings)
{
  m_aisSettings = aisSettings;
}

//------------------------------------------------------------------------------
const PgnSettings& Settings::getPgnSettings() const
{
  return m_pgnSettings;
}

//------------------------------------------------------------------------------
int Settings::parseTelnetSettings(libconfig::Config& settings)
{
  bool enabled;
  std::string bindIp;
  unsigned int bindPort;

  if(settings.lookupValue("telnet.enabled", enabled))
    m_telnetSettings.setEnabled(enabled);

  if(settings.lookupValue("telnet.bind", bindIp))
    m_telnetSettings.setBindIp(bindIp);

  if(settings.lookupValue("telnet.port", bindPort))
    m_telnetSettings.setPort(bindPort);

  return 0;
}

//------------------------------------------------------------------------------
int Settings::parseChessEngineSettings(libconfig::Config& settings)
{
  libconfig::Setting& enginesSettings = settings.lookup("engines");

  m_configuredChessengines.clear();
  for(auto& engineConfig: enginesSettings)
  {
    std::string foundSetting;
    ChessEngineSettings chessEngineSettings;

    if(!engineConfig.lookupValue("name", foundSetting))
    {
      LOGWA() << "Missing engine name in configuration, ignoring";
      continue;
    }
    chessEngineSettings.setName(foundSetting);

    if(!engineConfig.lookupValue("command", foundSetting))
    {
      LOGWA() << "Missing engine command in configuration, ignoring";
      continue;
    }
    chessEngineSettings.setCommand(foundSetting);

    m_configuredChessengines.addChessEngine(chessEngineSettings);
  }

  return 0;
}

//------------------------------------------------------------------------------
int Settings::parseAiSettings(libconfig::Config& settings)
{
  std::string hintEngine;
  AiSettings aiSettings;

  if(parseColorAiSettings(settings, "white", aiSettings))
    return -1;
  m_aisSettings.setWhiteAiSettings(aiSettings);

  if(parseColorAiSettings(settings, "black", aiSettings))
    return -1;
  m_aisSettings.setBlackAiSettings(aiSettings);

  if(parseColorAiSettings(settings, "hint", aiSettings))
    return -1;
  m_aisSettings.setHintAiSettings(aiSettings);

  return 0;
}

//------------------------------------------------------------------------------
int Settings::parseColorAiSettings(
    libconfig::Config& settings,
    const std::string& color,
    AiSettings& aiSettings)
{
  std::string aiName;

  if(!settings.lookupValue("ai." + color + ".engine", aiName))
    aiName =  AiSettings::ENGINE_NAME_HUMAN;
  aiSettings.setEngineName(aiName);

  if(aiName == AiSettings::ENGINE_NAME_HUMAN)
    return 0;
  return parseAiModeSettings(settings, color, aiSettings);
}

//------------------------------------------------------------------------------
int Settings::parseAiModeSettings(
    libconfig::Config& settings,
    const std::string& color,
    AiSettings& aiSettings)
{
  unsigned int search;

  if(settings.lookupValue("ai." + color + ".depth", search))
    aiSettings.setSearcDepth(search);
  else if(settings.lookupValue("ai." + color + ".time", search))
    aiSettings.setSearchTime(search);
  else
  {
    LOGER() << "No search mode configured for " << color << " AI";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int Settings::parsePgnSettings(libconfig::Config& settings)
{
  std::string setting;

  if(settings.lookupValue("pgn.destination", setting))
    m_pgnSettings.setDestFolder(setting);
  if(settings.lookupValue("pgn.event", setting))
    m_pgnSettings.setEvent(setting);
  if(settings.lookupValue("pgn.site", setting))
    m_pgnSettings.setSite(setting);

  return 0;
}

}       // namespace
