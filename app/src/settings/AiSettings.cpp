/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AiSettings.hpp"

namespace charguychess {

const std::string AiSettings::ENGINE_NAME_HUMAN      = "human";
static const AiSettings::Mode DEFAULT_SEARCH_MODE    = AiSettings::TIME;
static const unsigned int DEFAULT_SEARCH_DEPTH      = 15;
static const unsigned int DEFAULT_SEARCH_TIME       = 1000;

//------------------------------------------------------------------------------
AiSettings::AiSettings():
  m_engineName(ENGINE_NAME_HUMAN),
  m_searchMode(DEFAULT_SEARCH_MODE),
  m_searchDepth(DEFAULT_SEARCH_DEPTH),
  m_searchTime(DEFAULT_SEARCH_TIME)
{
}

//------------------------------------------------------------------------------
AiSettings::~AiSettings()
{
}

//------------------------------------------------------------------------------
bool AiSettings::isHuman() const
{
  return m_engineName == ENGINE_NAME_HUMAN;
}

//------------------------------------------------------------------------------
const std::string& AiSettings::getEngineName() const
{
  return m_engineName;
}

//------------------------------------------------------------------------------
void AiSettings::setEngineName(const std::string& engineName)
{
  m_engineName = engineName;
}

//------------------------------------------------------------------------------
AiSettings::Mode AiSettings::getSearchMode() const
{
  return m_searchMode;
}

//------------------------------------------------------------------------------
unsigned int AiSettings::getSearchTime() const
{
  return m_searchTime;
}

//------------------------------------------------------------------------------
void AiSettings::setSearchTime(unsigned int searchTime)
{
  m_searchMode = TIME;
  m_searchTime= searchTime;
}

//------------------------------------------------------------------------------
unsigned int AiSettings::getSearchDepth() const
{
  return m_searchDepth;
}

//------------------------------------------------------------------------------
void AiSettings::setSearcDepth(unsigned int searchDepth)
{
  m_searchMode = DEPTH;
  m_searchDepth = searchDepth;
}

}       // namespace
