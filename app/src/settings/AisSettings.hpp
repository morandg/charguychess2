/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_IAS_SETTINGS_HPP_
#define _CGC_IAS_SETTINGS_HPP_

#include "../chess/ChessDefs.hpp"
#include "AiSettings.hpp"

namespace charguychess {

class AisSettings
{
public:
  AisSettings();
  ~AisSettings();

  bool isHuman(Color color) const;
  void setWhiteAiSettings(const AiSettings& whiteSettings);
  const AiSettings& getWhiteAiSettings() const;
  void setBlackAiSettings(const AiSettings& blackSettings);
  const AiSettings& getBlackAiSettings() const;
  void setHintAiSettings(const AiSettings& hintSettings);
  const AiSettings& getHintAiSettings() const;

private:
  AiSettings m_whiteAiSettings;
  AiSettings m_blackAiSettings;
  AiSettings m_hintAiSettings;
};

}       // namespace
#endif  // _CGC_IAS_SETTINGS_HPP_
