/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessEngineSettings.hpp"
#include "AisSettings.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
AisSettings::AisSettings()
{
  m_hintAiSettings.setEngineName(ChessEngineSettings::DEFAULT_NAME);
}

//------------------------------------------------------------------------------
AisSettings::~AisSettings()
{
}

//------------------------------------------------------------------------------
bool AisSettings::isHuman(Color color) const
{
  if(color == Color::WHITE)
    return m_whiteAiSettings.isHuman();

  return m_blackAiSettings.isHuman();
}

//------------------------------------------------------------------------------
void AisSettings::setWhiteAiSettings(const AiSettings& whiteSettings)
{
  m_whiteAiSettings = whiteSettings;
}

//------------------------------------------------------------------------------
const AiSettings& AisSettings::getWhiteAiSettings() const
{
  return m_whiteAiSettings;
}

//------------------------------------------------------------------------------
void AisSettings::setBlackAiSettings(const AiSettings& blackSettings)
{
  m_blackAiSettings = blackSettings;
}

//------------------------------------------------------------------------------
const AiSettings& AisSettings::getBlackAiSettings() const
{
  return m_blackAiSettings;
}

//------------------------------------------------------------------------------
void AisSettings::setHintAiSettings(const AiSettings& hintSettings)
{
  m_hintAiSettings = hintSettings;
}

//------------------------------------------------------------------------------
const AiSettings& AisSettings::getHintAiSettings() const
{
  return m_hintAiSettings;
}

}       // namespace
