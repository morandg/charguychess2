/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CONFIGURED_CHESS_ENGINES_HPP_
#define _CGC_CONFIGURED_CHESS_ENGINES_HPP_

#include <unordered_map>

#include "../settings/ChessEngineSettings.hpp"

namespace charguychess {

class ConfiguredChessEngines
{
public:
  ConfiguredChessEngines();
  ~ConfiguredChessEngines();

  void clear();
  void addChessEngine(const ChessEngineSettings& engine);
  bool getChessEngineSettings(
      const std::string name, ChessEngineSettings& settings) const;

private:
  std::unordered_map<std::string, ChessEngineSettings> m_enginesOption;
};

}       // namespace
#endif  // _CGC_CONFIGURED_CHESS_ENGINES_HPP_
