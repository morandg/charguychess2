/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TelnetSettings.hpp"

namespace charguychess {

static bool DEFAULT_ENABLED                 = true;
static const unsigned int DEFAULT_PORT      = 2323;
static const std::string DEFAULT_BIND_IP    = "";

//------------------------------------------------------------------------------
TelnetSettings::TelnetSettings():
    m_isEnabled(DEFAULT_ENABLED),
    m_port(DEFAULT_PORT),
    m_bindIp(DEFAULT_BIND_IP)
{
}

//------------------------------------------------------------------------------
TelnetSettings::~TelnetSettings()
{
}

//------------------------------------------------------------------------------
bool TelnetSettings::isEnabled() const
{
  return m_isEnabled;
}

//------------------------------------------------------------------------------
void TelnetSettings::setEnabled(bool isEnabled)
{
  m_isEnabled = isEnabled;
}

//------------------------------------------------------------------------------
unsigned int TelnetSettings::getPort() const
{
  return m_port;
}

//------------------------------------------------------------------------------
void TelnetSettings::setPort(unsigned int port)
{
  m_port = port;
}

//------------------------------------------------------------------------------
const std::string& TelnetSettings::getBindIp() const
{
  return m_bindIp;
}

//------------------------------------------------------------------------------
void TelnetSettings::setBindIp(const std::string& bindIp)
{
  m_bindIp = bindIp;
}

}       // namespace
