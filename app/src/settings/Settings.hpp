/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_SETTINGS_HPP_
#define _CGC_SETTINGS_HPP_

#include <libconfig.h++>

#include "ConfiguredChessEngines.hpp"
#include "AisSettings.hpp"
#include "ChessEngineSettings.hpp"
#include "TelnetSettings.hpp"
#include "PgnSettings.hpp"

namespace charguychess {

class Settings
{
public:
  Settings();
  ~Settings();

  int parse(const std::string& file);
  const TelnetSettings& getTelnetSettings() const;
  const ConfiguredChessEngines& getConfiguredEngines() const;
  const AisSettings& getAisSettings() const;
  void setAisSettings(const AisSettings& aisSettings);
  const PgnSettings& getPgnSettings() const;

private:
  TelnetSettings m_telnetSettings;
  ConfiguredChessEngines m_configuredChessengines;
  AisSettings m_aisSettings;
  PgnSettings m_pgnSettings;

  int parseTelnetSettings(libconfig::Config& settings);
  int parseChessEngineSettings(libconfig::Config& settings);
  int parseAiSettings(libconfig::Config& settings);
  int parseColorAiSettings(
      libconfig::Config& settings,
      const std::string& color,
      AiSettings& aiSettings);
  int parseAiModeSettings(
      libconfig::Config& settings,
      const std::string& color,
      AiSettings& aiSettings);
  int parsePgnSettings(libconfig::Config& settings);
};

}       // namespace
#endif  // _CGC_SETTINGS_HPP_
