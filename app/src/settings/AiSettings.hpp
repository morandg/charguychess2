/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_IA_SETTINGS_HPP_
#define _CGC_IA_SETTINGS_HPP_

#include <string>

namespace charguychess {

class AiSettings
{
public:
  enum Mode {
    TIME,
    DEPTH
  };

  static const std::string ENGINE_NAME_HUMAN;

  AiSettings();
  ~AiSettings();

  bool isHuman() const;
  const std::string& getEngineName() const;
  void setEngineName(const std::string& engineName);
  Mode getSearchMode() const;
  unsigned int getSearchTime() const;
  void setSearchTime(unsigned int searchTime);
  unsigned int getSearchDepth() const;
  void setSearcDepth(unsigned int searchDepth);

private:
  std::string m_engineName;
  Mode m_searchMode;
  unsigned int m_searchDepth;
  unsigned int m_searchTime;
};

}       // namespace
#endif  // _CGC_IA_SETTINGS_HPP_
