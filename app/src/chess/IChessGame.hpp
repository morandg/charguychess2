/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_CHESS_GAME_HPP_
#define _CGC_I_CHESS_GAME_HPP_

#include <memory>

#include "PlayerPiece.hpp"
#include "History.hpp"
#include "BitBoard.hpp"
#include "CastlingStatus.hpp"
#include "IGameObserver.hpp"
#include "PiecesBitboards.hpp"

namespace charguychess {

class IChessGame
{
public:
  IChessGame();
  virtual ~IChessGame();

  virtual void newGame() = 0;
  virtual bool applyMove(Move& move) = 0;
  virtual bool pieceAt(const Square& at, PlayerPiece& piece) const = 0;
  virtual Color currentPlayer() const = 0;
  virtual unsigned int turnNumber() const = 0;
  virtual void clear(const Square& square) = 0;
  virtual BitBoard moveMap(const Square& from) = 0;
  virtual BitBoard allPieces() const = 0;
  virtual BitBoard playerPieces() = 0;
  virtual BitBoard opponentPieces() = 0;
  virtual BitBoard playerPieces(Piece piece) = 0;
  virtual void setPlayerPieces(Piece piece, const BitBoard& bitboard) = 0;
  virtual BitBoard opponentPieces(Piece piece) = 0;
  virtual void setOpponentPieces(Piece piece, const BitBoard& bitboard) = 0;
  virtual bool isQuenSideCastlingAllowed(const Square& from) = 0;
  virtual bool isKingSideCastlingAllowed(const Square& from) = 0;
  virtual BitBoard playerAattackMap() = 0;
  virtual BitBoard opponentAttackMap() = 0;
  virtual bool isPromotionalMove(const Square& from) const = 0;
  virtual bool isChecked() = 0;
  virtual bool isMate() = 0;
  virtual bool isStaleMate() = 0;
  virtual Turn lastTurn() const = 0;
  virtual std::string history() const = 0;
  virtual std::string historyUci() const = 0;
  virtual std::string historyPGN() const = 0;
  virtual void addObserver(IGameObserver& observer) = 0;
  virtual void removeObserver(IGameObserver& observer) = 0;
  virtual void clone(std::unique_ptr<IChessGame>& clone) const = 0;
};

}       // namespace
#endif  // _CGC_I_CHESS_GAME_HPP_
