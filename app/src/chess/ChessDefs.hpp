/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_DEFS_HPP_
#define _CGC_CHESS_DEFS_HPP_

namespace charguychess {

enum class Rank {
  ONE   = 0,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  INVALID
};

enum class File {
  A   = 0,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  INVALID
};

enum class Color {
  WHITE =0,
  BLACK,
  COUNT
};

enum class Piece {
  PAWN = 0,
  ROOK,
  KNIGHT,
  BISHOP,
  QUEEN,
  KING,
  COUNT
};

bool charToFile(char fileChar, File& file);
bool fileToChar(File file, char& fileChar);
bool charToRank(char rankChar, Rank& rank);
bool rankToChar(Rank rank, char& rankChar);

}       // namespace
#endif  // _CGC_CHESS_DEFS_HPP_
