/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>

#include "Move.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
Move::Move():
  m_promotion(Piece::PAWN),
  m_movedPiece(Piece::PAWN),
  m_isFileAmbiguous(false),
  m_isRankAmbiguous(false),
  m_isCapture(false),
  m_isKingSideCastling(false),
  m_isQueenSideCastling(false),
  m_isChecked(false),
  m_isMate(false)
{
}

//------------------------------------------------------------------------------
Move::Move(const Square& from, const Square& to):
  m_promotion(Piece::PAWN),
  m_movedPiece(Piece::PAWN),
  m_isFileAmbiguous(false),
  m_isRankAmbiguous(false),
  m_isCapture(false),
  m_isKingSideCastling(false),
  m_isQueenSideCastling(false),
  m_isChecked(false),
  m_isMate(false),
  m_from(from),
  m_to(to)
{
}

//------------------------------------------------------------------------------
Move::~Move()
{
}

//------------------------------------------------------------------------------
const Square& Move::from() const
{
  return m_from;
}

//------------------------------------------------------------------------------
const Square& Move::to() const
{
  return m_to;
}

//------------------------------------------------------------------------------
bool Move::isValid() const
{
  return m_from.isValid() &&
      m_to.isValid();
}

//------------------------------------------------------------------------------
bool Move::fromString(const std::string& moveString)
{
  std::string fromStr;
  std::string toStr;
  std::size_t strSize = moveString.length();

  // XXX Parse move from PGN with ambiguity, capture, check, promotion,
  // castling, ...
  // * O-O
  // * e4
  // * Qxe2
  // * Nb2

  if(strSize < 4 || strSize > 5)
    return false;

  fromStr = moveString.substr(0, 2);
  toStr = moveString.substr(2, 2);

  // Parse promotional move
  if(moveString.length() == 5)
  {
    switch(moveString[4])
    {
    case 'r':
      setPromotion(Piece::ROOK);
      break;
    case 'k':
      setPromotion(Piece::KNIGHT);
      break;
    case 'b':
      setPromotion(Piece::BISHOP);
      break;
    case 'q':
      setPromotion(Piece::QUEEN);
      break;
    default:
      return false;
    }
  }

  return m_from.fromString(fromStr) &&
      m_to.fromString(toStr);
}

//------------------------------------------------------------------------------
std::string Move::toString() const
{
  std::string moveStr = m_from.toString() + m_to.toString();

  switch(promotion())
  {
  case Piece::ROOK:
    moveStr += "r";
    break;
  case Piece::KNIGHT:
    moveStr += "k";
    break;
  case Piece::BISHOP:
    moveStr += "b";
    break;
  case Piece::QUEEN:
    moveStr += "q";
    break;
  default:
    break;
  }

  return moveStr;
}

//------------------------------------------------------------------------------
std::string Move::toPGN() const
{
  std::string pgnStr;

  if(m_isKingSideCastling ||
      m_isQueenSideCastling)
  {
    if(m_isKingSideCastling)
      pgnStr = "0-0";
    else
      pgnStr = "0-0-0";
  }
  else
  {
    pgnStr = pieceToPGN(m_movedPiece);

    if(m_isFileAmbiguous)
    {
      char fromFile;

      if(fileToChar(m_from.file(), fromFile))
        pgnStr += fromFile;
    }

    if(m_isRankAmbiguous)
    {
      char fromRank;

      if(rankToChar(m_from.rank(), fromRank))
        pgnStr += fromRank;
    }

    if(m_isCapture)
    {
      if(m_movedPiece == Piece::PAWN)
      {
        char fromFile;

        if(fileToChar(m_from.file(), fromFile))
          pgnStr += fromFile;
      }

      pgnStr += "x";
    }

    pgnStr += m_to.toString();

    if(m_promotion != Piece::PAWN)
      pgnStr += "=" + pieceToPGN(m_promotion);
  }

  if(m_isMate)
    pgnStr += "#";
  else if(m_isChecked)
    pgnStr += "+";

  return pgnStr;
}

//------------------------------------------------------------------------------
Piece Move::promotion() const
{
  return m_promotion;
}

//------------------------------------------------------------------------------
void Move::setPromotion(Piece piece)
{
  m_promotion = piece;
}

//------------------------------------------------------------------------------
void Move::setMovedPiece(Piece piece)
{
  m_movedPiece = piece;
}

//------------------------------------------------------------------------------
Piece Move::getMovedPiece() const
{
  return m_movedPiece;
}

//------------------------------------------------------------------------------
void Move::setFileAmbiguous(bool isAmbiguous)
{
  m_isFileAmbiguous = isAmbiguous;
}

//------------------------------------------------------------------------------
bool Move::isFileAmbiguous() const
{
  return m_isFileAmbiguous;
}

//------------------------------------------------------------------------------
void Move::setRankAmbiguous(bool isAmbiguous)
{
  m_isRankAmbiguous = isAmbiguous;
}

//------------------------------------------------------------------------------
bool Move::isRankAmbiguous() const
{
  return m_isRankAmbiguous;
}

//------------------------------------------------------------------------------
void Move::setCapture(bool isCapture)
{
  m_isCapture = isCapture;
}

//------------------------------------------------------------------------------
bool Move::isCapture() const
{
  return m_isCapture;
}

//------------------------------------------------------------------------------
void Move::setKingSideCastling(bool isKingSideCastling)
{
  m_isKingSideCastling = isKingSideCastling;
}

//------------------------------------------------------------------------------
bool Move::isKingSideCastling() const
{
  return m_isKingSideCastling;
}

//------------------------------------------------------------------------------
void Move::setQueenSideCastling(bool isQueenSideCastling)
{
  m_isQueenSideCastling = isQueenSideCastling;
}

//------------------------------------------------------------------------------
bool Move::isQueenSideCastling() const
{
  return m_isQueenSideCastling;
}

//------------------------------------------------------------------------------
void Move::setCheck(bool isChecked)
{
  m_isChecked = isChecked;
}

//------------------------------------------------------------------------------
bool Move::isCheck() const
{
  return m_isChecked;
}

//------------------------------------------------------------------------------
void Move::setMate(bool isMate)
{
  m_isMate = isMate;
}

//------------------------------------------------------------------------------
bool Move::isMate() const
{
  return m_isMate;
}

//------------------------------------------------------------------------------
bool Move::operator==(const Move& rhs) const
{
  return m_from == rhs.from() &&
      m_to == rhs.to();
}

//------------------------------------------------------------------------------
bool Move::operator!=(const Move& rhs) const
{
  return !(*this == rhs);
}

//------------------------------------------------------------------------------
std::string Move::pieceToPGN(Piece piece) const
{
  switch(piece)
  {
  case Piece::ROOK:
    return "R";
    break;
  case Piece::KNIGHT:
    return "N";
    break;
  case Piece::BISHOP:
    return "B";
    break;
  case Piece::QUEEN:
    return "Q";
    break;
  case Piece::KING:
    return "K";
    break;
  default:
    break;
  }

  return "";
}

}       // namespace
