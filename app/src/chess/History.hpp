/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_HISTORY_HPP_
#define _CGC_HISTORY_HPP_

#include <list>

#include "Turn.hpp"

namespace charguychess {

class History
{
public:
  History();
  ~History();

  void addMove(const Move& move);
  void setCheck();
  void setMate();
  Color currentPlayer() const;
  std::string toString() const;
  Turn lastTurn() const;
  Move lastMove() const;
  unsigned int turnNumber() const;
  std::string uciString() const;
  std::string pgnString() const;
  void clear();

private:
  std::list<Turn> m_turns;

  void replaceLastMove(const Move& move);
};

}       // namespace
#endif  // _CGC_HISTORY_HPP_
