/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>

#include "SquaresList.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
SquaresList::SquaresList()
{
}

//------------------------------------------------------------------------------
SquaresList::~SquaresList()
{
}

//------------------------------------------------------------------------------
void SquaresList::add(const Square& square)
{
  m_squares.push_back(square);
}

//------------------------------------------------------------------------------
bool SquaresList::contains(const Square& square)
{
  return
      std::find(m_squares.begin(), m_squares.end(), square) != m_squares.end();
}

//------------------------------------------------------------------------------
bool SquaresList::contains(const Move& move)
{
  return contains(move.from()) &&
      contains(move.to());
}

//------------------------------------------------------------------------------
bool SquaresList::at(unsigned int index, Square& square)
{
  if(index >= m_squares.size())
    return false;

  square = m_squares[index];
  return true;
}

}       // namespace
