/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PlayerPiece.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PlayerPiece::PlayerPiece():
    m_color(Color::WHITE),
    m_piece(Piece::PAWN)
{
}

//------------------------------------------------------------------------------
PlayerPiece::PlayerPiece(Color color, Piece piece):
    m_color(color),
    m_piece(piece)
{
}

//------------------------------------------------------------------------------
PlayerPiece::~PlayerPiece()
{
}

//------------------------------------------------------------------------------
Color PlayerPiece::color() const
{
  return m_color;
}

//------------------------------------------------------------------------------
Piece PlayerPiece::piece() const
{
  return m_piece;
}

//------------------------------------------------------------------------------
char PlayerPiece::toChar() const
{
  char pieceChar;

  switch(m_piece)
  {
  case Piece::PAWN:
    pieceChar = 'P';
    break;
  case Piece::ROOK:
    pieceChar = 'R';
    break;
  case Piece::KNIGHT:
    pieceChar = 'N';
    break;
  case Piece::BISHOP:
    pieceChar = 'B';
    break;
  case Piece::QUEEN:
    pieceChar = 'Q';
    break;
  case Piece::KING:
    pieceChar = 'K';
    break;
  default:
    return 0;
  }

  // To lower case
  if(m_color == Color::BLACK)
    pieceChar += 32;
  else if(m_color != Color::WHITE)
    return 0;

  return pieceChar;
}

}       // namespace
