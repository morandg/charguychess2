/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_GAME_HPP_
#define _CGC_CHESS_GAME_HPP_

#include <memory>
#include <functional>
#include <list>

#include "rules/IPieceRules.hpp"
#include "IChessGame.hpp"

namespace charguychess {

class ChessGame:
    public IChessGame
{
public:
  ChessGame();
  virtual ~ChessGame();

  // IChessGame
  virtual void newGame() override;
  virtual bool applyMove(Move& move) override;
  virtual bool pieceAt(const Square& at, PlayerPiece& piece) const override;
  virtual Color currentPlayer() const override;
  virtual unsigned int turnNumber() const override;
  virtual void clear(const Square& square) override;
  virtual BitBoard moveMap(const Square& from) override;
  virtual BitBoard allPieces() const override;
  virtual BitBoard playerPieces() override;
  virtual BitBoard opponentPieces() override;
  virtual BitBoard playerPieces(Piece piece) override;
  virtual void setPlayerPieces(
      Piece piece, const BitBoard& bitboard) override;
  virtual BitBoard opponentPieces(Piece piece) override;
  virtual void setOpponentPieces(
      Piece piece, const BitBoard& bitboard) override;
  virtual bool isQuenSideCastlingAllowed(const Square& from) override;
  virtual bool isKingSideCastlingAllowed(const Square& from) override;
  virtual BitBoard playerAattackMap() override;
  virtual BitBoard opponentAttackMap() override;
  virtual bool isPromotionalMove(const Square& from) const override;
  virtual bool isChecked() override;
  virtual bool isMate() override;
  virtual bool isStaleMate() override;
  virtual Turn lastTurn() const override;
  virtual std::string history() const override;
  virtual std::string historyUci() const override;
  virtual std::string historyPGN() const override;
  virtual void addObserver(IGameObserver& observer) override;
  virtual void removeObserver(IGameObserver& observer) override;
  virtual void clone(std::unique_ptr<IChessGame>& clone) const override;

private:
  History m_history;
  std::unordered_map<Color, PiecesBitboards> m_playerBitBoards;
  std::unordered_map<Piece, std::unique_ptr<IPieceRules>> m_piecesRules;
  std::list<std::reference_wrapper<IGameObserver>> m_observers;

  PiecesBitboards& playerPiecesBitboards();
  PiecesBitboards& opponentPiecesBitboards();
  BitBoard attackMap(PiecesBitboards& pBb);
  void updateCastlingStatus(const Square& from, Piece movedPiece);
  bool canPlayerMove();

protected:
  /* For spy */
  CastlingStatus m_castlingStatus;
};

}       // namespace
#endif  // _CGC_CHESS_GAME_HPP_
