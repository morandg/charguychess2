/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>

#include "History.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
History::History()
{
}

//------------------------------------------------------------------------------
History::~History()
{
}

//------------------------------------------------------------------------------
void History::addMove(const Move& move)
{
  if(currentPlayer() == Color::WHITE)
  {
    m_turns.push_back(Turn());
    m_turns.back().whiteMove(move);
  }
  else
    m_turns.back().blackMove(move);
}

//------------------------------------------------------------------------------
void History::setCheck()
{
  Move checkedMove = lastMove();

  if(!checkedMove.isValid())
    return;

  checkedMove.setCheck(true);
  replaceLastMove(checkedMove);
}

//------------------------------------------------------------------------------
void History::setMate()
{
  Move matedMove = lastMove();

  if(!matedMove.isValid())
    return;

  matedMove.setMate(true);
  replaceLastMove(matedMove);
}

//------------------------------------------------------------------------------
Color History::currentPlayer() const
{
  if(m_turns.empty())
    return Color::WHITE;

  Turn lastTurn = m_turns.back();
  if(lastTurn.blackMove().isValid())
    return Color::WHITE;
  else
    return Color::BLACK;
}

//------------------------------------------------------------------------------
std::string History::toString() const
{
  std::stringstream ss;
  int turnNumber = 1;

  for(const auto& turn: m_turns)
  {
    ss << turnNumber << ". " << turn.toString() << std::endl;
    ++turnNumber;
  }

  return ss.str();
}

//------------------------------------------------------------------------------
Turn History::lastTurn() const
{
  if(m_turns.empty())
    return Turn();

  return m_turns.back();
}

//------------------------------------------------------------------------------
Move History::lastMove() const
{
  Turn currentTurn = lastTurn();

  if(!currentTurn.blackMove().isValid())
    return currentTurn.whiteMove();
  else
    return currentTurn.blackMove();
}

//------------------------------------------------------------------------------
unsigned int History::turnNumber() const
{
  Turn lastTurn;

  if(m_turns.empty())
    return 1;

  lastTurn = m_turns.back();
  if(lastTurn.blackMove().isValid())
    return m_turns.size() + 1;
  else
    return m_turns.size();
}

//------------------------------------------------------------------------------
std::string History::uciString() const
{
  std::string uciString = "position startpos";

  if(!m_turns.empty())
  {
    uciString += " moves";

    for(const auto& turn: m_turns)
      uciString += " " + turn.toString();
  }

  return uciString;
}

//------------------------------------------------------------------------------
std::string History::pgnString() const
{
  std::stringstream ss;
  int turnNumber = 1;

  for(const auto& turn: m_turns)
  {
    ss << turnNumber << ". " << turn.toPGN();

    if(turnNumber != (int)m_turns.size())
      ss << " ";

    ++turnNumber;
  }

  return ss.str();
}

//------------------------------------------------------------------------------
void History::clear()
{
  m_turns.clear();
}

//------------------------------------------------------------------------------
void History::replaceLastMove(const Move& move)
{
  Turn turn = lastTurn();

  if(turn.blackMove().isValid())
    turn.blackMove(move);
  else
    turn.whiteMove(move);

  m_turns.pop_back();
  m_turns.push_back(turn);
}

}       // namespace
