/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PiecesBitboards.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PiecesBitboards::PiecesBitboards(Color color):
    m_color(color)
{
  initialPosition();
}

//------------------------------------------------------------------------------
PiecesBitboards::~PiecesBitboards()
{
}

//------------------------------------------------------------------------------
void PiecesBitboards::initialPosition()
{
  if(m_color == Color::WHITE)
  {
    m_pieces[Piece::PAWN]   = BitBoard(BitBoard::INITIAL_WHITE_PAWNS);
    m_pieces[Piece::ROOK]   = BitBoard(BitBoard::INITIAL_WHITE_ROOKS);
    m_pieces[Piece::KNIGHT] = BitBoard(BitBoard::INITIAL_WHITE_KNIGHTS);
    m_pieces[Piece::BISHOP] = BitBoard(BitBoard::INITIAL_WHITE_BISHOPS);
    m_pieces[Piece::QUEEN]  = BitBoard(BitBoard::INITIAL_WHITE_QUEEN);
    m_pieces[Piece::KING]   = BitBoard(BitBoard::INITIAL_WHITE_KING);
  }
  else
  {
    m_pieces[Piece::PAWN]   = BitBoard(BitBoard::INITIAL_BLACK_PAWNS);
    m_pieces[Piece::ROOK]   = BitBoard(BitBoard::INITIAL_BLACK_ROOKS);
    m_pieces[Piece::KNIGHT] = BitBoard(BitBoard::INITIAL_BLACK_KNIGHTS);
    m_pieces[Piece::BISHOP] = BitBoard(BitBoard::INITIAL_BLACK_BISHOPS);
    m_pieces[Piece::QUEEN]  = BitBoard(BitBoard::INITIAL_BLACK_QUEEN);
    m_pieces[Piece::KING]   = BitBoard(BitBoard::INITIAL_BLACK_KING);
  }
}

//------------------------------------------------------------------------------
Color PiecesBitboards::color() const
{
  return m_color;
}

//------------------------------------------------------------------------------
bool PiecesBitboards::pieceAt(const Square& square, Piece& piece) const
{
  for(int i = 0 ; i < (int)Piece::COUNT ; ++i)
  {
    if(m_pieces.at((Piece)i).isSet(square))
    {
      piece = (Piece)i;
      return true;
    }
  }

  return false;
}

//------------------------------------------------------------------------------
BitBoard PiecesBitboards::pieces() const
{
  BitBoard allPieces;

  for(auto& bitBoard: m_pieces)
    allPieces |= bitBoard.second;

  return allPieces;
}

//------------------------------------------------------------------------------
BitBoard PiecesBitboards::pieces(Piece piece) const
{
  return m_pieces.at(piece);
}

//------------------------------------------------------------------------------
void PiecesBitboards::setPieces(Piece piece, const BitBoard& bitBoard)
{
  m_pieces.at(piece) = bitBoard;
}

//------------------------------------------------------------------------------
void PiecesBitboards::clear(const Square& square)
{
  unsigned int bitNum = BitBoard::toBitNumber(square);

  for(auto& bitBoard: m_pieces)
    bitBoard.second.clear(bitNum);

}

}       // namespace
