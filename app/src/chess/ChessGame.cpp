/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>

#include <rglib/logging/LogMacros.hpp>

#include "rules/PieceRulesPawn.hpp"
#include "rules/PieceRulesKnight.hpp"
#include "rules/PieceRulesQueen.hpp"
#include "rules/PieceRulesKing.hpp"
#include "ChessGame.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessGame::ChessGame()
{
  m_playerBitBoards.emplace(Color::WHITE, PiecesBitboards(Color::WHITE));
  m_playerBitBoards.emplace(Color::BLACK, PiecesBitboards(Color::BLACK));

  m_piecesRules.emplace(
      Piece::PAWN, std::unique_ptr<IPieceRules>(new PieceRulesPawn()));
  m_piecesRules.emplace(
      Piece::KNIGHT, std::unique_ptr<IPieceRules>(new PieceRulesKnight()));
  m_piecesRules.emplace(
      Piece::BISHOP, std::unique_ptr<IPieceRules>(new PieceRulesBishop()));
  m_piecesRules.emplace(
      Piece::ROOK, std::unique_ptr<IPieceRules>(new PieceRulesRook()));
  m_piecesRules.emplace(
      Piece::QUEEN, std::unique_ptr<IPieceRules>(new PieceRulesQueen()));
  m_piecesRules.emplace(
      Piece::KING, std::unique_ptr<IPieceRules>(new PieceRulesKing()));
}

//------------------------------------------------------------------------------
ChessGame::~ChessGame()
{
}

//-----------------------------------------------------------------------------
void ChessGame::newGame()
{
  m_playerBitBoards.at(Color::WHITE).initialPosition();
  m_playerBitBoards.at(Color::BLACK).initialPosition();
  m_history.clear();

  for(IGameObserver& observer: m_observers)
    observer.onNewGameStarted();
}

//-----------------------------------------------------------------------------
bool ChessGame::applyMove(Move& move)
{
  Piece movedPiece;

  if(!playerPiecesBitboards().pieceAt(move.from(), movedPiece))
    return false;

  if(!m_piecesRules.at(movedPiece)->applyMove(move, *this))
    return false;

  updateCastlingStatus(move.from(), movedPiece);

  m_history.addMove(move);

  // Check / mate need to be done after that the move was added in history
  if(isChecked())
  {
    move.setCheck(true);
    m_history.setCheck();
  }
  if(isMate())
  {
    move.setMate(true);
    m_history.setMate();
  }

  for(IGameObserver& observer: m_observers)
    observer.onMovePlayed(move);

  return true;
}

//-----------------------------------------------------------------------------
bool ChessGame::pieceAt(const Square& at, PlayerPiece& piece) const
{
  Piece pieceType;

  if(m_playerBitBoards.at(Color::WHITE).pieceAt(at, pieceType))
    piece = PlayerPiece(Color::WHITE, pieceType);
  else if(m_playerBitBoards.at(Color::BLACK).pieceAt(at, pieceType))
    piece = PlayerPiece(Color::BLACK, pieceType);
  else
    return false;

  return true;
}

//------------------------------------------------------------------------------
Color ChessGame::currentPlayer() const
{
  return m_history.currentPlayer();
}

//------------------------------------------------------------------------------
unsigned int ChessGame::turnNumber() const
{
  return m_history.turnNumber();
}

//------------------------------------------------------------------------------
void ChessGame::clear(const Square& square)
{
  for(auto& playerBb: m_playerBitBoards)
    playerBb.second.clear(square);
}

//------------------------------------------------------------------------------
BitBoard ChessGame::moveMap(const Square& from)
{
  Piece movedPiece;
  PiecesBitboards playerPieces = playerPiecesBitboards();

  if(!playerPieces.pieceAt(from, movedPiece))
    return BitBoard();

  return m_piecesRules.at(movedPiece)->moveMap(from, *this);
}

//------------------------------------------------------------------------------
BitBoard ChessGame::allPieces() const
{
  BitBoard allPieces;

  for(auto playerBb: m_playerBitBoards)
    allPieces |= playerBb.second.pieces();

  return allPieces;
}

//------------------------------------------------------------------------------
BitBoard ChessGame::playerPieces()
{
  return playerPiecesBitboards().pieces();
}

//------------------------------------------------------------------------------
BitBoard ChessGame::opponentPieces()
{
  return opponentPiecesBitboards().pieces();
}

//------------------------------------------------------------------------------
BitBoard ChessGame::playerPieces(Piece piece)
{
  return playerPiecesBitboards().pieces(piece);
}

//------------------------------------------------------------------------------
void ChessGame::setPlayerPieces(Piece piece, const BitBoard& bitboard)
{
  playerPiecesBitboards().setPieces(piece, bitboard);
}

//------------------------------------------------------------------------------
BitBoard ChessGame::opponentPieces(Piece piece)
{
  return opponentPiecesBitboards().pieces(piece);
}

//------------------------------------------------------------------------------
void ChessGame::setOpponentPieces(Piece piece, const BitBoard& bitboard)
{
  opponentPiecesBitboards().setPieces(piece, bitboard);
}

//------------------------------------------------------------------------------
bool ChessGame::isQuenSideCastlingAllowed(const Square& from)
{
  PlayerPiece pp;

  // TODO
  // * Castling not allowed when crossing attacked square
  // * Castling not allowed when checked
  return
      m_castlingStatus.isQueenSideCastleAllowed(currentPlayer()) &&
        ((currentPlayer() == Color::BLACK &&
            from == Square(File::E, Rank::EIGHT) &&
            !pieceAt(Square(File::B, Rank::EIGHT), pp)) ||
        (currentPlayer() == Color::WHITE &&
            from == Square(File::E, Rank::ONE) &&
            !pieceAt(Square(File::B, Rank::ONE), pp)));
}

//------------------------------------------------------------------------------
bool ChessGame::isKingSideCastlingAllowed(const Square& from)
{
  // TODO
  // * Castling not allowed when crossing attacked square
  // * Castling not allowed when checked
  return
      m_castlingStatus.isKingSideCastleAllowed(currentPlayer()) &&
      ((currentPlayer() == Color::BLACK &&
          from == Square(File::E, Rank::EIGHT)) ||
       (currentPlayer() == Color::WHITE &&
          from == Square(File::E, Rank::ONE)));
}
//------------------------------------------------------------------------------
BitBoard ChessGame::playerAattackMap()
{
  return attackMap(playerPiecesBitboards());
}

//------------------------------------------------------------------------------
BitBoard ChessGame::opponentAttackMap()
{
  return attackMap(opponentPiecesBitboards());
}

//------------------------------------------------------------------------------
bool ChessGame::isPromotionalMove(const Square& from) const
{
  PlayerPiece pieceFrom;

  if(!pieceAt(from, pieceFrom) ||
      pieceFrom.piece() != Piece::PAWN)
    return false;

  if(currentPlayer() == Color::WHITE)
    return from.rank() == Rank::SEVEN;
  else
    return from.rank() == Rank::TWO;
}

//------------------------------------------------------------------------------
bool ChessGame::isChecked()
{
  BitBoard attack = opponentAttackMap();
  BitBoard king = playerPiecesBitboards().pieces(Piece::KING);

  return (attack & king).getValue();
}

//------------------------------------------------------------------------------
bool ChessGame::isMate()
{
  return !canPlayerMove() && isChecked();
}

//------------------------------------------------------------------------------
bool ChessGame::isStaleMate()
{
  return !canPlayerMove() && !isChecked();
}

//------------------------------------------------------------------------------
Turn ChessGame::lastTurn() const
{
  return m_history.lastTurn();
}

//------------------------------------------------------------------------------
std::string ChessGame::history() const
{
  return m_history.toString();
}

//------------------------------------------------------------------------------
std::string ChessGame::historyUci() const
{
  return m_history.uciString();
}

//------------------------------------------------------------------------------
std::string ChessGame::historyPGN() const
{
  return m_history.pgnString();
}

//------------------------------------------------------------------------------
void ChessGame::addObserver(IGameObserver& observer)
{
  for(IGameObserver& crt: m_observers)
    if(&crt == &observer)
      return;

  m_observers.push_back(observer);
}

//------------------------------------------------------------------------------
void ChessGame::removeObserver(IGameObserver& observer)
{
  for(auto it = m_observers.begin() ; it != m_observers.end() ; ++it)
  {
    if(&(*it).get() == &observer)
    {
      m_observers.erase(it);
      return;
    }
  }
}

//------------------------------------------------------------------------------
void ChessGame::clone(std::unique_ptr<IChessGame>& clone) const
{
  std::unique_ptr<ChessGame> cloneTmp(new ChessGame());

  cloneTmp->m_playerBitBoards = m_playerBitBoards;
  cloneTmp->m_castlingStatus = m_castlingStatus;
  cloneTmp->m_history = m_history;

  clone = std::move(cloneTmp);
}

//------------------------------------------------------------------------------
PiecesBitboards& ChessGame::playerPiecesBitboards()
{
  if(currentPlayer() == Color::WHITE)
    return m_playerBitBoards.at(Color::WHITE);
  else
    return m_playerBitBoards.at(Color::BLACK);
}

//------------------------------------------------------------------------------
PiecesBitboards& ChessGame::opponentPiecesBitboards()
{
  if(currentPlayer() == Color::WHITE)
    return m_playerBitBoards.at(Color::BLACK);
  else
    return m_playerBitBoards.at(Color::WHITE);
}

//------------------------------------------------------------------------------
BitBoard ChessGame::attackMap(PiecesBitboards& pBb)
{
  BitBoard attackMap;

  for(auto& pieceRule: m_piecesRules)
  {
    BitBoard opponentPieces = pBb.pieces(pieceRule.first);
    SquaresList piecesLocation = opponentPieces.activeSquares();
    unsigned int squareIndex = 0;
    Square fromSquare;

    while(piecesLocation.at(squareIndex++, fromSquare))
      attackMap |= pieceRule.second->attackMap(fromSquare, *this);
  }

  return attackMap;
}

//------------------------------------------------------------------------------
void ChessGame::updateCastlingStatus(const Square& from, Piece movedPiece)
{
  if(movedPiece == Piece::KING)
    m_castlingStatus.kingMoved(currentPlayer());
  else if(movedPiece == Piece::ROOK)
  {
    if(from == Square(File::A, Rank::ONE) ||
        from == Square(File::A, Rank::EIGHT))
      m_castlingStatus.queenRookMoved(currentPlayer());
    else if(from == Square(File::H, Rank::ONE) ||
        from == Square(File::H, Rank::EIGHT))
      m_castlingStatus.kingSideRookMoved(currentPlayer());
  }
}

//------------------------------------------------------------------------------
bool ChessGame::canPlayerMove()
{
  Square playerPieceLocation;
  PiecesBitboards playerBb = playerPiecesBitboards();
  BitBoard playerPieces = playerBb.pieces();
  SquaresList occupiedSquares = playerPieces.activeSquares();
  unsigned int squareIndex = 0;

  while(occupiedSquares.at(squareIndex++, playerPieceLocation))
  {
    BitBoard possibleMoves = moveMap(playerPieceLocation);

    if(possibleMoves.getValue())
      return true;
  }

  return false;
}

}       // namespace
