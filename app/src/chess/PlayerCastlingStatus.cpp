/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PlayerCastlingStatus.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PlayerCastlingStatus::PlayerCastlingStatus():
    m_isKingMoved(false),
    m_isRookQueenSideMoved(false),
    m_isRookKingSideMoved(false)
{
}

//------------------------------------------------------------------------------
PlayerCastlingStatus::~PlayerCastlingStatus()
{
}

//------------------------------------------------------------------------------
bool PlayerCastlingStatus::isQueenSideCastleAllowed() const
{
  return !m_isKingMoved &&
      !m_isRookQueenSideMoved;
}

//------------------------------------------------------------------------------
bool PlayerCastlingStatus::isKingSideCastleAllowed() const
{
  return !m_isKingMoved &&
      !m_isRookKingSideMoved;
}

//------------------------------------------------------------------------------
void PlayerCastlingStatus::reset()
{
  m_isKingMoved = false;
  m_isRookKingSideMoved = false;
  m_isRookQueenSideMoved = false;
}

//------------------------------------------------------------------------------
void PlayerCastlingStatus::kingSideRookMoved()
{
  m_isRookKingSideMoved = true;
}

//------------------------------------------------------------------------------
void PlayerCastlingStatus::queenRookMoved()
{
  m_isRookQueenSideMoved = true;
}

//------------------------------------------------------------------------------
void PlayerCastlingStatus::kingMoved()
{
  m_isKingMoved = true;
}

}       // namespace
