/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Square.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
Square::Square():
    m_file(File::INVALID),
    m_rank(Rank::INVALID)
{
}

//------------------------------------------------------------------------------
Square::Square(File file, Rank rank):
    m_file(file),
    m_rank(rank)
{
}

//------------------------------------------------------------------------------
Square::~Square()
{
}

//------------------------------------------------------------------------------
bool Square::isValid() const
{
  return m_file != File::INVALID &&
      m_rank != Rank::INVALID;
}

//------------------------------------------------------------------------------
File Square::file() const
{
  return m_file;
}

//------------------------------------------------------------------------------
Rank Square::rank() const
{
  return m_rank;
}

//------------------------------------------------------------------------------
bool Square::fromString(const std::string& squareString)
{
  if(squareString.length() != 2)
    return false;

  return charToFile(squareString[0], m_file) &&
      charToRank(squareString[1], m_rank);
}

//------------------------------------------------------------------------------
std::string Square::toString() const
{
  std::string squareStr = "??";
  char fileChar;
  char rankChar;

  if(fileToChar(m_file, fileChar) &&
      rankToChar(m_rank, rankChar))
    squareStr = std::string(&fileChar, 1) + std::string(&rankChar, 1);

  return squareStr;
}

//------------------------------------------------------------------------------
bool Square::operator==(const Square& rhs) const
{
  return m_file == rhs.file() &&
      m_rank == rhs.rank();
}

}       // namespace
