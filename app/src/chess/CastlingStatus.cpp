/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CastlingStatus.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
CastlingStatus::CastlingStatus()
{
}

//------------------------------------------------------------------------------
CastlingStatus::~CastlingStatus()
{
}

//------------------------------------------------------------------------------
bool CastlingStatus::isQueenSideCastleAllowed(Color color) const
{
  if(color == Color::WHITE)
    return m_whiteCastlingStatus.isQueenSideCastleAllowed();
  else
    return m_blackCastlintStatus.isQueenSideCastleAllowed();
}

//------------------------------------------------------------------------------
bool CastlingStatus::isKingSideCastleAllowed(Color color) const
{
  if(color == Color::WHITE)
    return m_whiteCastlingStatus.isKingSideCastleAllowed();
  else
    return m_blackCastlintStatus.isKingSideCastleAllowed();
}

//------------------------------------------------------------------------------
void CastlingStatus::reset()
{
  m_whiteCastlingStatus.reset();
  m_blackCastlintStatus.reset();
}

//------------------------------------------------------------------------------
void CastlingStatus::kingSideRookMoved(Color color)
{
  if(color == Color::WHITE)
    m_whiteCastlingStatus.kingSideRookMoved();
  else
    m_blackCastlintStatus.kingSideRookMoved();
}

//------------------------------------------------------------------------------
void CastlingStatus::queenRookMoved(Color color)
{
  if(color == Color::WHITE)
    m_whiteCastlingStatus.queenRookMoved();
  else
    m_blackCastlintStatus.queenRookMoved();
}

//------------------------------------------------------------------------------
void CastlingStatus::kingMoved(Color color)
{
  if(color == Color::WHITE)
    m_whiteCastlingStatus.kingMoved();
  else
    m_blackCastlintStatus.kingMoved();
}

}       // namespace
