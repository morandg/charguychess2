/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_CHESS_ENGINES_HPP_
#define _CGC_I_CHESS_ENGINES_HPP_

#include "IChessEngine.hpp"

namespace charguychess {

class IChessEngines
{
public:
  enum AiMode {
    HUMAN,
    COMPUTER
  };

  IChessEngines();
  virtual ~IChessEngines();

  virtual int startEngines() = 0;
  virtual AiMode getAiMode(Color color) = 0;
  virtual IChessEngine& getEngine(Color color) = 0;
  virtual IChessEngine& getHintEngine() = 0;
};

}       // namespace
#endif  // _CGC_I_CHESS_ENGINES_HPP_
