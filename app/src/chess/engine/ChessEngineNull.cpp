/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/logging/LogMacros.hpp>

#include "ChessEngineNull.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessEngineNull::ChessEngineNull():
    m_name("NullEngine"),
    m_author("R4nd0m6uy")
{
}

//------------------------------------------------------------------------------
ChessEngineNull::~ChessEngineNull()
{
}

//------------------------------------------------------------------------------
int ChessEngineNull::init()
{
  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineNull::start()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessEngineNull::addObserver(IChessEngineObserver& observer)
{
  (void)observer;
}

//------------------------------------------------------------------------------
void ChessEngineNull::removeObserver(IChessEngineObserver& observer)
{
  (void)observer;
}

//------------------------------------------------------------------------------
bool ChessEngineNull::isRunning() const
{
  return false;
}

//------------------------------------------------------------------------------
bool ChessEngineNull::isReady() const
{
  return false;
}

//------------------------------------------------------------------------------
const std::string& ChessEngineNull::getName() const
{
  return m_name;
}

//------------------------------------------------------------------------------
const std::string& ChessEngineNull::getAuthor() const
{
  return m_author;
}

//------------------------------------------------------------------------------
int ChessEngineNull::searchDepth(unsigned int depth, const IChessGame& game)
{
  (void)depth;
  (void)game;

  LOGWA() << "Searching move in NULL chess engine will never end!";

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineNull::searchTime(unsigned int ms, const IChessGame& game)
{
  (void)ms;
  (void)game;

  LOGWA() << "Searching move in NULL chess engine will never end!";

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineNull::stopSearch()
{
  return 0;
}

//------------------------------------------------------------------------------
const MoveInfo& ChessEngineNull::lastFoundMove() const
{
  return m_lastFoundMove;
}

}       // namespace
