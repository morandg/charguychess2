/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>
#include <cerrno>
#include <cstring>
#include <cassert>

#include <rglib/logging/LogMacros.hpp>
#include <rglib/lexical/TokenizerString.hpp>

#include "ChessEngineUci.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessEngineUci::ChessEngineUci(
    rglib::IEventLoop& el, const ChessEngineSettings& settings):
  m_el(el),
  m_settings(settings),
  m_isReady(false),
  m_isFirstLine(true),
  m_name("unkown"),
  m_author("unkown")
{
}

//------------------------------------------------------------------------------
ChessEngineUci::~ChessEngineUci()
{
}

//------------------------------------------------------------------------------
int ChessEngineUci::init()
{
  rglib::IChildProcess::create(m_engineProcess);
  if(!m_engineProcess)
  {
    LOGER() << "Cannot create chess engine process";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineUci::start()
{
  if(m_engineProcess->start(m_settings.getCommand()))
  {
    LOGER() << "Cannot start chess engine " << m_settings.getName() <<
      " (" << m_settings.getCommand() << ")";
    return -1;
  }

  if(m_el.addHandledIo(
      *this, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST))
  {
    LOGER() << "Cannot register chess engine " << m_settings.getName() <<
        " to event loop!";
    return -1;
  }

  if(sendCommand("uci") ||
      sendCommand("isready"))
    return -1;

  LOGDB() << "Started UCI engine: " << m_settings.getName() <<
      " (" << m_settings.getCommand() << ")";

  return 0;
}

//------------------------------------------------------------------------------
void ChessEngineUci::addObserver(IChessEngineObserver& observer)
{
  for(IChessEngineObserver& crt: m_observers)
    if(&crt == &observer)
      return;

  m_observers.push_back(observer);
}

//------------------------------------------------------------------------------
void ChessEngineUci::removeObserver(IChessEngineObserver& observer)
{
  for(auto it = m_observers.begin() ; it != m_observers.end() ; ++it)
  {
    if(&(*it).get() == &observer)
    {
      m_observers.erase(it);
      return;
    }
  }
}

//------------------------------------------------------------------------------
bool ChessEngineUci::isRunning() const
{
  return m_engineProcess->isRunning();
}

//------------------------------------------------------------------------------
bool ChessEngineUci::isReady() const
{
  return m_isReady;
}

//------------------------------------------------------------------------------
const std::string& ChessEngineUci::getName() const
{
  return m_name;
}

//------------------------------------------------------------------------------
const std::string& ChessEngineUci::getAuthor() const
{
  return m_author;
}

//------------------------------------------------------------------------------
int ChessEngineUci::searchDepth(unsigned int depth, const IChessGame& game)
{
  std::stringstream ss;

  if(sendCurrentPosition(game))
    return -1;

  ss << "go depth " << depth ;
  if(sendCommand(ss.str()))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineUci::searchTime(unsigned int ms, const IChessGame& game)
{
  std::stringstream ss;

  if(sendCurrentPosition(game))
    return -1;

  ss << "go movetime " << ms;
  if(sendCommand(ss.str()))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineUci::stopSearch()
{
  return sendCommand("stop");
}

//------------------------------------------------------------------------------
const MoveInfo& ChessEngineUci::lastFoundMove() const
{
  return m_lastFoundMove;
}

//------------------------------------------------------------------------------
rglib::FileDescriptor ChessEngineUci::getFileDescriptor()
{
  return m_engineProcess->getStdOut().getFileDescriptor();
}

//------------------------------------------------------------------------------
void ChessEngineUci::onReadReady()
{
  unsigned int buffSize = 128;
  char buffer[buffSize];
  int readSize;
  std::string output;

  readSize = m_engineProcess->getStdOut().read(buffer, buffSize);
  if(readSize < 0)
  {
    LOGER() << "Cannot read data from chess engine: " << strerror(-readSize);
    return;
  }

  m_uciBuffer += std::string(buffer, readSize);
  parseUciBuffer();
}

//------------------------------------------------------------------------------
int ChessEngineUci::sendCommand(const std::string& cmd)
{
  std::string fullCmd = cmd + "\n";
  int ret;

  LOGDB() << "Sending UCI command: " << cmd;

  if(!m_engineProcess->isRunning())
  {
    LOGER() << "BUG? Chess engine stopped unexpectly!";
    m_el.removeHandledIo(*this);
    m_engineProcess->stop();
    assert(0);  // Should not happen
    return -1;
  }

  ret = m_engineProcess->getStdIn().write(fullCmd.c_str(), fullCmd.size());
  if(ret < 0)
  {
    LOGER() << "Cannot send UCI command " << cmd << ": " << strerror(errno);
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
void ChessEngineUci::parseUciBuffer()
{
  rglib::TokenizerString bufferTokenizer;
  rglib::WordTokenizer lineSplitter(bufferTokenizer);
  std::string uciLine;

  bufferTokenizer.setBuffer(m_uciBuffer);
  while(lineSplitter.getNextWord('\n', uciLine))
  {
    rglib::TokenizerString lineTokenizer;
    rglib::WordTokenizer wordSplitter(lineTokenizer);
    std::string uciInfo;

    lineTokenizer.setBuffer(uciLine);
    wordSplitter.getNextWord(' ', uciInfo);
    if(uciInfo == "id")
      parseUciId(wordSplitter);
    else if(uciInfo == "readyok")
    {
      m_isReady = true;
      LOGIN() << "Chess engine \"" << getName() << "\" ready!";
    }
    else if(uciInfo == "bestmove")
      parseBestMove(wordSplitter);
    else if(uciInfo == "info" ||
        uciInfo == "option" ||
        uciInfo == "uciok" ||
        uciInfo == "")
      // Simply ignored for now
      ;
    else if(!m_isFirstLine)
      LOGWA() << "Unknown UCI line: " << uciLine;

    m_isFirstLine = false;
  }

  // Get the unparsed buffer part
  m_uciBuffer = uciLine;
}

//------------------------------------------------------------------------------
int ChessEngineUci::parseUciId(rglib::WordTokenizer& wordTokenizer)
{
  std::string word;

  if(wordTokenizer.getNextWord(' ', word))
  {
    if(word == "name")
    {
      m_name = "";
      while(wordTokenizer.getNextWord(' ', word))
        m_name += word + " ";
      m_name += word;
    }
    else if(word == "author")
    {
      m_author = "";
      while(wordTokenizer.getNextWord(' ', word))
        m_author += word + " ";
      m_author += word;
    }
    else
    {
      LOGWA() << "Unkown UCI id information " << word;
      return -1;
    }

    return 0;
  }

  return -1;
}

//------------------------------------------------------------------------------
int ChessEngineUci::sendCurrentPosition(const IChessGame& game)
{
  if(sendCommand(game.historyUci()))
    return -1;

  m_lastFoundMove.setColor(game.currentPlayer());
  m_lastFoundMove.setTurnNumber(game.turnNumber());
  m_lastFoundMove.setMove(Move());

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngineUci::parseBestMove(rglib::WordTokenizer& wordTokenizer)
{
  std::string moveStr;
  Move move;

  wordTokenizer.getNextWord(' ', moveStr);
  if(!move.fromString(moveStr))
  {
    LOGER() << "BUG? Cannot parse UCI best move " << moveStr;
    assert(0);  // Should not happen
    return -1;
  }
  m_lastFoundMove.setMove(move);

  // Don't invalidate observer list when observer is removed from callback
  std::list<std::reference_wrapper<IChessEngineObserver>> observers =
      m_observers;
  for(IChessEngineObserver& observer: observers)
    observer.onBestMoveFound(move);

  return 0;
}

}       // namespace
