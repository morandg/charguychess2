/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_ENGINES_HPP_
#define _CGC_CHESS_ENGINES_HPP_

#include <memory>

#include <rglib/event-loop/IEventLoop.hpp>
#include "../../settings/Settings.hpp"

#include "IChessEngines.hpp"

namespace charguychess {

class ChessEngines:
    public IChessEngines
{
public:

  ChessEngines();
  virtual ~ChessEngines();

  int init(rglib::IEventLoop& el, const Settings& settings);

  // IChessEngines
  virtual int startEngines() override;
  virtual IChessEngines::AiMode getAiMode(Color color) override;
  virtual IChessEngine& getEngine(Color color) override;
  virtual IChessEngine& getHintEngine() override;

private:
  AiMode m_whiteAiMode;
  AiMode m_blackAiMode;
  std::unique_ptr<IChessEngine> m_whiteEngine;
  std::unique_ptr<IChessEngine> m_blackEngine;
  std::unique_ptr<IChessEngine> m_hintEngine;

  int initEngine(
      rglib::IEventLoop& el,
      const std::string& name,
      const ConfiguredChessEngines& confEngines,
      std::unique_ptr<IChessEngine>& engine);
};

}       // namespace
#endif  // _CGC_CHESS_ENGINES_HPP_
