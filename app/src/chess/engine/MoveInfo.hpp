/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_MOVE_INFO_HPP_
#define _CGC_MOVE_INFO_HPP_

#include "../../chess/Move.hpp"

namespace charguychess {

class MoveInfo
{
public:
  MoveInfo();
  ~MoveInfo();

  const Move& getMove() const;
  void setMove(const Move& move);
  Color getColor() const;
  void setColor(Color color);
  unsigned int getTurnNumber() const;
  void setTurnNumber(unsigned int turnNumber);

private:
  Move m_move;
  Color m_color;
  unsigned int m_turnNumber;
};

}       // namespace
#endif  // _CGC_MOVE_INFO_HPP_
