/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_CHESS_ENGINE_HPP_
#define _CGC_I_CHESS_ENGINE_HPP_

#include "../../settings/ChessEngineSettings.hpp"
#include "../IChessGame.hpp"
#include "MoveInfo.hpp"
#include "IChessEngineObserver.hpp"

namespace charguychess {

class IChessEngine
{
public:
  IChessEngine();
  virtual ~IChessEngine();

  virtual int init() = 0;
  virtual int start() = 0;
  virtual void addObserver(IChessEngineObserver& observer) = 0;
  virtual void removeObserver(IChessEngineObserver& observer) = 0;
  virtual bool isRunning() const = 0;
  virtual bool isReady() const = 0;
  virtual const std::string& getName() const = 0;
  virtual const std::string& getAuthor() const = 0;
  virtual int searchDepth(unsigned int depth, const IChessGame& game) = 0;
  virtual int searchTime(unsigned int ms, const IChessGame& game) = 0;
  virtual int stopSearch() = 0;
  virtual const MoveInfo& lastFoundMove() const = 0;
};

}       // namespace
#endif  // _CGC_I_CHESS_ENGINE_HPP_
