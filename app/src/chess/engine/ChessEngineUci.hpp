/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_ENGINE_UCI_HPP_
#define _CGC_CHESS_ENGINE_UCI_HPP_

#include <rglib/event-loop/IEventLoop.hpp>
#include <rglib/process/IChildProcess.hpp>
#include <rglib/lexical/WordTokenizer.hpp>

#include "IChessEngine.hpp"

namespace charguychess {

class ChessEngineUci:
    public IChessEngine,
    public rglib::IHandledIo
{
public:
  ChessEngineUci(rglib::IEventLoop& el, const ChessEngineSettings& settings);
  virtual ~ChessEngineUci();

  // IChessEngine
  virtual int init() override;
  virtual int start() override;
  virtual void addObserver(IChessEngineObserver& observer) override;
  virtual void removeObserver(IChessEngineObserver& observer) override;
  virtual bool isRunning() const override;
  virtual bool isReady() const override;
  virtual const std::string& getName() const override;
  virtual const std::string& getAuthor() const override;
  virtual int searchDepth(unsigned int depth, const IChessGame& game) override;
  virtual int searchTime(unsigned int ms, const IChessGame& game) override;
  virtual int stopSearch() override;
  virtual const MoveInfo& lastFoundMove() const override;

  // rglib::IHandledIo
  virtual rglib::FileDescriptor getFileDescriptor() override;
  virtual void onReadReady() override;

private:
  rglib::IEventLoop& m_el;
  ChessEngineSettings m_settings;
  std::string m_uciBuffer;
  bool m_isReady;
  bool m_isFirstLine;
  MoveInfo m_lastFoundMove;
  std::string m_name;
  std::string m_author;
  std::list<std::reference_wrapper<IChessEngineObserver>> m_observers;
  std::unique_ptr<rglib::IChildProcess> m_engineProcess;

  int sendCommand(const std::string& cmd);
  void parseUciBuffer();
  int parseUciId(rglib::WordTokenizer& wordTokenizer);
  int sendCurrentPosition(const IChessGame& game);
  int parseBestMove(rglib::WordTokenizer& wordTokenizer);
};

}       // namespace
#endif  // _CGC_CHESS_ENGINE_UCI_HPP_
