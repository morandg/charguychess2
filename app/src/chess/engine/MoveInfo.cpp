/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "MoveInfo.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
MoveInfo::MoveInfo():
    m_color(Color::WHITE),
    m_turnNumber(0)
{
}

//------------------------------------------------------------------------------
MoveInfo::~MoveInfo()
{
}

//------------------------------------------------------------------------------
const Move& MoveInfo::getMove() const
{
  return m_move;
}

//------------------------------------------------------------------------------
void MoveInfo::setMove(const Move& move)
{
  m_move = move;
}

//------------------------------------------------------------------------------
Color MoveInfo::getColor() const
{
  return m_color;
}

//------------------------------------------------------------------------------
void MoveInfo::setColor(Color color)
{
  m_color = color;
}

//------------------------------------------------------------------------------
unsigned int MoveInfo::getTurnNumber() const
{
  return m_turnNumber;
}

//------------------------------------------------------------------------------
void MoveInfo::setTurnNumber(unsigned int turnNumber)
{
  m_turnNumber = turnNumber;
}

}       // namespace
