/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessEngineUci.hpp"
#include "ChessEngineNull.hpp"
#include "ChessEngines.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessEngines::ChessEngines():
    m_whiteAiMode(HUMAN),
    m_blackAiMode(HUMAN)
{
}

//------------------------------------------------------------------------------
ChessEngines::~ChessEngines()
{
}

//------------------------------------------------------------------------------
int ChessEngines::startEngines()
{
  if(!m_whiteEngine ||
      !m_blackEngine ||
      !m_hintEngine)
  {
    LOGER() << "BUG? Chess engines not initialized";
    assert(0);  // Should not happen
    return -1;
  }

  if(m_whiteEngine->start() ||
      m_blackEngine->start() ||
      m_hintEngine->start())
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int ChessEngines::init(rglib::IEventLoop& el, const Settings& settings)
{
  std::string engineName;
  const AisSettings aisSettings = settings.getAisSettings();
  const ConfiguredChessEngines& confEngines = settings.getConfiguredEngines();

  engineName = aisSettings.getWhiteAiSettings().getEngineName();
  if(engineName == AiSettings::ENGINE_NAME_HUMAN)
  {
    m_whiteAiMode = HUMAN;
    m_whiteEngine.reset(new ChessEngineNull());
  }
  else if(initEngine(el, engineName, confEngines, m_whiteEngine))
    return -1;

  engineName = aisSettings.getBlackAiSettings().getEngineName();
  if(engineName == AiSettings::ENGINE_NAME_HUMAN)
  {
    m_blackAiMode = HUMAN;
    m_blackEngine.reset(new ChessEngineNull());
  }
  else if(initEngine(el, engineName, confEngines, m_blackEngine))
    return -1;

  engineName = aisSettings.getHintAiSettings().getEngineName();
  if(engineName == AiSettings::ENGINE_NAME_HUMAN)
  {
    LOGER() << "Hint engine cannot be human!";
    return -1;
  }
  else if(initEngine(el, engineName, confEngines, m_hintEngine))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
IChessEngines::AiMode ChessEngines::getAiMode(Color color)
{
  if(color == Color::WHITE)
    return m_whiteAiMode;
  return m_blackAiMode;
}

//------------------------------------------------------------------------------
IChessEngine& ChessEngines::getEngine(Color color)
{
  if(color == Color::WHITE)
  {
    assert(m_whiteEngine);
    return *m_whiteEngine;
  }
  else
  {
    assert(m_blackEngine);
    return *m_blackEngine;
  }
}

//------------------------------------------------------------------------------
IChessEngine& ChessEngines::getHintEngine()
{
  assert(m_hintEngine);

  return *m_hintEngine;
}

//------------------------------------------------------------------------------
int ChessEngines::initEngine(
    rglib::IEventLoop& el,
    const std::string& name,
    const ConfiguredChessEngines& confEngines,
    std::unique_ptr<IChessEngine>& engine)
{
  ChessEngineSettings engineOptions;

  if(confEngines.getChessEngineSettings(name, engineOptions))
  {
    engine.reset(new ChessEngineUci(el, engineOptions));
    if(engine->init())
    {
      LOGER() << "Cannot initialize engine " << name;
      return -1;
    }
  }
  else
  {
    LOGER() << "Engine configuration not found for " << name;
    return -1;
  }

  return 0;
}

}       // namespace
