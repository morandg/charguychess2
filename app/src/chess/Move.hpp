/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_MOVE_HPP_
#define _CGC_MOVE_HPP_

#include "Square.hpp"

namespace charguychess {

class Move
{
public:
  Move();
  Move(const Square& from, const Square& to);
  ~Move();

  const Square& from() const;
  const Square& to() const;
  bool isValid() const;
  bool fromString(const std::string& moveString);
  std::string toString() const;
  std::string toPGN() const;
  Piece promotion() const;
  void setPromotion(Piece piece);
  void setMovedPiece(Piece piece);
  Piece getMovedPiece() const;
  void setFileAmbiguous(bool isAmbiguous);
  bool isFileAmbiguous() const;
  void setRankAmbiguous(bool isAmbiguous);
  bool isRankAmbiguous() const;
  void setCapture(bool isCapture);
  bool isCapture() const;
  void setKingSideCastling(bool isKingSideCastling);
  bool isKingSideCastling() const;
  void setQueenSideCastling(bool isQueenSideCastling);
  bool isQueenSideCastling() const;
  void setCheck(bool isChecked);
  bool isCheck() const;
  void setMate(bool isMate);
  bool isMate() const;

  bool operator==(const Move& rhs) const;
  bool operator!=(const Move& rhs) const;

private:
  Piece m_promotion;
  Piece m_movedPiece;
  bool m_isFileAmbiguous;
  bool m_isRankAmbiguous;
  bool m_isCapture;
  bool m_isKingSideCastling;
  bool m_isQueenSideCastling;
  bool m_isChecked;
  bool m_isMate;
  Square m_from;
  Square m_to;

  std::string pieceToPGN(Piece piece) const;
};

}       // namespace
#endif  // _CGC_MOVE_HPP_
