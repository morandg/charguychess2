/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessDefs.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
bool charToFile(char fileChar, File& file)
{
  // To upper case
  if(fileChar > 'H')
    fileChar -= 32;

  switch(fileChar)
  {
  case 'A':
    file = File::A;
    break;
  case 'B':
    file = File::B;
    break;
  case 'C':
    file = File::C;
    break;
  case 'D':
    file = File::D;
    break;
  case 'E':
    file = File::E;
    break;
  case 'F':
    file = File::F;
    break;
  case 'G':
    file = File::G;
    break;
  case 'H':
    file = File::H;
    break;
  default:
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------
bool fileToChar(File file, char& fileChar)
{
  switch(file)
  {
  case File::A:
    fileChar = 'a';
    break;
  case File::B:
    fileChar = 'b';
    break;
  case File::C:
    fileChar = 'c';
    break;
  case File::D:
    fileChar = 'd';
    break;
  case File::E:
    fileChar = 'e';
    break;
  case File::F:
    fileChar = 'f';
    break;
  case File::G:
    fileChar = 'g';
    break;
  case File::H:
    fileChar = 'h';
    break;
  default:
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------
bool charToRank(char rankChar, Rank& rank)
{
  switch(rankChar)
  {
  case '1':
    rank = Rank::ONE;
    break;
  case '2':
    rank = Rank::TWO;
    break;
  case '3':
    rank = Rank::THREE;
    break;
  case '4':
    rank = Rank::FOUR;
    break;
  case '5':
    rank = Rank::FIVE;
    break;
  case '6':
    rank = Rank::SIX;
    break;
  case '7':
    rank = Rank::SEVEN;
    break;
  case '8':
    rank = Rank::EIGHT;
    break;
  default:
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------
bool rankToChar(Rank rank, char& rankChar)
{
  switch(rank)
  {
  case Rank::ONE:
    rankChar = '1';
    break;
  case Rank::TWO:
    rankChar = '2';
    break;
  case Rank::THREE:
    rankChar = '3';
    break;
  case Rank::FOUR:
    rankChar = '4';
    break;
  case Rank::FIVE:
    rankChar = '5';
    break;
  case Rank::SIX:
    rankChar = '6';
    break;
  case Rank::SEVEN:
    rankChar = '7';
    break;
  case Rank::EIGHT:
    rankChar = '8';
    break;
  default:
    return false;
  }

  return true;
}
}       // namespace
