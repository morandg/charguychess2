/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <iomanip>

#include <rglib/logging/LogMacros.hpp>
#include <rglib/time/CalendarInstance.hpp>

#include "PgnSerializer.hpp"

namespace charguychess {

static const std::string DEFAULT_DEST_FOLDER    = "/tmp";

//------------------------------------------------------------------------------
PgnSerializer::PgnSerializer(IChessGame& chessGame):
    m_chessGame(chessGame),
    m_destFolder(DEFAULT_DEST_FOLDER),
    m_event("???"),
    m_site("???"),
    m_round(1)
{
}

//------------------------------------------------------------------------------
PgnSerializer::~PgnSerializer()
{
}

//------------------------------------------------------------------------------
int PgnSerializer::init(const PgnSettings& settings)
{
  m_chessGame.addObserver(*this);

  rglib::IFile::create(m_pgnFile);

  m_destFolder = settings.getDestFolder();
  if(m_destFolder[0] == '~')
    m_destFolder = getenv("HOME") + m_destFolder.substr(1);
  m_event = settings.getEvent();
  m_site = settings.getSite();

  buildDateInfo();

  return 0;
}

//------------------------------------------------------------------------------
void PgnSerializer::onMovePlayed(const Move& move)
{
  int ret;
  std::stringstream pgnStream;
  std::string pgnStr;
  std::string result;

  LOGDB() << "A move was played, saving PGN file ...";

  ret = m_pgnFile->open(
      m_filename,
      rglib::OpenFlag::CREATE |
        rglib::OpenFlag::WRITE_ONLY |
        rglib::OpenFlag::TRUNC);
  if(ret)
  {
    LOGER() << "Cannot open PGN file " << m_filename << ": " <<
        strerror(-ret);
    return;
  }

  if(move.isMate())
  {
    if(m_chessGame.currentPlayer() == Color::BLACK)
      result = "1-0";
    else
      result = "0-1";
  }
  else
    result = "*";

  pgnStream <<
      "[Event \"" << m_event << "\"]" << std::endl <<
      "[Site \"" << m_site << "\"]" << std::endl <<
      "[Date \"" << m_date << "\"]" << std::endl <<
      "[Round \"" << m_round << "\"]" << std::endl <<
      "[White \"GET_WHITE_ENGINE\"]" << std::endl <<
      "[Black \"GET_BLACK_ENGINE\"]" << std::endl <<
      "[Result \"" << result << "\"]" << std::endl << std::endl <<
      m_chessGame.historyPGN() << " " << result << std::endl;

  pgnStr = pgnStream.str();
  ret = m_pgnFile->write(pgnStr.c_str(), pgnStr.length());
  if(ret < 0)
    LOGER() << "Cannot write PGN file: " << strerror(-ret);
  else if(ret != (int)pgnStr.length())
    // FIXME!
    LOGWA() << "Partial PGN write!";

  m_pgnFile->close();
}

//------------------------------------------------------------------------------
void PgnSerializer::onNewGameStarted()
{
  buildDateInfo();
}

//------------------------------------------------------------------------------
void PgnSerializer::buildDateInfo()
{
  struct tm* localTime;
  std::stringstream destFile;
  std::stringstream date;
  time_t now;
  int year;
  int month;

  now = time(NULL);
  localTime = localtime(&now);
  year = localTime->tm_year + 1900;
  month = localTime->tm_mon + 1;

  destFile << m_destFolder << "/" <<
      year << "_" <<
      std::setfill('0') << std::setw(2) << month << "_" <<
      std::setfill('0') << std::setw(2) << localTime->tm_mday << "_" <<
      std::setfill('0') << std::setw(2) << localTime->tm_hour << "_" <<
      std::setfill('0') << std::setw(2) << localTime->tm_min << "_" <<
      std::setfill('0') << std::setw(2) << localTime->tm_sec << ".pgn";
  date << std::setw(2) << std::setfill('0') <<
      year << "." <<
      std::setfill('0') << std::setw(2) << month << "." <<
      std::setfill('0') << std::setw(2) << localTime->tm_mday;

  m_filename = destFile.str();
  m_date = date.str();
}

}       // namespace
