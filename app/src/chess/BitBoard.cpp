/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BitBoard.hpp"

namespace charguychess {

const BitBoardValue BitBoard::INITIAL_WHITE_PAWNS   = 0x000000000000FF00;
const BitBoardValue BitBoard::INITIAL_WHITE_ROOKS   = 0x0000000000000081;
const BitBoardValue BitBoard::INITIAL_WHITE_KNIGHTS = 0x0000000000000042;
const BitBoardValue BitBoard::INITIAL_WHITE_BISHOPS = 0x0000000000000024;
const BitBoardValue BitBoard::INITIAL_WHITE_QUEEN   = 0x0000000000000010;
const BitBoardValue BitBoard::INITIAL_WHITE_KING    = 0x0000000000000008;
const BitBoardValue BitBoard::INITIAL_BLACK_PAWNS   = 0x00FF000000000000;
const BitBoardValue BitBoard::INITIAL_BLACK_ROOKS   = 0x8100000000000000;
const BitBoardValue BitBoard::INITIAL_BLACK_KNIGHTS = 0x4200000000000000;
const BitBoardValue BitBoard::INITIAL_BLACK_BISHOPS = 0x2400000000000000;
const BitBoardValue BitBoard::INITIAL_BLACK_QUEEN   = 0x1000000000000000;
const BitBoardValue BitBoard::INITIAL_BLACK_KING    = 0x0800000000000000;
const unsigned int BitBoard::DIRECTION_NORTH        = 8;
const unsigned int BitBoard::DIRECTION_NORTH_EAST   = 7;
const unsigned int BitBoard::DIRECTION_EAST         = -1;
const unsigned int BitBoard::DIRECTION_SOUTH_EAST   = -9;
const unsigned int BitBoard::DIRECTION_SOUTH        = -8;
const unsigned int BitBoard::DIRECTION_WEST         = 1;
const unsigned int BitBoard::DIRECTION_SOUTH_WEST   = -7;
const unsigned int BitBoard::DIRECTION_NORTH_WEST   = 9;

//------------------------------------------------------------------------------
BitBoard::BitBoard():
    m_value(0)
{
}

//------------------------------------------------------------------------------
BitBoard::BitBoard(BitBoardValue value):
    m_value(value)
{
}

//------------------------------------------------------------------------------
BitBoard::~BitBoard()
{
}

//------------------------------------------------------------------------------
unsigned int BitBoard::toBitNumber(const Square& square)
{
  return
      ((int)square.rank() * 8) +
      (7 - (int)square.file());
}
//------------------------------------------------------------------------------
File BitBoard::toFile(unsigned int bit)
{
  return (File)(7 - (bit % 8));
}

//------------------------------------------------------------------------------
Rank BitBoard::toRank(unsigned int bit)
{
  if(bit <= 7)
    return Rank::ONE;
  else if(bit <= 15)
    return Rank::TWO;
  else if(bit <= 23)
    return Rank::THREE;
  else if(bit <= 31)
    return Rank::FOUR;
  else if(bit <= 39)
    return Rank::FIVE;
  else if(bit <= 47)
    return Rank::SIX;
  else if(bit <= 55)
    return Rank::SEVEN;

  return Rank::EIGHT;
}

//------------------------------------------------------------------------------
Square BitBoard::toSquare(unsigned int bit)
{
  return Square(toFile(bit), toRank(bit));
}

//------------------------------------------------------------------------------
BitBoardValue BitBoard::getValue() const
{
  return m_value;
}

//------------------------------------------------------------------------------
bool BitBoard::isSet(const Square& square) const
{
  return isSet(toBitNumber(square));
}

//------------------------------------------------------------------------------
void BitBoard::set(const Square& square)
{
  set(toBitNumber(square));
}

//------------------------------------------------------------------------------
void BitBoard::clear(const Square& square)
{
  clear(toBitNumber(square));
}

//------------------------------------------------------------------------------
void BitBoard::toggle(const Square& square)
{
  toggle(toBitNumber(square));
}

//------------------------------------------------------------------------------
bool BitBoard::isSet(unsigned int bit) const
{
  return m_value & ((BitBoardValue)1 << bit);
}

//------------------------------------------------------------------------------
void BitBoard::set(unsigned int bit)
{
  m_value |= (BitBoardValue)1 << bit;
}

//------------------------------------------------------------------------------
void BitBoard::clear(unsigned int bit)
{
  m_value &= ~((BitBoardValue)1 << bit);
}

//------------------------------------------------------------------------------
void BitBoard::toggle(unsigned int bit)
{
  if(isSet(bit))
    clear(bit);
  else
    set(bit);
}

//------------------------------------------------------------------------------
SquaresList BitBoard::activeSquares() const
{
  BitBoardValue searchValue = getValue();
  unsigned int currentBit = 0;
  SquaresList activeSquares;

  while(searchValue)
  {
    if(searchValue & 1)
      activeSquares.add(toSquare(currentBit));

    searchValue >>= 1;
    ++currentBit;
  }

  return activeSquares;
}

//------------------------------------------------------------------------------
unsigned int BitBoard::activeBits() const
{
  BitBoardValue searchValue = getValue();
  unsigned int occupiedSquares = 0;

  while(searchValue)
  {
    if(searchValue & 1)
      ++occupiedSquares;

    searchValue >>= 1;
  }

  return occupiedSquares;
}

//------------------------------------------------------------------------------
BitBoard BitBoard::operator&(const BitBoard& rhs) const
{
  return BitBoard(getValue() & rhs.getValue());
}

//------------------------------------------------------------------------------
BitBoard BitBoard::operator|(const BitBoard& rhs) const
{
  return BitBoard(getValue() | rhs.getValue());
}

//------------------------------------------------------------------------------
BitBoard& BitBoard::operator|=(const BitBoard& rhs)
{
  *this = *this | rhs;

  return *this;
}

//------------------------------------------------------------------------------
bool BitBoard::operator==(const BitBoard& rhs) const
{
  return getValue() == rhs.getValue();
}

//------------------------------------------------------------------------------
bool BitBoard::operator!=(const BitBoard& rhs) const
{
  return !(*this == rhs);
}

//------------------------------------------------------------------------------
BitBoard BitBoard::operator^(const BitBoard& rhs) const
{
  return BitBoard(getValue() ^ rhs.getValue());
}

}       // namespace
