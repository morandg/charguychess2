/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PieceRulesKing.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PieceRulesKing::PieceRulesKing()
{
}

//------------------------------------------------------------------------------
PieceRulesKing::~PieceRulesKing()
{
}

//------------------------------------------------------------------------------
Piece PieceRulesKing::piece() const
{
  return Piece::KING;
}

//------------------------------------------------------------------------------
bool PieceRulesKing::applyMove(Move& move, IChessGame& chessGame) const
{
  if(!PieceRules::applyMove(move, chessGame))
    return false;

  applyCastlingMove(move, chessGame);

  return true;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesKing::moveMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard moveMap;

  moveMap = computeMoves(from, chessGame, false);

  if(chessGame.isQuenSideCastlingAllowed(from))
    moveMap |= slide(from, chessGame, BitBoard::DIRECTION_WEST, false, 2);
  if(chessGame.isKingSideCastlingAllowed(from))
    moveMap |= slide(from, chessGame, BitBoard::DIRECTION_EAST, false, 2);

  clearCheckedSquares(from, moveMap, chessGame);

  return moveMap;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesKing::attackMap(
    const Square& from, IChessGame& chessGame) const
{
  return computeMoves(from, chessGame, true);
}

//------------------------------------------------------------------------------
BitBoard PieceRulesKing::computeMoves(
    const Square& from, IChessGame& chessGame, bool allowSelfCapture) const
{
  BitBoard moveMap;

  moveMap = slide(
      from, chessGame, BitBoard::DIRECTION_NORTH, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_NORTH_EAST, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_SOUTH_EAST, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_SOUTH, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_SOUTH_WEST, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_NORTH_WEST, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_WEST, allowSelfCapture, 1);
  moveMap |= slide(
      from, chessGame, BitBoard::DIRECTION_EAST, allowSelfCapture, 1);

  return moveMap;
}

//------------------------------------------------------------------------------
void PieceRulesKing::applyCastlingMove(Move& move, IChessGame& cg) const
{
  BitBoard rooksPosition = cg.playerPieces(Piece::ROOK);

  if(cg.currentPlayer() == Color::WHITE)
  {
    if(move.from() == Square(File::E, Rank::ONE) &&
        move.to() == Square(File::C, Rank::ONE))
    {
      rooksPosition.clear(Square(File::A, Rank::ONE));
      rooksPosition.set(Square(File::D, Rank::ONE));
      move.setQueenSideCastling(true);
    }
    else if(move.from() == Square(File::E, Rank::ONE) &&
        move.to() == Square(File::G, Rank::ONE))
    {
      rooksPosition.clear(Square(File::H, Rank::ONE));
      rooksPosition.set(Square(File::F, Rank::ONE));
      move.setKingSideCastling(true);
    }

    cg.setPlayerPieces(Piece::ROOK, rooksPosition);
  }
  else
  {
    if(move.from() == Square(File::E, Rank::EIGHT) &&
        move.to() == Square(File::C, Rank::EIGHT))
    {
      rooksPosition.clear(Square(File::A, Rank::EIGHT));
      rooksPosition.set(Square(File::D, Rank::EIGHT));
      move.setQueenSideCastling(true);
    }
    else if(move.from() == Square(File::E, Rank::EIGHT) &&
        move.to() == Square(File::G, Rank::EIGHT))
    {
      rooksPosition.clear(Square(File::H, Rank::EIGHT));
      rooksPosition.set(Square(File::F, Rank::EIGHT));
      move.setKingSideCastling(true);
    }

    cg.setPlayerPieces(Piece::ROOK, rooksPosition);
  }
}

}       // namespace
