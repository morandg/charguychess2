/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "PieceRulesPawn.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PieceRulesPawn::PieceRulesPawn()
{
}

//------------------------------------------------------------------------------
PieceRulesPawn::~PieceRulesPawn()
{
}

//------------------------------------------------------------------------------
Piece PieceRulesPawn::piece() const
{
  return Piece::PAWN;
}

//------------------------------------------------------------------------------
bool PieceRulesPawn::applyMove(Move& move, IChessGame& chessGame) const
{
  bool isPromotional = chessGame.isPromotionalMove(move.from());
  if(isPromotional &&
      move.promotion() == Piece::PAWN)
  {
    LOGER() << "Missing pawn promotion information!";
    return false;
  }

  if(!PieceRules::applyMove(move, chessGame))
    return false;

  if(isPromotional)
  {
    BitBoard pawnsBb = chessGame.playerPieces(Piece::PAWN);
    BitBoard promotionBb = chessGame.playerPieces(move.promotion());

    promotionBb.set(move.to());
    pawnsBb.clear(move.to());

    chessGame.setPlayerPieces(Piece::PAWN, pawnsBb);
    chessGame.setPlayerPieces(move.promotion(), promotionBb);
  }

  return true;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesPawn::moveMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard opponentPieces = chessGame.opponentPieces();
  BitBoard allPieces = chessGame.allPieces();
  unsigned int destinationBitSquare = BitBoard::toBitNumber(from);
  Color currentPlayer = chessGame.currentPlayer();
  SquaresList attackedSquares = attackMap(from, chessGame).activeSquares();
  unsigned int attackSquareIndex = 0;
  Square attackedSquare;
  BitBoard moveMap;

  // Can Advance one
  if(currentPlayer == Color::WHITE)
    destinationBitSquare += BitBoard::DIRECTION_NORTH;
  else
    destinationBitSquare += BitBoard::DIRECTION_SOUTH;
  if(!allPieces.isSet(destinationBitSquare))
    moveMap.set(destinationBitSquare);

  // Can advance two
  if(currentPlayer == Color::WHITE)
    destinationBitSquare += BitBoard::DIRECTION_NORTH;
  else
    destinationBitSquare += BitBoard::DIRECTION_SOUTH;
  if(isFirstPawnMove(from, currentPlayer) &&
      !allPieces.isSet(destinationBitSquare))
    moveMap.set(destinationBitSquare);

  // capture
  while(attackedSquares.at(attackSquareIndex++, attackedSquare))
  {
    if(opponentPieces.isSet(attackedSquare))
      moveMap.set(attackedSquare);
  }

  clearCheckedSquares(from, moveMap, chessGame);

  return moveMap;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesPawn::attackMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard attackMap;
  Color currentPawnColor;
  PlayerPiece movingPawn;
  unsigned int fromBit = BitBoard::toBitNumber(from);

  if(!chessGame.pieceAt(from, movingPawn))
  {
    LOGER() << "BUG? No pawn found on given square when computing attack map!";
    assert(0);  // Should not happen
    return attackMap;
  }
  currentPawnColor = movingPawn.color();

  if(from.file() != File::A)
  {
    if(currentPawnColor == Color::WHITE)
      attackMap.set(fromBit + BitBoard::DIRECTION_NORTH_WEST);
    else
      attackMap.set(fromBit + BitBoard::DIRECTION_SOUTH_WEST);
  }

  if(from.file() != File::H)
  {
    if(currentPawnColor == Color::WHITE)
      attackMap.set(fromBit + BitBoard::DIRECTION_NORTH_EAST);
    else
      attackMap.set(fromBit + BitBoard::DIRECTION_SOUTH_EAST);
  }

  return attackMap;
}

//------------------------------------------------------------------------------
bool PieceRulesPawn::isFirstPawnMove(const Square& from, Color color) const
{
  return (color == Color::WHITE && from.rank() == Rank::TWO) ||
      (color == Color::BLACK && from.rank() == Rank::SEVEN);
}

}       // namespace
