/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PieceRulesBishop.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PieceRulesBishop::PieceRulesBishop()
{
}

//------------------------------------------------------------------------------
PieceRulesBishop::~PieceRulesBishop()
{
}

//------------------------------------------------------------------------------
Piece PieceRulesBishop::piece() const
{
  return Piece::BISHOP;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesBishop::moveMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard moveMap;

  moveMap = computeMoves(from, chessGame, false);
  clearCheckedSquares(from, moveMap, chessGame);

  return moveMap;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesBishop::attackMap(
    const Square& from, IChessGame& chessGame) const
{
  return computeMoves(from, chessGame, true);
}

//------------------------------------------------------------------------------
BitBoard PieceRulesBishop::computeMoves(
    const Square& from, IChessGame& chessGame, bool allowSelfCapture) const
{
  BitBoard slideMap;

  slideMap = slide(
      from, chessGame, BitBoard::DIRECTION_NORTH_WEST, allowSelfCapture);
  slideMap |= slide(
      from, chessGame, BitBoard::DIRECTION_NORTH_EAST, allowSelfCapture);
  slideMap |= slide(
      from, chessGame, BitBoard::DIRECTION_SOUTH_WEST, allowSelfCapture);
  slideMap |= slide(
      from, chessGame, BitBoard::DIRECTION_SOUTH_EAST, allowSelfCapture);

  return slideMap;
}
}       // namespace
