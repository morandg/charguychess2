/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PieceRulesKnight.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PieceRulesKnight::PieceRulesKnight()
{
}

//------------------------------------------------------------------------------
PieceRulesKnight::~PieceRulesKnight()
{
}

//------------------------------------------------------------------------------
Piece PieceRulesKnight::piece() const
{
  return Piece::KNIGHT;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesKnight::moveMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard moveMap;

  moveMap = computeMoves(from, chessGame, false);
  clearCheckedSquares(from, moveMap, chessGame);

  return moveMap;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesKnight::attackMap(
    const Square& from, IChessGame& chessGame) const
{
  return computeMoves(from, chessGame, true);
}

//------------------------------------------------------------------------------
BitBoard PieceRulesKnight::computeMoves(
    const Square& from, IChessGame& chessGame, bool allowSelfCapture) const
{
  unsigned int startBit = BitBoard::toBitNumber(from);
  unsigned int destinationBit;
  BitBoard currentPlayerPieces;
  File startFile = from.file();
  Rank startRank = from.rank();
  BitBoard moveMap;

  if(!allowSelfCapture)
    currentPlayerPieces = chessGame.playerPieces();

  // North north
  if(startRank != Rank::SEVEN &&
      startRank != Rank::EIGHT)
  {
    // North / NorthEast
    destinationBit = startBit +
        BitBoard::DIRECTION_NORTH +
        BitBoard::DIRECTION_NORTH_EAST;
    if(startFile != File::H &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);

    // North / NorthWest
    destinationBit = startBit +
        BitBoard::DIRECTION_NORTH +
        BitBoard::DIRECTION_NORTH_WEST;
    if(startFile != File::A &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);
  }

  // East east
  if(startFile != File::G &&
      startFile != File::H)
  {
    // East / SouthEast
    destinationBit = startBit +
        BitBoard::DIRECTION_EAST +
        BitBoard::DIRECTION_SOUTH_EAST;
    if(startRank != Rank::ONE &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);

    // East / NorthEast
    destinationBit = startBit +
        BitBoard::DIRECTION_EAST +
        BitBoard::DIRECTION_NORTH_EAST;
    if(startRank != Rank::EIGHT &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);
  }

  // South / South
  if(startRank != Rank::TWO &&
      startRank != Rank::ONE)
  {
    // South / SouthWest
    destinationBit = startBit +
        BitBoard::DIRECTION_SOUTH +
        BitBoard::DIRECTION_SOUTH_WEST;
    if(startFile != File::A &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);

    // South / SouthEast
    destinationBit = startBit +
        BitBoard::DIRECTION_SOUTH +
        BitBoard::DIRECTION_SOUTH_EAST;
    if(startFile != File::H &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);
  }

  // West West
  if(startFile != File::B &&
      startFile != File::A)
  {
    // West / NordWest
    destinationBit = startBit +
        BitBoard::DIRECTION_WEST +
        BitBoard::DIRECTION_NORTH_WEST;
    if(startRank != Rank::EIGHT &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);

    // West / SouthWest
    destinationBit = startBit +
        BitBoard::DIRECTION_WEST +
        BitBoard::DIRECTION_SOUTH_WEST;
    if(startRank != Rank::ONE &&
        !currentPlayerPieces.isSet(destinationBit))
      moveMap.set(destinationBit);
  }

  return moveMap;

}
}       // namespace
