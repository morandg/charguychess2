/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_PIECE_RULES_HPP_
#define _CGC_I_PIECE_RULES_HPP_

#include "../IChessGame.hpp"

namespace charguychess {

class IPieceRules
{
public:
  IPieceRules();
  virtual ~IPieceRules();

  virtual Piece piece() const = 0;
  virtual bool applyMove(Move& move, IChessGame& chessGame) const = 0;
  virtual BitBoard moveMap(const Square& from, IChessGame& chessGame) const = 0;
  virtual BitBoard attackMap(
      const Square& from, IChessGame& chessGame) const = 0;
};

}       // namespace
#endif  // _CGC_I_PIECE_RULES_HPP_
