/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_PIECE_RULES_HPP_
#define _CGC_PIECE_RULES_HPP_

#include "IPieceRules.hpp"

namespace charguychess {

class PieceRules:
    public IPieceRules
{
public:
  PieceRules();
  virtual ~PieceRules();

  virtual bool applyMove(Move& move, IChessGame& chessGame) const override;

private:
  static void directionToMaxSquare(unsigned int direction, Square& maxSquare);
  void checkAmbiguity(Move& move, IChessGame& cg) const;

protected:
  bool isValidMove(const Move& move, IChessGame& chessGame) const;
  void writeMove(Move& move, IChessGame& chessGame) const;
  void clearCheckedSquares(
      const Square& from,
      BitBoard& moveMap,
      IChessGame& chessGame) const;
  static BitBoard slide(
      const Square& from,
      IChessGame& game,
      unsigned int direction,
      bool allowSelfCapture);
  static BitBoard slide(
      const Square& from,
      IChessGame& game,
      unsigned int direction,
      bool allowSelfCapture,
      int maxSteps);

};

}       // namespace
#endif  // _CGC_PIECE_RULES_HPP_
