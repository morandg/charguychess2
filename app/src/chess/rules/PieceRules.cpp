/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/logging/LogMacros.hpp>

#include "PieceRules.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PieceRules::PieceRules()
{
}

//------------------------------------------------------------------------------
PieceRules::~PieceRules()
{
}

//------------------------------------------------------------------------------
bool PieceRules::applyMove(Move& move, IChessGame& chessGame) const
{
  PlayerPiece capturedPiece;

  if(!isValidMove(move, chessGame))
    return false;

  move.setMovedPiece(piece());
  move.setCapture(chessGame.pieceAt(move.to(), capturedPiece));
  checkAmbiguity(move, chessGame);

  writeMove(move, chessGame);

  return true;
}

//------------------------------------------------------------------------------
void PieceRules::directionToMaxSquare(
    unsigned int direction, Square& maxSquare)
{
  if(direction == BitBoard::DIRECTION_NORTH)
    maxSquare = Square(File::INVALID, Rank::EIGHT);
  else if(direction == BitBoard::DIRECTION_NORTH_EAST)
    maxSquare = Square(File::H, Rank::EIGHT);
  else if(direction == BitBoard::DIRECTION_EAST)
    maxSquare = Square(File::H, Rank::INVALID);
  else if(direction == BitBoard::DIRECTION_SOUTH_EAST)
    maxSquare = Square(File::H, Rank::ONE);
  else if(direction == BitBoard::DIRECTION_SOUTH)
    maxSquare = Square(File::INVALID, Rank::ONE);
  else if(direction == BitBoard::DIRECTION_SOUTH_WEST)
    maxSquare = Square(File::A, Rank::ONE);
  else if(direction == BitBoard::DIRECTION_WEST)
    maxSquare = Square(File::A, Rank::INVALID);
  else if(direction == BitBoard::DIRECTION_NORTH_WEST)
    maxSquare = Square(File::A, Rank::EIGHT);
}

//------------------------------------------------------------------------------
void PieceRules::checkAmbiguity(Move& move, IChessGame& cg) const
{
  BitBoard samePieces;
  SquaresList samePiecesLocation;
  Square samePieceFrom;
  unsigned int squareIdx = 0;

  // Pawn move is never ambiguous
  if(piece() == Piece::PAWN)
    return;

  samePieces = cg.playerPieces(piece());
  samePieces.clear(move.from());
  samePiecesLocation = samePieces.activeSquares();

  while(samePiecesLocation.at(squareIdx++, samePieceFrom))
  {
    BitBoard movesMap = moveMap(samePieceFrom, cg);

    if(movesMap.isSet(move.to()))
    {
      if(move.isRankAmbiguous() || move.isFileAmbiguous())
      {
        move.setRankAmbiguous(true);
        move.setFileAmbiguous(true);
        return;
      }
      else
      {
        if(move.from().file() == samePieceFrom.file())
          move.setRankAmbiguous(true);
        else
          move.setFileAmbiguous(true);
      }
    }
  }
}

//------------------------------------------------------------------------------
bool PieceRules::isValidMove(const Move& move, IChessGame& chessGame) const
{
  return moveMap(move.from(), chessGame).isSet(move.to());
}

//------------------------------------------------------------------------------
void PieceRules::writeMove(Move& move, IChessGame& chessGame)const
{
  BitBoard currentPieceBb;

  chessGame.clear(move.to());
  chessGame.clear(move.from());

  currentPieceBb = chessGame.playerPieces(piece());
  currentPieceBb.set(move.to());
  chessGame.setPlayerPieces(piece(), currentPieceBb);
}

//------------------------------------------------------------------------------
void PieceRules::clearCheckedSquares(
    const Square& from, BitBoard& moveMap, IChessGame& chessGame) const
{
  Square destSquare;
  unsigned int currentSquareIdx = 0;
  SquaresList moveSquares = moveMap.activeSquares();

  while(moveSquares.at(currentSquareIdx++, destSquare))
  {
    Move move(from, destSquare);
    std::unique_ptr<IChessGame> tmpGame;

    chessGame.clone(tmpGame);
    writeMove(move, *tmpGame);

    if(tmpGame->isChecked())
      moveMap.clear(destSquare);
  }
}

//------------------------------------------------------------------------------
BitBoard PieceRules::slide(
    const Square& from,
    IChessGame& game,
    unsigned int direction,
    bool allowSelfCapture)
{
  return slide(from, game, direction, allowSelfCapture, 7);
}

//------------------------------------------------------------------------------
BitBoard PieceRules::slide(
    const Square& from,
    IChessGame& game,
    unsigned int direction,
    bool allowSelfCapture,
    int maxSteps)
{
  Square maxSquare;
  BitBoard slideMap;
  BitBoard playerPieces;
  BitBoard opponentPieces;
  unsigned int currentBit = BitBoard::toBitNumber(from);
  directionToMaxSquare(direction, maxSquare);

  if(allowSelfCapture)
    opponentPieces = game.allPieces();
  else
  {
    playerPieces = game.playerPieces();
    opponentPieces = game.opponentPieces();
  }

  if(BitBoard::toFile(currentBit) == maxSquare.file() ||
        BitBoard::toRank(currentBit) == maxSquare.rank())
    return slideMap;

  currentBit += direction;
  while(--maxSteps >= 0)
  {
    if(playerPieces.isSet(currentBit))
      break;

    slideMap.set(currentBit);

    if(opponentPieces.isSet(currentBit) ||
        BitBoard::toFile(currentBit) == maxSquare.file() ||
        BitBoard::toRank(currentBit) == maxSquare.rank())
      break;

    currentBit += direction;
  }

  return slideMap;
}

}       // namespace
