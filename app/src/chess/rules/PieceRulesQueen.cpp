/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PieceRulesQueen.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PieceRulesQueen::PieceRulesQueen()
{
}

//------------------------------------------------------------------------------
PieceRulesQueen::~PieceRulesQueen()
{
}

//------------------------------------------------------------------------------
Piece PieceRulesQueen::piece() const
{
  return Piece::QUEEN;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesQueen::moveMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard moveMap;

  moveMap = m_bishopRules.moveMap(from, chessGame);
  moveMap |= m_rookRules.moveMap(from, chessGame);

  return moveMap;
}

//------------------------------------------------------------------------------
BitBoard PieceRulesQueen::attackMap(
    const Square& from, IChessGame& chessGame) const
{
  BitBoard attackMap;

  attackMap = m_bishopRules.attackMap(from, chessGame);
  attackMap |= m_rookRules.attackMap(from, chessGame);

  return attackMap;
}

}       // namespace
