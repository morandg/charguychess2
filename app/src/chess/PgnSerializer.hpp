/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_PGN_SERIALIZER_HPP_
#define _CGC_PGN_SERIALIZER_HPP_

#include <rglib/io/IFile.hpp>
#include <rglib/time/TimeDuration.hpp>

#include "IChessGame.hpp"
#include "../settings/PgnSettings.hpp"

namespace charguychess {

class PgnSerializer:
    public IGameObserver
{
public:
  PgnSerializer(IChessGame& chessGame);
  virtual ~PgnSerializer();

  int init(const PgnSettings& settings);

  // IGameObserver
  virtual void onMovePlayed(const Move& move) override;
  virtual void onNewGameStarted() override;

private:
  IChessGame& m_chessGame;
  std::string m_destFolder;
  std::string m_event;
  std::string m_site;
  unsigned int m_round;
  std::string m_filename;
  std::string m_date;
  std::unique_ptr<rglib::IFile> m_pgnFile;

  void buildDateInfo();
};

}       // namespace
#endif  // _CGC_PGN_SERIALIZER_HPP_
