/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Turn.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
Turn::Turn()
{
}

//------------------------------------------------------------------------------
Turn::~Turn()
{
}

//------------------------------------------------------------------------------
void Turn::whiteMove(const Move& move)
{
  m_whiteMove = move;
}

//------------------------------------------------------------------------------
const Move& Turn::whiteMove() const
{
  return m_whiteMove;
}

//------------------------------------------------------------------------------
void Turn::blackMove(const Move& move)
{
  m_blackMove = move;
}

//------------------------------------------------------------------------------
const Move& Turn::blackMove() const
{
  return m_blackMove;
}

//------------------------------------------------------------------------------
std::string Turn::toString() const
{
  std::string turnStr;

  if(m_whiteMove.isValid())
  {
    turnStr += m_whiteMove.toString();
    if(m_blackMove.isValid())
      turnStr += " " + m_blackMove.toString();
  }

  return turnStr;
}

//------------------------------------------------------------------------------
std::string Turn::toPGN() const
{
  std::string turnPGN;

  if(m_whiteMove.isValid())
  {
    turnPGN += m_whiteMove.toPGN();
    if(m_blackMove.isValid())
      turnPGN += " " + m_blackMove.toPGN();
  }

  return turnPGN;
}

}       // namespace
