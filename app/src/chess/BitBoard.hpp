/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_BITBOARD_HPP_
#define _CGC_BITBOARD_HPP_

#include <cstdint>
#include <list>

#include "SquaresList.hpp"

namespace charguychess {

/*
 * Bitboard values are divided into ranks where the MSB is the eighth ranks and
 * LSB is the first rank. Each 8 bits row value has its MSB containing the A
 * file and LSB the H file:
 * 8877665544332211     -> 64 bits (8 x 8 bits)
 *            __/  \__
 *           |ABCDEFGH| -> 8 bits
 *
 * Consequently, bits are numbered as follow:
 *   +----+----+----+----+----+----+----+----+
 * 8 | 63 | 62 | 61 | 60 | 59 | 58 | 57 | 56 |
 *   +----+----+----+----+----+----+----+----+
 * 7 | 55 | 54 | 53 | 52 | 51 | 50 | 49 | 48 |
 *   +----+----+----+----+----+----+----+----+
 * 6 | 47 | 46 | 45 | 44 | 43 | 42 | 41 | 40 |
 *   +----+----+----+----+----+----+----+----+
 * 5 | 39 | 38 | 37 | 36 | 35 | 34 | 33 | 32 |
 *   +----+----+----+----+----+----+----+----+
 * 4 | 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 |
 *   +----+----+----+----+----+----+----+----+
 * 3 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 |
 *   +----+----+----+----+----+----+----+----+
 * 2 | 15 | 14 | 13 | 12 | 11 | 10 | 09 | 08 |
 *   +----+----+----+----+----+----+----+----+
 * 1 | 07 | 06 | 05 | 04 | 03 | 02 | 01 | 00 |
 *   +----+----+----+----+----+----+----+----+
 *     A    B    C    D    E    F    G    H
 */
typedef uint64_t BitBoardValue;

class BitBoard
{
public:
  static const BitBoardValue    INITIAL_WHITE_PAWNS;
  static const BitBoardValue    INITIAL_WHITE_ROOKS;
  static const BitBoardValue    INITIAL_WHITE_KNIGHTS;
  static const BitBoardValue    INITIAL_WHITE_BISHOPS;
  static const BitBoardValue    INITIAL_WHITE_QUEEN;
  static const BitBoardValue    INITIAL_WHITE_KING;
  static const BitBoardValue    INITIAL_BLACK_PAWNS;
  static const BitBoardValue    INITIAL_BLACK_ROOKS;
  static const BitBoardValue    INITIAL_BLACK_KNIGHTS;
  static const BitBoardValue    INITIAL_BLACK_BISHOPS;
  static const BitBoardValue    INITIAL_BLACK_QUEEN;
  static const BitBoardValue    INITIAL_BLACK_KING;
  static const unsigned int     DIRECTION_NORTH;
  static const unsigned int     DIRECTION_NORTH_EAST;
  static const unsigned int     DIRECTION_EAST;
  static const unsigned int     DIRECTION_SOUTH_EAST;
  static const unsigned int     DIRECTION_SOUTH;
  static const unsigned int     DIRECTION_WEST;
  static const unsigned int     DIRECTION_SOUTH_WEST;
  static const unsigned int     DIRECTION_NORTH_WEST;

  BitBoard();
  BitBoard(BitBoardValue value);
  ~BitBoard();

  static unsigned int toBitNumber(const Square& square);
  static File toFile(unsigned int bit);
  static Rank toRank(unsigned int bit);
  static Square toSquare(unsigned int bit);

  BitBoardValue getValue() const;
  bool isSet(const Square& square) const;
  void set(const Square& square);
  void clear(const Square& square);
  void toggle(const Square& square);
  bool isSet(unsigned int bit) const;
  void set(unsigned int bit);
  void clear(unsigned int bit);
  void toggle(unsigned int bit);
  SquaresList activeSquares() const;
  unsigned int activeBits() const;

  BitBoard operator&(const BitBoard& rhs) const;
  BitBoard operator|(const BitBoard& rhs) const;
  BitBoard& operator|=(const BitBoard& rhs);
  bool operator==(const BitBoard& rhs) const;
  bool operator!=(const BitBoard& rhs) const;
  BitBoard operator^(const BitBoard& rhs) const;

private:
  BitBoardValue m_value;
};

}       // namespace
#endif  // _CGC_BITBOARD_HPP_
