/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessHardwareDefs.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& s, const Led& led)
{
  switch(led)
  {
  case Led::A:
    s << "A";
    break;
  case Led::B:
    s << "B";
    break;
  case Led::C:
    s << "C";
    break;
  case Led::D:
    s << "D";
    break;
  case Led::E:
    s << "E";
    break;
  case Led::F:
    s << "F";
    break;
  case Led::G:
    s << "G";
    break;
  case Led::H:
    s << "H";
    break;
  case Led::ONE:
    s << "1";
    break;
  case Led::TWO:
    s << "2";
    break;
  case Led::THREE:
    s << "3";
    break;
  case Led::FOUR:
    s << "4";
    break;
  case Led::FIVE:
    s << "5";
    break;
  case Led::SIX:
    s << "6";
    break;
  case Led::SEVEN:
    s << "7";
    break;
  case Led::EIGHT:
    s << "8";
    break;
  case Led::WHITE:
    s << "WHITE";
    break;
  case Led::BLACK:
    s << "BLACK";
    break;
  default:
    s << "?";
  }

  return s;

}

//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& s, const Button& button)
{
  switch(button)
  {
  case Button::ROOK:
    s << "ROOK";
    break;
  case Button::KNIGHT:
    s << "KNIGHT";
    break;
  case Button::BISHOP:
    s << "BISHOP";
    break;
  case Button::QUEEN:
    s << "QUEEN";
    break;
  default:
    s << "???";
  }

  return s;
}

}       // namespace
