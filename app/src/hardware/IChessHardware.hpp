/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_CHESS_HARDWARE_HPP_
#define _CGC_I_CHESS_HARDWARE_HPP_

#include <memory>

#include <rglib/event-loop/IEventLoop.hpp>

#include "IChessHardwareObserver.hpp"

namespace charguychess {

class IChessHardware
{
public:
  static void create(
      rglib::IEventLoop& mainLoop,
      std::unique_ptr<IChessHardware>& hardware);

  IChessHardware();
  virtual ~IChessHardware();

  virtual int init() = 0;
  virtual int turnRankLed(bool isOn, Rank rank) = 0;
  virtual int turnFileLed(bool isOn, File file) = 0;
  virtual int turnSquareLed(bool isOn, const Square& square) = 0;
  virtual int turnLed(bool isOn, Led led) = 0;
  virtual int turnAllLeds(bool isOn) = 0;
  virtual int turnPlayerLed(bool isOn, Color color) = 0;
  virtual void addObserver(IChessHardwareObserver& observer) = 0;
  virtual void removeObserver(IChessHardwareObserver& observer) = 0;
  virtual BitBoard getPieces() = 0;
};

}       // namespace
#endif  // _CGC_I_CHESS_HARDWARE_HPP_
