/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "IChessState.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
IChessState::IChessState()
{
}

//------------------------------------------------------------------------------
IChessState::~IChessState()
{
}

//------------------------------------------------------------------------------
void IChessState::showPromotion(Piece promotion, IChessHardware& hardware)
{
  if(promotion != Piece::PAWN)
  {
    switch(promotion)
    {
    case Piece::ROOK:
      hardware.turnLed(true, Led::ROOK);
      break;
    case Piece::KNIGHT:
      hardware.turnLed(true, Led::KNIGHT);
      break;
    case Piece::BISHOP:
      hardware.turnLed(true, Led::BISHOP);
      break;
    case Piece::QUEEN:
      hardware.turnLed(true, Led::QUEEN);
      break;
    default:
      LOGER() <<
        "BUG? unhandled promotion piece type when requesting drop piece";
      assert(0);    // should not happen
    }
  }

}

}       // namespace
