/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStateHumanThinking.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStateHumanThinking::ChessStateHumanThinking(
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame):
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame)
{
}

//------------------------------------------------------------------------------
ChessStateHumanThinking::~ChessStateHumanThinking()
{
}

//------------------------------------------------------------------------------
int ChessStateHumanThinking::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStateHumanThinking::enter()
{
  LOGDB() << "Enter human thinking state";

  m_hardware.turnPlayerLed(true, m_chessGame.currentPlayer());

  m_hardware.addObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStateHumanThinking::exit()
{
  LOGDB() << "Exit human thinking state";

  m_hardware.removeObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStateHumanThinking::onBoardChanged(const BitBoard& boardStatus)
{
  BitBoard gameBb = m_chessGame.allPieces();
  unsigned int piecesInGame = gameBb.activeBits();
  unsigned int piecesOnBoard= boardStatus.activeBits();

  if(piecesOnBoard > piecesInGame)
  {
    LOGWA() << "Too many pieces on board!";
    m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }
  else if(piecesInGame - piecesOnBoard == 1)
  {
    BitBoard liftedPiece = m_chessGame.allPieces() ^ boardStatus;
    SquaresList liftedSquares = liftedPiece.activeSquares();
    Square liftedSquare;
    BitBoard moveMap;

    if(!liftedSquares.at(0, liftedSquare))
    {
      LOGER() << "BUG? Square not found in listed square list";
      assert(0);    // Should not happen
    }

    moveMap = m_chessGame.moveMap(liftedSquare);
    if(moveMap.getValue())
      m_statesPool.changeState(IChessStatesPool::PIECE_LIFTED);
    else
    {
      LOGWA() << "Cannot move lifted piece on " << liftedSquare.toString();
      m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
    }
  }
}

//------------------------------------------------------------------------------
void ChessStateHumanThinking::onButtonPressed(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateHumanThinking::onButtonReleased(Button button)
{
  (void)button;
}

}       // namespace
