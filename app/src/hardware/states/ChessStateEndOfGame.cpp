/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessStateEndOfGame.hpp"

#include <rglib/logging/LogMacros.hpp>


namespace charguychess {

static const unsigned int MATE_BLINK_TIMEOUT = 100;

//------------------------------------------------------------------------------
ChessStateEndOfGame::ChessStateEndOfGame(
    rglib::IEventLoop& el,
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame):
  m_el(el),
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame),
  m_isRisingSquare(true)
{
}

//------------------------------------------------------------------------------
ChessStateEndOfGame::~ChessStateEndOfGame()
{
}

//------------------------------------------------------------------------------
int ChessStateEndOfGame::init()
{
  m_el.createTimer(m_blinkTimer, "mateTimer", *this);
  if(!m_blinkTimer)
  {
    LOGER() << "Cannot create mate blink timer";
    return -1;
  }

  m_blinkTimer->setTimeout(MATE_BLINK_TIMEOUT);

  return 0;
}

//------------------------------------------------------------------------------
void ChessStateEndOfGame::enter()
{
  Color winner;

  LOGDB() << "Enter state end of game";

  m_hardware.addObserver(*this);

  if(m_chessGame.currentPlayer() == Color::WHITE)
  {
    if(m_chessGame.isStaleMate())
      winner = Color::WHITE;
    else
      winner = Color::BLACK;
  }
  else
  {
    if(m_chessGame.isStaleMate())
      winner = Color::BLACK;
    else
      winner = Color::WHITE;
  }

  m_blinkSquare = Square(File::A, Rank::ONE);
  m_hardware.turnAllLeds(false);
  m_hardware.turnSquareLed(true, m_blinkSquare);
  m_hardware.turnPlayerLed(true, winner);
  m_isRisingSquare = true;
  m_blinkTimer->start();
}

//------------------------------------------------------------------------------
void ChessStateEndOfGame::exit()
{
  LOGDB() << "Exit state end of game";

  m_hardware.removeObserver(*this);

  m_blinkTimer->stop();
}

//------------------------------------------------------------------------------
void ChessStateEndOfGame::onBoardChanged(const BitBoard& boardStatus)
{
  (void)boardStatus;
}

//------------------------------------------------------------------------------
void ChessStateEndOfGame::onButtonPressed(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateEndOfGame::onButtonReleased(Button button)
{
  LOGIN() << "Starting a new game";

  // Any button!
  (void)button;

  m_chessGame.newGame();
  m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
}

//------------------------------------------------------------------------------
void ChessStateEndOfGame::onTimeout(const std::string& who)
{
  int currentFile = (int)m_blinkSquare.file();
  int currentRank = (int)m_blinkSquare.rank();

  (void)who;    // unused

  m_hardware.turnSquareLed(false, m_blinkSquare);

  if(m_isRisingSquare)
  {
    currentFile++;
    currentRank++;
  }
  else
  {
    currentFile--;
    currentRank--;
  }

  m_blinkSquare = Square((File)currentFile, (Rank)currentRank);
  m_hardware.turnSquareLed(true, m_blinkSquare);

  if(m_blinkSquare == Square(File::H, Rank::EIGHT))
    m_isRisingSquare = false;
  else if(m_blinkSquare == Square(File::A, Rank::ONE))
    m_isRisingSquare = true;

  m_blinkTimer->start();
}

}       // namespace
