/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/logging/LogMacros.hpp>

#include "ChessStateComputerThinking.hpp"

namespace charguychess {

const unsigned int ChessStateComputerThinking:: BLINK_ON_TIMEOUT    = 1000;
const unsigned int ChessStateComputerThinking:: BLINK_OFF_TIMEOUT   = 100;

//------------------------------------------------------------------------------
ChessStateComputerThinking::ChessStateComputerThinking(
    IBlinkingLeds& blinkingLeds,
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    const IChessGame& chessGame,
    const AiSettings& aiSettings,
    IChessEngine& engine):
  m_blinkingLeds(blinkingLeds),
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame),
  m_aiSettings(aiSettings),
  m_engine(engine)
{
}

//------------------------------------------------------------------------------
ChessStateComputerThinking::~ChessStateComputerThinking()
{
}

//------------------------------------------------------------------------------
int ChessStateComputerThinking::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::enter()
{
  MoveInfo lastComputedMove = m_engine.lastFoundMove();

  LOGDB() << "Enter computer thinking state";

  if(isBestMoveFound(lastComputedMove))
    m_statesPool.changeState(IChessStatesPool::REQUEST_MOVE);
  else
  {
    m_engine.addObserver(*this);
    m_hardware.addObserver(*this);
    m_blinkingLeds.setOnTimeout(BLINK_ON_TIMEOUT);
    m_blinkingLeds.setOffTimeout(BLINK_OFF_TIMEOUT);

    if(!isAlreadyComputing(lastComputedMove))
      searchMove();

    if(m_chessGame.currentPlayer() == Color::WHITE)
      m_blinkingLeds.addLed(Led::WHITE);
    else
      m_blinkingLeds.addLed(Led::BLACK);

    m_blinkingLeds.start();
  }
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::exit()
{
  LOGDB() << "Exit computer thinking state";

  m_engine.removeObserver(*this);
  m_hardware.removeObserver(*this);
  m_blinkingLeds.stop();
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::onBoardChanged(const BitBoard& boardStatus)
{
  (void)boardStatus;

  m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::onButtonPressed(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::onButtonReleased(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::onBestMoveFound(const Move& move)
{
  (void)move;

  m_statesPool.changeState(IChessStatesPool::REQUEST_MOVE);
}

//------------------------------------------------------------------------------
void ChessStateComputerThinking::searchMove()
{
  if(m_aiSettings.getSearchMode() == AiSettings::DEPTH)
    m_engine.searchDepth(
        m_aiSettings.getSearchDepth(), m_chessGame);
  else
    m_engine.searchTime(
        m_aiSettings.getSearchTime(), m_chessGame);
}

//------------------------------------------------------------------------------
bool ChessStateComputerThinking::isBestMoveFound(
    const MoveInfo& lastComputedMove)
{
  return lastComputedMove.getColor() == m_chessGame.currentPlayer() &&
      lastComputedMove.getTurnNumber() == m_chessGame.turnNumber() &&
      lastComputedMove.getMove().isValid();
}

//------------------------------------------------------------------------------
bool ChessStateComputerThinking::isAlreadyComputing(
    const MoveInfo& lastComputedMove)
{
  return lastComputedMove.getColor() == m_chessGame.currentPlayer() &&
      lastComputedMove.getTurnNumber() == m_chessGame.turnNumber() &&
      !lastComputedMove.getMove().isValid();
}

}       // namespace
