/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStateRequestMove.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStateRequestMove::ChessStateRequestMove(
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame,
    IChessEngine& chessEngine):
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame),
  m_chessEngine(chessEngine)
{
}

//------------------------------------------------------------------------------
ChessStateRequestMove::~ChessStateRequestMove()
{
}

//------------------------------------------------------------------------------
int ChessStateRequestMove::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStateRequestMove::enter()
{
  Move bestmove = m_chessEngine.lastFoundMove().getMove();

  LOGDB() << "Enter request move state";

  m_hardware.addObserver(*this);
  m_hardware.turnAllLeds(false);
  if(m_chessGame.currentPlayer() == Color::WHITE)
    m_hardware.turnLed(true, Led::WHITE);
  else
    m_hardware.turnLed(true, Led::BLACK);

  m_hardware.turnSquareLed(true, bestmove.from());
}

//------------------------------------------------------------------------------
void ChessStateRequestMove::exit()
{
  LOGDB() << "Exit request move state";

  m_hardware.removeObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStateRequestMove::onBoardChanged(const BitBoard& boardStatus)
{
  Move foundBestMove = m_chessEngine.lastFoundMove().getMove();
  BitBoard allGamePieces = m_chessGame.allPieces();
  BitBoard diff = boardStatus ^ allGamePieces;
  unsigned int changedSquares = diff.activeBits();
  SquaresList liftedSquares = diff.activeSquares();

  if(changedSquares == 0)
  {
    m_hardware.turnSquareLed(false, foundBestMove.to());
    m_hardware.turnSquareLed(true, foundBestMove.from());
  }
  else if(changedSquares == 1)
  {
    Square lifted;

    if(!liftedSquares.at(0, lifted))
    {
      LOGER() << "BUG? No lifted square found when requesting move!";
      assert(0);    // Should not happen
    }

    if(lifted == foundBestMove.from())
    {
      m_hardware.turnSquareLed(false, foundBestMove.from());
      m_hardware.turnSquareLed(true, foundBestMove.to());
      showPromotion(foundBestMove.promotion(), m_hardware);
    }
    else
      m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }
  else if(changedSquares == 2)
  {
    if(liftedSquares.contains(foundBestMove))
    {
      if(allGamePieces.isSet(foundBestMove.to()))
        m_statesPool.changeState(IChessStatesPool::REQUEST_DROP_PIECE);
      else
      {
        if(!m_chessGame.applyMove(foundBestMove))
        {
          LOGER() << "BUG? Cannot apply computer best move " <<
              foundBestMove.toString();
          assert(0);  // Should not happen
          m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
        }
        else
          m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
      }
    }
    else
      m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }
  else
    m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
}

//------------------------------------------------------------------------------
void ChessStateRequestMove::onButtonPressed(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateRequestMove::onButtonReleased(Button button)
{
  (void)button;
}

}       // namespace
