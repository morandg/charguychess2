/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_STATES_POOL_HPP_
#define _CGC_CHESS_STATES_POOL_HPP_

#include <unordered_map>

#include "IChessStatesPool.hpp"

namespace charguychess {

class ChessStatesPool:
    public IChessStatesPool
{
public:
  ChessStatesPool();
  virtual ~ChessStatesPool();

  // IChessStatesPool
  virtual int init() override;
  virtual void addState(
      State stateType, std::unique_ptr<IChessState>& state) override;
  virtual void changeState(State state) override;

private:
  IChessState* m_currentState;
  std::unordered_map<State, std::unique_ptr<IChessState>> m_states;
};

}       // namespace
#endif  // _CGC_CHAR_GUY_CHESS_HPP_
