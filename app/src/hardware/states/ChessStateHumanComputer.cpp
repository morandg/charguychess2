/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessStateHumanComputer.hpp"

#include <rglib/logging/LogMacros.hpp>


namespace charguychess {

//------------------------------------------------------------------------------
ChessStateHumanComputer::ChessStateHumanComputer(
    const IChessGame& chessGame,
    const AisSettings& aisSettings,
    std::unique_ptr<IChessState>& humanState,
    std::unique_ptr<IChessState>& computerState):
  m_chessGame(chessGame),
  m_aisSettings(aisSettings),
  m_humanState(std::move(humanState)),
  m_computerState(std::move(computerState))
{
}

//------------------------------------------------------------------------------
ChessStateHumanComputer::~ChessStateHumanComputer()
{
}

//------------------------------------------------------------------------------
int ChessStateHumanComputer::init()
{
  if(m_humanState->init())
    return -1;
  return m_computerState->init();
}

//------------------------------------------------------------------------------
void ChessStateHumanComputer::enter()
{
  LOGDB() << "Enter human/computer state";

  if(m_aisSettings.isHuman(m_chessGame.currentPlayer()))
    m_humanState->enter();
  else
    m_computerState->enter();
}

//------------------------------------------------------------------------------
void ChessStateHumanComputer::exit()
{
  LOGDB() << "Exit human/computer state";

  if(m_aisSettings.isHuman(m_chessGame.currentPlayer()))
    m_humanState->exit();
  else
    m_computerState->exit();
}

}       // namespace
