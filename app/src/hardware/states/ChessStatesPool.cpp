/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStatesPool.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStatesPool::ChessStatesPool():
    m_currentState(nullptr)
{
}

//------------------------------------------------------------------------------
ChessStatesPool::~ChessStatesPool()
{
}

//------------------------------------------------------------------------------
int ChessStatesPool::init()
{
  for(auto& state: m_states)
    if(state.second->init())
      return -1;

  return 0;
}

//------------------------------------------------------------------------------
void ChessStatesPool::addState(
    State stateType, std::unique_ptr<IChessState>& state)
{
  m_states.emplace(stateType, std::move(state));
}

//------------------------------------------------------------------------------
void ChessStatesPool::changeState(State state)
{
  auto found = m_states.find(state);

  if(found == m_states.end())
  {
    LOGER() << "Chess state not found " << state;
    assert(0);  // should not happen
    return;
  }

  if(m_currentState)
    m_currentState->exit();

  m_currentState = found->second.get();
  m_currentState->enter();
}

}       // namespace
