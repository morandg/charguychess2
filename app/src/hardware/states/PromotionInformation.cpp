/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PromotionInformation.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
PromotionInformation::PromotionInformation():
    m_promotion(Piece::PAWN)
{
}

//------------------------------------------------------------------------------
PromotionInformation::PromotionInformation(Piece promotion):
    m_promotion(promotion)
{
}

//------------------------------------------------------------------------------
PromotionInformation::~PromotionInformation()
{
}

//------------------------------------------------------------------------------
void PromotionInformation::reset()
{
  m_promotion = Piece::PAWN;
}

//------------------------------------------------------------------------------
void PromotionInformation::buttonPressed(Button button)
{
  switch(button)
  {
  case Button::ROOK:
    m_promotion = Piece::ROOK;
    break;
  case Button::KNIGHT:
    m_promotion = Piece::KNIGHT;
    break;
  case Button::BISHOP:
    m_promotion = Piece::BISHOP;
    break;
  case Button::QUEEN:
    m_promotion = Piece::QUEEN;
    break;
  default:
    break;
  }
}

//------------------------------------------------------------------------------
Piece PromotionInformation::promotion() const
{
  return m_promotion;
}

}       // namespace
