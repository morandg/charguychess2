/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_STATE_WHITE_BLACK_HPP_
#define _CGC_CHESS_STATE_WHITE_BLACK_HPP_

#include <memory>

#include "../../chess/IChessGame.hpp"
#include "IChessState.hpp"

namespace charguychess {

class ChessStateWhiteBlack:
    public IChessState
{
public:
  ChessStateWhiteBlack(
        const IChessGame& chessGame,
        std::unique_ptr<IChessState>& whiteState,
        std::unique_ptr<IChessState>& blackState);
  virtual ~ChessStateWhiteBlack();

  // IChessState
  virtual int init() override;
  virtual void enter() override;
  virtual void exit() override;

private:
  const IChessGame& m_chessGame;
  std::unique_ptr<IChessState> m_whiteState;
  std::unique_ptr<IChessState> m_blackState;
  Color m_currentState;
};

}       // namespace
#endif  // _CGC_CHESS_STATE_WHITE_BLACK_HPP_
