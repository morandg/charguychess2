/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStatePieceLifted.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStatePieceLifted::ChessStatePieceLifted(
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame):
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame)
{
}

//------------------------------------------------------------------------------
ChessStatePieceLifted::~ChessStatePieceLifted()
{
}

//------------------------------------------------------------------------------
int ChessStatePieceLifted::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::enter()
{
  BitBoard gameBoard = m_chessGame.allPieces();
  BitBoard hwBoard = m_hardware.getPieces();
  BitBoard mask = gameBoard ^ hwBoard;
  SquaresList maskSquares = mask.activeSquares();
  Square liftedSquare;

  LOGDB() << "Piece lifted enter";

  if(!maskSquares.at(0, liftedSquare))
  {
    LOGER() << "BUG? No lifted square found when entering piece lifted";
    assert(0);  // Should not happen
  }

  m_promotion.reset();
  m_hardware.turnAllLeds(false);
  m_hardware.turnPlayerLed(true, m_chessGame.currentPlayer());
  m_hardware.turnSquareLed(true, liftedSquare);

  m_hardware.addObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::exit()
{
  LOGDB() << "Piece lifted exit";

  m_hardware.removeObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::onBoardChanged(const BitBoard& boardStatus)
{
  BitBoard gameBoard = m_chessGame.allPieces();

  if(boardStatus == gameBoard)
    m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
  else
    checkMoveDone(gameBoard, boardStatus);

}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::onButtonPressed(Button button)
{
  m_promotion.buttonPressed(button);
}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::onButtonReleased(Button button)
{
  (void)button;

  checkMoveDone(m_chessGame.allPieces(), m_hardware.getPieces());
}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::checkMoveDone(
    const BitBoard& gameBoard, const BitBoard& hwBoard)
{
  BitBoard moveMap;
  Move move;

  computeMovedSquares(gameBoard, hwBoard, move);
  moveMap = m_chessGame.moveMap(move.from());

  if(!moveMap.isSet(move.to()))
  {
    LOGWA() << "Move " << move.toString() << " not allowed";
    m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }
  else if(gameBoard.isSet(move.to()))
    m_statesPool.changeState(IChessStatesPool::CAPTURE);
  else
  {
    if(m_chessGame.isPromotionalMove(move.from()))
    {
      if(m_promotion.promotion() == Piece::PAWN)
      {
        LOGWA() << "Waiting for promotion";

        m_hardware.turnLed(true, Led::ROOK);
        m_hardware.turnLed(true, Led::KNIGHT);
        m_hardware.turnLed(true, Led::BISHOP);
        m_hardware.turnLed(true, Led::QUEEN);

        return;
      }
      else
        move.setPromotion(m_promotion.promotion());
    }

    if(!m_chessGame.applyMove(move))
    {
      LOGER() << "BUG? Unexpected invalid move in piece lifted!";
      assert(0);  // Should not happen
      m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
    }

    m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
  }
}

//------------------------------------------------------------------------------
void ChessStatePieceLifted::computeMovedSquares(
    const BitBoard& gameBoard,
    const BitBoard& hwBoard,
    Move& move)
{
  PlayerPiece pieceFrom;
  PlayerPiece pieceTo;
  bool hasPieceFrom;
  bool hasPieceTo;
  BitBoard diff = gameBoard ^ hwBoard;
  SquaresList changedSquares = diff.activeSquares();
  Square from;
  Square to;

  if(!changedSquares.at(0, from) || !changedSquares.at(1, to))
  {
    LOGER() << "BUG? Not two lifted saures when computing moved squares!";
    assert(0);  // Should not happen
  }

  hasPieceFrom = m_chessGame.pieceAt(from, pieceFrom);
  hasPieceTo = m_chessGame.pieceAt(to, pieceTo);
  move = Move(from, to);

  if(hasPieceFrom && hasPieceTo)
  {
    if(pieceFrom.color() != m_chessGame.currentPlayer())
      move = Move(move.to(), move.from());
  }
  else if(!hasPieceFrom)
    move = Move(move.to(), move.from());
}

}       // namespace
