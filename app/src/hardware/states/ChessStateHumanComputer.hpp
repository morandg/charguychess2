/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_STATE_HUMAN_COMPUTER_HPP_
#define _CGC_CHESS_STATE_HUMAN_COMPUTER_HPP_

#include "../../chess/IChessGame.hpp"
#include "../../settings/Settings.hpp"
#include "IChessState.hpp"

namespace charguychess {

class ChessStateHumanComputer:
    public IChessState
{
public:
    ChessStateHumanComputer(
        const IChessGame& chessGame,
        const AisSettings& aisSettings,
        std::unique_ptr<IChessState>& humanState,
        std::unique_ptr<IChessState>& computerState);
  virtual ~ChessStateHumanComputer();

  // IChessState
  virtual int init() override;
  virtual void enter() override;
  virtual void exit() override;

private:
  const IChessGame& m_chessGame;
  const AisSettings& m_aisSettings;
  std::unique_ptr<IChessState> m_humanState;
  std::unique_ptr<IChessState> m_computerState;

  bool isHumanPlaying();
};

}       // namespace
#endif  // _CGC_CHESS_STATE_HUMAN_COMPUTER_HPP_
