/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_STATE_INVALID_POSITION_HPP_
#define _CGC_CHESS_STATE_INVALID_POSITION_HPP_

#include "../IChessHardware.hpp"
#include "../IBlinkingLeds.hpp"
#include "IChessStatesPool.hpp"

namespace charguychess {

class ChessStateInvalidPosition:
    public IChessState,
    public IChessHardwareObserver
{
public:
  static const unsigned int BLINK_TIMEOUT;

  ChessStateInvalidPosition(
      IBlinkingLeds& blinkingLeds,
      IChessStatesPool& statesPool,
      IChessHardware& hardware,
      IChessGame& chessGame);
  virtual ~ChessStateInvalidPosition();

  // IChessState
  virtual int init() override;
  virtual void enter() override;
  virtual void exit() override;

  // IChessHardwareObserver
  virtual void onBoardChanged(const BitBoard& boardStatus) override;
  virtual void onButtonPressed(Button button) override;
  virtual void onButtonReleased(Button button) override;

private:
  IBlinkingLeds& m_blinkingLeds;
  IChessStatesPool& m_statesPool;
  IChessHardware& m_hardware;
  IChessGame& m_chessGame;

  bool isHardwarePositionCorrect(const BitBoard& hwState);
};

}       // namespace
#endif  // _CGC_CHESS_STATE_INVALID_POSITION_HPP_
