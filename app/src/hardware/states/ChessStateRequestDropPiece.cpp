/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStateRequestDropPiece.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStateRequestDropPiece::ChessStateRequestDropPiece(
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame,
    IChessEngine& engine):
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame),
  m_engine(engine)
{
}

//------------------------------------------------------------------------------
ChessStateRequestDropPiece::~ChessStateRequestDropPiece()
{
}

//------------------------------------------------------------------------------
int ChessStateRequestDropPiece::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStateRequestDropPiece::enter()
{
  Move foundBestMove = m_engine.lastFoundMove().getMove();

  LOGDB() << "Enter request drop piece";

  m_hardware.addObserver(*this);
  showPromotion(foundBestMove.promotion(), m_hardware);
}

//------------------------------------------------------------------------------
void ChessStateRequestDropPiece::exit()
{
  LOGDB() << "Exit request drop piece";

  m_hardware.removeObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStateRequestDropPiece::onBoardChanged(const BitBoard& boardStatus)
{
  BitBoard diff = boardStatus ^ m_chessGame.allPieces();

  if(diff.activeBits() == 1)
  {
    SquaresList liftedSquares = diff.activeSquares();
    Square liftedSquare;
    Move foundBestMove = m_engine.lastFoundMove().getMove();

    if(!liftedSquares.at(0, liftedSquare))
    {
      LOGER() << "BUG? No squares on dropped piece board changed";
      assert(0);    // should not happen
    }

    if(liftedSquare == foundBestMove.from())
    {
      if(!m_chessGame.applyMove(foundBestMove))
      {
        LOGER() << "BUG? Cannot apply computer capture move";
        assert(0);    // should not happen
        m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
      }
      else
        m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
    }
    else
      m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }
  else
    m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
}

//------------------------------------------------------------------------------
void ChessStateRequestDropPiece::onButtonPressed(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateRequestDropPiece::onButtonReleased(Button button)
{
  (void)button;
}

}       // namespace
