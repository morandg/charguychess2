/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/logging/LogMacros.hpp>

#include "ChessStateWhiteBlack.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStateWhiteBlack::ChessStateWhiteBlack(
    const IChessGame& chessGame,
    std::unique_ptr<IChessState>& whiteState,
    std::unique_ptr<IChessState>& blackState):
  m_chessGame(chessGame),
  m_whiteState(std::move(whiteState)),
  m_blackState(std::move(blackState)),
  m_currentState(Color::WHITE)
{
}

//------------------------------------------------------------------------------
ChessStateWhiteBlack::~ChessStateWhiteBlack()
{
}

//------------------------------------------------------------------------------
int ChessStateWhiteBlack::init()
{
  if(m_whiteState->init())
    return -1;

  return m_blackState->init();
}

//------------------------------------------------------------------------------
void ChessStateWhiteBlack::enter()
{
  LOGDB() << "Enter white/black state";

  m_currentState = m_chessGame.currentPlayer();

  if(m_currentState == Color::WHITE)
    m_whiteState->enter();
  else
    m_blackState->enter();
}

//------------------------------------------------------------------------------
void ChessStateWhiteBlack::exit()
{
  LOGDB() << "Exit white/black state";

  if(m_currentState == Color::WHITE)
    m_whiteState->exit();
  else
    m_blackState->exit();
}

}       // namespace
