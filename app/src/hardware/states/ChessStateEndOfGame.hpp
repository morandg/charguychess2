/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_STATE_END_OF_GAME_HPP_
#define _CGC_CHESS_STATE_END_OF_GAME_HPP_

#include <rglib/event-loop/IEventLoop.hpp>

#include "../IChessHardware.hpp"
#include "IChessStatesPool.hpp"

namespace charguychess {

class ChessStateEndOfGame:
    public IChessState,
    public IChessHardwareObserver,
    public rglib::ITimedOut
{
public:
  static const unsigned int BLINK_TIMEOUT;

  ChessStateEndOfGame(
      rglib::IEventLoop& el,
      IChessStatesPool& statesPool,
      IChessHardware& hardware,
      IChessGame& chessGame);
  virtual ~ChessStateEndOfGame();

  // IChessState
  virtual int init() override;
  virtual void enter() override;
  virtual void exit() override;

  // IChessHardwareObserver
  virtual void onBoardChanged(const BitBoard& boardStatus) override;
  virtual void onButtonPressed(Button button) override;
  virtual void onButtonReleased(Button button) override;

  // rglib::ITimedOut
  virtual void onTimeout(const std::string& who) override;

private:
  rglib::IEventLoop& m_el;
  IChessStatesPool& m_statesPool;
  IChessHardware& m_hardware;
  IChessGame& m_chessGame;
  std::unique_ptr<rglib::ITimer> m_blinkTimer;
  bool m_isRisingSquare;
  Square m_blinkSquare;
};

}       // namespace
#endif  // _CGC_CHESS_STATE_END_OF_GAME_HPP_
