/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_STATE_COMPUTER_THINKING_HPP_
#define _CGC_CHESS_STATE_COMPUTER_THINKING_HPP_

#include "../IChessHardware.hpp"
#include "../IBlinkingLeds.hpp"
#include "../../chess/IChessGame.hpp"
#include "../../chess/engine/IChessEngine.hpp"
#include "../../settings/AisSettings.hpp"
#include "IChessStatesPool.hpp"

namespace charguychess {

class ChessStateComputerThinking:
    public IChessState,
    public IChessHardwareObserver,
    public IChessEngineObserver
{
public:
  static const unsigned int BLINK_ON_TIMEOUT;
  static const unsigned int BLINK_OFF_TIMEOUT;

  ChessStateComputerThinking(
      IBlinkingLeds& blinkingLeds,
      IChessStatesPool& statesPool,
      IChessHardware& hardware,
      const IChessGame& chessGame,
      const AiSettings& aiSettings,
      IChessEngine& engine);
  virtual ~ChessStateComputerThinking();

  // IChessState
  virtual int init() override;
  virtual void enter() override;
  virtual void exit() override;

  // IChessHardwareObserver
  virtual void onBoardChanged(const BitBoard& boardStatus) override;
  virtual void onButtonPressed(Button button) override;
  virtual void onButtonReleased(Button button) override;

  // IChessEngineObserver
  virtual void onBestMoveFound(const Move& move) override;

private:
  IBlinkingLeds& m_blinkingLeds;
  IChessStatesPool& m_statesPool;
  IChessHardware& m_hardware;
  const IChessGame& m_chessGame;
  const AiSettings& m_aiSettings;
  IChessEngine& m_engine;

  void searchMove();
  bool isBestMoveFound(const MoveInfo& lastComputedMove);
  bool isAlreadyComputing(const MoveInfo& lastComputedMove);
};

}       // namespace
#endif  // _CGC_CHESS_STATE_COMPUTER_THINKING_HPP_
