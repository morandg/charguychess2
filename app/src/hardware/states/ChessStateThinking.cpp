/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStateThinking.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStateThinking::ChessStateThinking(
    IChessStatesPool& statesPool,
    IChessGame& chessGame,
    IChessHardware& hardware,
    std::unique_ptr<IChessState>& humanComputer):
  m_statesPool(statesPool),
  m_chessGame(chessGame),
  m_hardware(hardware),
  m_humanComputerState(std::move(humanComputer))
{
}

//------------------------------------------------------------------------------
ChessStateThinking::~ChessStateThinking()
{
}

//------------------------------------------------------------------------------
int ChessStateThinking::init()
{
  return m_humanComputerState->init();
}

//------------------------------------------------------------------------------
void ChessStateThinking::enter()
{
  LOGDB() << "Enter thinking state";

  m_hardware.turnAllLeds(false);

  if(m_chessGame.isMate())
    m_statesPool.changeState(IChessStatesPool::END_OF_GAME);
  else
  {
    if(m_chessGame.isChecked())
    {
      BitBoard kingBitBoard = m_chessGame.playerPieces(Piece::KING);
      SquaresList kingSquares = kingBitBoard.activeSquares();
      Square kingSquare;

      if(!kingSquares.at(0, kingSquare))
      {
        LOGER() << "BUG? King not found when showing check!";
        assert(0);  // Should not happen
      }

      m_hardware.turnSquareLed(true, kingSquare);
    }

    m_humanComputerState->enter();
  }
}

//------------------------------------------------------------------------------
void ChessStateThinking::exit()
{
  LOGDB() << "Exit thinking state";

  m_humanComputerState->exit();
}

}       // namespace
