/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "ChessStateCapture.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessStateCapture::ChessStateCapture(
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame):
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame)
{
}

//------------------------------------------------------------------------------
ChessStateCapture::~ChessStateCapture()
{
}

//------------------------------------------------------------------------------
int ChessStateCapture::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStateCapture::enter()
{
  BitBoard liftedPieces = m_hardware.getPieces() ^ m_chessGame.allPieces();
  SquaresList liftedSquares = liftedPieces.activeSquares();
  PlayerPiece foundPiece;
  Square from;
  Square to;

  LOGDB() << "Enter state capture";

  if(!liftedSquares.at(0, from) ||
      !liftedSquares.at(1, to))
  {
    LOGER() << "BUG? Not two lifted squares found when entering capture!";
    assert(0);  // Should not happen
  }

  m_hardware.addObserver(*this);
  m_hardware.turnSquareLed(true, from);
  m_hardware.turnSquareLed(true, to);

  m_chessGame.pieceAt(from, foundPiece);
  if(foundPiece.color() == m_chessGame.currentPlayer())
    m_destination = to;
  else
  {
    m_destination = from;
    from = to;
  }

  m_promotion.reset();
  if(m_chessGame.isPromotionalMove(from))
  {
    m_hardware.turnLed(true, Led::ROOK);
    m_hardware.turnLed(true, Led::KNIGHT);
    m_hardware.turnLed(true, Led::BISHOP);
    m_hardware.turnLed(true, Led::QUEEN);
  }
}

//------------------------------------------------------------------------------
void ChessStateCapture::exit()
{
  LOGDB() << "Exit state capture";

  m_hardware.removeObserver(*this);
}

//------------------------------------------------------------------------------
void ChessStateCapture::onBoardChanged(const BitBoard& boardStatus)
{
  BitBoard liftedPieces = boardStatus ^ m_chessGame.allPieces();

  if(liftedPieces.activeBits() == 1)
  {
    PlayerPiece removedPiece;
    Square emptySquare;
    SquaresList emptySquares = liftedPieces.activeSquares();

    if(!emptySquares.at(0, emptySquare))
    {
      LOGER() << "BUG? No empty square found when computing capture!";
      assert(0);    // Should not happen
    }
    if(m_chessGame.pieceAt(emptySquare, removedPiece))
    {
      Move move(emptySquare, m_destination);

      if(removedPiece.color() == m_chessGame.currentPlayer())
        applyMove(move);
      else
        m_statesPool.changeState(IChessStatesPool::PIECE_LIFTED);
    }
    else
      m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }
  else if(liftedPieces.activeBits() != 2)
    m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
}

//------------------------------------------------------------------------------
void ChessStateCapture::onButtonPressed(Button button)
{
  m_promotion.buttonPressed(button);
}

//------------------------------------------------------------------------------
void ChessStateCapture::onButtonReleased(Button button)
{
  (void)button;

  onBoardChanged(m_hardware.getPieces());
}

//------------------------------------------------------------------------------
void ChessStateCapture::applyMove(Move& move)
{
  if(m_chessGame.isPromotionalMove(move.from()))
  {
    Piece promotion = m_promotion.promotion();
    if(promotion == Piece::PAWN)
    {
      LOGWA() << "Waiting for promotion information!";
      return;
    }
    else
      move.setPromotion(promotion);
  }

  if(!m_chessGame.applyMove(move))
  {
    LOGER() << "BUG? Cannot apply move " << move.toString() <<
        " in capture state";
    assert(0);    // Should not happen
    m_statesPool.changeState(IChessStatesPool::INVALID_POSITION);
  }

  m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
}

}       // namespace

