/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/logging/LogMacros.hpp>

#include "ChessStateInvalidPosition.hpp"

namespace charguychess {

const unsigned int ChessStateInvalidPosition::BLINK_TIMEOUT     = 500;

//------------------------------------------------------------------------------
ChessStateInvalidPosition::ChessStateInvalidPosition(
    IBlinkingLeds& blinkingLeds,
    IChessStatesPool& statesPool,
    IChessHardware& hardware,
    IChessGame& chessGame):
  m_blinkingLeds(blinkingLeds),
  m_statesPool(statesPool),
  m_hardware(hardware),
  m_chessGame(chessGame)
{
}

//------------------------------------------------------------------------------
ChessStateInvalidPosition::~ChessStateInvalidPosition()
{
}

//------------------------------------------------------------------------------
int ChessStateInvalidPosition::init()
{
  return 0;
}

//------------------------------------------------------------------------------
void ChessStateInvalidPosition::enter()
{
  LOGDB() << "Enter invalid position state";

  if(isHardwarePositionCorrect(m_hardware.getPieces()))
    m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
  else
  {
    m_hardware.addObserver(*this);
    m_blinkingLeds.setOnTimeout(BLINK_TIMEOUT);
    m_blinkingLeds.setOffTimeout(BLINK_TIMEOUT);
    m_blinkingLeds.addLed(Led::A);
    m_blinkingLeds.addLed(Led::B);
    m_blinkingLeds.addLed(Led::C);
    m_blinkingLeds.addLed(Led::D);
    m_blinkingLeds.addLed(Led::E);
    m_blinkingLeds.addLed(Led::F);
    m_blinkingLeds.addLed(Led::G);
    m_blinkingLeds.addLed(Led::H);
    m_blinkingLeds.addLed(Led::ONE);
    m_blinkingLeds.addLed(Led::TWO);
    m_blinkingLeds.addLed(Led::THREE);
    m_blinkingLeds.addLed(Led::FOUR);
    m_blinkingLeds.addLed(Led::FIVE);
    m_blinkingLeds.addLed(Led::SIX);
    m_blinkingLeds.addLed(Led::SEVEN);
    m_blinkingLeds.addLed(Led::EIGHT);
    m_blinkingLeds.addLed(Led::WHITE);
    m_blinkingLeds.addLed(Led::BLACK);
    m_blinkingLeds.start();
  }
}

//------------------------------------------------------------------------------
void ChessStateInvalidPosition::exit()
{
  LOGDB() << "Exit invalid position state";

  m_hardware.removeObserver(*this);
  m_blinkingLeds.stop();
}

//------------------------------------------------------------------------------
void ChessStateInvalidPosition::onBoardChanged(const BitBoard& boardStatus)
{
  if(isHardwarePositionCorrect(boardStatus))
    m_statesPool.changeState(IChessStatesPool::PLAYER_THINKING);
}

//------------------------------------------------------------------------------
void ChessStateInvalidPosition::onButtonPressed(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
void ChessStateInvalidPosition::onButtonReleased(Button button)
{
  (void)button;
}

//------------------------------------------------------------------------------
bool ChessStateInvalidPosition::isHardwarePositionCorrect(
    const BitBoard& hwState)
{
  return m_chessGame.allPieces() == hwState;
}

}       // namespace
