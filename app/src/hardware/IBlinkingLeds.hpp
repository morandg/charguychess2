  /**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_BLINKING_LEDS_HPP_
#define _CGC_I_BLINKING_LEDS_HPP_

#include "ChessHardwareDefs.hpp"

namespace charguychess {

class IBlinkingLeds
{
public:
  IBlinkingLeds();
  virtual ~IBlinkingLeds();

  virtual void start() = 0;
  virtual void setOnTimeout(unsigned int ms) = 0;
  virtual void setOffTimeout(unsigned int ms) = 0;
  virtual void addLed(Led led) = 0;
  virtual void stop() = 0;
};

}       // namespace
#endif  // _CGC_I_BLINKING_LEDS_HPP_
