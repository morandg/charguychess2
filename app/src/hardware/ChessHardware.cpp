/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessHardware.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
ChessHardware::ChessHardware()
{
}

//------------------------------------------------------------------------------
ChessHardware::~ChessHardware()
{
}

//------------------------------------------------------------------------------
int ChessHardware::turnRankLed(bool isOn, Rank rank)
{
  Led led;

  switch(rank)
  {
  case Rank::ONE:
    led = Led::ONE;
    break;
  case Rank::TWO:
    led = Led::TWO;
    break;
  case Rank::THREE:
    led = Led::THREE;
    break;
  case Rank::FOUR:
    led = Led::FOUR;
    break;
  case Rank::FIVE:
    led = Led::FIVE;
    break;
  case Rank::SIX:
    led = Led::SIX;
    break;
  case Rank::SEVEN:
    led = Led::SEVEN;
    break;
  case Rank::EIGHT:
    led = Led::EIGHT;
    break;
  default:
    return -1;
  }

  return turnLed(isOn, led);
}

//------------------------------------------------------------------------------
int ChessHardware::turnFileLed(bool isOn, File file)
{
  Led led;

  switch(file)
  {
  case File::A:
    led = Led::A;
    break;
  case File::B:
    led = Led::B;
    break;
  case File::C:
    led = Led::C;
    break;
  case File::D:
    led = Led::D;
    break;
  case File::E:
    led = Led::E;
    break;
  case File::F:
    led = Led::F;
    break;
  case File::G:
    led = Led::G;
    break;
  case File::H:
    led = Led::H;
    break;
  default:
    return -1;
  }

  return turnLed(isOn, led);
}

//------------------------------------------------------------------------------
int ChessHardware::turnAllLeds(bool isOn)
{
  int errors = 0;

  for(unsigned int i = (unsigned int)Led::A ;
      i <= (unsigned int)Led::WHITE ;
      ++i)
    errors += turnLed(isOn, (Led)i);

  if(errors == 0)
    return 0;
  else
    return -1;
}

//------------------------------------------------------------------------------
int ChessHardware::turnSquareLed(bool isOn, const Square& square)
{
  if((turnFileLed(isOn, square.file()) < 0) |
      (turnRankLed(isOn, square.rank()) < 0))
    return -1;

  return 0;
}
//------------------------------------------------------------------------------
int ChessHardware::turnPlayerLed(bool isOn, Color color)
{
  if(color == Color::WHITE)
    return turnLed(isOn, Led::WHITE);
  else
    return turnLed(isOn, Led::BLACK);
}

//------------------------------------------------------------------------------
void ChessHardware::addObserver(IChessHardwareObserver& observer)
{
  for(IChessHardwareObserver& crt: m_observers)
    if(&crt == &observer)
      return;

  m_observers.push_back(observer);
}

//------------------------------------------------------------------------------
void ChessHardware::removeObserver(IChessHardwareObserver& observer)
{
  for(auto it = m_observers.begin() ; it != m_observers.end() ; ++it)
  {
    if(&(*it).get() == &observer)
    {
      m_observers.erase(it);
      return;
    }
  }
}

//------------------------------------------------------------------------------
void ChessHardware::raiseBoardChanged(const BitBoard& boardStatus)
{
  // Don't invalidate iterator when deregistering within callback
  std::list<std::reference_wrapper<IChessHardwareObserver>> observers =
      m_observers;

  for(IChessHardwareObserver& observer: observers)
    observer.onBoardChanged(boardStatus);
}

//------------------------------------------------------------------------------
void ChessHardware::raiseButtonPressed(Button button)
{
  // Don't invalidate iterator when deregistering within callback
  std::list<std::reference_wrapper<IChessHardwareObserver>> observers =
      m_observers;

  for(IChessHardwareObserver& observer: observers)
    observer.onButtonPressed(button);
}

//------------------------------------------------------------------------------
void ChessHardware::raiseButtonReleased(Button button)
{
  // Don't invalidate iterator when deregistering within callback
  std::list<std::reference_wrapper<IChessHardwareObserver>> observers =
      m_observers;

  for(IChessHardwareObserver& observer: observers)
    observer.onButtonReleased(button);
}

}       // namespace

