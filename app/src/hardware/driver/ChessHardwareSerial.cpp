/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iomanip>
#include <cstring>

#include <rglib/logging/LogMacros.hpp>

#include <cgc-protocol/registers_defs.h>

#include "ChessHardwareSerial.hpp"

namespace charguychess {

const std::string ChessHardwareSerial::DEFAULT_SERIAL_PORT =
    "/dev/charguychess";

//------------------------------------------------------------------------------
ChessHardwareSerial::ChessHardwareSerial(
    std::unique_ptr<rglib::IFile>& serialPort,
    rglib::IEventLoop& el,
    std::unique_ptr<IRegisters>& registers,
    std::unique_ptr<ICgcProtocolParser>& parser,
    std::unique_ptr<ICgcProtocolSerializer>& serializer):
  m_serialPort(std::move(serialPort)),
  m_el(el),
  m_registers(std::move(registers)),
  m_parser(std::move(parser)),
  m_serializer(std::move(serializer))
{
}

//------------------------------------------------------------------------------
ChessHardwareSerial::~ChessHardwareSerial()
{
}

//------------------------------------------------------------------------------
int ChessHardwareSerial::init()
{
  int ret;

  ret = m_serialPort->open(DEFAULT_SERIAL_PORT, rglib::OpenFlag::READ_WRITE);
  if(ret != 0)
  {
    LOGER() << "Cannot open charguychess serial port " <<
        DEFAULT_SERIAL_PORT << ": " << strerror(-ret);
    return -1;
  }

  if(m_registers->init())
  {
    LOGER() << "Could not initialize charguychess registers";
    return -1;
  }

  if(m_parser->init(*this))
  {
    LOGER() << "Could not initialize charguyches protocol parser";
    return -1;
  }

  if(requestAllStatus())
    return -1;

  if(m_el.addHandledIo(
      *this, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST))
  {
    LOGER() << "Could not register charguychess serial port to event loop";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int ChessHardwareSerial::turnLed(bool isOn, Led led)
{
  uint8_t reg = 0;
  uint8_t bit = 0;
  uint8_t regValue;

  switch(led)
  {
  case Led::A:
    bit = CGC_REGISTER_MASK_FILE_A;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::B:
    bit = CGC_REGISTER_MASK_FILE_B;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::C:
    bit = CGC_REGISTER_MASK_FILE_C;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::D:
    bit = CGC_REGISTER_MASK_FILE_D;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::E:
    bit = CGC_REGISTER_MASK_FILE_E;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::F:
    bit = CGC_REGISTER_MASK_FILE_F;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::G:
    bit = CGC_REGISTER_MASK_FILE_G;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::H:
    bit = CGC_REGISTER_MASK_FILE_H;
    reg = CGC_REGISTER_RANK_LEDS;
    break;
  case Led::ONE:
    bit = CGC_REGISTER_MASK_RANK_ONE;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::TWO:
    bit = CGC_REGISTER_MASK_RANK_TWO;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::THREE:
    bit = CGC_REGISTER_MASK_RANK_THREE;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::FOUR:
    bit = CGC_REGISTER_MASK_RANK_FOUR;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::FIVE:
    bit = CGC_REGISTER_MASK_RANK_FIVE;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::SIX:
    bit = CGC_REGISTER_MASK_RANK_SIX;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::SEVEN:
    bit = CGC_REGISTER_MASK_RANK_SEVEN;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::EIGHT:
    bit = CGC_REGISTER_MASK_RANK_EIGHT;
    reg = CGC_REGISTER_FILE_LEDS;
    break;
  case Led::WHITE:
    bit = CGC_REGISTER_MASK_LED_WHITE;
    reg = CGC_REGISTER_PIECES_LEDS;
    break;
  case Led::BLACK:
    bit = CGC_REGISTER_MASK_LED_BLACK;
    reg = CGC_REGISTER_PIECES_LEDS;
    break;
  case Led::ROOK:
    bit = CGC_REGISTER_MASK_LED_ROOK;
    reg = CGC_REGISTER_PIECES_LEDS;
    break;
  case Led::KNIGHT:
    bit = CGC_REGISTER_MASK_LED_KNIGHT;
    reg = CGC_REGISTER_PIECES_LEDS;
    break;
  case Led::BISHOP:
    bit = CGC_REGISTER_MASK_LED_BISHOP;
    reg = CGC_REGISTER_PIECES_LEDS;
    break;
  case Led::QUEEN:
    bit = CGC_REGISTER_MASK_LED_QUEEN;
    reg = CGC_REGISTER_PIECES_LEDS;
    break;
  default:
    LOGWA() << "Unhandled led " << led << " in serial driver";
    return -1;
  }

  m_registers->read(reg, regValue);

  if(isOn)
    regValue |= bit;
  else
    regValue &= ~bit;

  m_registers->write(reg, regValue);

  return writeRegister(reg, regValue);
}

//------------------------------------------------------------------------------
BitBoard ChessHardwareSerial::getPieces()
{
  uint8_t rank1Value;
  uint8_t rank2Value;
  uint8_t rank3Value;
  uint8_t rank4Value;
  uint8_t rank5Value;
  uint8_t rank6Value;
  uint8_t rank7Value;
  uint8_t rank8Value;

  if(m_registers->read(CGC_REGISTER_RANK1, rank1Value) |
      m_registers->read(CGC_REGISTER_RANK2, rank2Value) |
      m_registers->read(CGC_REGISTER_RANK3, rank3Value) |
      m_registers->read(CGC_REGISTER_RANK4, rank4Value) |
      m_registers->read(CGC_REGISTER_RANK5, rank5Value) |
      m_registers->read(CGC_REGISTER_RANK6, rank6Value) |
      m_registers->read(CGC_REGISTER_RANK7, rank7Value) |
      m_registers->read(CGC_REGISTER_RANK8, rank8Value))
  {
    LOGWA() << "BUG? Reading one ore more registers failed, " <<
        "hardware status might not be correct!";
  }

  return BitBoard(
      (BitBoardValue)rank1Value |
      (BitBoardValue)rank2Value << 8 |
      (BitBoardValue)rank3Value << 16 |
      (BitBoardValue)rank4Value << 24 |
      (BitBoardValue)rank5Value << 32 |
      (BitBoardValue)rank6Value << 40 |
      (BitBoardValue)rank7Value << 48 |
      (BitBoardValue)rank8Value << 56);
}

//------------------------------------------------------------------------------
void ChessHardwareSerial::onRegisterChanged(uint8_t reg, uint8_t value)
{
  LOGDB() << "Got register changed event: 0x" <<
      std::hex << (int)reg << ":0x" << (int)value;


  switch(reg)
  {
  case CGC_REGISTER_RANK1:
  case CGC_REGISTER_RANK2:
  case CGC_REGISTER_RANK3:
  case CGC_REGISTER_RANK4:
  case CGC_REGISTER_RANK5:
  case CGC_REGISTER_RANK6:
  case CGC_REGISTER_RANK7:
  case CGC_REGISTER_RANK8:
    m_registers->write(reg, value);
    raiseBoardChanged(getPieces());
    break;
  case CGC_REGISTER_BUTTONS_STATUS:
    buttonsStatusChanged(value);
    break;
  default:
    m_registers->write(reg, value);
  }
}

//------------------------------------------------------------------------------
rglib::FileDescriptor ChessHardwareSerial::getFileDescriptor()
{
  return m_serialPort->getFileDescriptor();
}

//------------------------------------------------------------------------------
void ChessHardwareSerial::onReadReady()
{
  unsigned int bufferSize = 32;
  char buffer[bufferSize];
  int readSize;

  readSize = m_serialPort->read(buffer, bufferSize);
  if(readSize < 0)
  {
    LOGER() << "Cannot read charguychess serial port: " << strerror(-readSize);
    return;
  }

  m_parser->parseBuffer(buffer, readSize);
}

//------------------------------------------------------------------------------
int ChessHardwareSerial::requestAllStatus()
{
  unsigned int bufferSize = 8;
  char buffer[bufferSize];
  unsigned int serializedSize;
  int ret;

  if(m_serializer->serializeReadRequest(
      buffer, bufferSize, CGC_REGISTER_ALL_STATUS, serializedSize))
  {
    LOGER() << "Cannot serialize read all registers request!";
    return -1;
  }

  ret = m_serialPort->write(buffer, serializedSize);
  if(ret < 0)
  {
    LOGER() << "Could not write to charguychess serial port: " <<
        strerror(-ret);
    return -1;
  }

  return 0;
}
//------------------------------------------------------------------------------
void ChessHardwareSerial::raiseButtonEvent(bool isPressed, Button button)
{
  if(isPressed)
    raiseButtonPressed(button);
  else
    raiseButtonReleased(button);
}

//------------------------------------------------------------------------------
void ChessHardwareSerial::buttonsStatusChanged(uint8_t newValue)
{
  uint8_t oldValue;
  uint8_t changedBits;

  m_registers->read(CGC_REGISTER_BUTTONS_STATUS, oldValue);
  m_registers->write(CGC_REGISTER_BUTTONS_STATUS, newValue);

  changedBits = oldValue ^ newValue;

  if(changedBits & CGC_REGISTER_MASK_BUTTON_QUEEN)
    raiseButtonEvent(newValue & CGC_REGISTER_MASK_BUTTON_QUEEN, Button::QUEEN);
  if(changedBits & CGC_REGISTER_MASK_BUTTON_BISHOP)
    raiseButtonEvent(newValue & CGC_REGISTER_MASK_BUTTON_BISHOP, Button::BISHOP);
  if(changedBits & CGC_REGISTER_MASK_BUTTON_KNIGHT)
    raiseButtonEvent(newValue & CGC_REGISTER_MASK_BUTTON_KNIGHT, Button::KNIGHT);
  if(changedBits & CGC_REGISTER_MASK_BUTTON_ROOK)
    raiseButtonEvent(newValue & CGC_REGISTER_MASK_BUTTON_ROOK, Button::ROOK);
}

//------------------------------------------------------------------------------
int ChessHardwareSerial::writeRegister(uint8_t reg, uint8_t value)
{
  unsigned int bufferSize = 8;
  char buffer[bufferSize];
  unsigned int serializedSize;
  int ret;

  ret = m_serializer->serializeWriteRequest(
      buffer, bufferSize, reg, value, serializedSize);
  if(ret)
  {
    LOGER() << "Cannot serialize writer register request: " << strerror(-ret);
    return -1;
  }

  ret = m_serialPort->write(buffer, serializedSize);
  if(ret < 0)
  {
    LOGER() << "Cannot write to charguychess serial port" << strerror(-ret);
    return -1;
  }

  return 0;
}

}       // namespace
