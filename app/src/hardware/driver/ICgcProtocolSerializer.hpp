/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_CGC_PROTOCOL_SERIALIZER_HPP_
#define _CGC_I_CGC_PROTOCOL_SERIALIZER_HPP_

#include <cstdint>

namespace charguychess {

class ICgcProtocolSerializer
{
public:
  ICgcProtocolSerializer();
  virtual ~ICgcProtocolSerializer();

  virtual int serializeReadRequest(
      char* buffer,
      unsigned int bufferSize,
      uint8_t reg,
      unsigned int& serializedSize) = 0;
  virtual int serializeWriteRequest(
      char* buffer,
      unsigned int bufferSize,
      uint8_t reg,
      uint8_t value,
      unsigned int& serializedSize) = 0;
};

}       // namespace
#endif  // _CGC_I_CGC_PROTOCOL_SERIALIZER_HPP_
