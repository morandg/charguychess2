/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iomanip>
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "CgcProtocolParser.hpp"

namespace charguychess {

extern "C"
{

//------------------------------------------------------------------------------
static void read_callback(void* data, uint8_t reg)
{
  (void)data;
  (void)reg;

  LOGER() << "BUG? Got register read request 0x" << std::hex << (int)reg <<
      " from charguychess driver";

  assert(0);    // Should not happen
}

//------------------------------------------------------------------------------
static void write_callback(void* data, uint8_t reg, uint8_t value)
{
  ICgcProtocolParserEvent* cb = static_cast<ICgcProtocolParserEvent*>(data);

  cb->onRegisterChanged(reg, value);
}

}   // extern "C"

//------------------------------------------------------------------------------
CgcProtocolParser::CgcProtocolParser():
    m_parser(nullptr)
{
}

//------------------------------------------------------------------------------
CgcProtocolParser::~CgcProtocolParser()
{
  if(m_parser != nullptr)
    cgc_parser_free(m_parser);
}

//------------------------------------------------------------------------------
int CgcProtocolParser::init(ICgcProtocolParserEvent& parserEvent)
{
  if(m_parser != nullptr)
  {
    LOGER() << "BUG? CGC protocol parser initialized twice";
    assert(0);  // Should not happen
  }

  m_parser = cgc_parser_create(read_callback, write_callback, &parserEvent);

  if(m_parser == nullptr)
    return -ENOMEM;

  return 0;
}

//------------------------------------------------------------------------------
void CgcProtocolParser::parseBuffer(const char* buffer, unsigned int bufferSize)
{
  if(m_parser == nullptr)
  {
    LOGER() << "BUG? Parsing CGC protocol without parser initialized";
    assert(0);  // Should not happen
  }

  cgc_parser_parse_buffer(m_parser, buffer, bufferSize);
}

}       // namespace
