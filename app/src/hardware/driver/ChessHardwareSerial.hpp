/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHESS_HARDWARE_SERIAL_HPP_
#define _CGC_CHESS_HARDWARE_SERIAL_HPP_

#include <rglib/io/IFile.hpp>
#include <rglib/event-loop/IEventLoop.hpp>

#include "../ChessHardware.hpp"
#include "ICgcProtocolParser.hpp"
#include "ICgcProtocolParserEvent.hpp"
#include "ICgcProtocolSerializer.hpp"
#include "IRegisters.hpp"

namespace charguychess {

class ChessHardwareSerial:
    public ChessHardware,
    public ICgcProtocolParserEvent,
    public rglib::IHandledIo
{
public:
  static const std::string DEFAULT_SERIAL_PORT;

  ChessHardwareSerial(
      std::unique_ptr<rglib::IFile>& serialPort,
      rglib::IEventLoop& el,
      std::unique_ptr<IRegisters>& registers,
      std::unique_ptr<ICgcProtocolParser>& parser,
      std::unique_ptr<ICgcProtocolSerializer>& serializer);
  virtual ~ChessHardwareSerial();

  // IChessHardware
  virtual int init() override;
  virtual int turnLed(bool isOn, Led led) override;
  virtual BitBoard getPieces() override;

  // ICgcProtocolParserEvent
  virtual void onRegisterChanged(uint8_t reg, uint8_t value) override;

  // rglib::IHandledIo
  virtual rglib::FileDescriptor getFileDescriptor() override;
  virtual void onReadReady() override;

private:
  std::unique_ptr<rglib::IFile> m_serialPort;
  rglib::IEventLoop& m_el;
  std::unique_ptr<IRegisters> m_registers;
  std::unique_ptr<ICgcProtocolParser> m_parser;
  std::unique_ptr<ICgcProtocolSerializer> m_serializer;

  int requestAllStatus();
  void raiseButtonEvent(bool isPressed, Button button);
  void buttonsStatusChanged(uint8_t newValue);
  int writeRegister(uint8_t reg, uint8_t value);
};

}       // namespace
#endif  // _CGC_CHESS_HARDWARE_SERIAL_HPP_
