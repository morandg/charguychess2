  /**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>
#include <cassert>

#include <rglib/logging/LogMacros.hpp>

#include "BlinkingLeds.hpp"

namespace charguychess {

const std::string BlinkingLeds::BLINK_TIMER_NAME = "BlinkingLedsTimer";

//------------------------------------------------------------------------------
BlinkingLeds::BlinkingLeds(IChessHardware& hardware):
    m_hardware(hardware),
    m_onTimeout(0),
    m_offTimeout(0),
    m_blinkStatus(true)
{
}

//------------------------------------------------------------------------------
BlinkingLeds::~BlinkingLeds()
{
}

//------------------------------------------------------------------------------
int BlinkingLeds::init(rglib::IEventLoop& el)
{
  if(el.createTimer(m_blinkTimer, BLINK_TIMER_NAME, *this))
  {
    LOGER() << "Cannot create LEDs blink timer";
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
void BlinkingLeds::start()
{
  if(!m_blinkTimer)
  {
    LOGER() << "BUG? No timer when start blinking!";
    assert(0);  // Should not happen
    return;
  }

  if(m_blinkLeds.empty())
  {
    LOGWA() << "Blinking LEDs started without LEDs to blink!";
    return;
  }

  m_blinkStatus = true;
  turnLeds();
  m_blinkTimer->setTimeout(m_onTimeout);
  m_blinkTimer->start();
}

//------------------------------------------------------------------------------
void BlinkingLeds::setOnTimeout(unsigned int ms)
{
  m_onTimeout = ms;
}

//------------------------------------------------------------------------------
void BlinkingLeds::setOffTimeout(unsigned int ms)
{
  m_offTimeout = ms;
}

//------------------------------------------------------------------------------
void BlinkingLeds::addLed(Led led)
{
  if(std::find(
      m_blinkLeds.begin(), m_blinkLeds.end(), led) != m_blinkLeds.end())
  {
    LOGWA() << "Adding twice the same led to blink " << led;
    return;
  }

  m_blinkLeds.push_back(led);
}

//------------------------------------------------------------------------------
void BlinkingLeds::stop()
{
  m_blinkTimer->stop();
  m_blinkLeds.clear();
}

//------------------------------------------------------------------------------
void BlinkingLeds::onTimeout(const std::string& who)
{
  (void)who;

  if(!m_blinkTimer)
  {
    LOGER() << "BUG? No timer when start blinking!";
    assert(0);  // Should not happen
    return;
  }

  m_blinkStatus = !m_blinkStatus;
  turnLeds();
  if(m_blinkStatus)
    m_blinkTimer->setTimeout(m_onTimeout);
  else
    m_blinkTimer->setTimeout(m_offTimeout);
  m_blinkTimer->start();
}

//------------------------------------------------------------------------------
void BlinkingLeds::turnLeds()
{
  for(auto& led : m_blinkLeds)
    m_hardware.turnLed(m_blinkStatus, led);
}

}       // namespace
