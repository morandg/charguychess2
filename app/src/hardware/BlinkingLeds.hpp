  /**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_BLINKING_LEDS_HPP_
#define _CGC_BLINKING_LEDS_HPP_

#include <list>

#include <rglib/event-loop/IEventLoop.hpp>

#include "IChessHardware.hpp"
#include "IBlinkingLeds.hpp"

namespace charguychess {

class BlinkingLeds:
    public IBlinkingLeds,
    public rglib::ITimedOut
{
public:
  static const std::string BLINK_TIMER_NAME;

  BlinkingLeds(IChessHardware& hardware);
  virtual ~BlinkingLeds();

  int init(rglib::IEventLoop& el);

  // IBlinkingLeds
  virtual void start() override;
  virtual void setOnTimeout(unsigned int ms) override;
  virtual void setOffTimeout(unsigned int ms) override;
  virtual void addLed(Led led) override;
  virtual void stop() override;

  // rglib::ITimedOut
  virtual void onTimeout(const std::string& who) override;

private:
  IChessHardware& m_hardware;
  unsigned int m_onTimeout;
  unsigned int m_offTimeout;
  std::list<Led> m_blinkLeds;
  bool m_blinkStatus;

  void turnLeds();

protected:
  /* For spy */
  std::unique_ptr<rglib::ITimer> m_blinkTimer;
};

}       // namespace
#endif  // _CGC_BLINKING_LEDS_HPP_
