/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TelnetCommandEvent.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandEvent::TelnetCommandEvent(
    IChessGame& game, IChessHardware& hw, IChessEngine& engine):
  m_game(game),
  m_hw(hw),
  m_engine(engine)
{
}

//------------------------------------------------------------------------------
TelnetCommandEvent::~TelnetCommandEvent()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandEvent::command()
{
  return "event";
}

//------------------------------------------------------------------------------
std::string TelnetCommandEvent::description()
{
  return "Register or deregister to event";
}

//------------------------------------------------------------------------------
std::string TelnetCommandEvent::helpMessage()
{
  std::string message;

  message = command() + " <enable|disable> [event]:\n";
  message += "disable    Disable all events\n";
  message += "enable     Enable all events\n";
  message += "event      Enable/disable specific event:\n";
  message += "             * hw: Hardware events\n";
  message += "             * game: game events\n";
  message += "             * engine: chess engine events\n";

  return message;
}

//------------------------------------------------------------------------------
void TelnetCommandEvent::execute(ArgumentsList& args, ITelnetClient& client)
{
  bool isGameEvent = false;
  bool isHwEvent = false;
  bool isEngineEvent = false;
  bool isEnable = false;
  std::string crtArg;
  std::string message;


  // Enable/disable
  if(!args.getNext(crtArg))
  {
    client.sendMessage("Parameter required!\n");
    return;
  }
  if(crtArg == "enable")
    isEnable = true;
  else if(crtArg == "disable")
    isEnable = false;
  else
  {
    client.sendMessage("Invalid argument: " + crtArg + "\n");
    return;
  }

  // Event type
  if(args.getNext(crtArg))
  {
    if(crtArg == "hw")
      isHwEvent = true;
    else if(crtArg == "game")
      isGameEvent = true;
    else if(crtArg == "engine")
      isEngineEvent = true;
    else
    {
      client.sendMessage("Invalid event " + crtArg + "\n");
      return;
    }
  }
  else
  {
    isHwEvent = true;
    isGameEvent = true;
    isEngineEvent = true;
  }

  // Register/deregister observer
  if(isEnable)
  {
    if(isGameEvent)
      m_game.addObserver(client);
    if(isHwEvent)
      m_hw.addObserver(client);
    if(isEngineEvent)
      m_engine.addObserver(client);
  }
  else
  {
    if(isGameEvent)
      m_game.removeObserver(client);
    if(isHwEvent)
      m_hw.removeObserver(client);
    if(isEngineEvent)
      m_engine.removeObserver(client);
  }
}

}       // namespace
