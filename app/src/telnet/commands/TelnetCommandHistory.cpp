/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TelnetCommandHistory.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandHistory::TelnetCommandHistory(const IChessGame& chessGame):
    m_chessGame(chessGame)
{
}

//------------------------------------------------------------------------------
TelnetCommandHistory::~TelnetCommandHistory()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandHistory::command()
{
  return "history";
}

//------------------------------------------------------------------------------
std::string TelnetCommandHistory::description()
{
  return "Display game history";
}

//------------------------------------------------------------------------------
std::string TelnetCommandHistory::helpMessage()
{
  std::string desc;

  desc = description() + "\n";
  desc += "uci       Display history in UCI format\n";
  desc += "pgn       Display history in PGN format\n";

  return desc;
}

//------------------------------------------------------------------------------
void TelnetCommandHistory::execute(ArgumentsList& args, ITelnetClient& client)
{
  std::string how;

  args.getNext(how);

  if(how == "uci")
    client.sendMessage(m_chessGame.historyUci() + "\n");
  if(how == "pgn")
    client.sendMessage(m_chessGame.historyPGN() + "\n");
  else if(how == "")
    client.sendMessage(m_chessGame.history());
  else
    client.sendMessage("Invalid argument: " + how + "\n");
}

}       // namespace
