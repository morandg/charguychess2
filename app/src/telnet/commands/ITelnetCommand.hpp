/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_I_TELNET_COMMAND_HPP_
#define _CGC_I_TELNET_COMMAND_HPP_

#include <memory>
#include <unordered_map>

#include "../ITelnetClient.hpp"
#include "ArgumentsList.hpp"

namespace charguychess {

class ITelnetCommand
{
public:
  ITelnetCommand();
  virtual ~ITelnetCommand();

  virtual std::string command() = 0;
  virtual std::string description() = 0;
  virtual std::string helpMessage() = 0;
  virtual void execute(ArgumentsList& args, ITelnetClient& client) = 0;
};

typedef std::unordered_map<std::string, std::unique_ptr<ITelnetCommand>> CommandsList;

}       // namespace
#endif  // _CGC_I_TELNET_SERVER_HPP_
