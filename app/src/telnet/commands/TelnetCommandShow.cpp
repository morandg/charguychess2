/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TelnetCommandShow.hpp"

#include "../ChessObjectsSerializer.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandShow::TelnetCommandShow(IChessGame& chessgame, IChessHardware& hw):
    m_chessgame(chessgame),
    m_hw(hw)
{
}

//------------------------------------------------------------------------------
TelnetCommandShow::~TelnetCommandShow()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandShow::command()
{
  return "show";
}

//------------------------------------------------------------------------------
std::string TelnetCommandShow::description()
{
  return "Show board status";
}

//------------------------------------------------------------------------------
std::string TelnetCommandShow::helpMessage()
{
  std::string message;

  message = description() + " <what>\n";
  message += "game           Show current game status (default)\n";
  message += "hw             Show hardware status\n";
  message += "attack         Show opponent attack map\n";
  message += "move <square>  Show possible moves from <square>\n";

  return message;
}

//------------------------------------------------------------------------------
void TelnetCommandShow::execute(ArgumentsList& args, ITelnetClient& client)
{
  std::string arg;
  bool hasArg = args.getNext(arg);

  if(!hasArg ||
      arg == "game")
    client.sendMessage(ChessObjectsSerializer::toString(m_chessgame));
  else if(arg == "attack")
    client.sendMessage(
        ChessObjectsSerializer::toString(m_chessgame.opponentAttackMap()));
  else if(arg == "hw")
    client.sendMessage(
        ChessObjectsSerializer::toString(m_hw.getPieces()));
  else if(arg == "move")
    showMoves(args, client);
  else if(!hasArg)
    client.sendMessage("Please tell what to show\n");
  else
    client.sendMessage("Unkown argument: " + arg + "\n");
}

//------------------------------------------------------------------------------
void TelnetCommandShow::showMoves(ArgumentsList& args, ITelnetClient& client)
{
  std::string squareStr;
  Square from;

  if(!args.getNext(squareStr))
  {
    client.sendMessage("Please indicate a square!\n");
    return;
  }

  if(!from.fromString(squareStr))
  {
    client.sendMessage("Cannot parse square: " + squareStr + "\n");
    return;
  }

  client.sendMessage(
      ChessObjectsSerializer::toString(m_chessgame.moveMap(from)) + "\n");
}

}       // namespace
