/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TelnetCommandHelp.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandHelp::TelnetCommandHelp(const CommandsList& cmdList):
    m_cmdList(cmdList)
{
}

//------------------------------------------------------------------------------
TelnetCommandHelp::~TelnetCommandHelp()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandHelp::command()
{
  return "help";
}

//------------------------------------------------------------------------------
std::string TelnetCommandHelp::description()
{
  return "Show help about a command";
}

//------------------------------------------------------------------------------
std::string TelnetCommandHelp::helpMessage()
{
  std::string message = "List available commands\n";
  message += "For more information type \"help <cmd>\"\n";

  return message;
}

//------------------------------------------------------------------------------
void TelnetCommandHelp::execute(ArgumentsList& args, ITelnetClient& client)
{
  std::string cmd;

  if(!args.getNext(cmd) || cmd == "")
    listCommands(client);
  else
    helpCommand(cmd, client);
}

//------------------------------------------------------------------------------
void TelnetCommandHelp::listCommands(ITelnetClient& client)
{
  int ident = 10;
  std::string helpLine;

  for(const auto& cmd: m_cmdList)
  {
    int spaces = ident - cmd.second->command().length();

    helpLine = cmd.second->command();
    for(int i = 0 ; i < spaces ; ++i)
      helpLine += " ";
    helpLine += cmd.second->description() + "\n";

    client.sendMessage(helpLine);
  }

  helpLine = "\nFor more informationm, type \"help <cmd>\"\n";
  client.sendMessage(helpLine);
}

//------------------------------------------------------------------------------
void TelnetCommandHelp::helpCommand(
    const std::string& cmd, ITelnetClient& client)
{
  std::string helpMessage;

  const auto foundCmd = m_cmdList.find(cmd);

  if(foundCmd == m_cmdList.end())
    helpMessage = "Unkown command: " + cmd + "\n";
  else
    helpMessage = foundCmd->second->helpMessage();

  client.sendMessage(helpMessage);
}

}       // namespace
