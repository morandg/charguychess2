/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>

#include "TelnetCommandEngine.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandEngine::TelnetCommandEngine(
    IChessGame& game, IChessEngines& engines):
  m_game(game),
  m_engines(engines)
{
}

//------------------------------------------------------------------------------
TelnetCommandEngine::~TelnetCommandEngine()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandEngine::command()
{
  return "engine";
}

//------------------------------------------------------------------------------
std::string TelnetCommandEngine::description()
{
  return "Interact with the chess engine";
}

//------------------------------------------------------------------------------
std::string TelnetCommandEngine::helpMessage()
{
  std::string message;

  message = command() + ":\n";
  message += "info <white|black|hint>  Display engine info\n";
  message += "search depth <depth>     Search for hint for <depth> depth\n";
  message += "search time <time>       Search for hint for <time> ms\n";
  message += "search stop              Stop search for hint\n";

  return message;
}

//------------------------------------------------------------------------------
void TelnetCommandEngine::execute(ArgumentsList& args, ITelnetClient& client)
{
  std::string command;

  if(!args.getNext(command))
  {
    client.sendMessage("Parameter required\n");
    return;
  }

  if(command == "info")
    showInfo(args, client);
  else if(command == "search")
    search(args, client);
  else
    client.sendMessage("Unknown engine command " + command + "\n");
}

//------------------------------------------------------------------------------
void TelnetCommandEngine::showInfo(ArgumentsList& args, ITelnetClient& client)
{
  std::string which;

  if(!args.getNext(which))
    which = "hint";

  if(which == "white")
    showInfo(m_engines.getEngine(Color::WHITE), client);
  else if(which == "black")
    showInfo(m_engines.getEngine(Color::BLACK), client);
  else if(which == "hint")
    showInfo(m_engines.getHintEngine(), client);
  else
    client.sendMessage("Unkown engine " + which);
}

//------------------------------------------------------------------------------
void TelnetCommandEngine::showInfo(IChessEngine& engine, ITelnetClient& client)
{
  std::stringstream ss;

  ss << "Running: " << engine.isRunning() << std::endl;
  ss << "Ready:   " << engine.isReady() << std::endl;
  ss << "Name:    " << engine.getName() << std::endl;
  ss << "Author:  " << engine.getAuthor() << std::endl;

  client.sendMessage(ss.str());
}

//------------------------------------------------------------------------------
void TelnetCommandEngine::search(ArgumentsList& args, ITelnetClient& client)
{
  std::string mode;

  if(!args.getNext(mode))
  {
    client.sendMessage("Missing search info: [depth|time|stop]\n");
    return;
  }

  if(mode == "depth")
    searchDepth(args, client);
  else if(mode == "time")
    searchTime(args, client);
  else if(mode == "stop")
  {
    if(m_engines.getHintEngine().stopSearch())
      client.sendMessage("Engine communcation error\n");
  }
  else
    client.sendMessage("Unkown search mode " + mode + "\n");
}

//------------------------------------------------------------------------------
void TelnetCommandEngine::searchDepth(
    ArgumentsList& args, ITelnetClient& client)
{
  std::string depthStr;
  unsigned int depth;
  std::stringstream ss;

  if(!args.getNext(depthStr))
  {
    client.sendMessage("Missing depth number\n");
    return;
  }

  ss << depthStr;
  ss >> depth;

  if(depth == 0)
  {
    client.sendMessage("Invalid depth " + depthStr + "\n");
    return;
  }

  if(m_engines.getHintEngine().searchDepth(depth, m_game))
    client.sendMessage("Engine communcation error\n");
}

//------------------------------------------------------------------------------
void TelnetCommandEngine::searchTime(ArgumentsList& args, ITelnetClient& client)
{
  std::string timeStr;
  unsigned int time;
  std::stringstream ss;

  if(!args.getNext(timeStr))
  {
    client.sendMessage("Missing time duration\n");
    return;
  }

  ss << timeStr;
  ss >> time;

  if(m_engines.getHintEngine().searchTime(time, m_game))
    client.sendMessage("Engine communcation error\n");
}

}       // namespace
