/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TelnetCommandExit.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandExit::TelnetCommandExit()
{
}

//------------------------------------------------------------------------------
TelnetCommandExit::~TelnetCommandExit()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandExit::command()
{
  return "exit";
}

//------------------------------------------------------------------------------
std::string TelnetCommandExit::description()
{
  return "Close the connection";
}

//------------------------------------------------------------------------------
std::string TelnetCommandExit::helpMessage()
{
  return description() + "\n";
}

//------------------------------------------------------------------------------
void TelnetCommandExit::execute(ArgumentsList& args, ITelnetClient& client)
{
  (void)args;

  client.disconnect();
}

}       // namespace
