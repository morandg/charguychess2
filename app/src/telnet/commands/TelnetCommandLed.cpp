/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TelnetCommandLed.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetCommandLed::TelnetCommandLed(IChessHardware& hw):
    m_hw(hw)
{
}

//------------------------------------------------------------------------------
TelnetCommandLed::~TelnetCommandLed()
{
}

//------------------------------------------------------------------------------
std::string TelnetCommandLed::command()
{
  return "led";
}

//------------------------------------------------------------------------------
std::string TelnetCommandLed::description()
{
  return "Turn a led on or off";
}

//------------------------------------------------------------------------------
std::string TelnetCommandLed::helpMessage()
{
  std::string message;

  message = command() + ": <list|on|off> <led|all|square>\n";
  message += "list      List available LEDs\n";
  message += "on        Turn a led on\n";
  message += "off       Turn a led off\n";
  message += "led       LED to turn on/off\n";
  message += "all       Turn all LEDs on/off\n";
  message += "square    Square to turn on/off (e2)\n";

  return message;
}

//------------------------------------------------------------------------------
void TelnetCommandLed::execute(ArgumentsList& args, ITelnetClient& client)
{
  std::string cmd;

  if(!args.getNext(cmd))
  {
    client.sendMessage("Argument required!\n");
    return;
  }

  if(cmd == "list")
    listLeds(client);
  else if(cmd == "on" || cmd == "off")
  {
    std::string ledName;
    bool isOn = cmd == "on";

    if(!args.getNext(ledName))
    {
      client.sendMessage("LED name required\n");
      return;
    }

    if(ledName == "all")
      m_hw.turnAllLeds(isOn);
    else if(ledName.size() == 2)
      squareOnOff(isOn, ledName, client);
    else
      ledOnOff(isOn, ledName, client);
  }
  else
    client.sendMessage("Unkown argument: " + cmd + "\n");
}

//------------------------------------------------------------------------------
void TelnetCommandLed::listLeds(ITelnetClient& client)
{
  std::string message;

  message = "* A\n";
  message += "* B\n";
  message += "* C\n";
  message += "* D\n";
  message += "* E\n";
  message += "* F\n";
  message += "* G\n";
  message += "* H\n";
  message += "* 1\n";
  message += "* 2\n";
  message += "* 3\n";
  message += "* 4\n";
  message += "* 5\n";
  message += "* 6\n";
  message += "* 7\n";
  message += "* 8\n";
  message += "* rook\n";
  message += "* knight\n";
  message += "* bishop\n";
  message += "* queen\n";
  message += "* white\n";
  message += "* black\n";

  client.sendMessage(message);
}

//------------------------------------------------------------------------------
void TelnetCommandLed::ledOnOff(
    bool isOn, const std::string& led, ITelnetClient& client)
{
  Led ledType;

  if(led == "A")
    ledType = Led::A;
  else if(led == "B")
    ledType = Led::B;
  else if(led == "C")
    ledType = Led::C;
  else if(led == "D")
    ledType = Led::D;
  else if(led == "E")
    ledType = Led::E;
  else if(led == "F")
    ledType = Led::F;
  else if(led == "G")
    ledType = Led::G;
  else if(led == "H")
    ledType = Led::H;
  else if(led == "1")
    ledType = Led::ONE;
  else if(led == "2")
    ledType = Led::TWO;
  else if(led == "3")
    ledType = Led::THREE;
  else if(led == "4")
    ledType = Led::FOUR;
  else if(led == "5")
    ledType = Led::FIVE;
  else if(led == "6")
    ledType = Led::SIX;
  else if(led == "7")
    ledType = Led::SEVEN;
  else if(led == "8")
    ledType = Led::EIGHT;
  else if(led == "rook")
    ledType = Led::ROOK;
  else if(led == "knight")
    ledType = Led::KNIGHT;
  else if(led == "bishop")
    ledType = Led::BISHOP;
  else if(led == "queen")
    ledType = Led::QUEEN;
  else if(led == "white")
    ledType = Led::WHITE;
  else if(led == "black")
    ledType = Led::BLACK;
  else
  {
    client.sendMessage("Unkown led " + led + "\n");
    return;
  }

  m_hw.turnLed(isOn, ledType);
}

//------------------------------------------------------------------------------
void TelnetCommandLed::squareOnOff(
    bool isOn, const std::string& square, ITelnetClient& client)
{
  Square led;

  if(!led.fromString(square))
  {
    client.sendMessage("Invalid square " + square + "\n");
    return;
  }

  m_hw.turnSquareLed(isOn, led);
}

}       // namespace
