/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_TELNET_COMMAND_ENGINE_HPP_
#define _CGC_TELNET_COMMAND_ENGINE_HPP_

#include "../../chess/engine/IChessEngines.hpp"
#include "../../chess/IChessGame.hpp"
#include "ITelnetCommand.hpp"

namespace charguychess {

class TelnetCommandEngine:
    public ITelnetCommand
{
public:
  TelnetCommandEngine(IChessGame& game, IChessEngines& engines);
  virtual ~TelnetCommandEngine();

  // ITelnetCommand
  virtual std::string command() override;
  virtual std::string description() override;
  virtual std::string helpMessage() override;
  virtual void execute(ArgumentsList& args, ITelnetClient& client) override;

private:
  IChessGame& m_game;
  IChessEngines& m_engines;

  void showInfo(ArgumentsList& args, ITelnetClient& client);
  void showInfo(IChessEngine& engine, ITelnetClient& client);
  void search(ArgumentsList& args, ITelnetClient& client);
  void searchDepth(ArgumentsList& args, ITelnetClient& client);
  void searchTime(ArgumentsList& args, ITelnetClient& client);
};

}       // namespace
#endif  // _CGC_TELNET_COMMAND_ENGINE_HPP_
