/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>

#include <rglib/lexical/TokenizerString.hpp>
#include <rglib/lexical/WordTokenizer.hpp>

#include "TelnetCommandParser.hpp"

namespace charguychess {

static const std::string PROMPT = "CharGuyChess> ";

//------------------------------------------------------------------------------
TelnetCommandParser::TelnetCommandParser()
{
}

//------------------------------------------------------------------------------
TelnetCommandParser::~TelnetCommandParser()
{
}

//------------------------------------------------------------------------------
void TelnetCommandParser::addCommand(std::unique_ptr<ITelnetCommand>& command)
{
  m_commands.emplace(command->command(), std::move(command));
}

//------------------------------------------------------------------------------
void TelnetCommandParser::welcomeMessage(ITelnetClient& client)
{
  std::string message =
      "Welcome to CharGuyChess telnet server!\n"
      "Type \"help\" for more information\n\n";

  client.sendMessage(message);
  displayPrompt(client);
}

//------------------------------------------------------------------------------
void TelnetCommandParser::parseCommand(
    std::string& buffer, ITelnetClient& client)
{
  rglib::TokenizerString tokenizer;
  rglib::WordTokenizer lineSplitter(tokenizer);
  std::string line;

  // Cleanup stupid carriage return
  buffer.erase(std::remove(buffer.begin(), buffer.end(), '\r'), buffer.end());

  // Split lines
  tokenizer.setBuffer(buffer);
  while(lineSplitter.getNextWord('\n', line))
  {
    std::list<std::string> args;
    std::string command;

    // split args
    rglib::WordTokenizer::split(line, ' ', args);
    ArgumentsList argsList(args);

    if(!argsList.getNext(command))
      continue;
    if(command == "")
      continue;

    auto foundCmd = m_commands.find(command);
    if(foundCmd != m_commands.end())
      foundCmd->second->execute(argsList, client);
    else
      client.sendMessage("Unknown command: " + command + "\n");
  }

  // Strip the rest of the buffer
  buffer = line;

  displayPrompt(client);
}

//------------------------------------------------------------------------------
const CommandsList& TelnetCommandParser::getCommands()
{
  return m_commands;
}

//------------------------------------------------------------------------------
void TelnetCommandParser::displayPrompt(ITelnetClient& client)
{
  client.sendMessage(PROMPT);
}

}       // namespace
