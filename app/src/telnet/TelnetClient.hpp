/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_TELNET_CLIENT_HPP_
#define _CGC_TELNET_CLIENT_HPP_

#include <rglib/ipc/ISocketClient.hpp>
#include <rglib/event-loop/IHandledIo.hpp>

#include "../chess/IChessGame.hpp"
#include "commands/TelnetCommandParser.hpp"
#include "ITelnetServer.hpp"
#include "ITelnetClient.hpp"

namespace charguychess {

class TelnetClient:
    public rglib::IHandledIo,
    public ITelnetClient
{
public:
  TelnetClient(
      std::unique_ptr<rglib::ISocketClient>& socket,
      ITelnetServer& server,
      TelnetCommandParser& commandParser,
      const IChessGame& game);
  virtual ~TelnetClient();

  // rglib::IHandledIo
  virtual rglib::FileDescriptor getFileDescriptor() override;
  virtual void onReadReady() override;

  // ITelnetClient
  virtual void sendMessage(const std::string& message) override;
  virtual void disconnect() override;

  // IBoardObserver
  virtual void onMovePlayed(const Move& move) override;
  virtual void onNewGameStarted() override;

  // IChessHardwareObserver
  virtual void onBoardChanged(const BitBoard& boardStatus) override;
  virtual void onButtonPressed(Button button) override;
  virtual void onButtonReleased(Button button) override;

  // IChessEngineObserver
  virtual void onBestMoveFound(const Move& move) override;

private:
  std::unique_ptr<rglib::ISocketClient> m_socket;
  ITelnetServer& m_server;
  TelnetCommandParser& m_commandParser;
  const IChessGame& m_game;
  std::string m_buffer;
};

}       // namespace
#endif  // _CGC_TELNET_CLIENT_HPP_
