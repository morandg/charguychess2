/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include <rglib/logging/LogMacros.hpp>

#include "TelnetClient.hpp"

#include "ChessObjectsSerializer.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetClient::TelnetClient(
      std::unique_ptr<rglib::ISocketClient>& socket,
      ITelnetServer& server,
      TelnetCommandParser& commandParser,
      const IChessGame& game):
    m_socket(std::move(socket)),
    m_server(server),
    m_commandParser(commandParser),
    m_game(game),
    m_buffer("")
{
}

//------------------------------------------------------------------------------
TelnetClient::~TelnetClient()
{
}

//------------------------------------------------------------------------------
rglib::FileDescriptor TelnetClient::getFileDescriptor()
{
  return m_socket->getFileDescriptor();
}

//------------------------------------------------------------------------------
void TelnetClient::onReadReady()
{
  unsigned int buffSize = 32;
  char buffer[buffSize];
  int ret;

  LOGDB() << "Incoming telnet data on " << getFileDescriptor();

  ret = m_socket->read(buffer, buffSize);
  if(ret < 0)
  {
    LOGER() << "Cannot read from telnet client " << getFileDescriptor() <<
        ": " << strerror(errno);
    m_server.onClientDisconnecting(getFileDescriptor());
    return;
  }
  else if(ret == 0)
  {
    LOGDB() << "Telnet client " << getFileDescriptor() << " disconnected";
    m_server.onClientDisconnecting(getFileDescriptor());
    return;
  }

  m_buffer += std::string(buffer, ret);
  m_commandParser.parseCommand(m_buffer, *this);
}

//------------------------------------------------------------------------------
void TelnetClient::sendMessage(const std::string& message)
{
  m_socket->write(message.c_str(), message.length());
}

//------------------------------------------------------------------------------
void TelnetClient::disconnect()
{
  m_server.onClientDisconnecting(m_socket->getFileDescriptor());
  m_socket->close();
}

//------------------------------------------------------------------------------
void TelnetClient::onMovePlayed(const Move& move)
{
  sendMessage(ChessObjectsSerializer::toStringWithMove(m_game, move));
}

//------------------------------------------------------------------------------
void TelnetClient::onNewGameStarted()
{
  sendMessage("\nNew game started!\n" +
      ChessObjectsSerializer::toString(m_game));
}

//------------------------------------------------------------------------------
void TelnetClient::onBoardChanged(const BitBoard& boardStatus)
{
  std::stringstream ss;

  ss << std::endl << "Board changed:" << std::endl <<
      ChessObjectsSerializer::toString(boardStatus) << std::endl;

  sendMessage(ss.str());
}

//------------------------------------------------------------------------------
void TelnetClient::onButtonPressed(Button button)
{
  std::stringstream ss;

  ss << std::endl << "Button pressed " << button;
  sendMessage(ss.str());
}

//------------------------------------------------------------------------------
void TelnetClient::onButtonReleased(Button button)
{
  std::stringstream ss;

  ss << std::endl << "Button released " << button;
  sendMessage(ss.str());
}

//------------------------------------------------------------------------------
void TelnetClient::onBestMoveFound(const Move& move)
{
  sendMessage("\nFound best move: " + move.toString() + "\n");
}

}       // namespace
