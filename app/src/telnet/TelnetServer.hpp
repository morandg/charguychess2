/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_TELNET_SERVER_HPP_
#define _CGC_TELNET_SERVER_HPP_

#include <unordered_map>

#include <rglib/event-loop/IEventLoop.hpp>
#include <rglib/ipc/ISocketServer.hpp>

#include "../hardware/IChessHardware.hpp"
#include "../chess/engine/IChessEngines.hpp"
#include "../settings/TelnetSettings.hpp"
#include "TelnetClient.hpp"

namespace charguychess {

class TelnetServer:
    public ITelnetServer,
    public rglib::IHandledIo
{
public:
  TelnetServer(
      rglib::IEventLoop& el,
      IChessGame& game,
      IChessHardware& hardware,
      IChessEngines& engines);
  virtual ~TelnetServer();

  int init(const TelnetSettings& settings);

  // ITelnetServer
  virtual void onClientDisconnecting(rglib::FileDescriptor id) override;

  // rglib::IHandledIo
  virtual rglib::FileDescriptor getFileDescriptor() override;
  virtual void onReadReady() override;

private:
  rglib::IEventLoop& m_el;
  IChessGame& m_game;
  IChessHardware& m_hardware;
  IChessEngines& m_engines;
  std::unique_ptr<rglib::ISocketServer> m_socket;
  TelnetCommandParser m_parser;
  std::unordered_map<rglib::FileDescriptor, std::unique_ptr<TelnetClient>> m_clients;
  std::unique_ptr<TelnetClient> m_disconnectedClient;

  void addTelnetCommands();
};

}       // namespace
#endif  // _CGC_TELNET_SERVER_HPP_
