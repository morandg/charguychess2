/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cerrno>
#include <cstring>

#include <rglib/logging/LogMacros.hpp>

#include "commands/TelnetCommandHelp.hpp"
#include "commands/TelnetCommandExit.hpp"
#include "commands/TelnetCommandShow.hpp"
#include "commands/TelnetCommandEvent.hpp"
#include "commands/TelnetCommandLed.hpp"
#include "commands/TelnetCommandHistory.hpp"
#include "commands/TelnetCommandEngine.hpp"
#include "TelnetServer.hpp"

namespace charguychess {

//------------------------------------------------------------------------------
TelnetServer::TelnetServer(
      rglib::IEventLoop& el,
      IChessGame& game,
      IChessHardware& hardware,
      IChessEngines& engines):
    m_el(el),
    m_game(game),
    m_hardware(hardware),
    m_engines(engines)
{
}

//------------------------------------------------------------------------------
TelnetServer::~TelnetServer()
{
}

//------------------------------------------------------------------------------
int TelnetServer::init(const TelnetSettings& settings)
{
  int ret;

  if(!settings.isEnabled())
  {
    LOGDB() << "Telnet server disabled";
    return 0;
  }

  rglib::ISocketServer::create(m_socket);
  if(!m_socket)
  {
    LOGER() << "Could not create telnet socket server";
    return -1;
  }

  ret = m_socket->bindIpV4(
      rglib::SocketType::STREAM,
      settings.getBindIp(),
      settings.getPort(),
      5);
  if(ret)
  {
    LOGER() << "Cannot bind telnet socket: " << strerror(-ret);
    return ret;
  }

  ret = m_el.addHandledIo(
      *this, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST);
  if(ret)
  {
    LOGER() << "Cannot register telnet server to event loop";
    return ret;
  }

  addTelnetCommands();

  LOGIN() << "Telnet server started on " <<
      settings.getBindIp() << ":" << settings.getPort();

  return 0;
}

//------------------------------------------------------------------------------
void TelnetServer::onClientDisconnecting(rglib::FileDescriptor id)
{
  auto foundClient = m_clients.find(id);

  // Delete previously disconnected client
  m_disconnectedClient.reset(nullptr);

  if(foundClient == m_clients.end())
  {
    LOGER() << "BUG? Telnet client not found when disconnecting";
    return;
  }

  LOGDB() << "Disconnecting client " << id;

  // Store the pointer to not delete it right now, we might still want to access
  // this client from somewhere else (the client itself, command parser, ...)
  m_disconnectedClient = std::move(foundClient->second);

  m_el.removeHandledIo(*m_disconnectedClient);
  m_game.removeObserver(*m_disconnectedClient);
  m_hardware.removeObserver(*m_disconnectedClient);
  m_engines.getHintEngine().removeObserver(*m_disconnectedClient);

  m_clients.erase(foundClient);
}

//------------------------------------------------------------------------------
rglib::FileDescriptor TelnetServer::getFileDescriptor()
{
  return m_socket->getFileDescriptor();
}

//------------------------------------------------------------------------------
void TelnetServer::onReadReady()
{
  std::unique_ptr<TelnetClient> telnetClient;
  std::unique_ptr<rglib::ISocketClient> socketClient;
  int ret;

  LOGDB() << "Incoming telnet client connection";

  ret = m_socket->accept(socketClient);
  if(ret)
  {
    LOGER() << "Cannot accept new telnet client: " << strerror(-ret);
    return;
  }

  telnetClient.reset(new TelnetClient(socketClient, *this, m_parser, m_game));
  if(m_el.addHandledIo(
      *telnetClient, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST))
  {
    LOGER() << "Cannot register telnet client to event loop";
    return;
  }

  m_game.addObserver(*telnetClient);
  m_hardware.addObserver(*telnetClient);
  m_engines.getHintEngine().addObserver(*telnetClient);
  m_parser.welcomeMessage(*telnetClient);
  m_clients.emplace(
      telnetClient->getFileDescriptor(), std::move(telnetClient));
}

//------------------------------------------------------------------------------
void TelnetServer::addTelnetCommands()
{
  std::unique_ptr<ITelnetCommand> cmd;

  cmd.reset(new TelnetCommandHelp(m_parser.getCommands()));
  m_parser.addCommand(cmd);

  cmd.reset(new TelnetCommandExit());
  m_parser.addCommand(cmd);

  cmd.reset(new TelnetCommandShow(m_game, m_hardware));
  m_parser.addCommand(cmd);

  cmd.reset(
      new TelnetCommandEvent(m_game, m_hardware, m_engines.getHintEngine()));
  m_parser.addCommand(cmd);

  cmd.reset(new TelnetCommandLed(m_hardware));
  m_parser.addCommand(cmd);

  cmd.reset(new TelnetCommandHistory(m_game));
  m_parser.addCommand(cmd);

  cmd.reset(new TelnetCommandEngine(m_game, m_engines));
  m_parser.addCommand(cmd);
}

}       // namespace
