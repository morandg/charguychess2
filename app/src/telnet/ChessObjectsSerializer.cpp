/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ChessObjectsSerializer.hpp"

#include <sstream>


namespace charguychess {

static const std::string ROW_SEPARATOR  = "+---+---+---+---+---+---+---+---+";
static const std::string FILE_LABEL     = "A   B   C   D   E   F   G   H";

//------------------------------------------------------------------------------
std::string ChessObjectsSerializer::toString(const IChessGame& game)
{
  return toStringWithMove(game, Move());
}

//------------------------------------------------------------------------------
std::string ChessObjectsSerializer::toStringWithMove(
    const IChessGame& game, const Move& move)
{
  std::stringstream boardStream;

  boardStream << " " << ROW_SEPARATOR << std::endl;

  for(int rank = (int)Rank::EIGHT ; rank >= (int)Rank::ONE ; --rank)
  {
    boardStream << ((int)rank + 1) << "|";

    for(int file = (int)File::A ; file <= (int)File::H ; ++file)
    {
      Square square((File)file, (Rank)rank);
      PlayerPiece foundPiece;
      bool isMovedSquare = (square == move.from() || square == move.to());

      if(isMovedSquare)
        boardStream << "*";
      else
        boardStream << " ";

      if(game.pieceAt(square, foundPiece))
        boardStream << foundPiece.toChar();
      else
        boardStream << " ";

      if(isMovedSquare)
        boardStream << "*";
      else
        boardStream << " ";

      boardStream << "|";
    }

    boardStream << std::endl;
    boardStream << " " << ROW_SEPARATOR << std::endl;
  }

  boardStream << "   " << FILE_LABEL << std::endl;

  if(move.isValid())
    boardStream << " => " << move.toPGN() << std::endl;

  return boardStream.str();
}

//------------------------------------------------------------------------------
std::string ChessObjectsSerializer::toString(const BitBoard& bitBoard)
{
  std::stringstream boardStream;

  boardStream << " " << ROW_SEPARATOR << std::endl;

  for(int rank = (int)Rank::EIGHT ; rank >= (int)Rank::ONE ; --rank)
  {
    boardStream << ((int)rank + 1) << "|";

    for(int file = (int)File::A ; file <= (int)File::H ; ++file)
    {
      Square square((File)file, (Rank)rank);

      if(bitBoard.isSet(square))
        boardStream << " *";
      else
        boardStream << "  ";

      boardStream << " |";
    }

    boardStream << std::endl;
    boardStream << " " << ROW_SEPARATOR << std::endl;
  }

  boardStream << "   " << FILE_LABEL << std::endl;

  return boardStream.str();
}

}       // namespace
