/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CGC_CHAR_GUY_CHESS_HPP_
#define _CGC_CHAR_GUY_CHESS_HPP_

#include <rglib/event-loop/IEventLoop.hpp>

#include "chess/ChessGame.hpp"
#include "chess/engine/IChessEngines.hpp"
#include "chess/PgnSerializer.hpp"
#include "hardware/IChessHardware.hpp"
#include "hardware/BlinkingLeds.hpp"
#include "hardware/states/IChessStatesPool.hpp"
#include "telnet/TelnetServer.hpp"
#include "QuitHandler.hpp"
#include "settings/Settings.hpp"

namespace charguychess {

class CharGuyChess
{
public:
  CharGuyChess();
  ~CharGuyChess();

  int init(const std::string& configFile);
  int reinit();
  int run();

private:
  Settings m_settings;
  ChessGame m_chessGame;
  std::unique_ptr<rglib::IEventLoop> m_el;
  std::unique_ptr<QuitHandler> m_quitHandler;
  std::unique_ptr<IChessHardware> m_hardware;
  std::unique_ptr<BlinkingLeds> m_blinkingLeds;
  std::unique_ptr<IChessEngines> m_chessEngines;
  std::unique_ptr<IChessStatesPool> m_statesPool;
  std::unique_ptr<TelnetServer> m_telnetServer;
  std::unique_ptr<PgnSerializer> m_pgnSerializer;

  int initEventLoop();
  int initQuitHandler();
  int initHardware();
  int initTelnetServer();
  int initChessEngines();
  int initStatesPool();
  int initPgnSerializer();
};

}       // namespace
#endif  // _CGC_CHAR_GUY_CHESS_HPP_
