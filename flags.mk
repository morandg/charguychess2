COMMON_FLAGS       += -Wall -Werror -Wextra -MMD

# Debug/Release mode
ifneq ($(DEBUG),)
  COMMON_FLAGS      += -g
  BUILD_DIR         := $(BUILD_DIR)/debug
else
  COMMON_FLAGS      += -DNDEBUG -O3
  BUILD_DIR         := $(BUILD_DIR)/release
endif

CFLAGS              += $(COMMON_FLAGS)
CXXFLAGS            += $(COMMON_FLAGS) -std=c++14
