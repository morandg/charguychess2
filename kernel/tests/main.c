/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#include <cgc-protocol/serializer.h>

#define DEVICE "/dev/charguychess"

//------------------------------------------------------------------------------
int read_register(int fd, uint8_t reg)
{
  char buffer[CGC_READ_REQUEST_SIZE];
  unsigned int serializedSize;

  printf("Reading register 0x%x\n", reg);

  if(cgc_serializer_read_request(
      buffer, CGC_READ_REQUEST_SIZE, reg, &serializedSize))
  {
    fprintf(stderr, "Error serializing read request\n");
    return -1;
  }

  if(write(fd, buffer, serializedSize) < 0)
  {
    fprintf(stderr, "Cannot write to device: %s\n", strerror(errno));
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int write_register(int fd, uint8_t reg, uint8_t value)
{
  char buffer[CGC_WRITE_REQUEST_SIZE];
  unsigned int serializedSize;

  printf("Writing register 0x%x with 0x%x\n", reg, value);

  if(cgc_serializer_write_request(
      buffer, CGC_WRITE_REQUEST_SIZE, reg, value, &serializedSize))
  {
    fprintf(stderr, "Error serializing write request\n");
    return -1;
  }

  if(write(fd, buffer, serializedSize) < 0)
  {
    fprintf(stderr, "Cannot write to device: %s\n", strerror(errno));
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
void empty_device(int fd)
{
  unsigned int buffer_size = 128;
  char buffer[buffer_size];
  int read_size;

  while(1)
  {
    read_size = read(fd, buffer, buffer_size - 1);

    if(read_size > 0)
    {
      buffer[read_size] = 0;
      printf("Got data: %s\n", buffer);
    }
    else
    {
      if(read_size != 0 && errno != EAGAIN)
        fprintf(
            stderr, "Error reading charguychess device: %s\n", strerror(errno));
      break;
    }
  }
}

//------------------------------------------------------------------------------
int doTest(int fd)
{
  if(read_register(fd, 0xff))
    return -1;
  empty_device(fd);

  if(write_register(fd, 0x08, rand()))
    return -1;
  empty_device(fd);

  if(read_register(fd, 0x08))
    return -1;
  empty_device(fd);

  return 0;
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  int ret;
  int fd;

  srand(time(NULL));

  fd = open(DEVICE, O_NONBLOCK | O_RDWR);
  if(fd < 0)
  {
    fprintf(stderr, "Cannot open %s: %s\n", DEVICE, strerror(errno));
    return -1;    
  }

  ret = doTest(fd);

  return ret;
}
