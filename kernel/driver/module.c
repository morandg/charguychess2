/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/kfifo.h>
#include <linux/list.h>
#include <linux/poll.h>
#include <linux/i2c.h>
#include <linux/regmap.h>
#include <linux/kthread.h>
#include <linux/delay.h>

#include <cgc-protocol/registers_defs.h>
#include <cgc-protocol/registers.h>
#include <cgc-protocol/parser.h>
#include <cgc-protocol/serializer.h>

#define CHARGUYCHESS_CLASS      "charguychess"
#define CHARGUYCHESS_CHAR_DEV   "charguychess"
#define OUT_FRAMES_FIFO_SIZE    256

#define I2C_ID_RANK_12          0
#define I2C_ID_RANK_34          1
#define I2C_ID_RANK_56          2
#define I2C_ID_RANK_78          3
#define I2C_ID_RF_LEDS          4
#define I2C_POLL_INTERVAL       100

#define MCP23017_REG_IODIRA     0x00
#define MCP23017_REG_IODIRB     0x01
#define MCP23017_REG_GPIOA      0x12
#define MCP23017_REG_GPIOB      0x13
#define MCP23017_REG_OLATA      0x14
#define MCP23017_REG_OLATB      0x15

typedef struct _CharGuyChessUserData {
  CGC_Parser* parser;
  struct mutex out_frames_lock;
  struct kfifo out_frames;
  wait_queue_head_t wait_data_queue;
  int has_data;
  struct list_head list;
} CharGuyChessUserData;

typedef struct _CharguyChessDriver {
  int was_probed;
  struct class* class;
  int char_major;
  struct device* chardev;
  struct mutex registers_lock;
  struct mutex users_list_lock;
  CharGuyChessUserData users_list;
  int has_i2c;
  struct regmap* rank_12;
  struct regmap* rank_34;
  struct regmap* rank_56;
  struct regmap* rank_78;
  struct regmap* rf_leds;
  struct task_struct* i2c_poll_thread;
} CharguyChessDriver;

static CharguyChessDriver cgc_driver;

//------------------------------------------------------------------------------
static uint8_t invert_bits(uint8_t value)
{
  uint8_t inverted = 0;

  // File bits are inverted on hardware (H = 1 ... A = 8), invert the bits to be
  // compliant with the cgc protoco (H = 8 ... A = 1)
  inverted |= (value & 0x01) << 7;
  inverted |= (value & 0x02) << 5;
  inverted |= (value & 0x04) << 3;
  inverted |= (value & 0x08) << 1;
  inverted |= (value & 0x10) >> 1;
  inverted |= (value & 0x20) >> 3;
  inverted |= (value & 0x40) >> 5;
  inverted |= (value & 0x80) >> 7;

  return inverted;
}

//------------------------------------------------------------------------------
static int cgc_register_to_regmap(
    uint8_t cgc_register, struct regmap** regs)
{

  switch(cgc_register)
  {
  case CGC_REGISTER_RANK2:
  case CGC_REGISTER_RANK1:
    *regs = cgc_driver.rank_12;
    break;
  case CGC_REGISTER_RANK3:
  case CGC_REGISTER_RANK4:
    *regs = cgc_driver.rank_34;
    break;
  case CGC_REGISTER_RANK5:
  case CGC_REGISTER_RANK6:
    *regs = cgc_driver.rank_56;
    break;
  case CGC_REGISTER_RANK7:
  case CGC_REGISTER_RANK8:
    *regs = cgc_driver.rank_78;
    break;
  case CGC_REGISTER_RANK_LEDS:
  case CGC_REGISTER_FILE_LEDS:
    *regs = cgc_driver.rf_leds;
    break;
  default:
//    dev_warn(
//        cgc_driver.chardev,
//        "Register 0x%x to device not supported on i2c\n",
//        cgc_register);
    return -EINVAL;
  }

  return 0;
}

//------------------------------------------------------------------------------
static int cgc_register_to_mcp_gpio(uint8_t cgc_register, uint8_t* gpio_reg)
{
  switch(cgc_register)
  {
  case CGC_REGISTER_RANK1:
  case CGC_REGISTER_RANK3:
  case CGC_REGISTER_RANK5:
  case CGC_REGISTER_RANK7:
  case CGC_REGISTER_RANK_LEDS:
    *gpio_reg = MCP23017_REG_GPIOA;
    break;
  case CGC_REGISTER_RANK2:
  case CGC_REGISTER_RANK4:
  case CGC_REGISTER_RANK6:
  case CGC_REGISTER_RANK8:
  case CGC_REGISTER_FILE_LEDS:
    *gpio_reg = MCP23017_REG_GPIOB;
    break;
  default:
//    dev_warn(
//        cgc_driver.chardev,
//        "Register 0x%x to GPIO not supported on i2c\n",
//        cgc_register);
    return -EINVAL;
  }

  return 0;
}

//------------------------------------------------------------------------------
static int cgc_register_to_mcp_olat(uint8_t cgc_register, uint8_t* olat_reg)
{
  switch(cgc_register)
  {
  case CGC_REGISTER_RANK1:
  case CGC_REGISTER_RANK3:
  case CGC_REGISTER_RANK5:
  case CGC_REGISTER_RANK7:
  case CGC_REGISTER_RANK_LEDS:
    *olat_reg = MCP23017_REG_OLATA;
    break;
  case CGC_REGISTER_RANK2:
  case CGC_REGISTER_RANK4:
  case CGC_REGISTER_RANK6:
  case CGC_REGISTER_RANK8:
  case CGC_REGISTER_FILE_LEDS:
    *olat_reg = MCP23017_REG_OLATB;
    break;
  default:
//    dev_warn(
//        cgc_driver.chardev,
//        "Register 0x%x to OLAT not supported on i2c\n",
//        cgc_register);
    return -EINVAL;
  }

  return 0;
}

//------------------------------------------------------------------------------
static int read_mcp_gpio(uint8_t cgc_register, uint8_t* value)
{
  struct regmap* regs;
  uint8_t reg;
  int read_value;
  int ret;

  if(cgc_register_to_regmap(cgc_register, &regs) ||
      cgc_register_to_mcp_gpio(cgc_register, &reg))
    return -EINVAL;

  ret = regmap_read(regs, reg, &read_value);
  if(ret < 0)
  {
    dev_err(cgc_driver.chardev, "Cannot read register 0x%x: %i\n", reg, ret);
    return ret;
  }

  if(cgc_register == CGC_REGISTER_RANK_LEDS ||
      cgc_register == CGC_REGISTER_FILE_LEDS)
    dev_info(cgc_driver.chardev, "Read register 0x%x: %i\n", reg, read_value);

  *value = invert_bits(read_value);

  return 0;
}

//------------------------------------------------------------------------------
static int write_mcp_gpio(uint8_t cgc_register, uint8_t value)
{
  struct regmap* regs;
  uint8_t reg;
  int ret;

  if(cgc_register_to_regmap(cgc_register, &regs) ||
      cgc_register_to_mcp_olat(cgc_register, &reg))
    return -EINVAL;

  if(cgc_register == CGC_REGISTER_RANK_LEDS ||
      cgc_register == CGC_REGISTER_FILE_LEDS)
    dev_info(cgc_driver.chardev, "Write register 0x%x: %i\n", reg, value);

  ret = regmap_write(regs, reg, invert_bits(value));
  if(ret < 0)
  {
    dev_err(cgc_driver.chardev, "Cannot write register 0x%x: %i\n", reg, ret);
    return ret;
  }

  return 0;
}

//------------------------------------------------------------------------------
static int write_read_mcp_gpio(
    uint8_t cgc_register, uint8_t value, uint8_t* read_value)
{
  int ret;

  ret = write_mcp_gpio(cgc_register, value);
  if(ret)
    return ret;

  ret = read_mcp_gpio(cgc_register, read_value);
  if(ret)
    return ret;

  return 0;
}

//------------------------------------------------------------------------------
static void send_data_to_user(
    CharGuyChessUserData* user, char* data, unsigned int size)
{
  mutex_lock(&user->out_frames_lock);

  if(kfifo_avail(&user->out_frames) < size) {
    mutex_unlock(&user->out_frames_lock);
    dev_warn(cgc_driver.chardev, "Not enough space in kfifo\n");
    return;
  }

  kfifo_in(&user->out_frames, data, size);
  user->has_data = 1;
  wake_up_interruptible(&user->wait_data_queue);

  mutex_unlock(&user->out_frames_lock);
}

//------------------------------------------------------------------------------
static void send_reg_info_to_user(
    CharGuyChessUserData* user, uint8_t reg, uint8_t value)
{
  char buffer[CGC_WRITE_REQUEST_SIZE];
  unsigned int serializedSize;

  if(cgc_serializer_write_request(
      buffer, CGC_WRITE_REQUEST_SIZE, reg, value, &serializedSize))
  {
    dev_err(cgc_driver.chardev,
        "Error serialized user request 0x%x:0x%x\n", reg, value);
    return;
  }

  send_data_to_user(user, buffer, serializedSize);
}

//------------------------------------------------------------------------------
static void dispatch_register_change(uint8_t reg, uint8_t value)
{
  CharGuyChessUserData* cgc_user;
  char buffer[CGC_WRITE_REQUEST_SIZE];
  unsigned int serializedSize;

  if(cgc_serializer_write_request(
      buffer, CGC_WRITE_REQUEST_SIZE, reg, value, &serializedSize))
  {
    dev_err(cgc_driver.chardev,
        "Error serialized user request 0x%x:0x%x\n", reg, value);
    return;
  }

  mutex_lock(&cgc_driver.users_list_lock);
  list_for_each_entry(cgc_user, &cgc_driver.users_list.list, list)
    send_data_to_user(cgc_user, buffer, serializedSize);
  mutex_unlock(&cgc_driver.users_list_lock);
}

//------------------------------------------------------------------------------
static void sync_i2c_registers(void)
{
  uint8_t reg;
  uint8_t i2c_value;
  uint8_t current_value;

  mutex_lock(&cgc_driver.registers_lock);
  for(reg = CGC_REGISTER_FIRST ; reg <= CGC_REGISTER_LAST ; ++reg)
  {
    if(cgc_registers_read(reg, &current_value))
    {
      dev_warn(cgc_driver.chardev, "Cannot read internal register 0x%x\n", reg);
      continue;
    }

    if(read_mcp_gpio(reg, &i2c_value))
      continue;

    if(i2c_value != current_value)
    {
      if(cgc_registers_write(reg, i2c_value))
        dev_warn(
            cgc_driver.chardev, "Cannot update internal register 0x%x\n", reg);
      dispatch_register_change(reg, i2c_value);
    }
  }
  mutex_unlock(&cgc_driver.registers_lock);
}

//------------------------------------------------------------------------------
static int poll_thread_func(void* data)
{
  do
  {
    sync_i2c_registers();
    msleep(I2C_POLL_INTERVAL);
  } while(!kthread_should_stop());

  return 0;
}

//------------------------------------------------------------------------------
static void parser_read_cb(void* data, uint8_t reg)
{
  uint8_t value;
  int ret;
  CharGuyChessUserData* cgc_user = (CharGuyChessUserData*)data;

  mutex_lock(&cgc_driver.registers_lock);
  ret = cgc_registers_read(reg, &value);
  mutex_unlock(&cgc_driver.registers_lock);

  if(ret)
  {
    dev_err(cgc_driver.chardev, "Error reading register 0x%x\n", reg);
    return;
  }

  // Ack on char device
  send_reg_info_to_user(cgc_user, reg, value);
}

//------------------------------------------------------------------------------
static void parser_write_cb(void* data, uint8_t reg, uint8_t value)
{
  uint8_t old_value;
  uint8_t i2c_value;
  int ret;

  if(cgc_driver.has_i2c)
  {
    if(!write_read_mcp_gpio(reg, value, &i2c_value))
      value = i2c_value;
  }

  mutex_lock(&cgc_driver.registers_lock);
  ret = cgc_registers_read(reg, &old_value);
  if(!ret)
    ret = cgc_registers_write(reg, value);
  mutex_unlock(&cgc_driver.registers_lock);

  if(ret)
  {
    dev_err(cgc_driver.chardev, "Error reading writing 0x%x\n", reg);
    return;
  }

  if(old_value == value)
    send_reg_info_to_user((CharGuyChessUserData*)data, reg, value);
  else
    dispatch_register_change(reg, value);
}

//------------------------------------------------------------------------------
static int char_dev_open(struct inode* i, struct file* f)
{
  CharGuyChessUserData* user_data;

  user_data = kmalloc(sizeof(CharGuyChessUserData), GFP_KERNEL);
  if(user_data == NULL)
    return -ENOMEM;

  user_data->parser = cgc_parser_create(
     parser_read_cb,
     parser_write_cb,
     user_data);
  if(user_data->parser == NULL)
  {
    kfree(user_data);
    return -ENOMEM;
  }

  if(kfifo_alloc(
      &user_data->out_frames, OUT_FRAMES_FIFO_SIZE, GFP_KERNEL))
  {
    cgc_parser_free(user_data->parser);
    kfree(user_data);
    return -ENOMEM;
  }

  mutex_init(&user_data->out_frames_lock); 
  kfifo_reset(&user_data->out_frames);
  init_waitqueue_head(&user_data->wait_data_queue);
  user_data->has_data = 0;

  mutex_lock(&cgc_driver.users_list_lock);
  list_add_tail(&user_data->list, &cgc_driver.users_list.list);
  mutex_unlock(&cgc_driver.users_list_lock);

  f->private_data = user_data;

  return 0;
}

//------------------------------------------------------------------------------
static ssize_t char_dev_read(
    struct file* f, char* __user b, size_t s, loff_t* o)
{
  int ret_val;
  int data_count;
  unsigned int data_read_count;
  unsigned int bytes_sent;
  CharGuyChessUserData* user_data;

  user_data = (CharGuyChessUserData*)f->private_data;

  if(mutex_lock_interruptible(&user_data->out_frames_lock))
    return -ERESTARTSYS;

  data_count = kfifo_len(&user_data->out_frames);

  while(!data_count)
  {
    mutex_unlock(&user_data->out_frames_lock);

    if(f->f_flags & O_NONBLOCK)
      return -EAGAIN;

    if(wait_event_interruptible(
        user_data->wait_data_queue,
        user_data->has_data != 0))
      return -ERESTARTSYS;

    if(mutex_lock_interruptible(&user_data->out_frames_lock))
      return -ERESTARTSYS;

    data_count = kfifo_len(&user_data->out_frames);
  }

  if(data_count > s)
    data_read_count = s;
  else
    data_read_count = data_count;
  user_data->has_data = 0;

  ret_val = kfifo_to_user(
      &user_data->out_frames,
      b,
      data_read_count,
      &bytes_sent);

  mutex_unlock(&user_data->out_frames_lock);

  if(ret_val)
    return ret_val;

  return bytes_sent;
}

//------------------------------------------------------------------------------
static unsigned int char_dev_poll(struct file *f, poll_table *wait)
{
  unsigned int mask = 0;
  unsigned int data_available;
  CharGuyChessUserData* user_data;

  user_data = (CharGuyChessUserData*)f->private_data;

  poll_wait(f, &user_data->wait_data_queue, wait);

  if(mutex_lock_interruptible(&user_data->out_frames_lock))
    return -ERESTARTSYS;

  data_available = kfifo_len(&user_data->out_frames);

  mutex_unlock(&user_data->out_frames_lock);

  if(data_available)
    mask |= POLLIN | POLLRDNORM;

  return mask;
}

//------------------------------------------------------------------------------
static ssize_t char_dev_write(
  struct file* f,
  const char* __user buff,
  size_t s,
  loff_t * o)
{
  unsigned int buffer_size = 256;
  char buffer[buffer_size];
  unsigned long parsed_bytes;
  CharGuyChessUserData* user_data;

  user_data = (CharGuyChessUserData*)f->private_data;
  parsed_bytes = s - copy_from_user(buffer, buff, buffer_size);
  cgc_parser_parse_buffer(user_data->parser, buffer, parsed_bytes);

  return parsed_bytes;
}

//------------------------------------------------------------------------------
static int char_dev_release(struct inode* i, struct file* f)
{
  CharGuyChessUserData* user_data = (CharGuyChessUserData*)f->private_data;

  mutex_lock(&cgc_driver.users_list_lock);
  list_del(&user_data->list);
  mutex_unlock(&cgc_driver.users_list_lock);

  cgc_parser_free(user_data->parser);
  kfifo_free(&user_data->out_frames);
  kfree(user_data);

  return 0;
}

//------------------------------------------------------------------------------
static const struct file_operations charguychess_fops = {
  .owner   = THIS_MODULE,
  .open    = char_dev_open,
  .read    = char_dev_read,
  .write   = char_dev_write,
  .poll    = char_dev_poll,
  .release = char_dev_release,
};

//------------------------------------------------------------------------------
static const struct regmap_config mcp23817_regmap = {
  .reg_bits = 8,
  .val_bits = 8,
  .reg_stride = 1,
  .cache_type = REGCACHE_NONE,
};

//------------------------------------------------------------------------------
static int charguychess_i2c_probe(
    struct i2c_client *client, const struct i2c_device_id *id)
{
  struct regmap* regs;
  uint8_t iodir_a;
  uint8_t iodir_b;
  int ret;

  switch(id->driver_data)
  {
  case I2C_ID_RANK_12:
  case I2C_ID_RANK_34:
  case I2C_ID_RANK_56:
  case I2C_ID_RANK_78:
    iodir_a = 0xff;
    iodir_b = 0xff;
    break;
  case I2C_ID_RF_LEDS:
    iodir_a = 0x00;
    iodir_b = 0x00;
    break;
  default:
    dev_err(&client->dev, "Unkown i2c device ID %lu\n", id->driver_data);
    return -EINVAL;
  }

  regs = devm_regmap_init_i2c(client, &mcp23817_regmap);

  ret = regmap_write(regs, MCP23017_REG_IODIRA, iodir_a);
  if(ret)
  {
    dev_err(&client->dev, "Cannot write IODIRA: %i\n", ret);
    return ret;
  }
  ret = regmap_write(regs, MCP23017_REG_IODIRB, iodir_b);
  if(ret)
  {
    dev_err(&client->dev, "Cannot write IODIRB: %i\n", ret);
    return ret;
  }

  switch(id->driver_data)
  {
  case I2C_ID_RANK_12:
    cgc_driver.rank_12 = regs;
    break;
  case I2C_ID_RANK_34:
    cgc_driver.rank_34 = regs;
    break;
  case I2C_ID_RANK_56:
    cgc_driver.rank_56 = regs;
    break;
  case I2C_ID_RANK_78:
    cgc_driver.rank_78 = regs;
    break;
  case I2C_ID_RF_LEDS:
    cgc_driver.rf_leds= regs;
    break;
  default:
    dev_err(&client->dev, "Unkown i2c device ID %lu\n", id->driver_data);
    return -EINVAL;
  }

  dev_info(&client->dev, "IC2  %lu device initialized\n", id->driver_data);


  return 0;
}

//------------------------------------------------------------------------------
static const struct i2c_device_id cgc_i2c_rank12_id[] = {
    { "rank_12", I2C_ID_RANK_12 },
    { "rank_34", I2C_ID_RANK_34 },
    { "rank_56", I2C_ID_RANK_56 },
    { "rank_78", I2C_ID_RANK_78 },
    { "rf_leds", I2C_ID_RF_LEDS },
    { }
};

MODULE_DEVICE_TABLE(i2c, cgc_i2c_rank12_id);

static struct i2c_driver charguychess_i2c_clients = {
  .driver = {
    .name    = "charguychess_i2_client",
    .owner   = THIS_MODULE,
  },
 .id_table   = cgc_i2c_rank12_id,
 .probe      = charguychess_i2c_probe,
 .remove     = NULL
};

//------------------------------------------------------------------------------
static int charguychess_i2c_init(void)
{
  int err;

  err = i2c_add_driver(&charguychess_i2c_clients);
  if(err)
    return err;

  if(cgc_driver.rank_12 == NULL ||
      cgc_driver.rank_34 == NULL ||
      cgc_driver.rank_56 == NULL ||
      cgc_driver.rank_78 == NULL ||
      cgc_driver.rf_leds == NULL)
  {
    i2c_del_driver(&charguychess_i2c_clients);
    return -ENODEV;
  }

  sync_i2c_registers();

  // Start a polling task until we have the interrupt connected
  cgc_driver.i2c_poll_thread =
      kthread_run(poll_thread_func, NULL, "cgc_i2c_poll");
  if(IS_ERR(cgc_driver.i2c_poll_thread))
  {
    dev_err(cgc_driver.chardev, "Start i2c polling thread error\n");
    cgc_driver.i2c_poll_thread = NULL;
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
static void charguychess_i2c_remove(void)
{
  if(cgc_driver.i2c_poll_thread)
  {
    if(kthread_stop(cgc_driver.i2c_poll_thread))
      dev_err(cgc_driver.chardev, "Cannot stop i2c polling thread\n");
    else
      dev_info(cgc_driver.chardev, "i2c polling thread stopped\n");
  }

  i2c_del_driver(&charguychess_i2c_clients);
}

//------------------------------------------------------------------------------
static int charguychess_chardev_init(void)
{
  int err;

  if(cgc_registers_init())
  {
    pr_err("Could not initialize charguychess registers\n");
    return -EIO;
  }

  cgc_driver.class = class_create(THIS_MODULE, CHARGUYCHESS_CLASS);
  if(IS_ERR(cgc_driver.class))
  {
    printk(KERN_ALERT "Failed to create charguychess class\n");
    return PTR_ERR(cgc_driver.class);
  }

  cgc_driver.char_major =
      register_chrdev(0, CHARGUYCHESS_CHAR_DEV, &charguychess_fops);
  if(cgc_driver.char_major < 0)
  {
    pr_err(
      "Cannot register char device %s: %i\n",
      CHARGUYCHESS_CHAR_DEV,
      cgc_driver.char_major);
    err = cgc_driver.char_major;
    goto remove_class;
  }

  cgc_driver.chardev = device_create(
      cgc_driver.class,
    NULL,
    MKDEV(cgc_driver.char_major, 0),
    NULL,
    CHARGUYCHESS_CHAR_DEV);
  if(IS_ERR(cgc_driver.chardev))
  {
    pr_err("Cannot create char device\n");
    err = -ENOMEM;
    goto deregister_char_dev;
  }

  mutex_init(&cgc_driver.registers_lock);
  mutex_init(&cgc_driver.users_list_lock);
  INIT_LIST_HEAD(&cgc_driver.users_list.list);

  dev_info(cgc_driver.chardev, "char device ready!\n");

  return 0;

deregister_char_dev:
  unregister_chrdev(cgc_driver.char_major, CHARGUYCHESS_CHAR_DEV);
remove_class:
  class_destroy(cgc_driver.class);

  return err;
}

//------------------------------------------------------------------------------
static void charguychess_cleanup(void)
{
  if(cgc_driver.has_i2c)
    charguychess_i2c_remove();

  device_destroy(cgc_driver.class, MKDEV(cgc_driver.char_major, 0));
  unregister_chrdev(cgc_driver.char_major, CHARGUYCHESS_CHAR_DEV);
  class_destroy(cgc_driver.class);
}

//------------------------------------------------------------------------------
static int charguychess_probe(struct platform_device *pdev)
{
  int ret;

  ret = charguychess_chardev_init();
  if(ret)
    return ret;

  // We allow running without i2c subsystem for simulation
  cgc_driver.has_i2c = charguychess_i2c_init() == 0;
  if(!cgc_driver.has_i2c)
    dev_warn(cgc_driver.chardev, "i2c subsystem initialization failed!\n");

  cgc_driver.was_probed = 1;

  return 0;
}

//------------------------------------------------------------------------------
static int charguycess_remove(struct platform_device *pdev)
{
  charguychess_cleanup();

  return 0;
}

//------------------------------------------------------------------------------
static const struct of_device_id charguychess_match[] = {
  { .compatible = "charguychess" },
  { }
};
MODULE_DEVICE_TABLE(of, charguychess_match);

//------------------------------------------------------------------------------
static struct platform_driver charguychess_pdrv = {
  .probe    = charguychess_probe,
  .remove   = charguycess_remove,
  .driver   = {
    .name   = "charguychess",
    .of_match_table = charguychess_match
  },
};

//------------------------------------------------------------------------------
static int charguychess_init(void)
{
  int ret;

  memset(&cgc_driver, 0, sizeof(CharguyChessDriver));
  ret = platform_driver_register(&charguychess_pdrv);
  if(ret)
  {
    pr_warn("Platform driver register failed: %i\n", ret);
    return ret;
  }

  if(!cgc_driver.was_probed)
  {
    // We don't have a device tree, create the char device for simulation
    pr_info("charguychess probing not called,"
        " creating char device for simulation\n");

    ret = charguychess_chardev_init();
    if(ret)
      return ret;
  }

  return ret;
}
module_init(charguychess_init);

//------------------------------------------------------------------------------
static void charguychess_exit(void)
{
  if(!cgc_driver.was_probed)
  {
    pr_info("Destroying char device\n");
    charguychess_cleanup();
  }

  platform_driver_unregister(&charguychess_pdrv);

  pr_info("Charguychess module unloaded\n");
}
module_exit(charguychess_exit);

MODULE_AUTHOR("Guy Morand");
MODULE_DESCRIPTION("Kernel driver for charguychess hardware");
MODULE_VERSION("0.0");
MODULE_LICENSE("GPL v2");

