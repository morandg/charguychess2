/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CHARGUY_CHESS_SERIALIZER_H_
#define _CHARGUY_CHESS_SERIALIZER_H_

#include "platforms.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CGC_READ_REQUEST_SIZE   (unsigned int)3
#define CGC_WRITE_REQUEST_SIZE  (unsigned int)5

int cgc_serializer_read_request(
    char* buffer,
    unsigned int buffer_size,
    uint8_t reg,
    unsigned int* serialize_size);
int cgc_serializer_write_request(
    char* buffer,
    unsigned int buffer_size,
    uint8_t reg,
    uint8_t value,
    unsigned int* serialize_size);

#ifdef __cplusplus
}       // extern "C"
#endif

#endif  // _CHARGUY_CHESS_SERIALIZER_H_
