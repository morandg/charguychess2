/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CHARGUY_CHESS_REGISTERS_DEFS_H_
#define _CHARGUY_CHESS_REGISTERS_DEFS_H_

#include "platforms.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CGC_REGISTER_RANK1                  (uint8_t)0x00
#define CGC_REGISTER_RANK2                  (uint8_t)0x01
#define CGC_REGISTER_RANK3                  (uint8_t)0x02
#define CGC_REGISTER_RANK4                  (uint8_t)0x03
#define CGC_REGISTER_RANK5                  (uint8_t)0x04
#define CGC_REGISTER_RANK6                  (uint8_t)0x05
#define CGC_REGISTER_RANK7                  (uint8_t)0x06
#define CGC_REGISTER_RANK8                  (uint8_t)0x07
#define CGC_REGISTER_RANK_LEDS              (uint8_t)0x08
#define CGC_REGISTER_FILE_LEDS              (uint8_t)0x09
#define CGC_REGISTER_PIECES_LEDS            (uint8_t)0x0A
#define CGC_REGISTER_BUTTONS_STATUS         (uint8_t)0x0B
#define CGC_REGISTER_ALL_STATUS             (uint8_t)0xFF

#define CGC_REGISTER_FIRST                  CGC_REGISTER_RANK1
#define CGC_REGISTER_LAST                   CGC_REGISTER_BUTTONS_STATUS
#define CGC_REGISTERS_COUNT                 CGC_REGISTER_LAST - \
                                              CGC_REGISTER_FIRST + 1

#define CGC_REGISTER_MASK_FILE_A            (uint8_t)0x80
#define CGC_REGISTER_MASK_FILE_B            (uint8_t)0x40
#define CGC_REGISTER_MASK_FILE_C            (uint8_t)0x20
#define CGC_REGISTER_MASK_FILE_D            (uint8_t)0x10
#define CGC_REGISTER_MASK_FILE_E            (uint8_t)0x08
#define CGC_REGISTER_MASK_FILE_F            (uint8_t)0x04
#define CGC_REGISTER_MASK_FILE_G            (uint8_t)0x02
#define CGC_REGISTER_MASK_FILE_H            (uint8_t)0x01

#define CGC_REGISTER_MASK_RANK_ONE          (uint8_t)0x80
#define CGC_REGISTER_MASK_RANK_TWO          (uint8_t)0x40
#define CGC_REGISTER_MASK_RANK_THREE        (uint8_t)0x20
#define CGC_REGISTER_MASK_RANK_FOUR         (uint8_t)0x10
#define CGC_REGISTER_MASK_RANK_FIVE         (uint8_t)0x08
#define CGC_REGISTER_MASK_RANK_SIX          (uint8_t)0x04
#define CGC_REGISTER_MASK_RANK_SEVEN        (uint8_t)0x02
#define CGC_REGISTER_MASK_RANK_EIGHT        (uint8_t)0x01

#define CGC_REGISTER_MASK_BUTTON_QUEEN      (uint8_t)0x01
#define CGC_REGISTER_MASK_BUTTON_BISHOP     (uint8_t)0x02
#define CGC_REGISTER_MASK_BUTTON_KNIGHT     (uint8_t)0x04
#define CGC_REGISTER_MASK_BUTTON_ROOK       (uint8_t)0x08

#define CGC_REGISTER_MASK_LED_WHITE         (uint8_t)0x80
#define CGC_REGISTER_MASK_LED_BLACK         (uint8_t)0x40
#define CGC_REGISTER_MASK_LED_ROOK          (uint8_t)0x08
#define CGC_REGISTER_MASK_LED_KNIGHT        (uint8_t)0x04
#define CGC_REGISTER_MASK_LED_BISHOP        (uint8_t)0x02
#define CGC_REGISTER_MASK_LED_QUEEN         (uint8_t)0x01

#ifdef __cplusplus
}       // extern "C"
#endif

#endif  // _CHARGUY_CHESS_REGISTERS_DEFS_H_
