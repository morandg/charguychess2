/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CHARGUY_PLATFORMS_H_
#define _CHARGUY_PLATFORMS_H_

#ifdef __KERNEL__
  #include <linux/types.h>
  #include <linux/string.h>
  #include <linux/slab.h>

  #include <uapi/asm/errno.h>

  #define malloc(x) kmalloc(x, GFP_KERNEL)
  #define free(x) kfree(x)
#else
  #include <stdlib.h>
  #include <stdint.h>
  #include <errno.h>
  #include <string.h>
#endif  // __KERNEL__

#endif  // _CHARGUY_PLATFORMS_H_
