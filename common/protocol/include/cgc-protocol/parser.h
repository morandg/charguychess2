/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CHARGUY_CHESS_PARSER_H_
#define _CHARGUY_CHESS_PARSER_H_

#include "platforms.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*CGC_ReadCallback)(void* data, uint8_t reg);
typedef void (*CGC_WriteCallback)(void* data, uint8_t reg, uint8_t value);

typedef void* CGC_Parser;

CGC_Parser cgc_parser_create(
    CGC_ReadCallback read_callback,
    CGC_WriteCallback write_callback,
    void* callback_data);
void cgc_parser_free(CGC_Parser parser);
void cgc_parser_parse_buffer(
    CGC_Parser me, const char* buffer, unsigned int buffer_size);

#ifdef __cplusplus
}       // extern "C"
#endif

#endif  // _CHARGUY_CHESS_PARSER_H_
