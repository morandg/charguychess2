/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cgc-protocol/registers_defs.h>
#include <cgc-protocol/registers.h>

#include <CppUTest/TestHarness.h>

//------------------------------------------------------------------------------
TEST_GROUP(RegistersTest)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(RegistersTest, writeAndReadBackAfterInit)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t readValue = 55;
  uint8_t writtenValue = 1;

  cgc_registers_write(CGC_REGISTER_FILE_LEDS, writtenValue);
  cgc_registers_init();
  cgc_registers_read(reg, &readValue);

  CHECK_EQUAL(0, readValue);
}

//------------------------------------------------------------------------------
TEST(RegistersTest, writeAndReadBack)
{
  uint8_t reg = CGC_REGISTER_FILE_LEDS;
  uint8_t readValue = 55;
  uint8_t writtenValue = 1;

  cgc_registers_write(CGC_REGISTER_FILE_LEDS, writtenValue);
  cgc_registers_read(reg, &readValue);

  CHECK_EQUAL(writtenValue, readValue);
}

//------------------------------------------------------------------------------
TEST(RegistersTest, writeWritableRegister)
{
  uint8_t value = 1;

  CHECK_EQUAL(0, cgc_registers_write(CGC_REGISTER_FILE_LEDS, value));
}

//------------------------------------------------------------------------------
TEST(RegistersTest, writeInvalidRegister)
{
  uint8_t value = 1;

  CHECK_EQUAL(-EINVAL, cgc_registers_write(CGC_REGISTER_LAST + 1, value));
}

//------------------------------------------------------------------------------
TEST(RegistersTest, readRegister)
{
  uint8_t value = 1;

  CHECK_EQUAL(0, cgc_registers_read(CGC_REGISTER_RANK1, &value));
  CHECK_EQUAL(0, value);
}

//------------------------------------------------------------------------------
TEST(RegistersTest, readInvalidRegister)
{
  uint8_t value;

  CHECK_EQUAL(-EINVAL, cgc_registers_read(CGC_REGISTER_LAST + 1, &value));
}

//------------------------------------------------------------------------------
TEST(RegistersTest, initSucceed)
{
  CHECK_EQUAL(0, cgc_registers_init());
}
