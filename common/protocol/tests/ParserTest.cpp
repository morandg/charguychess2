/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/parser_private.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

extern "C" {

//------------------------------------------------------------------------------
static void read_cb_mock(void* data, uint8_t reg)
{
  mock().actualCall(__func__).
      withParameter("data", data).
      withParameter("reg", reg);
}

//------------------------------------------------------------------------------
static void write_cb_mock(void* data, uint8_t reg, uint8_t value)
{
  mock().actualCall(__func__).
      withParameter("data", data).
      withParameter("reg", reg).
      withParameter("value", value);
}

}   // extern "C"

//------------------------------------------------------------------------------
TEST_GROUP(Parser)
{
  TEST_SETUP()
  {
    m_parser = cgc_parser_create(read_cb_mock, write_cb_mock, this);
  }

  TEST_TEARDOWN()
  {
    mock().clear();
    cgc_parser_free(m_parser);
  }

  CGC_Parser getParser()
  {
    return m_parser;
  }

private:
  CGC_Parser m_parser;
};

//------------------------------------------------------------------------------
TEST(Parser, parseWithLeftOver)
{
  char buffer[4] = { '0', '8', '\n', '0'};
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x08);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();

  CHECK_EQUAL(1, parser->left_over_bytes);
  MEMCMP_EQUAL("0", parser->left_over, 1);
}

//------------------------------------------------------------------------------
TEST(Parser, twoSeparatedByteNothingHappens)
{
  char buffer1 = '0';
  char buffer2 = '1';

  cgc_parser_parse_buffer(getParser(), &buffer1, 1);
  cgc_parser_parse_buffer(getParser(), &buffer2, 1);
}

//------------------------------------------------------------------------------
TEST(Parser, parseCompletedWriteFrameInTwoCalls)
{
  char buffer1[1] = { '3' };
  char buffer2[4] = { '\n' , '0', '8', '\n'};
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();

  parser->left_over[0] = '0';
  parser->left_over_bytes = 1;

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x03);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x08);

  cgc_parser_parse_buffer(getParser(), buffer1, sizeof(buffer1));
  cgc_parser_parse_buffer(getParser(), buffer2, sizeof(buffer2));

  mock().checkExpectations();

  CHECK_EQUAL(0, parser->left_over_bytes);
}

//------------------------------------------------------------------------------
TEST(Parser, parseCompletedReadrameWithAnotherFrame)
{
  char buffer[4] = { '\n', '0', '3', '\n'};
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();

  parser->left_over[0] = '0';
  parser->left_over[1] = '8';
  parser->left_over_bytes = 2;

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x08);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x03);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();

  CHECK_EQUAL(0, parser->left_over_bytes);
}

//------------------------------------------------------------------------------
TEST(Parser, parseCompletedReadrame)
{
  char buffer[1] = { '\n' };
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();

  parser->left_over[0] = '0';
  parser->left_over[1] = '8';
  parser->left_over_bytes = 2;

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x08);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();

  CHECK_EQUAL(0, parser->left_over_bytes);
}

//------------------------------------------------------------------------------
TEST(Parser, parseCompletedWriteFrame)
{
  char buffer[2] = { 'D', '\n' };
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();

  parser->left_over[0] = '0';
  parser->left_over[1] = '8';
  parser->left_over[2] = 'C';
  parser->left_over_bytes = 3;

  mock().expectOneCall("write_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x08).
      withParameter("value", 0xCD);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();

  CHECK_EQUAL(0, parser->left_over_bytes);
}

//------------------------------------------------------------------------------
TEST(Parser, parseReadAllRegister)
{
  char buffer[3] = {'F', 'F', '\n'};

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x00);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x01);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x02);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x03);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x04);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x05);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x06);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x07);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x08);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x09);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x0A);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x0B);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(Parser, parseInvalidRegisterWrite)
{
  char buffer[5] = { 'F', '6', '3', '4', '\n' };

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));
}

//------------------------------------------------------------------------------
TEST(Parser, parseInvalidRegisterRead)
{
  char buffer[3] = { 'F', '6', '\n' };

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));
}

//------------------------------------------------------------------------------
TEST(Parser, parseInvalidWithValid)
{
  char buffer[5] = {'g', '\n' , '0', 'a', '\n'};

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x0A);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(Parser, parsePartialBuffer)
{
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();
  char buffer[3] = { 'A', 'B', 'C' };

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  CHECK_EQUAL(3, parser->left_over_bytes);
  MEMCMP_EQUAL(buffer, parser->left_over, 3);
}

//------------------------------------------------------------------------------
TEST(Parser, parseTwoRequests)
{
  char buffer[6] = { '0', '1', '\n', '0', '5', '\n' };

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x01);
  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x05);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(Parser, parseWriteRequest)
{
  char buffer[5] = { '0', '7', '3', '4', '\n' };

  mock().expectOneCall("write_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x07).
      withParameter("value", 0x34);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(Parser, parseReadRequest)
{
  char buffer[3] = { '0', '6', '\n' };

  mock().expectOneCall("read_cb_mock").
      withParameter("data", this).
      withParameter("reg", 0x06);

  cgc_parser_parse_buffer(getParser(), buffer, sizeof(buffer));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(Parser, create)
{
  CGC_Parser_t* parser = (CGC_Parser_t*)getParser();

  CHECK(parser != NULL);
  POINTERS_EQUAL(read_cb_mock, parser->read_callback);
  POINTERS_EQUAL(write_cb_mock, parser->write_callback);
  CHECK_EQUAL(0, parser->left_over_bytes);
  POINTERS_EQUAL(this, parser->callback_data);
}

