/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cgc-protocol/serializer.h>

#include <CppUTest/TestHarness.h>

//------------------------------------------------------------------------------
TEST_GROUP(Serializer)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Serializer, serializeWrite)
{
  unsigned int bufferSize = CGC_WRITE_REQUEST_SIZE;
  char buffer[bufferSize];
  uint8_t reg = 0x3A;
  uint8_t value = 0xBC;
  unsigned int serializedSize;

  CHECK_EQUAL(0,
      cgc_serializer_write_request(
          buffer, bufferSize, reg, value, &serializedSize));
  CHECK_EQUAL(CGC_WRITE_REQUEST_SIZE, serializedSize);
  MEMCMP_EQUAL("3ABC\n", buffer, CGC_WRITE_REQUEST_SIZE);
}

//------------------------------------------------------------------------------
TEST(Serializer, serializeWriteBufferTooSmall)
{
  unsigned int bufferSize = CGC_WRITE_REQUEST_SIZE - 1;
  char buffer[bufferSize];
  uint8_t reg = 0x3A;
  uint8_t value = 0xBC;
  unsigned int serializedSize;

  CHECK_EQUAL(-ENOMEM,
      cgc_serializer_write_request(
          buffer, bufferSize, reg, value, &serializedSize));
}

//------------------------------------------------------------------------------
TEST(Serializer, serializerReadRegBiggerThan9)
{
  unsigned int bufferSize = CGC_READ_REQUEST_SIZE;
  char buffer[bufferSize];
  uint8_t reg = 0x3A;
  unsigned int serializedSize;

  CHECK_EQUAL(0,
      cgc_serializer_read_request(buffer, bufferSize, reg, &serializedSize));
  CHECK_EQUAL(CGC_READ_REQUEST_SIZE, serializedSize);
  MEMCMP_EQUAL("3A\n", buffer, CGC_READ_REQUEST_SIZE);
}

//------------------------------------------------------------------------------
TEST(Serializer, serializerRead)
{
  unsigned int bufferSize = CGC_READ_REQUEST_SIZE;
  char buffer[bufferSize];
  uint8_t reg = 0x12;
  unsigned int serializedSize;

  CHECK_EQUAL(0,
      cgc_serializer_read_request(buffer, bufferSize, reg, &serializedSize));
  CHECK_EQUAL(CGC_READ_REQUEST_SIZE, serializedSize);
  MEMCMP_EQUAL("12\n", buffer, CGC_READ_REQUEST_SIZE);
}

//------------------------------------------------------------------------------
TEST(Serializer, serializerReadBufferTooSmall)
{
  unsigned int bufferSize = CGC_READ_REQUEST_SIZE - 1;
  char buffer[bufferSize];
  uint8_t reg = 0x12;
  unsigned int serializedSize;

  CHECK_EQUAL(-ENOMEM,
      cgc_serializer_read_request(buffer, bufferSize, reg, &serializedSize));
}
