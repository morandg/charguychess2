/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cgc-protocol/registers_defs.h>
#include "../src/parser_private.h"

#include <CppUTest/TestHarness.h>

//------------------------------------------------------------------------------
TEST_GROUP(ParserPrivate)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadInvalidBetweenFAnda)
{
  ParseResult result;
  char buffer[3] = { 'A', 'U', '\n' };

  CHECK_EQUAL(-EINVAL, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadInvalidBetweenAAnd9)
{
  ParseResult result;
  char buffer[3] = { 'A', '?', '\n' };

  CHECK_EQUAL(-EINVAL, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseWriteInvalid)
{
  ParseResult result;
  char buffer[3] = { 'A', 'B', 'g' };

  CHECK_EQUAL(-EINVAL, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseWriteBufferTooSmallNotParsed)
{
  ParseResult result;
  char buffer[3] = { 'A', 'B', 'C' };

  CHECK_EQUAL(0, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(0, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseWriteRequestNotTerminated)
{
  ParseResult result;
  char buffer[5] = { 'c', 'd', 'e', 'f', 'a' };

  CHECK_EQUAL(-EINVAL, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(5, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseWriteRequest)
{
  ParseResult result;
  char buffer[5] = { 'c', 'd', 'e', 'f', '\n' };

  CHECK_EQUAL(1, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(CGC_WRITE_REQUEST, result.request_type);
  CHECK_EQUAL(0xCD, result.reg);
  CHECK_EQUAL(0xEF, result.value);
  CHECK_EQUAL(5, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadRequestSecondHexCharBiggerThanf)
{
  ParseResult result;
  char buffer[3] = { '0', 'g', '\n' };

  CHECK_EQUAL(-EINVAL, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadRequestFirstHexCharSmallerThan0)
{
  ParseResult result;
  char buffer[3] = { '0' - 1, '2', '\n' };

  CHECK_EQUAL(-EINVAL, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadRequestNumeric)
{
  ParseResult result;
  char buffer[3] = { '1', '2', '\n' };

  CHECK_EQUAL(1, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(CGC_READ_REQUEST, result.request_type);
  CHECK_EQUAL(0x12, result.reg);
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadRequestLowerHex)
{
  ParseResult result;
  char buffer[3] = { 'd', 'e', '\n' };

  CHECK_EQUAL(1, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(CGC_READ_REQUEST, result.request_type);
  CHECK_EQUAL(0xDE, result.reg);
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadRequestCapitalHex)
{
  ParseResult result;
  char buffer[3] = { 'A', 'B', '\n' };

  CHECK_EQUAL(1, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(CGC_READ_REQUEST, result.request_type);
  CHECK_EQUAL(0xAB, result.reg);
  CHECK_EQUAL(3, result.parsed_bytes);
}

//------------------------------------------------------------------------------
TEST(ParserPrivate, parseReadRequestBufferTooSmall)
{
  ParseResult result;
  char buffer[1] = { 'A' };

  CHECK_EQUAL(0, cgc_parser_parse_frame(buffer, sizeof(buffer), &result));
  CHECK_EQUAL(0, result.parsed_bytes);
}
