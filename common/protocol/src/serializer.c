/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "cgc-protocol/serializer.h"

//------------------------------------------------------------------------------
static void char_to_hex(uint8_t hex, char* buffer)
{
  if(hex <= 9)
    *buffer = hex + '0';
  else
    *buffer = (hex - 10) + 'A';
}

//------------------------------------------------------------------------------
static void uint8_to_ascii(uint8_t value, char* buffer)
{
  char_to_hex(value >> 4, buffer);
  char_to_hex(0x0F & value, buffer + 1);
}

//------------------------------------------------------------------------------
int cgc_serializer_read_request(
    char* buffer,
    unsigned int buffer_size,
    uint8_t reg,
    unsigned int* serialize_size)
{
  if(buffer_size < CGC_READ_REQUEST_SIZE)
    return -ENOMEM;

  uint8_to_ascii(reg, buffer);
  buffer[2] = '\n';
  *serialize_size = CGC_READ_REQUEST_SIZE;

  return 0;
}

//------------------------------------------------------------------------------
int cgc_serializer_write_request(
    char* buffer,
    unsigned int buffer_size,
    uint8_t reg,
    uint8_t value,
    unsigned int* serialize_size)
{
  if(buffer_size < CGC_WRITE_REQUEST_SIZE)
  return -ENOMEM;

  uint8_to_ascii(reg, buffer);
  uint8_to_ascii(value, buffer + 2);
  buffer[4] = '\n';
  *serialize_size = CGC_WRITE_REQUEST_SIZE;

  return 0;
}

