/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "cgc-protocol/registers_defs.h"
#include "cgc-protocol/registers.h"

static uint8_t regs_copy[CGC_REGISTERS_COUNT];

//------------------------------------------------------------------------------
int cgc_registers_init()
{
  memset(regs_copy, 0, sizeof(regs_copy));

  return 0;
}

//------------------------------------------------------------------------------
int cgc_registers_read(uint8_t reg, uint8_t* value)
{
  if(reg > CGC_REGISTER_LAST)
    return -EINVAL;

  *value = regs_copy[reg];
  return 0;
}

//------------------------------------------------------------------------------
int cgc_registers_write(uint8_t reg, uint8_t value)
{
  if(reg > CGC_REGISTER_LAST)
    return -EINVAL;

  regs_copy[reg] = value;

  return 0;
}
