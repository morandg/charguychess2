/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CHARGUY_CHESS_PARSER_PRIVATE_H_
#define _CHARGUY_CHESS_PARSER_PRIVATE_H_

#include "../include/cgc-protocol/parser.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CGC_READ_REQUEST    (int)1
#define CGC_WRITE_REQUEST   (int)2
#define CGC_MAX_FRAME_SIZE  (int)5

typedef struct _ParseResult{
  int request_type;
  unsigned int parsed_bytes;
  uint8_t reg;
  uint8_t value;
} ParseResult;

typedef struct _CGC_Parser_t
{
  CGC_ReadCallback read_callback;
  CGC_WriteCallback write_callback;
  char left_over[CGC_MAX_FRAME_SIZE];
  unsigned int left_over_bytes;
  void* callback_data;
} CGC_Parser_t;

int cgc_parser_parse_frame(
    const char* buffer,
    unsigned int buffer_size,
    ParseResult* result);

#ifdef __cplusplus
}       // extern "C"
#endif

#endif  // _CHARGUY_CHESS_PARSER_PRIVATE_H_
