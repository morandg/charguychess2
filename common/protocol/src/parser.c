/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../include/cgc-protocol/registers_defs.h"
#include "parser_private.h"

#define FRAME_SIZE_READ     (unsigned int)3
#define FRAME_SIZE_WRITE    (unsigned int)5
#define END_OF_FRAME        '\n'

//------------------------------------------------------------------------------
static void find_end_of_frame(
    const char* buffer,
    unsigned int buffer_size,
    unsigned int* idx)
{
  while(buffer[*idx] != END_OF_FRAME &&
      *idx < buffer_size - 1)
    ++(*idx);
}

//------------------------------------------------------------------------------
static int char_to_hex(char char_value, uint8_t* hex_value)
{
  if(char_value >= '0' && char_value <= '9')
    *hex_value = char_value - 48;
  else if(char_value >= 'A' && char_value <= 'F')
    *hex_value = char_value - 55;
  else if(char_value >= 'a' && char_value <= 'f')
    *hex_value = char_value - 87;
  else
    return -EINVAL;

  return 0;
}

//------------------------------------------------------------------------------
static int char_to_byte(
    const char* buffer,
    unsigned int buffer_size,
    unsigned int* idx,
    uint8_t* byte)
{
  int ret;

  if(*idx + 1 > buffer_size)
    return 0;

  ret = char_to_hex(buffer[*idx], byte);
  if(ret)
    return ret;

  return 1;
}

//------------------------------------------------------------------------------
static int ascii_hex_to_uint8(
    const char* buffer,
    unsigned int buffer_size,
    unsigned int* idx,
    uint8_t* hex)
{
  int ret;
  uint8_t msb;
  uint8_t lsb;

  ret = char_to_byte(buffer, buffer_size, idx, &msb);
  if(ret == 0 || ret < 0)
    return ret;

  if(++(*idx) >= buffer_size)
    return 0;

  ret = char_to_byte(buffer, buffer_size, idx, &lsb);
  if(ret == 0 || ret < 0)
    return ret;

  *hex = (msb << 4) | lsb;

  return 1;
}

//------------------------------------------------------------------------------
static int parse_hex_value(
    const char* buffer,
    unsigned int buffer_size,
    unsigned int* idx,
    uint8_t* value,
    ParseResult* result)
{
  int ret;

  ret = ascii_hex_to_uint8(buffer, buffer_size, idx, value);
  if(!ret)
    result->parsed_bytes = 0;
  else if(ret < 0)
  {
    find_end_of_frame(buffer, buffer_size, idx);
    result->parsed_bytes = *idx + 1;
  }

  return ret;
}

//------------------------------------------------------------------------------
int cgc_parser_parse_frame(
    const char* buffer,
    unsigned int buffer_size,
    ParseResult* result)
{
  unsigned int idx = 0;
  int ret;

  ret = parse_hex_value(
      buffer, buffer_size, &idx, &result->reg, result);
  if(!ret || ret < 0)
    return ret;

  if(++idx >= buffer_size)
    return 0;

  if(buffer[idx] ==  END_OF_FRAME)
  {
    result->request_type = CGC_READ_REQUEST;
    result->parsed_bytes = idx + 1;
    return 1;
  }

  ret = parse_hex_value(
      buffer, buffer_size, &idx, &result->value, result);
  if(!ret || ret < 0)
    return ret;

  if(buffer[++idx] ==  END_OF_FRAME)
  {
    result->request_type = CGC_WRITE_REQUEST;
    result->parsed_bytes = idx + 1;
    ret = 1;
  }
  else
  {
    find_end_of_frame(buffer, buffer_size, &idx);
    result->parsed_bytes = idx + 1;
    ret = -EINVAL;
  }

  return ret;
}

//------------------------------------------------------------------------------
static void read_all_registers(CGC_Parser_t* parser)
{
  uint8_t reg;

  for(reg = CGC_REGISTER_FIRST ; reg <= CGC_REGISTER_LAST ; ++reg)
    parser->read_callback(parser->callback_data, reg);
}

//------------------------------------------------------------------------------
static void call_callback(CGC_Parser_t* parser, ParseResult* result)
{
  if(result->request_type == CGC_READ_REQUEST)
  {
    if(result->reg == CGC_REGISTER_ALL_STATUS)
      read_all_registers(parser);
    else if(result->reg <= CGC_REGISTER_LAST)
      parser->read_callback(parser->callback_data, result->reg);
  }
  else if(result->request_type == CGC_WRITE_REQUEST &&
      result->reg <= CGC_REGISTER_LAST)
    parser->write_callback(parser->callback_data, result->reg, result->value);
}

//------------------------------------------------------------------------------
static int complete_partial_frame(
    CGC_Parser_t* parser, const char* buffer, unsigned int buffer_size)
{
  ParseResult result;
  int parsedBytes;
  unsigned int missingBytes = CGC_MAX_FRAME_SIZE - parser->left_over_bytes;

  if(missingBytes > buffer_size)
    missingBytes = buffer_size;

  memcpy(parser->left_over + parser->left_over_bytes, buffer, missingBytes);

  if(cgc_parser_parse_frame(
      parser->left_over, parser->left_over_bytes + missingBytes, &result) == 1)
  {
    call_callback(parser, &result);

    parsedBytes = result.parsed_bytes - parser->left_over_bytes;
    parser->left_over_bytes = 0;

    return parsedBytes;
  }

  parser->left_over_bytes += missingBytes;

  return 0;
}

//------------------------------------------------------------------------------
CGC_Parser cgc_parser_create(
    CGC_ReadCallback read_callback,
    CGC_WriteCallback write_callback,
    void* callback_data)
{
  CGC_Parser_t* parser = (CGC_Parser_t*)malloc(sizeof(CGC_Parser_t));

  if(parser != NULL)
  {
    parser->read_callback = read_callback;
    parser->write_callback = write_callback;
    parser->left_over_bytes = 0;
    parser->callback_data = callback_data;
  }

  return parser;
}

//------------------------------------------------------------------------------
void cgc_parser_free(CGC_Parser parser)
{
  free(parser);
}

//------------------------------------------------------------------------------
void cgc_parser_parse_buffer(
    CGC_Parser me, const char* buffer, unsigned int buffer_size)
{
  CGC_Parser_t* parser = (CGC_Parser_t*)me;
  unsigned int totalRead = 0;
  int ret;
  ParseResult result;

  if(parser->left_over_bytes > 0)
  {
    totalRead = complete_partial_frame(parser, buffer, buffer_size);
    if(totalRead == 0)
      return;
  }

  while(totalRead < buffer_size)
  {
    ret = cgc_parser_parse_frame(
        buffer + totalRead, buffer_size - totalRead, &result);

    if(ret == 1)
      call_callback(parser, &result);
    else if(ret == 0)
    {
      parser->left_over_bytes = buffer_size - totalRead;
      memcpy(parser->left_over, buffer + totalRead, parser->left_over_bytes);
      break;
    }

    totalRead += result.parsed_bytes;
  }
}
