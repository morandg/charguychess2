all: app driver
.PHONY: all

app:
	$(MAKE) -C app
.PHONY:app

driver: 
	cd kernel/driver && $(MAKE)
.PHONY:driver

clean:
	$(MAKE) -C app clean
	cd kernel/driver && $(MAKE) clean
.PHONY: clean